package com.koudaiplus.buy.web.model

import com.koudaiplus.buy.api.model.OrderBizModel
import com.play.core.common.ValidationBuilder
import com.play.core.common.Validator

object OrderWebModel {

    data class Create(
            val skuList: List<OrderBizModel.SkuItem>,
            val buyerContactId: String
    ): Validator {
        override fun validate() {
            ValidationBuilder().`if`(skuList.size !in 1..5) {
                it["skuList"] = "请选择1至5个以内商品"
            }.build()
        }
    }

    data class DirectPay (
            val skuList: List<OrderBizModel.SkuItem>,
            val buyerContactId: String,
            val tradeType: String,
            val tradeSubtype: String
    ): Validator {
        override fun validate() {
            ValidationBuilder().`if`(skuList.size !in 1..5) {
                it["skuList"] = "请选择1至5个以内商品"
            }.build()
        }
    }

}