package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.ExpressBiz
import com.koudaiplus.buy.api.model.ExpressBizModel
import com.play.core.common.RespResult
import com.play.core.common.WebAppContext
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/express")
@Api(description = "快递", tags = ["Express"])
class ExpressController (
    @Qualifier("express")
    val express: List<Map<String, String>>,
    val expressBiz: ExpressBiz
) {

    @ApiOperation(value = "列表")
    @GetMapping
    fun findList(@RequestParam("id") id: String): RespResult<ExpressBizModel.ExpressVO, Unit, Unit> {
        val data = expressBiz.findList(keyId = id)
        return RespResult.success(data = data)
    }

    @ApiOperation(value = "快递公司")
    @GetMapping("/metadata")
    fun findMetadata(): RespResult<List<Map<String, String>>, Unit, Unit> {
        return RespResult.success(data = express)
    }

}