package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.TopupOrderBiz
import com.koudaiplus.buy.api.feign.TopupClient
import com.koudaiplus.buy.api.model.TopupFeignModel
import com.koudaiplus.buy.api.model.TopupOrderBizModel
import com.play.core.common.WebAppContext
import com.play.core.common.web.AbstractController
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class TopupClientImpl (
    val topupOrderBiz: TopupOrderBiz
): AbstractController(), TopupClient {

    override fun pay(@RequestBody req: TopupFeignModel.CreateByLoginUser): String {
        req.validate()
        val loginUserInfo = WebAppContext.getPrincipal()!!
        return topupOrderBiz.pay(param = TopupOrderBizModel.Create(
            amount = req.amount.toBigDecimal(), tradeType = req.tradeType,
            tradeSubtype = req.tradeSubtype, buyerId = loginUserInfo.id
        ))
    }

    override fun pay(@RequestBody req: TopupFeignModel.CreateByMerchant): String {
        return topupOrderBiz.pay(param = req)
    }

}