package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.WalletBiz
import com.koudaiplus.buy.api.model.WalletBizModel
import com.koudaiplus.wallet.api.model.AccountServiceModel
import com.koudaiplus.wallet.api.model.BillItemServiceModel
import com.play.core.common.Limit
import com.play.core.common.Pages
import com.play.core.common.RespResult
import com.play.core.common.WebAppContext
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/wallet")
@Api(description = "钱包", tags = ["Wallet"])
class WalletController (
    private val walletBiz: WalletBiz
) {

    @GetMapping
    @ApiOperation("查询资金账户详情")
    fun info(): RespResult<AccountServiceModel.AccountVO?, Unit, Unit> {
        return walletBiz.info(userId = WebAppContext.getPrincipal()!!.id)
    }

    @PutMapping("/reset-password")
    @ApiOperation("修改密码")
    fun resetPassword(@RequestBody req: WalletBizModel.ResetPassword): RespResult<Unit, Unit, Unit> {
        return walletBiz.resetPassword(userId = WebAppContext.getPrincipal()!!.id, req = req)
    }

    @PutMapping("/transfer")
    @ApiOperation("转账给其它账户")
    fun transfer(@RequestBody req: WalletBizModel.TransferByPassword): RespResult<Unit, Unit, Unit> {
        return walletBiz.transfer(userId = WebAppContext.getPrincipal()!!.id, req = req)
    }

    @GetMapping("/bill/list")
    @ApiOperation("查询账单列表")
    fun findList(@ModelAttribute limit: Limit): RespResult<Pages<BillItemServiceModel.BillItemVO>, Unit, Unit> {
        return walletBiz.findList(userId = WebAppContext.getPrincipal()!!.id, limit = limit)
    }

}