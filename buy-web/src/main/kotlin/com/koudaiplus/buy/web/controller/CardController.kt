package com.koudaiplus.buy.web.controller

import com.koudaiplus.message.api.biz.CardBiz
import com.koudaiplus.message.api.model.CardBizModel
import com.koudaiplus.message.api.model.CardServiceModel
import com.koudaiplus.message.api.service.CardService
import com.play.core.common.RespResult
import com.play.core.common.WebAppContext
import com.play.core.common.web.AbstractController
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/card")
@Api(description = "卡片服务", tags = ["Card"])
class CardController (
    private val cardService: CardService,
    private val cardBiz: CardBiz
) : AbstractController() {

    @ApiOperation(value = "删除卡片")
    @DeleteMapping("/{cardId}")
    fun delete(@PathVariable("cardId")cardId: String): RespResult<Unit, Unit, Unit> {
        cardService.delete(ownerId = WebAppContext.getPrincipal()!!.id, keyId = cardId)
        return RespResult.success()
    }

    @ApiOperation(value = "创建卡片")
    @PostMapping
    fun create(@RequestBody req: CardBizModel.Create): RespResult<Unit, Unit, Unit> {
        cardBiz.create(ownerId = WebAppContext.getPrincipal()!!.id, param = req)
        return RespResult.success()
    }

    @ApiOperation(value = "卡片列表")
    @PostMapping("/list")
    fun findList(): RespResult<List<CardServiceModel.CardVO>, Unit, Unit>{
        val data = cardService.findList(ownerId = WebAppContext.getPrincipal()!!.id)
        return RespResult.success(data = data)
    }

}