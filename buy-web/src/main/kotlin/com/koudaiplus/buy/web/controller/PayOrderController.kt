package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.PayOrderBiz
import com.koudaiplus.buy.api.model.PayOrderBizModel
import com.koudaiplus.buy.api.model.PayOrderServiceModel
import com.koudaiplus.buy.api.service.PayOrderService
import com.koudaiplus.buy.web.model.PayOrderWebModel
import com.play.core.common.DataNotFoundException
import com.play.core.common.RespResult
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/orders")
@Api(description = "订单", tags = ["Order"])
class PayOrderController (
    val payOrderBiz: PayOrderBiz,
    val payOrderService: PayOrderService
) {

    @ApiOperation(value = "预备订单")
    @PostMapping("/prepay")
    fun prepay(@RequestBody req: PayOrderWebModel.PrepayOrderCreate): RespResult<String, Unit, Unit> {
        req.validate()
        val payOrderKeyId = payOrderBiz.createByMarketOrder(param = PayOrderBizModel.Create(orderIdList = req.orderIdList))
        return RespResult.success(payOrderKeyId)
    }

    @ApiOperation(value = "付款")
    @PostMapping("/pay", produces = [MediaType.TEXT_HTML_VALUE])
    fun pay(@RequestBody req: PayOrderBizModel.Pay): String {
        return payOrderBiz.pay(param = req)
    }

    @ApiOperation(value = "支付状态")
    @GetMapping("/status")
    fun getPayStatus(@RequestParam("keyId") keyId: String): RespResult<PayOrderServiceModel.PayOrderDTO, Unit, Unit> {
        val info = payOrderService.getInfoByKeyId(keyId = keyId) ?: throw DataNotFoundException()
        return RespResult.success(info)
    }

}