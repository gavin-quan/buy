package com.koudaiplus.buy.web.model

object ContactWebModel {

    data class Create (
            val name: String,
            val province: String,
            val city: String,
            val county: String,
            val address: String,
            val telephone: String,
            val areaCode: String?,
            val postalCode: String?,
            val default: Boolean
    )

}