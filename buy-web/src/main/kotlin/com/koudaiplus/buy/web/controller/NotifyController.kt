package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.PayOrderBiz
import com.koudaiplus.buy.api.biz.RefundOrderBiz
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.pay.api.model.PayConstants
import com.koudaiplus.refund.api.model.MerchantRefundOrderFeignModel
import com.play.core.common.web.AbstractController
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.MediaType
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.view.RedirectView

@RestController
@RequestMapping("/notify")
@Api(description = "渠道通知", tags = ["MarketNotify"])
class NotifyController (
    val payOrderBiz: PayOrderBiz,
    val refundOrderBiz: RefundOrderBiz
) : AbstractController() {

    @ApiOperation(value = "异步接收第三方付款通知")
    @PostMapping("/pay", produces = [MediaType.TEXT_PLAIN_VALUE])
    fun payNotify(@RequestBody req: NotifyConstants.WebHookEvent<PayConstants.TradePaySuccessResponse>) : String {
        return payOrderBiz.receivePayFinishNotify(param = req)
    }

    @ApiOperation(value = "同步接收第三方付款通知")
    @GetMapping("/return")
    fun payReturn(@RequestParam req: MultiValueMap<String, String>): ModelAndView {
        val url = payOrderBiz.receivePayReturnNotify(param = req.toSingleValueMap())
        return ModelAndView(RedirectView(url))
    }

    @ApiOperation(value = "异步接收第三方退款通知")
    @PostMapping("/refund", produces = [MediaType.TEXT_PLAIN_VALUE])
    fun refundNotify(@RequestBody req: NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>) : String {
        return refundOrderBiz.receiveRefundNotify(param = req)
    }

}