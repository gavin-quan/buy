package com.koudaiplus.buy.web.model

object CartWebModel {

    data class Create(
            val skuId: String,
            val quantity: Int
    )

}