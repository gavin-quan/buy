package com.koudaiplus.buy.web.controller

import com.alibaba.csp.sentinel.annotation.SentinelResource
import com.alibaba.csp.sentinel.slots.block.BlockException
import com.koudaiplus.buy.api.biz.OrderBiz
import com.koudaiplus.buy.api.biz.PaymentBiz
import com.koudaiplus.buy.api.model.OrderBizModel
import com.koudaiplus.buy.api.model.OrderServiceModel
import com.koudaiplus.buy.api.model.PayBizModel
import com.koudaiplus.buy.api.service.OrderService
import com.koudaiplus.buy.web.model.OrderWebModel
import com.play.core.common.DataNotFoundException
import com.play.core.common.RespResult
import com.play.core.common.WebAppContext
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/orders")
@Api(description = "订单", tags = ["Order"])
class OrderController (
    val orderBiz: OrderBiz,
    val payBiz: PaymentBiz,
    val orderService: OrderService
) {

    @SentinelResource("createOrder")
    @ApiOperation(value = "创建订单")
    @PostMapping("/create")
    @Throws(BlockException::class)
    //@Retryable(value = [Exception::class], maxAttempts = 2, backoff = Backoff(delay = 2000L, multiplier = 1.5))
    fun create(@RequestBody req: OrderWebModel.Create): RespResult<Map<String, Any>, Unit, Unit> {
        req.validate()
        val loginUserId = WebAppContext.getPrincipal()!!.id
        val resp = fun(): Map<String, Any> {
            val param = OrderBizModel.Create(
                skuList = req.skuList, buyerContactId = req.buyerContactId, buyerId = loginUserId
            )
            val orderIdList = orderBiz.createBySync(param = param)
            return mapOf("type" to "sync", "id" to orderIdList)
            /*
            //如果触发了限流,则使用异步创建订单
            val batchId = orderBiz.createByAsync(param = param)
            return mapOf("type" to "async", "id" to batchId)
             */
        }()
        return RespResult.success(resp)
    }

    @ApiOperation(value = "SKU直付", notes = "包含了创建订单、创建付款订单、获取实际支付参数3个接口")
    @PostMapping("/direct-pay", produces = [MediaType.TEXT_HTML_VALUE])
    fun directPay(@RequestBody req: OrderWebModel.DirectPay): String {
        req.validate()
        val loginUserId = WebAppContext.getPrincipal()!!.id
        return payBiz.directPay(param = PayBizModel.DirectPay(
            skuList = req.skuList, buyerContactId = req.buyerContactId,
            tradeType = req.tradeType, tradeSubtype = req.tradeSubtype,
            buyerId = loginUserId
        ))
    }

    @ApiOperation(value = "订单直付", notes = "包含了创建付款订单、获取实际支付参数2个接口")
    @PostMapping("/order-pay", produces = [MediaType.TEXT_HTML_VALUE])
    fun orderPay(@RequestBody req: PayBizModel.OrderPay): String {
        return payBiz.orderPay(param = req)
    }

    @ApiOperation(value = "根据批id获取订单id列表")
    @GetMapping("/id")
    fun findOrderIdListByBatchId(@RequestParam("batchId")batchId: String): RespResult<List<String>, Unit, Unit> {
        val orderIdList = orderService.findOrderKeyIdListByBatchId(batchId = batchId)
        return RespResult.success(orderIdList)
    }

    @ApiOperation(value = "详情")
    @GetMapping("/info")
    fun getPayStatus(@RequestParam("keyId") keyId: String): RespResult<OrderServiceModel.OrderDTO, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        val info = orderService.getInfoByKeyId(keyId = keyId) ?: throw DataNotFoundException()
        if(info.buyerId != loginUserId) throw DataNotFoundException()
        return RespResult.success(info)
    }

    @ApiOperation(value = "延迟结算")
    @PostMapping("/delay")
    fun delay(@RequestParam("keyId") keyId: String): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        orderBiz.delay(keyId = keyId, buyerId = loginUserId)
        return RespResult.success()
    }

    @ApiOperation(value = "确认收货")
    @PostMapping("/confirm")
    fun confirm(@RequestParam("keyId") keyId: String): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        orderBiz.confirm(keyId = keyId, buyerId = loginUserId)
        return RespResult.success()
    }

}