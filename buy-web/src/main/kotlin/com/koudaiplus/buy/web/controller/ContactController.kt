package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.model.ContactServiceModel
import com.koudaiplus.buy.api.service.ContactService
import com.koudaiplus.buy.web.model.ContactWebModel
import com.play.core.common.DataNotFoundException
import com.play.core.common.RespResult
import com.play.core.common.WebAppContext
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/contact")
@Api(description = "联络人", tags = ["Contact"])
class ContactController(
    val contactService: ContactService
) {

    @ApiOperation(value = "列表")
    @GetMapping
    fun findList(): RespResult<List<ContactServiceModel.ContactVO>, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        val list = contactService.findListByUserKeyId(userKeyId = loginUserId)
        return RespResult.success(list)
    }

    @ApiOperation(value = "详情")
    @GetMapping("/info")
    fun getInfo(@RequestParam("keyId")keyId: String): RespResult<ContactServiceModel.ContactVO, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        val info = contactService.getInfoByKeyId(keyId = keyId, userKeyId = loginUserId) ?: throw DataNotFoundException()
        return RespResult.success(info.toContactVO())
    }

    @ApiOperation(value = "添加")
    @PostMapping
    fun save(@RequestBody req: ContactWebModel.Create): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        contactService.create(ContactServiceModel.Create(
            name = req.name, address = req.address, telephone = req.telephone, default = req.default, province = req.province, city = req.city,
            county = req.county, areaCode = req.areaCode, postalCode = req.postalCode, userKeyId = loginUserId
        ))
        return RespResult.success()
    }

    @ApiOperation(value = "更新")
    @PutMapping("/{keyId}")
    fun update(@PathVariable("keyId") keyId: String, @RequestBody req: ContactWebModel.Create): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        contactService.update(ContactServiceModel.Update(
            keyId = keyId, name = req.name, address = req.address, telephone = req.telephone,
            default = req.default, province = req.province, city = req.city, county = req.county,
            areaCode = req.areaCode, postalCode = req.postalCode, userKeyId = loginUserId
        ))
        return RespResult.success()
    }

    @ApiOperation(value = "移除")
    @DeleteMapping("/{keyId}")
    fun delete(@PathVariable("keyId")keyId: String): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        contactService.deleteByKeyId(keyId = keyId, userKeyId = loginUserId)
        return RespResult.success()
    }

    @ApiOperation(value = "设置默认")
    @PutMapping("/default")
    fun setDefault(@RequestParam("keyId")keyId: String): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        contactService.setDefaultByKeyId(keyId = keyId, userKeyId = loginUserId)
        return RespResult.success()
    }

    @ApiOperation(value = "获取默认")
    @GetMapping("/default")
    fun getDefault(): RespResult<ContactServiceModel.ContactVO?, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        val info = contactService.getDefaultInfoByUserKeyId(userKeyId = loginUserId)
        return  RespResult.success(info?.toContactVO())
    }

}