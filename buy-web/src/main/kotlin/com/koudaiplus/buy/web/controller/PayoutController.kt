package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.PayoutBiz
import com.koudaiplus.buy.api.model.PayoutBizModel
import com.play.core.common.RespResult
import com.play.core.common.WebAppContext
import com.play.core.common.web.AbstractController
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/payout")
@Api(description = "出款服务", tags = ["Payout"])
class PayoutController (
    private val payoutBiz: PayoutBiz
) : AbstractController() {

    @GetMapping
    @ApiOperation("获取出款基本信息")
    fun get(): RespResult<Map<*, *>, Unit, Unit> {
        val data = payoutBiz.getPayoutViewData(userId = WebAppContext.getPrincipal()!!.id)
        return RespResult.success(data = data)
    }

    @PostMapping("/transfer")
    @ApiOperation("申请转账")
    fun transfer(@RequestBody param : PayoutBizModel.CreateTransfer): RespResult<Unit, Unit, Unit> {
        return payoutBiz.createTransfer(userId = WebAppContext.getPrincipal()!!.id, param = param)
    }

    @PostMapping("/cancel")
    @ApiOperation("撤销出款")
    fun transfer(@RequestBody param : PayoutBizModel.CancelPayout): RespResult<Unit, Unit, Unit> {
        return payoutBiz.cancel(userId = WebAppContext.getPrincipal()!!.id, param = param)
    }

}