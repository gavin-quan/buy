package com.koudaiplus.buy.web.model

import com.play.core.common.ValidationBuilder
import com.play.core.common.Validator

object PayOrderWebModel {

    data class PrepayOrderCreate(
            val orderIdList: List<String>
    ) : Validator {
        override fun validate() {
            ValidationBuilder().`if`(orderIdList.size !in 1..5) {
                it["orderIdList"] = "请选择1至5个以内订单"
            }.build()
        }
    }

}