package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.CartBiz
import com.koudaiplus.buy.api.model.CartBizModel
import com.koudaiplus.buy.api.model.CartServiceModel
import com.koudaiplus.buy.api.service.CartService
import com.koudaiplus.buy.web.model.CartWebModel
import com.play.core.common.*
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/cart")
@Api(description = "购物车", tags = ["Cart"])
class CartController(
    private val cartService: CartService,
    private val cartBiz: CartBiz
) {

    @ApiOperation(value = "添加")
    @PostMapping
    fun create(@RequestBody req: CartWebModel.Create): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        cartBiz.create(param = CartBizModel.Create(
            skuId = req.skuId, quantity = req.quantity, buyerId = loginUserId
        ))
        return RespResult.success()
    }

    @ApiOperation(value = "移除")
    @DeleteMapping
    fun delete(@RequestBody skuIdList : Array<String>): RespResult<Unit, Unit, Unit> {
        val idList = skuIdList.limit(1, 10) //最多1-10条数据
        val loginUserId = WebAppContext.getPrincipal()!!.id
        cartService.delete(param = CartServiceModel.Delete(keyIdList = idList.toList(), buyerId = loginUserId))
        return RespResult.success()
    }

}