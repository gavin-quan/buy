package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.model.GoodsServiceModel
import com.koudaiplus.buy.api.service.GoodsService
import com.play.core.common.DataNotFoundException
import com.play.core.common.Limit
import com.play.core.common.Pages
import com.play.core.common.RespResult
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/goods")
@Api(description = "商品", tags = ["Goods"])
class GoodsController(
    private val goodsService: GoodsService
) {

    @ApiOperation(value = "所有列表")
    @GetMapping
    fun findList(@ModelAttribute limit: Limit): RespResult<Pages<GoodsServiceModel.GoodsDTO>, Unit, Unit> {
        val pages = goodsService.findList(limit = limit)
        return RespResult.success(data = pages)
    }

    @ApiOperation(value = "店铺商品列表")
    @GetMapping("/shop/{shopKeyId}")
    fun findListByShopId(@PathVariable("shopKeyId")shopKeyId: String, @ModelAttribute limit: Limit): RespResult<Pages<GoodsServiceModel.GoodsDTO>, Unit, Unit> {
        val pages = goodsService.findListByShopId(shopKeyId = shopKeyId, limit = limit)
        return RespResult.success(data = pages)
    }

    @ApiOperation(value = "详情")
    @GetMapping("/{keyId}")
    fun getInfo(@PathVariable("keyId") keyId: String): RespResult<GoodsServiceModel.GoodsDTO?, Unit, Unit> {
        val info = goodsService.getInfoByKeyId(keyId = keyId) ?: throw DataNotFoundException()
        return RespResult.success(info)
    }

    @ApiOperation(value = "内容")
    @GetMapping("/content/{keyId}")
    fun getContent(@PathVariable("keyId") keyId: String): RespResult<String, Unit, Unit> {
        val info = goodsService.getContentByKeyId(keyId = keyId) ?: throw DataNotFoundException()
        return RespResult.success(info)
    }

}