package com.koudaiplus.buy.web.controller

import com.koudaiplus.buy.api.biz.RefundOrderBiz
import com.koudaiplus.buy.api.model.RefundOrderBizModel
import com.play.core.common.RespResult
import com.play.core.common.WebAppContext
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/refunds")
@Api(description = "退款", tags = ["Refund"])
class RefundController (
    val refundOrderBiz: RefundOrderBiz
) {

    @ApiOperation(value = "申请退款")
    @PostMapping("/create")
    fun create(@RequestParam("orderKeyId") orderKeyId: String): RespResult<String, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        val refundOrderKeyId = refundOrderBiz.createByMarketOrder(param = RefundOrderBizModel.Create(
            orderKeyId = orderKeyId, userId = loginUserId
        ))
        return RespResult.success(refundOrderKeyId)
    }

    @ApiOperation(value = "申请退款")
    @PostMapping("/easy-order/create")
    fun createByEasyOrder(@RequestParam("orderKeyId") orderKeyId: String): RespResult<String, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        val refundOrderKeyId = refundOrderBiz.createByEasyOrder(param = RefundOrderBizModel.Create(
            orderKeyId = orderKeyId, userId = loginUserId
        ))
        return RespResult.success(refundOrderKeyId)
    }

    @ApiOperation(value = "撤销退款")
    @PostMapping("/cancel")
    fun cancel(@RequestParam("refundOrderKeyId") refundOrderKeyId: String): RespResult<Unit, Unit, Unit> {
        val loginUserId = WebAppContext.getPrincipal()!!.id
        refundOrderBiz.cancel(RefundOrderBizModel.Cancel(refundOrderKeyId = refundOrderKeyId, userId = loginUserId))
        return RespResult.success()
    }

}