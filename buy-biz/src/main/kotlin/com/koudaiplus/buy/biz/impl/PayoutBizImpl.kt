package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.PayoutBiz
import com.koudaiplus.buy.api.model.PayoutBizModel
import com.koudaiplus.message.api.service.CardService
import com.koudaiplus.pay.api.service.MerchantAppService
import com.koudaiplus.pay.api.service.MerchantService
import com.koudaiplus.payout.api.feign.OpenPayoutTransferClient
import com.koudaiplus.payout.api.model.PayoutTransferOrderBizModel
import com.koudaiplus.user.api.service.UserService
import com.koudaiplus.wallet.api.service.AccountService
import com.play.core.common.AppProperties
import com.play.core.common.DataNotFoundException
import com.play.core.common.RespResult
import com.play.core.common.SnowFlakeGenerator
import org.springframework.stereotype.Service

@Service
class PayoutBizImpl (
    private val idGenerator: SnowFlakeGenerator,
    private val accountService: AccountService,
    private val userService: UserService,
    private val cardService: CardService,
    private val merchantService: MerchantService,
    private val merchantAppService: MerchantAppService,
    private val appProperties: AppProperties,
    private val openPayoutTransferClient: OpenPayoutTransferClient
) : PayoutBiz {

    override fun getPayoutViewData(userId: String): Map<String, *> {
        val user = userService.getInfoByUserKeyId(userKeyId = userId) ?: throw DataNotFoundException()
        if(user.walletNo == null) throw DataNotFoundException()
        val cardList = cardService.findList(ownerId = userId)
        val walletDTO = accountService.getInfoByNumber(user.walletNo!!) ?: throw DataNotFoundException()
        val wallet = PayoutBizModel.WalletDTO()
        wallet.name = walletDTO.name
        wallet.amount = walletDTO.amount
        wallet.currency = walletDTO.currency.alias
        wallet.freezeAmount = walletDTO.totalFreezeAmount
        return mapOf("cardList" to cardList, "wallet" to wallet)
    }

    override fun createTransfer(userId: String, param: PayoutBizModel.CreateTransfer): RespResult<Unit, Unit, Unit> {
        val merchantAppId = appProperties["merchantAppId"] as String
        val requestKey = appProperties["requestKey"] as String
        val merchantApp = merchantAppService.getInfoByKeyId(keyId = merchantAppId) ?: throw DataNotFoundException()
        val merchant = merchantService.getInfoByKeyId(keyId = merchantApp.merchantId)?: throw DataNotFoundException()
        val user = userService.getInfoByUserKeyId(userKeyId = userId) ?: throw DataNotFoundException()
        if(user.tenantCode != merchant.tenantCode) throw DataNotFoundException()
        val card = cardService.getInfoById(keyId = param.cardId) ?: throw DataNotFoundException()
        if(card.ownerId != user.keyId) throw DataNotFoundException()
        val req = PayoutTransferOrderBizModel.CreateByApi(
            merchantAppId = merchantApp.keyId, password = param.password, remarks = "手动申请转账",
            amount = param.amount.toBigDecimal(), bizContent = null, proposerWalletNo = user.walletNo!!,
            proposerId = user.keyId, requestNo = idGenerator.nextId().toString(), cardId = param.cardId
        )
        req.sign(requestKey = requestKey)
        return openPayoutTransferClient.create(param = req)
    }

    override fun cancel(userId: String, param: PayoutBizModel.CancelPayout): RespResult<Unit, Unit, Unit> {
        val merchantAppId = appProperties["merchantAppId"] as String
        val requestKey = appProperties["requestKey"] as String
        val merchantApp = merchantAppService.getInfoByKeyId(keyId = merchantAppId) ?: throw DataNotFoundException()
        val merchant = merchantService.getInfoByKeyId(keyId = merchantApp.merchantId)?: throw DataNotFoundException()
        val user = userService.getInfoByUserKeyId(userKeyId = userId) ?: throw DataNotFoundException()
        if(user.tenantCode != merchant.tenantCode) throw DataNotFoundException()
        val req = PayoutTransferOrderBizModel.CancelByApi(
            merchantAppId = merchantApp.keyId, requestNo = param.requestNo, proposerId = user.keyId
        )
        req.sign(requestKey = requestKey)
        return openPayoutTransferClient.cancel(param = req)
    }

}