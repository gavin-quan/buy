package com.koudaiplus.buy.biz.listener

import com.google.common.eventbus.AsyncEventBus
import com.google.common.eventbus.Subscribe
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CLOSE_QUEUE_BEAN
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_TIMEOUT_DURATION
import com.play.core.common.BusinessException
import org.apache.activemq.ScheduledMessage
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.jms.core.JmsTemplate
import org.springframework.jms.core.MessagePostProcessor
import org.springframework.stereotype.Component
import javax.jms.Queue

@Component
class TimeoutEasyOrderListener(
    val eventBus: AsyncEventBus,
    val jmsTemplate: JmsTemplate,
    @Qualifier(ORDER_CLOSE_QUEUE_BEAN)
    val orderCloseQueue: Queue,
    val messagePostProcessor: MessagePostProcessor
): InitializingBean {

    private val logger = LoggerFactory.getLogger(this::class.java)

    //@JmsListener(destination = ORDER_CREATED_TOPIC, containerFactory = Constants.JMS_TOPIC_CONTAINER_FACTORY_BEAN)
    fun createOrderTimeoutSchedulerByOrderCreatedTopic(orderKeyId: String) {
        createOrderTimeoutScheduler(orderKeyId = orderKeyId)
    }

    @Subscribe
    fun createOrderTimeoutScheduler(event: BuyConstants.OrderCreatedEvent) {
        createOrderTimeoutScheduler(orderKeyId = event.orderKeyId)
    }

    override fun afterPropertiesSet() {
        eventBus.register(this)
    }

    private fun createOrderTimeoutScheduler(orderKeyId: String) {
        logger.info("接收到订单超时进行订单关闭的消息,报文->$orderKeyId")
        try {
            jmsTemplate.convertAndSend(orderCloseQueue, orderKeyId) {
                it.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, ORDER_TIMEOUT_DURATION.toMillis()) //延迟投递的时间
                messagePostProcessor.postProcessMessage(it)
            }
        } catch (e: BusinessException) {
            logger.warn("订单超时进行订单关闭出现业务异常, orderKeyId=$orderKeyId", e)
            if (e.retryable) throw e
        } catch (e: Exception) {
            logger.error("订单超时进行订单关闭处理失败, orderKeyId=$orderKeyId", e)
        }
    }

}