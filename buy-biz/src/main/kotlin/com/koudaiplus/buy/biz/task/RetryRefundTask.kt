package com.koudaiplus.buy.biz.task

import com.koudaiplus.buy.api.biz.RefundOrderBiz
import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.IJobHandler
import com.xxl.job.core.handler.annotation.XxlJob
import com.xxl.job.core.log.XxlJobLogger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class RetryRefundTask (
    val refundOrderBiz: RefundOrderBiz
) : IJobHandler() {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @XxlJob(value = "RetryRefundTask")
    override fun execute(keyId: String): ReturnT<String> {
        return try {
            refundOrderBiz.retry(refundOrderKeyId = keyId)
            ReturnT.SUCCESS
        } catch (e: Exception) {
            logger.error("人工重试退款失败, keyId={}", keyId)
            logger.error("人工重试退款失败", e)
            XxlJobLogger.log("处理失败, {}", e.message)
            ReturnT.FAIL
        }
    }

}