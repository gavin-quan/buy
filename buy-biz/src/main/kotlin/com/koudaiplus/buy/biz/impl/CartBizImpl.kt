package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.CartBiz
import com.koudaiplus.buy.api.model.CartBizModel
import com.koudaiplus.buy.api.model.CartServiceModel
import com.koudaiplus.buy.api.service.CartService
import com.koudaiplus.buy.api.service.GoodsService
import com.play.core.common.DataNotFoundException
import org.springframework.stereotype.Service

@Service
class CartBizImpl(
    private val goodsService: GoodsService,
    private val cartService: CartService
): CartBiz {

    override fun create(param: CartBizModel.Create) {
        val sku = goodsService.getSkuInfoByKeyId(skuKeyId = param.skuId) ?: throw DataNotFoundException()
        val goods = goodsService.getInfoByKeyId(keyId = sku.goodsKeyId) ?: throw DataNotFoundException()
        cartService.create(param = CartServiceModel.Create(
            shopKeyId = goods.shopKeyId, goodsKeyId = sku.goodsKeyId,
            quantity = param.quantity, buyerId = param.buyerId, goodsSkuId = sku.keyId
        ))
    }

}