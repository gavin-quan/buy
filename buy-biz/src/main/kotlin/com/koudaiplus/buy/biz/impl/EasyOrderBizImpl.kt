package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.EasyOrderBiz
import com.koudaiplus.buy.api.model.EasyOrderBizModel
import com.koudaiplus.buy.api.model.EasyOrderServiceModel
import com.koudaiplus.buy.api.model.RefundOrderApiModel
import com.koudaiplus.buy.api.service.EasyOrderService
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.api.model.BuyConstants.REPEAT_PAY_ERROR_CODE
import com.koudaiplus.wallet.api.biz.WalletBiz
import com.koudaiplus.wallet.api.model.WalletBizModel
import com.koudaiplus.wallet.api.service.TopupOrderService
import com.play.core.common.BusinessException
import com.play.core.common.DataNotFoundException
import com.play.core.common.JSON
import com.play.core.common.lock
import org.redisson.api.RedissonClient
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class EasyOrderBizImpl (
    val redisson: RedissonClient,
    val easyOrderService: EasyOrderService,
    val walletBiz: WalletBiz,
    val topupOrderService: TopupOrderService
) : EasyOrderBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun create(param: EasyOrderBizModel.Create): String {
        return easyOrderService.create(param = EasyOrderServiceModel.Create(
            buyerId = param.buyerId, amount = param.amount, sourceId = param.sourceId,
            sourceType = param.sourceType, subject = param.subject
        ))
    }

    override fun tryCloseByKeyId(keyId: String) {
        redisson.lock(key = BuyConstants.getOrderLockKey(orderKeyId = keyId)) {
            easyOrderService.tryCloseByKeyId(keyId = keyId)
        }
    }

    override fun getRefundBuilder(keyId: String, userId: String): RefundOrderApiModel.RefundBuilder? {
        //根据EasyOrder的source id确认是否需要进行退款
        return null
    }

    @Transactional
    override fun finishTopupOrderPay(param: EasyOrderServiceModel.FinishPay) {
        val order = easyOrderService.getInfoByKeyId(keyId = param.keyId) ?: throw DataNotFoundException()
        //处理Order重复通知
        if(order.isRepeatNotify(transOrderNo = param.transOrderNo, payOrderKeyId = param.payOrderKeyId)) {
            return
        }
        if(order.isPaid()) {
            logger.error("!!!!!!!!!!!!!重复付款!!!!!!!!!!!! req={}", JSON.getJSON().toJson(param))
            throw BusinessException(code = REPEAT_PAY_ERROR_CODE, message = "完成付款失败")
        }
        if(order.state.isInactive()) {
            logger.error("!!!!!!!!!!!!!数据已经关闭，但付款完成消息刚刚到来!!!!!!!!!!!! req={}", JSON.getJSON().toJson(param))
            throw BusinessException(message = "完成付款失败")
        }
        easyOrderService.finishPay(param = param)
        when(order.sourceType) {
            BuyConstants.SourceType.TOPUP -> {
                val topupOrder = topupOrderService.getInfoByKeyId(keyId = order.sourceId) ?: throw DataNotFoundException()
                topupOrderService.finishPay(keyId = topupOrder.keyId)
                walletBiz.takeIn(param = WalletBizModel.TakeInByAmount(
                    txId = order.keyId, walletNo = topupOrder.walletNo, amount = order.amount, remarks = "用户充值", sourceId = topupOrder.keyId
                ))
            }
            else -> {
                throw BusinessException(message = "未知订单", sysMsg = "未知订单, orderId=${order.keyId}")
            }
        }
    }

}