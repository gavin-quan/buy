package com.koudaiplus.buy.biz.listener

import com.koudaiplus.buy.api.biz.EasyOrderBiz
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CLOSE_QUEUE
import com.play.core.common.BusinessException
import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component

@Component
class CloseEasyOrderListener(
    val orderBiz: EasyOrderBiz
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @JmsListener(destination = ORDER_CLOSE_QUEUE)
    fun closeOrder(orderKeyId: String) {
        logger.info("接收到关闭订单通知,orderKeyId=$orderKeyId")
        try {
            orderBiz.tryCloseByKeyId(keyId = orderKeyId)
        } catch (e: BusinessException) {
            logger.warn("关闭订单出现业务异常, orderKeyId=$orderKeyId", e)
            if(e.retryable) throw e
        } catch (e: Exception) {
            logger.error("关闭订单出现系统异常, orderKeyId=$orderKeyId", e)
        }
    }

}