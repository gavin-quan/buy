package com.koudaiplus.buy.biz.impl

import com.google.common.eventbus.AsyncEventBus
import com.koudaiplus.buy.api.biz.EasyOrderBiz
import com.koudaiplus.buy.api.biz.OrderBiz
import com.koudaiplus.buy.api.biz.RefundOrderBiz
import com.koudaiplus.buy.api.model.ExpressServiceModel
import com.koudaiplus.buy.api.model.RefundOrderBizModel
import com.koudaiplus.buy.api.model.RefundOrderServiceModel
import com.koudaiplus.buy.api.service.*
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_REFUND_MSG_ARRIVED_QUEUE_BEAN
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.refund.api.feign.OpenMerchantRefundOrderClient
import com.koudaiplus.refund.api.model.MerchantRefundOrderFeignModel
import com.play.core.common.*
import org.apache.commons.lang3.StringUtils
import org.redisson.api.RedissonClient
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.jms.core.JmsTemplate
import org.springframework.jms.core.MessagePostProcessor
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*
import javax.jms.Queue

@Service
class RefundOrderBizImpl(
    val redisson: RedissonClient,
    val orderBiz: OrderBiz,
    val easyOrderBiz: EasyOrderBiz,
    val payOrderService: PayOrderService,
    val refundOrderService: RefundOrderService,
    val appProperties: AppProperties,
    val merchantRefundOrderClient: OpenMerchantRefundOrderClient,
    val refundOrderBizService: RefundOrderBizService,
    val jmsTemplate: JmsTemplate,
    @Qualifier(ORDER_REFUND_MSG_ARRIVED_QUEUE_BEAN)
    val channelRefundMsgArrivedQueue: Queue,
    val messagePostProcessor: MessagePostProcessor,
    val asyncEventBus: AsyncEventBus
): RefundOrderBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun createByMarketOrder(param: RefundOrderBizModel.Create): String {
        //该场景满足用户对订单商品逐个申请退款的功能
        return redisson.quickLock(key = BuyConstants.getCreateRefundOrderLockKey(orderItemKeyId = param.orderKeyId)) {
            val refundBuilder = orderBiz.getRefundBuilder(keyId = param.orderKeyId, userId = param.userId) ?: throw DataNotFoundException()
            val refundOrder = refundOrderService.getInfoByOrderItemKeyId(orderItemKeyId = refundBuilder.orderItemKeyId!!)
            val refundOrderKeyId = if(refundOrder == null) {
                val payOrder = payOrderService.getInfoByKeyId(keyId = refundBuilder.payOrderKeyId) ?: throw DataNotFoundException()
                checkBiz(!payOrder.state.isPaid()) { "订单尚未付款" }
                refundOrderService.create(param = RefundOrderServiceModel.Create(
                    amount = refundBuilder.price, orderItemKeyId = refundBuilder.orderItemKeyId,
                    orderKeyId = refundBuilder.orderKeyId, payOrderKeyId = payOrder.keyId,
                    createdBy = param.userId, shopKeyId = refundBuilder.shopKeyId
                ))
            } else {
                if(!refundOrder.state.isPending()) throw BusinessException(message = "请勿重复提交")
                refundOrder.keyId
            }
            doRequest(refundOrderKeyId = refundOrderKeyId, refundAmount = refundBuilder.price, payOrderKeyId = refundBuilder.payOrderKeyId)
            refundOrderKeyId
        }
    }

    override fun createByEasyOrder(param: RefundOrderBizModel.Create): String {
        //该场景满足用户对订单商品逐个申请退款的功能
        return redisson.quickLock(key = BuyConstants.getCreateRefundOrderLockKey(orderItemKeyId = param.orderKeyId)) {
            val refundBuilder = easyOrderBiz.getRefundBuilder(keyId = param.orderKeyId, userId = param.userId) ?: throw DataNotFoundException()
            val refundOrder = if (refundBuilder.orderItemKeyId == null) {
                //适配虚拟商品（无SKU的数据）
                refundOrderService.getInfoByOrderKeyId(orderKeyId = refundBuilder.orderKeyId)
            } else {
                refundOrderService.getInfoByOrderItemKeyId(orderItemKeyId = refundBuilder.orderItemKeyId!!)
            }
            val refundOrderKeyId = if(refundOrder == null) {
                val payOrder = payOrderService.getInfoByKeyId(keyId = refundBuilder.payOrderKeyId) ?: throw DataNotFoundException()
                checkBiz(!payOrder.state.isPaid()) { "订单尚未付款" }
                refundOrderService.create(param = RefundOrderServiceModel.Create(
                    amount = refundBuilder.price, orderItemKeyId = refundBuilder.orderItemKeyId,
                    orderKeyId = refundBuilder.orderKeyId, payOrderKeyId = payOrder.keyId,
                    createdBy = param.userId, shopKeyId = refundBuilder.shopKeyId
                ))
            } else {
                if(!refundOrder.state.isPending()) throw BusinessException(message = "请勿重复提交")
                refundOrder.keyId
            }
            doRequest(refundOrderKeyId = refundOrderKeyId, refundAmount = refundBuilder.price, payOrderKeyId = refundBuilder.payOrderKeyId)
            refundOrderKeyId
        }
    }

    override fun retry(refundOrderKeyId: String) {
        //可能请求接口失败
        val refundOrder = refundOrderService.getInfoByKeyId(keyId = refundOrderKeyId) ?: throw DataNotFoundException()
        if(!refundOrder.state.isPending()) {
            return
        }
        doRequest(refundOrderKeyId = refundOrderKeyId, refundAmount = refundOrder.amount, payOrderKeyId = refundOrder.payOrderKeyId)
    }

    private fun doRequest(refundOrderKeyId: String, refundAmount: BigDecimal, payOrderKeyId: String) {
        val merchantAppId = appProperties["merchantAppId"] as String
        val requestKey = appProperties["requestKey"] as String
        val req = MerchantRefundOrderFeignModel.CreateByApi(
            merchantAppId = merchantAppId,
            requestNo = refundOrderKeyId,
            amount = refundAmount.toPlainString(),
            bizContent = "",
            orderNo = payOrderKeyId,
            metadata = ""
        )
        req.sign(requestKey = requestKey)
        try {
            val resp = merchantRefundOrderClient.create(req = req)
            if(resp.success) {
                refundOrderService.setRequestStatus(param = RefundOrderServiceModel.SetStatus(
                    keyId = refundOrderKeyId, state = ActivityStateEnum.PENDING, finishTime = Date()
                ))
            } else {
                val respText = resp.json()
                logger.error("申请退款出现问题, resp=$respText")
                refundOrderService.setRequestStatus(param = RefundOrderServiceModel.SetStatus(
                    keyId = refundOrderKeyId, state = ActivityStateEnum.CANCEL, finishTime = Date()
                ))
                throw BusinessException(message = "申请退款异常")
            }
        } catch (e: Exception) {
            refundOrderService.setRequestStatus(param = RefundOrderServiceModel.SetStatus(
                keyId = refundOrderKeyId, state = ActivityStateEnum.PENDING, finishTime = Date()
            ))
            throw BusinessException(message = "调用退款接口异常", cause = e)
        }
    }

    override fun cancel(param: RefundOrderBizModel.Cancel) {
        redisson.quickLock(key = BuyConstants.getRefundOrderLockKey(refundOrderKeyId = param.refundOrderKeyId)) {
            val refundOrder = refundOrderService.getInfoByKeyId(keyId = param.refundOrderKeyId) ?: throw DataNotFoundException()
            checkBiz(!refundOrder.state.isPending()) { "退款已开始,无法取消" }
            checkBiz(!StringUtils.equals(refundOrder.createdBy, param.userId)) { "数据未找到" }
            val merchantAppId = appProperties["merchantAppId"] as String
            val requestKey = appProperties["requestKey"] as String
            val req = MerchantRefundOrderFeignModel.CancelByApi(merchantAppId = merchantAppId, requestNo = param.refundOrderKeyId)
            req.sign(requestKey = requestKey)
            try {
                val resp = merchantRefundOrderClient.cancel(req = req)
                if(resp.success) {
                    //调用成功立即设置状态
                    refundOrderService.setRequestStatus(param = RefundOrderServiceModel.SetStatus(
                        keyId = param.refundOrderKeyId, state = ActivityStateEnum.CANCEL, finishTime = Date()
                    ))
                } else {
                    val respText = resp.json()
                    logger.error("申请取消退款异常, resp=$respText")
                    throw BusinessException(message = "申请取消退款异常")
                }
            } catch (e: Exception) {
                throw BusinessException(message = "申请取消退款异常", cause = e)
            }
        }
    }

    override fun receiveRefundNotify(param: NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>): String {
        val requestKey = appProperties["requestKey"] as String
        checkBiz(!param.body.checkSign(requestKey = requestKey)) {"验证签名错误"}
        jmsTemplate.convertAndSend(channelRefundMsgArrivedQueue, param.json(), messagePostProcessor)
        return "success"
    }

    override fun setRefundResponse(param: NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>) {
        redisson.quickLock(key = BuyConstants.getRefundOrderLockKey(refundOrderKeyId = param.body.requestNo)) {
            val refundOrder = refundOrderService.getInfoByKeyId(keyId = param.body.requestNo) ?: throw DataNotFoundException()
            val state = ActivityStateEnum.valueOf(param.body.state)
            if(state.isExpired(refundOrder.state)) {
                //状态相同,不需要处理
                if(logger.isInfoEnabled) logger.info("状态已失效,系统丢弃处理,${param.json()}")
                return@quickLock
            }
            refundOrderService.setResponseStatus(param = RefundOrderServiceModel.SetStatus(
                keyId = param.body.requestNo,  state = state,
                finishTime = if(param.body.finishTime == null) null
                else SimpleDateFormat(Constants.YYYYMMDDHHMMSS_FORMAT_PATTERN).parse(param.body.finishTime)
            ))
            //发布退款完成队列
            //jmsTemplate.convertAndSend(refundStateChangeQueue, refund.requestNo)
            asyncEventBus.post(BuyConstants.RefundStateChangeEvent(refundOrderKeyId = param.body.requestNo))
        }
    }

    override fun createRefundExpress(refundOrderKeyId: String) {
        redisson.quickLock(key = BuyConstants.getRefundOrderLockKey(refundOrderKeyId = refundOrderKeyId)) {
            refundOrderBizService.createRefundExpress(refundOrderKeyId = refundOrderKeyId)
            //发布退款物流信息生成完成
            asyncEventBus.post(BuyConstants.RefundExpressCreatedEvent(refundOrderKeyId = refundOrderKeyId))
        }
    }

}

@Service
class RefundOrderBizService(
    val refundOrderService: RefundOrderService,
    val contactService: ContactService,
    val shopService: ShopService,
    val expressService: ExpressService
) {

    @Transactional
    fun createRefundExpress(refundOrderKeyId: String) {
        val refundOrder = refundOrderService.getInfoByKeyId(keyId = refundOrderKeyId) ?: throw DataNotFoundException()
        checkBiz(refundOrder.expressId != null) { "已生成退款订单,refundOrderKeyId=$refundOrderKeyId" }
        val title = "退货-订单${refundOrder.orderKeyId}-${refundOrder.orderItemKeyId}"
        val shop = shopService.getInfoByKeyId(keyId = refundOrder.shopKeyId) ?: throw DataNotFoundException()
        val contact = contactService.getDefaultInfoByUserKeyId(userKeyId = shop.ownerId) ?: throw DataNotFoundException(message = "未发现收货地址")
        val expressId = expressService.create(ExpressServiceModel.Create(
            shopKeyId = shop.keyId, receiverUserId = shop.ownerId, receiverTelephone = contact.telephone,
            receiverAddress = contact.toContactVO().fullAddress, receiverName = contact.name, title = title
        ))
        refundOrderService.setExpressIdByKeyId(keyId = refundOrderKeyId, expressId = expressId)
    }

}