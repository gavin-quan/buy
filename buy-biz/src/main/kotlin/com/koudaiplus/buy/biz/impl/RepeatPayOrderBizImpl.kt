package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.RepeatPayOrderBiz
import com.koudaiplus.buy.api.model.RepeatPayOrderServiceModel
import com.koudaiplus.buy.api.service.OrderService
import com.koudaiplus.buy.api.service.PayOrderService
import com.koudaiplus.buy.api.service.RepeatPayOrderService
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.api.model.BuyConstants.REPEAT_PAY_REFUND_METADATA
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.refund.api.feign.OpenMerchantRefundOrderClient
import com.koudaiplus.refund.api.model.MerchantRefundOrderFeignModel
import com.play.core.common.*
import org.redisson.api.RedissonClient
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class RepeatPayOrderBizImpl(
    val redisson: RedissonClient,
    val payOrderService: PayOrderService,
    val orderService: OrderService,
    val appProperties: AppProperties,
    val merchantRefundOrderClient: OpenMerchantRefundOrderClient,
    val repeatPayOrderService: RepeatPayOrderService
): RepeatPayOrderBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun createRefund(payOrderItemKeyId: String): String {
        //该场景满足订单被多次付款时系统自动退款的功能
        return redisson.quickLock(key = BuyConstants.getCreateRefundOrderLockKey(orderItemKeyId = payOrderItemKeyId)) {
            val repeatPayOrder = repeatPayOrderService.getInfoByPayOrderItemKeyId(payOrderItemKeyId = payOrderItemKeyId)
            if(repeatPayOrder != null) {
                return@quickLock repeatPayOrder.keyId
            }
            val payOrderItem = payOrderService.getItemInfoByKeyId(payOrderItemKeyId = payOrderItemKeyId) ?: throw DataNotFoundException()
            val order = orderService.getInfoByKeyId(keyId = payOrderItem.orderKeyId) ?: throw DataNotFoundException()
            val repeatPayKeyId = repeatPayOrderService.create(param = RepeatPayOrderServiceModel.Create(
                payOrderItemKeyId = payOrderItemKeyId, amount = order.amount,
                payOrderKeyId = payOrderItem.payOrderKeyId, orderKeyId = payOrderItem.orderKeyId
            ))
            doRequest(repeatPayKeyId = repeatPayKeyId, refundAmount = order.amount, payOrderKeyId = payOrderItem.payOrderKeyId)
            repeatPayKeyId
        }
    }

    override fun retryRefund(repeatPayKeyId: String) {
        val repeatPayOrder = repeatPayOrderService.getInfoByKeyId(keyId = repeatPayKeyId) ?: throw DataNotFoundException()
        if(!repeatPayOrder.state.isPending()) return
        doRequest(repeatPayKeyId = repeatPayKeyId, refundAmount = repeatPayOrder.amount, payOrderKeyId = repeatPayOrder.payOrderKeyId)
    }

    private fun doRequest(repeatPayKeyId: String, refundAmount: BigDecimal, payOrderKeyId: String) {
        val merchantAppId = appProperties["merchantAppId"] as String
        val requestKey = appProperties["requestKey"] as String
        //val refundUrl = appProperties["refundUrl"] as String
        //val refundNotifyUrl = appProperties["refundNotifyUrl"] as String
        val req = MerchantRefundOrderFeignModel.CreateByApi (
            merchantAppId = merchantAppId,
            requestNo = repeatPayKeyId,
            amount = refundAmount.toPlainString(),
            bizContent = "",
            orderNo = payOrderKeyId,
            metadata = REPEAT_PAY_REFUND_METADATA
        )
        req.sign(requestKey = requestKey)
        try {
            val resp = merchantRefundOrderClient.create(req = req)
            if(resp.success) {
                repeatPayOrderService.setRefundRespStatus(param = RepeatPayOrderServiceModel.SetStatus(
                    keyId = repeatPayKeyId, state = ActivityStateEnum.PENDING //重复支付退款只请求一次，所以不论什么结果，都是PENDING
                ))
            } else {
                val respText = resp.json()
                logger.error("申请退款出现问题, resp=$respText")
                repeatPayOrderService.setRefundRespStatus(param = RepeatPayOrderServiceModel.SetStatus(
                    keyId = repeatPayKeyId, state = ActivityStateEnum.PENDING //重复支付退款只请求一次，所以不论什么结果，都是PENDING
                ))
                throw BusinessException(message = "申请退款异常")
            }
        } catch (e: Exception) {
            repeatPayOrderService.setRefundRespStatus(param = RepeatPayOrderServiceModel.SetStatus(
                keyId = repeatPayKeyId, state = ActivityStateEnum.PENDING //重复支付退款只请求一次，所以不论什么结果，都是PENDING
            ))
            throw BusinessException(message = "调用退款接口异常", cause = e)
        }
    }

    override fun setRefundResponse(param: NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>) {
        redisson.quickLock(key = BuyConstants.getRefundOrderLockKey(refundOrderKeyId = param.body.requestNo)) {
            val repeatPayOrder = repeatPayOrderService.getInfoByKeyId(keyId = param.body.requestNo) ?: throw DataNotFoundException()
            val state = ActivityStateEnum.valueOf(param.body.state)
            if(state.isExpired(repeatPayOrder.state)) {
                //状态相同,不需要处理
                if(logger.isInfoEnabled) logger.info("状态已失效,系统丢弃处理,${param.json()}")
                return@quickLock
            }
            repeatPayOrderService.setRefundRespStatus(param = RepeatPayOrderServiceModel.SetStatus(
                keyId = param.body.requestNo, state = state
            ))
        }
    }

}