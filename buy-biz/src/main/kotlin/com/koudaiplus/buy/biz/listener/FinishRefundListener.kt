package com.koudaiplus.buy.biz.listener

import com.google.common.reflect.TypeToken
import com.koudaiplus.buy.api.biz.RefundOrderBiz
import com.koudaiplus.buy.api.biz.RepeatPayOrderBiz
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_REFUND_MSG_ARRIVED_QUEUE
import com.koudaiplus.buy.api.model.BuyConstants.REPEAT_PAY_REFUND_METADATA
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.refund.api.model.MerchantRefundOrderFeignModel
import com.play.core.common.BusinessException
import com.play.core.common.JSON
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component

@Component
class FinishRefundListener(
    val refundOrderBiz: RefundOrderBiz,
    val repeatPayOrderBiz: RepeatPayOrderBiz
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @JmsListener(destination = ORDER_REFUND_MSG_ARRIVED_QUEUE)
    fun finishRefund(msg: String) {
        logger.info("接收到退款回调通知,msg=$msg")
        val req : NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse> = try {
            JSON.getJSON().fromJson(msg, object : TypeToken<NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>>(){}.type)
        } catch (e: Exception) {
            logger.error("退款回调解析失败, msg=$msg", e)
            return
        }
        try {
            //如果metadata为REPEAT-PAY则调用重复付款服务逻辑
            if(StringUtils.equalsIgnoreCase(req.body.metadata, REPEAT_PAY_REFUND_METADATA)) {
                repeatPayOrderBiz.setRefundResponse(param = req)
            } else {
                refundOrderBiz.setRefundResponse(param = req)
            }
        } catch (e: BusinessException) {
            logger.warn("退款回调出现业务异常, msg=$msg", e)
            if(e.retryable) throw e
        } catch (e: Exception) {
            logger.error("退款回调出现系统异常, msg=$msg", e)
        }
    }

}