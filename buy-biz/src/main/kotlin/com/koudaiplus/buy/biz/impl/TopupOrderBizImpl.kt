package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.EasyOrderBiz
import com.koudaiplus.buy.api.biz.PayOrderBiz
import com.koudaiplus.buy.api.biz.TopupOrderBiz
import com.koudaiplus.buy.api.model.EasyOrderBizModel
import com.koudaiplus.buy.api.model.PayOrderBizModel
import com.koudaiplus.buy.api.model.TopupFeignModel
import com.koudaiplus.buy.api.model.TopupOrderBizModel
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.user.api.service.UserService
import com.koudaiplus.wallet.api.model.TopupOrderServiceModel
import com.koudaiplus.wallet.api.service.TopupOrderService
import com.play.core.common.AppProperties
import com.play.core.common.DataNotFoundException
import com.play.core.common.checkBiz
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class TopupOrderBizImpl (
    val userService: UserService,
    val payOrderBiz: PayOrderBiz,
    val easyOrderBiz: EasyOrderBiz,
    val appProperties: AppProperties,
    val topupOrderService: TopupOrderService
) : TopupOrderBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun create(buyerId: String, walletNo: String, amount: BigDecimal): String {
        val orderId = topupOrderService.create(param = TopupOrderServiceModel.Create(
            walletNo = walletNo,  amount = amount
        ))
        return easyOrderBiz.create(param = EasyOrderBizModel.Create(
            buyerId = buyerId, amount = amount, sourceType = BuyConstants.SourceType.TOPUP,
            sourceId = orderId, subject = "${walletNo}充值"
        ))
    }

    override fun pay(param: TopupOrderBizModel.Create): String {
        val userInfo = userService.getInfoByUserKeyId(userKeyId = param.buyerId) ?: throw DataNotFoundException()
        checkBiz(StringUtils.isBlank(userInfo.walletNo)) {"尚未开通钱包"}
        val orderId = create(buyerId = param.buyerId, walletNo = userInfo.walletNo!!, amount = param.amount)
        val payOrderKeyId = payOrderBiz.createByEasyOrder(param = PayOrderBizModel.Create(orderIdList = listOf(orderId)))
        val merchantAppId = appProperties["merchantAppId"] as String
        val requestKey = appProperties["requestKey"] as String
        val returnUrl = appProperties["returnUrl"] as String
        return payOrderBiz.pay(param = PayOrderBizModel.Pay(
            payOrderKeyId = payOrderKeyId, tradeType = param.tradeType, tradeSubtype = param.tradeSubtype,
            merchantAppId = merchantAppId, merchantRequestKey = requestKey, returnUrl = returnUrl, metadata = "easyOrder.topup"
        ))
    }

    override fun pay(param: TopupFeignModel.CreateByMerchant): String {
        val orderId = create(buyerId = param.buyerId, walletNo = param.walletNo, amount = param.amount.toBigDecimal())
        val payOrderKeyId = payOrderBiz.createByEasyOrder(param = PayOrderBizModel.Create(orderIdList = listOf(orderId)))
        return payOrderBiz.pay(param = PayOrderBizModel.Pay(
            payOrderKeyId = payOrderKeyId, tradeType = param.tradeType, tradeSubtype = param.tradeSubtype, metadata = "easyOrder.topup",
            merchantAppId = param.merchantAppId, merchantRequestKey = param.merchantRequestKey, returnUrl = param.returnUrl
        ))
    }

}