package com.koudaiplus.buy.biz.task

import com.koudaiplus.buy.api.biz.OrderBiz
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class RecoverStockTask (
    val orderBiz: OrderBiz
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Scheduled(cron = "0 0/10 * * * ?")
    fun executeByDay() {
        logger.info("开始执行库存恢复任务")
        orderBiz.recoverStock()
    }

}