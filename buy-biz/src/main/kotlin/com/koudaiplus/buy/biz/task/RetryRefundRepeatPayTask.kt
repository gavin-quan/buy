package com.koudaiplus.buy.biz.task

import com.koudaiplus.buy.api.biz.RepeatPayOrderBiz
import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.IJobHandler
import com.xxl.job.core.handler.annotation.XxlJob
import com.xxl.job.core.log.XxlJobLogger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class RetryRefundRepeatPayTask (
    val repeatPayOrderBiz: RepeatPayOrderBiz
) : IJobHandler() {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @XxlJob(value = "RetryRefundRepeatPayTask")
    override fun execute(keyId: String): ReturnT<String> {
        return try {
            repeatPayOrderBiz.retryRefund(repeatPayKeyId = keyId)
            ReturnT.SUCCESS
        } catch (e: Exception) {
            logger.error("人工重试重复付款退款失败, keyId={}", keyId)
            logger.error("人工重试重复付款退款失败", e)
            XxlJobLogger.log("处理失败, {}", e.message)
            ReturnT.FAIL
        }
    }

}