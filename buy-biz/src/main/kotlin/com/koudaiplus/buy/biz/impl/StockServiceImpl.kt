package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.StockService
import com.koudaiplus.buy.api.service.GoodsService
import com.koudaiplus.buy.api.service.OrderService
import com.koudaiplus.buy.api.model.BuyConstants.REDO_RECOVER_THRESHOLD
import com.koudaiplus.buy.api.model.BuyConstants.getRecoverSkuLockKey
import com.koudaiplus.buy.api.model.BuyConstants.getSkuKey
import com.koudaiplus.buy.api.model.BuyConstants.getTryLockSkuLockKey
import com.play.core.common.*
import org.redisson.api.RedissonClient
import org.slf4j.LoggerFactory
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
class StockServiceImpl (
    val redisson: RedissonClient,
    val goodsService: GoodsService,
    val orderService: OrderService,
    val redisTemplate: StringRedisTemplate
) : StockService {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun tryLock(skuId: String, quantity: Int) {
        val qty1 = tryLock0(skuId = skuId, quantity = quantity)
        checkBiz(qty1 == null) {"库存不足"}
        if(qty1!! >= 0L) return
        redisson.lock(getTryLockSkuLockKey(skuId = skuId)) {
            val qty2 = tryLock0(skuId = skuId, quantity = quantity)
            checkBiz(qty2 == null) {"库存不足"}
            if(qty2!! >= 0L) return@lock
            forceRecoverLock(skuId = skuId)
            val qty3 = tryLock0(skuId = skuId, quantity = quantity)
            checkBiz(qty3 == null) {"库存不足"}
            if(qty3!! >= 0L) return@lock
            throw BusinessException(message = "库存不足")
        }
    }

    private fun tryLock0(skuId: String, quantity: Int): Long? {
        val key = getSkuKey(skuId = skuId)
        return redisson.readLock(getRecoverSkuLockKey(skuId = skuId)) {
            val availableQuantity = redisTemplate.opsForHash<String, String>().increment(key, skuId, -quantity.toLong())
            if(availableQuantity <= 0) {
                val hold = redisTemplate.opsForHash<String, String>().hasKey(key, "hold")
                if(hold) return@readLock null
            }
            return@readLock availableQuantity
        }
    }

    override fun recoverLock(skuId: String): Long {
        val key = getSkuKey(skuId = skuId)
        val availableQuantity = redisTemplate.opsForHash<String, Long>().get(key, skuId) ?: 0
        if (availableQuantity > REDO_RECOVER_THRESHOLD) return availableQuantity
        val sku = goodsService.getSkuInfoByKeyId(skuKeyId = skuId) ?: throw DataNotFoundException()
        val stockId = redisTemplate.opsForHash<String, String>().get(key, "stockId") ?: ""
        if(sku.stockId == stockId) return availableQuantity
        return forceRecoverLock(skuId = skuId)
    }

    override fun forceRecoverLock(skuId: String): Long {
        val key = getSkuKey(skuId = skuId)
        return redisson.writeLock(getRecoverSkuLockKey(skuId = skuId)) {
            val sku = goodsService.getSkuInfoByKeyId(skuKeyId = skuId) ?: throw DataNotFoundException()
            val hold = sku.availableQuantity <= 0
            if(hold) {
                //设置防止缓存穿透
                redisTemplate.opsForHash<String, String>().putAll(key, mapOf(
                    skuId to sku.availableQuantity.toString(),
                    "stockId" to sku.stockId,
                    "hold" to hold.toString())
                )
                redisTemplate.expire(key, 5, TimeUnit.SECONDS)
            } else {
                redisTemplate.opsForHash<String, String>().delete(key, "hold")
                redisTemplate.opsForHash<String, String>().putAll(key, mapOf(
                    skuId to sku.availableQuantity.toString(),
                    "stockId" to sku.stockId)
                )
                //redisTemplate.expire(key, -1, TimeUnit.SECONDS)
            }
            logger.warn("!!!!!!!!恢复库存锁数据完成: stockId=${sku.stockId}, skuId=$skuId, quantity=${sku.availableQuantity}!!!!!!!!")
            sku.availableQuantity.toLong()
        }
    }

    override fun push(skuId: String, quantity: Int) {
        redisson.writeLock(getRecoverSkuLockKey(skuId = skuId)) {
            goodsService.pushStock(skuKeyId = skuId, quantity = quantity)
        }
    }

    override fun lock(orderItemKeyId: String) {
        goodsService.lockStock(orderItemKeyId = orderItemKeyId)
    }

    override fun deduct(orderItemKeyId: String) {
        goodsService.deductStock(orderItemKeyId = orderItemKeyId)
    }

    override fun recover(orderItemKeyId: String) {
        val orderItem = orderService.getOrderItemInfoByKeyId(keyId = orderItemKeyId) ?: return
        redisson.writeLock(getRecoverSkuLockKey(skuId = orderItem.goodsSkuKeyId)) {
            goodsService.recoverStock(orderItemKeyId = orderItemKeyId)
        }
    }

}