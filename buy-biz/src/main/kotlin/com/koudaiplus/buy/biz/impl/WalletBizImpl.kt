package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.WalletBiz
import com.koudaiplus.buy.api.model.WalletBizModel
import com.koudaiplus.user.api.service.UserService
import com.koudaiplus.wallet.api.feign.AccountClient
import com.koudaiplus.wallet.api.feign.BillItemClient
import com.koudaiplus.wallet.api.model.AccountServiceModel
import com.koudaiplus.wallet.api.model.BillItemServiceModel
import com.play.core.common.DataNotFoundException
import com.play.core.common.Limit
import com.play.core.common.Pages
import com.play.core.common.RespResult
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class WalletBizImpl (
    private val userService: UserService,
    private val billItemClient: BillItemClient,
    private val accountClient: AccountClient
) : WalletBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun info(userId: String): RespResult<AccountServiceModel.AccountVO?, Unit, Unit> {
        val data = userService.getInfoByUserKeyId(userKeyId = userId) ?: throw DataNotFoundException()
        if(data.walletNo == null) throw DataNotFoundException()
        return accountClient.info(accountNumber = data.walletNo!!)
    }

    override fun resetPassword(userId: String, req: WalletBizModel.ResetPassword): RespResult<Unit, Unit, Unit> {
        val data = userService.getInfoByUserKeyId(userKeyId = userId) ?: throw DataNotFoundException()
        return accountClient.resetPassword(req = AccountServiceModel.ResetPassword(
            number = data.walletNo!!, newPassword = req.newPassword, oldPassword = req.oldPassword
        ))
    }

    override fun transfer(userId: String, req: WalletBizModel.TransferByPassword): RespResult<Unit, Unit, Unit> {
        val data = userService.getInfoByUserKeyId(userKeyId = userId) ?: throw DataNotFoundException()
        return accountClient.transfer(req = com.koudaiplus.wallet.api.model.WalletBizModel.TransferByPassword(
            fromNumber = data.walletNo!!, toNumber = req.toNumber, password = req.password, amount = req.amount
        ))
    }

    override fun findList(userId: String, limit: Limit): RespResult<Pages<BillItemServiceModel.BillItemVO>, Unit, Unit> {
        val data = userService.getInfoByUserKeyId(userKeyId = userId) ?: throw DataNotFoundException()
        return billItemClient.findList(accountNumber = data.walletNo!!, limit = limit)
    }

}