package com.koudaiplus.buy.biz.impl

import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.koudaiplus.buy.api.biz.ExpressBiz
import com.koudaiplus.buy.api.model.ExpressBizModel
import com.koudaiplus.buy.api.service.ExpressService
import com.koudaiplus.buy.integration.model.YiYuanServiceModel
import com.koudaiplus.buy.integration.service.YiYuan
import com.play.core.common.BusinessException
import com.play.core.common.DataNotFoundException
import com.play.core.common.JSON
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class ExpressBizImpl(
    val expressService: ExpressService,
    val yiyuan: YiYuan
): ExpressBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun findList(keyId: String): ExpressBizModel.ExpressVO {
        val express = expressService.getInfoByKeyId(keyId = keyId) ?: throw DataNotFoundException()
        if(express.state.isPending()) {
            throw BusinessException(message = "尚未生成物流信息")
        }
        val data : List<ExpressBizModel.ExpressItemVO> = try {
            val resp = yiyuan.fetchList(param = YiYuanServiceModel.Query(
                company = yiyuan.getCompanyCode(code = express.companyCode!!),
                serialNo = express.serialNo!!, phone = express.receiverTelephone
            ))
            val obj = JsonParser.parseString(resp)
            val str = obj.asJsonObject.get("showapi_res_body").asJsonObject.get("data").asJsonArray
            JSON.getJSON().fromJson(str, object : TypeToken<List<ExpressBizModel.ExpressItemVO>>() {}.type)
        } catch (e: Exception) {
            logger.error("查询物流信息失败, expressId=$keyId", e)
            emptyList()
        }
        val companyName = yiyuan.getCompanyName(express.companyCode!!)
        val vo = ExpressBizModel.ExpressVO(serialNo = express.serialNo!!, companyName = companyName, itemList = data)
        return vo
    }

}