package com.koudaiplus.buy.biz.listener

import com.koudaiplus.buy.api.biz.RepeatPayOrderBiz
import com.koudaiplus.buy.api.model.BuyConstants.PAY_ORDER_REPEAT_PAY_QUEUE
import com.play.core.common.BusinessException
import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component

@Component
class RepeatPayListener (
    val repeatPayOrderBiz: RepeatPayOrderBiz
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @JmsListener(destination = PAY_ORDER_REPEAT_PAY_QUEUE)
    fun createPayOrderItemRefund(payOrderItemKeyId: String) {
        if (logger.isInfoEnabled) logger.info("接收创建付款子订单退款消息,msg=$payOrderItemKeyId")
        try {
            repeatPayOrderBiz.createRefund(payOrderItemKeyId = payOrderItemKeyId)
        } catch (e: BusinessException) {
            logger.warn("接收创建付款子订单退款出现业务异常, msg=$payOrderItemKeyId", e)
            if(e.retryable) throw e
        } catch (e: Exception) {
            logger.error("接收创建付款子订单退款出现系统异常, msg=$payOrderItemKeyId", e)
        }
    }

}