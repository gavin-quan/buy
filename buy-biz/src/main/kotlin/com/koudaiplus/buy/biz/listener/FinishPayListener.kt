package com.koudaiplus.buy.biz.listener

import com.google.common.reflect.TypeToken
import com.koudaiplus.buy.api.biz.PayOrderBiz
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_PAYMENT_MSG_ARRIVED_QUEUE
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.pay.api.model.PayConstants
import com.play.core.common.BusinessException
import com.play.core.common.JSON
import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component

@Component
class FinishPayListener(
    val payOrderBiz: PayOrderBiz
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @JmsListener(destination = ORDER_PAYMENT_MSG_ARRIVED_QUEUE)
    fun finishPay(msg: String) {
        logger.info("接收到支付回调通知,msg=$msg")
        val req : NotifyConstants.WebHookEvent<PayConstants.TradePaySuccessResponse> = try {
            JSON.getJSON().fromJson(msg, object : TypeToken<NotifyConstants.WebHookEvent<PayConstants.TradePaySuccessResponse>>(){}.type)
        } catch (e: Exception) {
            logger.error("支付回调解析失败, msg=$msg", e)
            return
        }
        try {
            payOrderBiz.finishPay(param = req)
        } catch (e: BusinessException) {
            logger.warn("支付回调出现业务异常, msg=$msg", e)
            if(e.retryable) throw e
        } catch (e: Exception) {
            logger.error("支付回调出现系统异常, msg=$msg", e)
        }
    }

}