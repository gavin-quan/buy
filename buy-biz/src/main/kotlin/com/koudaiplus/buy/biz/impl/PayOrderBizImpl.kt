package com.koudaiplus.buy.biz.impl

import com.google.common.eventbus.AsyncEventBus
import com.koudaiplus.buy.api.biz.EasyOrderBiz
import com.koudaiplus.buy.api.biz.OrderBiz
import com.koudaiplus.buy.api.biz.PayOrderBiz
import com.koudaiplus.buy.api.model.*
import com.koudaiplus.buy.api.service.EasyOrderService
import com.koudaiplus.buy.api.service.OrderService
import com.koudaiplus.buy.api.service.PayOrderService
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_PAYMENT_MSG_ARRIVED_QUEUE_BEAN
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.pay.api.service.MerchantAppService
import com.koudaiplus.pay.api.model.PayConstants
import com.koudaiplus.settlement.api.model.SettlementBizModel
import com.koudaiplus.settlement.api.model.SettlementConstants
import com.play.core.common.*
import com.play.core.common.Constants.DATE_FORMATTER
import com.play.core.common.Constants.HELP_LOGGER
import com.play.core.common.Constants.YYYYMMDDHHMMSS_FORMAT_PATTERN
import org.apache.commons.codec.binary.Base64
import org.apache.commons.lang3.StringUtils
import org.redisson.api.RedissonClient
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.jms.core.JmsTemplate
import org.springframework.jms.core.MessagePostProcessor
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.time.LocalDate
import javax.jms.Queue

@Service
class PayOrderBizImpl(
    val redisson: RedissonClient,
    val payOrderService: PayOrderService,
    val appProperties: AppProperties,
    val jmsTemplate: JmsTemplate,
    @Qualifier(ORDER_PAYMENT_MSG_ARRIVED_QUEUE_BEAN)
    val channelPaymentMsgArrivedQueue: Queue,
    val messagePostProcessor: MessagePostProcessor,
    val payOrderBizService: PayOrderBizService,
    val easyOrderBiz: EasyOrderBiz,
    val orderService: OrderService,
    val easyOrderService: EasyOrderService,
    val orderBiz: OrderBiz,
    val asyncEventBus: AsyncEventBus,
    private val holidayService: HolidayService,
    val merchantAppService: MerchantAppService
): PayOrderBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Transactional
    override fun createByMarketOrder(param: PayOrderBizModel.Create): String {
        val createBO = PayOrderBizModel.CreateBO()
        param.orderIdList.forEach { orderId ->
            val order = orderService.getInfoByKeyId(keyId = orderId) ?: throw BusinessException(message = "订单不存在,$orderId")
            checkBiz(order.state.isInactive()) {"订单${orderId}已失效"}
            createBO.orderList.add(order.getOrder())
        }
        //合并付款订单
        val payOrderKeyId = payOrderService.create(param = PayOrderServiceModel.Create(
            subject = createBO.subject(), amount = createBO.totalAmount()
        ))
        createBO.orderList.forEach { order ->
            payOrderService.createItem(param = PayOrderServiceModel.CreateItem(
                orderKeyId = order.keyId, payOrderKeyId = payOrderKeyId
            ))
        }
        return payOrderKeyId
    }

    override fun createByEasyOrder(param: PayOrderBizModel.Create): String {
        val createBO = PayOrderBizModel.CreateBO()
        param.orderIdList.forEach { orderId ->
            val order = easyOrderService.getInfoByKeyId(keyId = orderId) ?: throw BusinessException(message = "订单不存在,$orderId")
            checkBiz(order.state.isInactive()) {"订单${orderId}已失效"}
            createBO.orderList.add(order.getOrder())
        }
        //合并付款订单
        val payOrderKeyId = payOrderService.create(param = PayOrderServiceModel.Create(
            subject = createBO.subject(), amount = createBO.totalAmount()
        ))
        createBO.orderList.forEach { order ->
            payOrderService.createItem(param = PayOrderServiceModel.CreateItem(
                orderKeyId = order.keyId, payOrderKeyId = payOrderKeyId
            ))
        }
        return payOrderKeyId
    }

    override fun pay(param: PayOrderBizModel.Pay): String {
        val payOrder = payOrderService.getInfoByKeyId(keyId = param.payOrderKeyId) ?: throw DataNotFoundException()
        payOrder.checkWhenPay()
        val settles = mutableListOf<Map<String, Any>>()
        payOrderService.findItemListByKeyId(payOrderKeyId = payOrder.keyId).forEach {
            val order = orderService.getInfoByKeyId(keyId = it.orderKeyId) ?: throw DataNotFoundException()
            settles.add(mapOf(
                "ownerId" to order.shopKeyId,
                "settle_day" to holidayService.getWorkDayByOffset(LocalDate.now(), "T+1").format(DATE_FORMATTER),
                "items" to listOf(SettlementBizModel.ClearItem(
                    amount = order.amount,
                    receiverAccount = "600951912090863105",
                    settleType = SettlementConstants.SettleTypeEnum.WALLET,
                    receiverType = "ACCOUNT_NO", //WEIXIN = { MERCHANT_ID,PERSONAL_OPENID}, ALIPAY = { cardAliasNo,userId,loginName,defaultSettle }
                    remarks = "店铺分账",
                    bizContent = ""
                ))
            ))
        }
        val bizContent = if(param.tradeType == PayConstants.TradeTypeEnum.KOUDAI.name) {
            val directPayUrl = "${appProperties["walletpayurl"]}"
            Base64.encodeBase64String(JSON.getJSON().toJson(mapOf(
                "pay_url" to directPayUrl,
                "settles" to settles
            )).toByteArray(Charset.forName("utf-8")))
        } else Base64.encodeBase64String(JSON.getJSON().toJson(mapOf(
            "settles" to settles
        )).toByteArray(Charset.forName("utf-8")))
        val paymentUrl = appProperties["paymentUrl"] as String
        val request = PayConstants.TradePayCreateByApi(
            merchantAppId = param.merchantAppId,
            orderNo = payOrder.keyId,
            amount = payOrder.amount.toPlainString(),
            subject = payOrder.subject,
            bizContent = bizContent,
            metadata = param.metadata,
            returnUrl = param.returnUrl,
            tradeType = param.tradeType, //"ALIPAY"
            tradeSubtype = param.tradeSubtype //QR,WAP
        )
        request.sign(requestKey = param.merchantRequestKey)
        return Commons.buildAutoSubmitHtmlForm(action = paymentUrl, fields = request.buildMap())
    }

    override fun receivePayFinishNotify(param: NotifyConstants.WebHookEvent<PayConstants.TradePaySuccessResponse>): String {
        //通过返回的商户应用id获取requestKey
        val requestKey = merchantAppService.getRequestKeyByKeyId(keyId = param.body.merchantAppId)
        //val requestKey = appProperties["requestKey"] as String
        checkBiz(!param.body.checkSign(requestKey = requestKey)) {"验签失败"}
        jmsTemplate.convertAndSend(channelPaymentMsgArrivedQueue, param.json(), messagePostProcessor)
        return "success"
    }

    override fun receivePayReturnNotify(param: Map<String, String>): String {
        val orderNo = param["order_no"]
        val orderStatusPageUrl = appProperties["orderDetailUrl"] as String
        return "$orderStatusPageUrl?order_no=$orderNo"
    }

    override fun finishPay(param: NotifyConstants.WebHookEvent<PayConstants.TradePaySuccessResponse>) {
        redisson.lock(key = BuyConstants.getPayOrderLockKey(payOrderKeyId = param.body.orderNo)) {
            when(param.eventCode) {
                NotifyConstants.EventCode.PAYMENT_COMPLETED.name -> {
                    val payOrder = payOrderService.getInfoByKeyId(keyId = param.body.orderNo) ?: throw DataNotFoundException()
                    when(param.body.metadata) {
                        "easyOrder.topup" -> {
                            payOrderBizService.finishPay(req = param.body, payOrder = payOrder) { param ->
                                easyOrderBiz.finishTopupOrderPay(param = EasyOrderServiceModel.FinishPay(
                                    keyId = param.orderKeyId, payOrderKeyId = param.payOrderKeyId,
                                    transOrderNo = param.transOrderNo
                                ))
                            }
                        }
                        else -> {
                            payOrderBizService.finishPay(req = param.body, payOrder = payOrder) { param ->
                                orderBiz.finishPay(param = OrderServiceModel.FinishPay(
                                    keyId = param.orderKeyId, payOrderKeyId = param.payOrderKeyId,
                                    transOrderNo = param.transOrderNo
                                ))
                            }
                        }
                    }
                    //发布订单完成通知
                    //jmsTemplate.convertAndSend(orderPayFinishQueue, param.orderNo)
                    asyncEventBus.post(BuyConstants.OrderPayFinishEvent(payOrderKeyId = param.body.orderNo))
                }
                else -> {
                    logger.warn("订单尚未付款完成,系统暂不进行处理, param={}", param.json())
                }
            }
        }
    }

}

@Service
class PayOrderBizService(
    val payOrderService: PayOrderService,
    val jmsTemplate: JmsTemplate,
    @Qualifier(BuyConstants.PAY_ORDER_REPEAT_PAY_QUEUE_BEAN)
    val payOrderRepeatPayQueue: Queue,
    val messagePostProcessor: MessagePostProcessor
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun finishPay(req: PayConstants.TradePaySuccessResponse, payOrder: PayOrderServiceModel.PayOrderDTO, finishPayOrderItem : (req: OrderApiModel.FinishPay) -> Unit) {
        //处理PayOrder重复通知
        if(payOrder.isRepeatNotify(transOrderNo = req.outOrderNo)) {
            //如果重复通知则直接返回
            logger.info("重复通知, 程序返回. req={}", req.json())
            return
        }
        /*由于下单付款时复用了旧的付款号，在实际付款页面用户可能会切换支付方式。
        如：从原来的支付宝选择了微信或钱包付款，
        此时支付中台会先取消原来支付宝所在的交易，再重新添加一个交易，此时就存在了2个交易对应一个付款号，
        就存在了重复支付的风险。
        但在支付中台已经处理了2个交易的场景，即当交易已取消是不会完成付款的，也就不会通知到此系统来。
        故：此处只打印了一个错误消息，如果确实发生了这种情况，待人工处理。
        */
        if(payOrder.state.isPaid()) {
            HELP_LOGGER.warn("订单完成付款###订单发生重复支付###${req.json()}###$payOrder###此场景理论不存在，需要找上游渠道协商处理")
            throw BusinessException(message = "完成付款失败")
        }
        if(payOrder.state.isClosed() || payOrder.state.isCancel()) {
            HELP_LOGGER.warn("订单完成付款###订单已关闭###${req.json()}###$payOrder###接收付款成功消息太慢，需要和渠道确认支付信息，然后在系统手工完成")
            throw BusinessException(message = "完成付款失败")
        }
        if(!payOrder.state.isWaitPay()) {
            HELP_LOGGER.warn("订单完成付款###订单不是等待付款状态###${req.json()}###$payOrder###需要和渠道确认支付信息，然后在系统手工完成")
            throw BusinessException(message = "完成付款失败")
        }
        /*if(req.buyerPayAmount.toBigDecimal() != payOrder.amount) {
            HELP_LOGGER.warn("订单完成付款###付款金额不相等###${req.json()}###$payOrder###需要和渠道确认支付信息，然后在系统手工完成")
            throw BusinessException(message = "完成付款失败")
        }*/
        val finishTime = SimpleDateFormat(YYYYMMDDHHMMSS_FORMAT_PATTERN).parse(req.finishTime)
        payOrderService.findItemListByKeyId(payOrderKeyId = payOrder.keyId).forEach { payOrderItem ->
            try {
                finishPayOrderItem(OrderApiModel.FinishPay(
                    orderKeyId = payOrderItem.orderKeyId, transOrderNo = req.outOrderNo, payOrderKeyId = payOrder.keyId
                ))
            } catch (e: TransactionNotRollbackException) {
                if(StringUtils.equals(BuyConstants.REPEAT_PAY_ERROR_CODE, e.code)) {
                    logger.warn("系统发生重复付款, req={}", req.json())
                    //如果是重复付款
                    jmsTemplate.convertAndSend(payOrderRepeatPayQueue, payOrderItem.keyId, messagePostProcessor)
                }
                //TransactionAspectSupport.currentTransactionStatus().setRollbackOnly()
            }
        }
        payOrderService.finishPay(param = PayOrderServiceModel.FinishPay(
            keyId = payOrder.keyId, transOrderNo = req.outOrderNo, finishTime = finishTime
        ))
    }

}