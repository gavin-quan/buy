package com.koudaiplus.buy.biz.listener

import com.koudaiplus.buy.api.biz.OrderBiz
import com.koudaiplus.buy.api.model.OrderBizModel
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CREATE_QUEUE
import com.play.core.common.BusinessException
import com.play.core.common.JSON
import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component

@Component
class CreateOrderListener(
    val orderBiz: OrderBiz
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @JmsListener(destination = ORDER_CREATE_QUEUE)
    fun createOrder(msg: String) {
        logger.info("接收到异步创建订单任务,msg=$msg")
        val event = try {
            JSON.getJSON().fromJson(msg, BuyConstants.OrderCreateEvent::class.java)
        } catch (e: Exception) {
            logger.error("创建订单任务参数错误, msg=$msg", e)
            return
        }
        val param = try {
            JSON.getJSON().fromJson(event.msg, OrderBizModel.Create::class.java)
        } catch (e: Exception) {
            logger.error("创建订单任务参数错误, msg=${event.msg}", e)
            return
        }
        try {
            val orderIdList = orderBiz.create(batchId = event.batchId, param = param)
            if(logger.isInfoEnabled) logger.info("异步创建订单完成, orderIdList=$orderIdList, msg=$msg")
        } catch (e: BusinessException) {
            logger.warn("异步创建订单任务出现业务异常, msg=$msg", e)
            if(e.retryable) throw e
        } catch (e: Exception) {
            logger.error("异步创建订单任务出现系统异常, msg=$msg", e)
        }
    }

}