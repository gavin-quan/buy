package com.koudaiplus.buy.biz.listener

import com.google.common.eventbus.AsyncEventBus
import com.google.common.eventbus.Subscribe
import com.koudaiplus.buy.api.biz.RefundOrderBiz
import com.koudaiplus.buy.api.service.RefundOrderService
import com.koudaiplus.buy.api.model.BuyConstants
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.stereotype.Component

@Component
class CreateRefundExpressListener (
    val eventBus: AsyncEventBus,
    val refundOrderService: RefundOrderService,
    val refundOrderBiz: RefundOrderBiz
): InitializingBean {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Subscribe
    fun createRefundExpress(event: BuyConstants.RefundStateChangeEvent) {
        if (logger.isInfoEnabled) logger.info("接收创建退款物流消息,refundOrderKeyId=${event.refundOrderKeyId}")
        val refundOrder = refundOrderService.getInfoByKeyId(keyId = event.refundOrderKeyId)
        if(refundOrder == null) {
            logger.warn("创建退款物流处理失败,未发现退款订单,refundOrderKeyId=${event.refundOrderKeyId}")
            return
        }
        if(refundOrder.state.isProcessing()) {
            refundOrderBiz.createRefundExpress(refundOrderKeyId = event.refundOrderKeyId)
        } else {
            logger.warn("忽略创建退款物流,退款订单尚未进行,refundOrderKeyId=${event.refundOrderKeyId}")
        }
    }

    override fun afterPropertiesSet() {
        eventBus.register(this)
    }

}