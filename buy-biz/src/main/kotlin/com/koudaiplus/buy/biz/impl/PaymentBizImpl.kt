package com.koudaiplus.buy.biz.impl

import com.koudaiplus.buy.api.biz.OrderBiz
import com.koudaiplus.buy.api.biz.PaymentBiz
import com.koudaiplus.buy.api.biz.PayOrderBiz
import com.koudaiplus.buy.api.model.OrderBizModel
import com.koudaiplus.buy.api.model.PayBizModel
import com.koudaiplus.buy.api.model.PayOrderBizModel
import com.koudaiplus.buy.api.service.OrderService
import com.koudaiplus.buy.api.service.PayOrderService
import com.play.core.common.AppProperties
import com.play.core.common.DataNotFoundException
import com.play.core.common.checkBiz
import org.apache.commons.collections4.CollectionUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class PaymentBizImpl (
    val appProperties: AppProperties,
    val orderService: OrderService,
    val payOrderService: PayOrderService,
    var payOrderBiz: PayOrderBiz,
    val orderBiz: OrderBiz
) : PaymentBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun directPay(param: PayBizModel.DirectPay): String {
        val createParam = OrderBizModel.Create(skuList = param.skuList, buyerContactId = param.buyerContactId, buyerId = param.buyerId)
        val orderIdList = orderBiz.createBySync(param = createParam)
        val payOrderKeyId = payOrderBiz.createByMarketOrder(param = PayOrderBizModel.Create(orderIdList = orderIdList))
        val merchantAppId = appProperties["merchantAppId"] as String
        val requestKey = appProperties["requestKey"] as String
        val returnUrl = appProperties["returnUrl"] as String
        return payOrderBiz.pay(param = PayOrderBizModel.Pay(
            payOrderKeyId = payOrderKeyId, tradeType = param.tradeType, tradeSubtype = param.tradeSubtype,
            merchantAppId = merchantAppId, merchantRequestKey = requestKey, returnUrl = returnUrl
        ))
    }

    override fun orderPay(param: PayBizModel.OrderPay): String {
        //确认订单是否付款
        val order = orderService.getInfoByKeyId(keyId = param.orderId) ?: throw DataNotFoundException()
        checkBiz(order.isPaid()) {"订单已付款"}
        //根据订单集合搜索出可重复利用的往期支付订单
        val retryablePayOrderIdList = payOrderService.findRetryablePayOrderIdList(listOf(param.orderId))
        val payOrderKeyId = if(CollectionUtils.isEmpty(retryablePayOrderIdList)) {
            payOrderBiz.createByMarketOrder(param = PayOrderBizModel.Create(orderIdList = listOf(param.orderId)))
        } else {
            retryablePayOrderIdList.shuffled()[0]
        }
        val merchantAppId = appProperties["merchantAppId"] as String
        val requestKey = appProperties["requestKey"] as String
        val returnUrl = appProperties["returnUrl"] as String
        return payOrderBiz.pay(param = PayOrderBizModel.Pay(
            payOrderKeyId = payOrderKeyId, tradeType = param.tradeType, tradeSubtype = param.tradeSubtype,
            merchantAppId = merchantAppId, merchantRequestKey = requestKey, returnUrl = returnUrl
        ))
    }

}