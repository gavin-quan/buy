package com.koudaiplus.buy.biz.impl

import com.google.common.eventbus.AsyncEventBus
import com.koudaiplus.buy.api.biz.OrderBiz
import com.koudaiplus.buy.api.biz.StockService
import com.koudaiplus.buy.api.model.*
import com.koudaiplus.buy.api.service.*
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CREATE_QUEUE_BEAN
import com.koudaiplus.buy.api.model.BuyConstants.REPEAT_PAY_ERROR_CODE
import com.koudaiplus.settlement.api.feign.SettlementClient
import com.play.core.common.*
import com.play.core.common.Constants.HELP_LOGGER
import org.apache.commons.lang3.StringUtils
import org.redisson.api.RedissonClient
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.jms.core.JmsTemplate
import org.springframework.jms.core.MessagePostProcessor
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import javax.jms.Queue
import kotlin.collections.HashMap

@Service
class OrderBizImpl (
    val redisson: RedissonClient,
    val goodsService: GoodsService,
    val orderService: OrderService,
    val shopService: ShopService,
    val stockService: StockService,
    val contactService: ContactService,
    val orderBizService: OrderBizService,
    val asyncEventBus: AsyncEventBus,
    val expressService: ExpressService,
    val settlementClient: SettlementClient,
    val jmsTemplate: JmsTemplate,
    @Qualifier(ORDER_CREATE_QUEUE_BEAN)
    val orderCreateQueue: Queue,
    val messagePostProcessor: MessagePostProcessor,
    val idGenerator: SnowFlakeGenerator
): OrderBiz {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun createBySync(param: OrderBizModel.Create): List<String> {
        val batchId = idGenerator.nextId().toString()
        return create(batchId = batchId, param = param)
    }

    override fun createByAsync(param: OrderBizModel.Create): String {
        val batchId = idGenerator.nextId().toString()
        val event = BuyConstants.OrderCreateEvent(batchId = batchId, msg = JSON.getJSON().toJson(param))
        val msg = JSON.getJSON().toJson(event)
        jmsTemplate.convertAndSend(orderCreateQueue, msg, messagePostProcessor)
        return batchId
    }

    override fun create(batchId: String, param: OrderBizModel.Create): List<String> {
        val buyerContact = contactService.getInfoByKeyId(keyId = param.buyerContactId) ?: throw DataNotFoundException(message = "收货地址未找到")
        checkBiz(buyerContact.state.isInactive()) {"收货地址已失效"}
        val shopGoodsMap = HashMap<String, OrderBizModel.CreateBO>()
        param.skuList.forEach { item ->
            val sku = goodsService.getSkuInfoByKeyId(skuKeyId = item.skuId) ?: throw BusinessException(message = "商品未找到")
            val goods = goodsService.getInfoByKeyId(keyId = sku.goodsKeyId) ?: throw BusinessException(message = "商品未找到")
            checkBiz(sku.state.isInactive()) {"商品${goods.name}已失效"}
            checkBiz(sku.availableQuantity <= 0 || sku.availableQuantity < item.quantity) {"商品${goods.name}库存不足"}
            val shop = shopService.getInfoByKeyId(keyId = goods.shopKeyId) ?: throw DataNotFoundException(message = "店铺未找到")
            checkBiz(shop.state.isInactive()) {"店铺${shop.name}暂时无法访问"}
            if(!shopGoodsMap.containsKey(key = goods.shopKeyId)) {
                shopGoodsMap[goods.shopKeyId] = OrderBizModel.CreateBO(shopKeyId = goods.shopKeyId)
            }
            shopGoodsMap[goods.shopKeyId]!!.skuList.add(OrderBizModel.SkuBO(sku = sku, item = item, goods = goods))
        }
        val orderIdList = LinkedList<String>()
        shopGoodsMap.forEach { item ->
            try {
                val orderKeyId = orderBizService.create(batchId = batchId, shop = item.value, param = param, buyerContact = buyerContact)
                orderIdList.add(orderKeyId)
                //发布订单创建完成
                asyncEventBus.post(BuyConstants.OrderCreatedEvent(orderKeyId = orderKeyId))
            } catch (e: Exception) {
                logger.error("创建订单失败, ${item.value}", e)
            }
        }
        return orderIdList
    }

    override fun tryCloseByKeyId(keyId: String) {
        redisson.quickLock(key = BuyConstants.getOrderLockKey(orderKeyId = keyId)) {
            orderService.tryCloseByKeyId(keyId = keyId)
        }
    }

    override fun getRefundBuilder(keyId: String, userId: String): RefundOrderApiModel.RefundBuilder? {
        val orderItem = orderService.getOrderItemInfoByKeyId(keyId = keyId) ?: return null
        val order = orderService.getInfoByKeyId(keyId = orderItem.orderKeyId) ?: return null
        checkBiz(!StringUtils.equals(userId, order.buyerId)) {"订单不存在"}
        //未付款订单不支持退款
        checkBiz(Objects.isNull(order.payOrderKeyId)) { "订单尚未付款" }
        return RefundOrderApiModel.RefundBuilder(
            payOrderKeyId = order.payOrderKeyId!!, orderKeyId = order.keyId,
            orderItemKeyId = keyId, price = orderItem.totalPrice, shopKeyId = order.shopKeyId
        )
    }

    @Transactional(noRollbackFor = [TransactionNotRollbackException::class])
    override fun finishPay(param: OrderServiceModel.FinishPay) {
        val order = orderService.getInfoByKeyId(keyId = param.keyId) ?: throw DataNotFoundException()
        //处理Order重复通知
        if(order.isRepeatNotify(transOrderNo = param.transOrderNo, payOrderKeyId = param.payOrderKeyId)) {
            return
        }
        if(order.isPaid()) {
            HELP_LOGGER.warn("订单完成付款###订单发生重复支付###$param###$order###系统将自动退款，请做审核")
            throw TransactionNotRollbackException(code = REPEAT_PAY_ERROR_CODE, message = "完成付款失败")
        }
        if(order.state.isInactive()) {
            HELP_LOGGER.warn("订单完成付款###订单付款超时###$param###$order###需要确认时间，如果正确则在系统设置已完成")
            throw TransactionNotRollbackException(message = "完成付款失败")
        }
        orderService.finishPay(param = param)
        orderService.findOrderItemList(orderKeyId = param.keyId).forEach {
            stockService.deduct(orderItemKeyId = it.keyId)
        }
    }

    override fun recoverStock() {
        orderService.findCancelStockOrderItemList().forEach {
            stockService.recover(orderItemKeyId = it.keyId)
        }
    }

    override fun delay(keyId: String, buyerId: String) {
        val order = orderService.getInfoByKeyId(keyId = keyId) ?: throw DataNotFoundException()
        if(order.buyerId != buyerId) throw DataNotFoundException()
        if (!order.isPaid()) return
        settlementClient.delay(ownerId = order.shopKeyId, orderNo = order.transOrderNo!!)
    }

    @Transactional
    override fun confirm(keyId: String, buyerId: String) {
        val order = orderService.getInfoByKeyId(keyId = keyId) ?: throw DataNotFoundException()
        if(order.buyerId != buyerId) throw DataNotFoundException()
        expressService.confirm(keyId = order.expressId!!)
        settlementClient.confirm(ownerId = order.shopKeyId, orderNo = order.transOrderNo!!)
    }

}

@Service
class OrderBizService (
    val orderService: OrderService,
    val stockService: StockService,
    val cartService: CartService,
    val expressService: ExpressService
) {

    @Transactional
    fun create(batchId: String, shop: OrderBizModel.CreateBO, param: OrderBizModel.Create, buyerContact: ContactServiceModel.ContactDTO): String {
        val subject = shop.subject()
        //生成物流信息
        val expressId = expressService.create(param = ExpressServiceModel.Create(
            shopKeyId = shop.shopKeyId, receiverName = buyerContact.name, receiverAddress = buyerContact.toContactVO().fullAddress,
            receiverTelephone = buyerContact.telephone, title = subject, receiverUserId = param.buyerId
        ))
        //商铺订单拆分
        val orderKeyId = orderService.create(param = OrderServiceModel.Create(
            batchId = batchId, shopKeyId = shop.shopKeyId, expressId = expressId,
            buyerId = param.buyerId, amount = shop.totalPrice(), subject = subject
        ))
        shop.skuList.forEach { item ->
            val orderItemKeyId = orderService.createItem(param = OrderServiceModel.CreateItem(
                orderKeyId = orderKeyId, shopKeyId = shop.shopKeyId, goodsKeyId = item.goods.keyId,
                goodsName = item.sku.name, goodsPrice = item.sku.discountPrice,
                goodsQuantity = item.item.quantity, goodsSkuId = item.sku.keyId
            ))
            stockService.tryLock(skuId = item.sku.keyId, quantity = item.item.quantity)
            stockService.lock(orderItemKeyId = orderItemKeyId)
            cartService.delete(buyerId = param.buyerId, skuId = item.sku.keyId)
        }
        return orderKeyId
    }

}