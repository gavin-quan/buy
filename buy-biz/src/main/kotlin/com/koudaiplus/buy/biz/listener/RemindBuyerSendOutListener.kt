package com.koudaiplus.buy.biz.listener

import com.google.common.eventbus.AsyncEventBus
import com.google.common.eventbus.Subscribe
import com.koudaiplus.buy.api.model.BuyConstants
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.stereotype.Component

@Component
class RemindBuyerSendOutListener(
        val eventBus: AsyncEventBus
): InitializingBean {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Subscribe
    fun remindSendOut(event: BuyConstants.RefundExpressCreatedEvent) {
        //发邮件/短信/QQ等
        logger.info("提醒买家发货通知,payOrderKeyId=${event.refundOrderKeyId}")
        logger.info("提醒买家发货通知,payOrderKeyId=${event.refundOrderKeyId}")
        logger.info("提醒买家发货通知,payOrderKeyId=${event.refundOrderKeyId}")
        logger.info("提醒买家发货通知,payOrderKeyId=${event.refundOrderKeyId}")
    }

    override fun afterPropertiesSet() {
        eventBus.register(this)
    }

}