package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Express {
    private String epsKeyId;

    private Long epsId;

    private String epsShopKeyId;

    private String epsReceiverUserId;

    private String epsTitle;

    private String epsReceiverName;

    private String epsReceiverTelephone;

    private String epsReceiverAddress;

    private String epsConsignorName;

    private String epsConsignorTelephone;

    private String epsConsignorAddress;

    private String epsCompanyCode;

    private String epsSerialNo;

    private Date epsStartTime;

    private String epsRemarks;

    private String epsState;

    private Date epsCreateTime;

    private Date epsUpdateTime;

    public String getEpsKeyId() {
        return epsKeyId;
    }

    public void setEpsKeyId(String epsKeyId) {
        this.epsKeyId = epsKeyId == null ? null : epsKeyId.trim();
    }

    public Long getEpsId() {
        return epsId;
    }

    public void setEpsId(Long epsId) {
        this.epsId = epsId;
    }

    public String getEpsShopKeyId() {
        return epsShopKeyId;
    }

    public void setEpsShopKeyId(String epsShopKeyId) {
        this.epsShopKeyId = epsShopKeyId == null ? null : epsShopKeyId.trim();
    }

    public String getEpsReceiverUserId() {
        return epsReceiverUserId;
    }

    public void setEpsReceiverUserId(String epsReceiverUserId) {
        this.epsReceiverUserId = epsReceiverUserId == null ? null : epsReceiverUserId.trim();
    }

    public String getEpsTitle() {
        return epsTitle;
    }

    public void setEpsTitle(String epsTitle) {
        this.epsTitle = epsTitle == null ? null : epsTitle.trim();
    }

    public String getEpsReceiverName() {
        return epsReceiverName;
    }

    public void setEpsReceiverName(String epsReceiverName) {
        this.epsReceiverName = epsReceiverName == null ? null : epsReceiverName.trim();
    }

    public String getEpsReceiverTelephone() {
        return epsReceiverTelephone;
    }

    public void setEpsReceiverTelephone(String epsReceiverTelephone) {
        this.epsReceiverTelephone = epsReceiverTelephone == null ? null : epsReceiverTelephone.trim();
    }

    public String getEpsReceiverAddress() {
        return epsReceiverAddress;
    }

    public void setEpsReceiverAddress(String epsReceiverAddress) {
        this.epsReceiverAddress = epsReceiverAddress == null ? null : epsReceiverAddress.trim();
    }

    public String getEpsConsignorName() {
        return epsConsignorName;
    }

    public void setEpsConsignorName(String epsConsignorName) {
        this.epsConsignorName = epsConsignorName == null ? null : epsConsignorName.trim();
    }

    public String getEpsConsignorTelephone() {
        return epsConsignorTelephone;
    }

    public void setEpsConsignorTelephone(String epsConsignorTelephone) {
        this.epsConsignorTelephone = epsConsignorTelephone == null ? null : epsConsignorTelephone.trim();
    }

    public String getEpsConsignorAddress() {
        return epsConsignorAddress;
    }

    public void setEpsConsignorAddress(String epsConsignorAddress) {
        this.epsConsignorAddress = epsConsignorAddress == null ? null : epsConsignorAddress.trim();
    }

    public String getEpsCompanyCode() {
        return epsCompanyCode;
    }

    public void setEpsCompanyCode(String epsCompanyCode) {
        this.epsCompanyCode = epsCompanyCode == null ? null : epsCompanyCode.trim();
    }

    public String getEpsSerialNo() {
        return epsSerialNo;
    }

    public void setEpsSerialNo(String epsSerialNo) {
        this.epsSerialNo = epsSerialNo == null ? null : epsSerialNo.trim();
    }

    public Date getEpsStartTime() {
        return epsStartTime;
    }

    public void setEpsStartTime(Date epsStartTime) {
        this.epsStartTime = epsStartTime;
    }

    public String getEpsRemarks() {
        return epsRemarks;
    }

    public void setEpsRemarks(String epsRemarks) {
        this.epsRemarks = epsRemarks == null ? null : epsRemarks.trim();
    }

    public String getEpsState() {
        return epsState;
    }

    public void setEpsState(String epsState) {
        this.epsState = epsState == null ? null : epsState.trim();
    }

    public Date getEpsCreateTime() {
        return epsCreateTime;
    }

    public void setEpsCreateTime(Date epsCreateTime) {
        this.epsCreateTime = epsCreateTime;
    }

    public Date getEpsUpdateTime() {
        return epsUpdateTime;
    }

    public void setEpsUpdateTime(Date epsUpdateTime) {
        this.epsUpdateTime = epsUpdateTime;
    }

    public static Express.Builder builder() {
        return new Express.Builder();
    }

    public static class Builder {
        private Express obj;

        public Builder() {
            this.obj = new Express();
        }

        public Builder epsKeyId(String epsKeyId) {
            obj.setEpsKeyId(epsKeyId);
            return this;
        }

        public Builder epsId(Long epsId) {
            obj.setEpsId(epsId);
            return this;
        }

        public Builder epsShopKeyId(String epsShopKeyId) {
            obj.setEpsShopKeyId(epsShopKeyId);
            return this;
        }

        public Builder epsReceiverUserId(String epsReceiverUserId) {
            obj.setEpsReceiverUserId(epsReceiverUserId);
            return this;
        }

        public Builder epsTitle(String epsTitle) {
            obj.setEpsTitle(epsTitle);
            return this;
        }

        public Builder epsReceiverName(String epsReceiverName) {
            obj.setEpsReceiverName(epsReceiverName);
            return this;
        }

        public Builder epsReceiverTelephone(String epsReceiverTelephone) {
            obj.setEpsReceiverTelephone(epsReceiverTelephone);
            return this;
        }

        public Builder epsReceiverAddress(String epsReceiverAddress) {
            obj.setEpsReceiverAddress(epsReceiverAddress);
            return this;
        }

        public Builder epsConsignorName(String epsConsignorName) {
            obj.setEpsConsignorName(epsConsignorName);
            return this;
        }

        public Builder epsConsignorTelephone(String epsConsignorTelephone) {
            obj.setEpsConsignorTelephone(epsConsignorTelephone);
            return this;
        }

        public Builder epsConsignorAddress(String epsConsignorAddress) {
            obj.setEpsConsignorAddress(epsConsignorAddress);
            return this;
        }

        public Builder epsCompanyCode(String epsCompanyCode) {
            obj.setEpsCompanyCode(epsCompanyCode);
            return this;
        }

        public Builder epsSerialNo(String epsSerialNo) {
            obj.setEpsSerialNo(epsSerialNo);
            return this;
        }

        public Builder epsStartTime(Date epsStartTime) {
            obj.setEpsStartTime(epsStartTime);
            return this;
        }

        public Builder epsRemarks(String epsRemarks) {
            obj.setEpsRemarks(epsRemarks);
            return this;
        }

        public Builder epsState(String epsState) {
            obj.setEpsState(epsState);
            return this;
        }

        public Builder epsCreateTime(Date epsCreateTime) {
            obj.setEpsCreateTime(epsCreateTime);
            return this;
        }

        public Builder epsUpdateTime(Date epsUpdateTime) {
            obj.setEpsUpdateTime(epsUpdateTime);
            return this;
        }

        public Express build() {
            return this.obj;
        }
    }

    public enum Column {
        epsKeyId("eps_key_id", "epsKeyId", "VARCHAR", false),
        epsId("eps_id", "epsId", "BIGINT", false),
        epsShopKeyId("eps_shop_key_id", "epsShopKeyId", "VARCHAR", false),
        epsReceiverUserId("eps_receiver_user_id", "epsReceiverUserId", "VARCHAR", false),
        epsTitle("eps_title", "epsTitle", "VARCHAR", false),
        epsReceiverName("eps_receiver_name", "epsReceiverName", "VARCHAR", false),
        epsReceiverTelephone("eps_receiver_telephone", "epsReceiverTelephone", "VARCHAR", false),
        epsReceiverAddress("eps_receiver_address", "epsReceiverAddress", "VARCHAR", false),
        epsConsignorName("eps_consignor_name", "epsConsignorName", "VARCHAR", false),
        epsConsignorTelephone("eps_consignor_telephone", "epsConsignorTelephone", "VARCHAR", false),
        epsConsignorAddress("eps_consignor_address", "epsConsignorAddress", "VARCHAR", false),
        epsCompanyCode("eps_company_code", "epsCompanyCode", "VARCHAR", false),
        epsSerialNo("eps_serial_no", "epsSerialNo", "VARCHAR", false),
        epsStartTime("eps_start_time", "epsStartTime", "TIMESTAMP", false),
        epsRemarks("eps_remarks", "epsRemarks", "VARCHAR", false),
        epsState("eps_state", "epsState", "VARCHAR", false),
        epsCreateTime("eps_create_time", "epsCreateTime", "TIMESTAMP", false),
        epsUpdateTime("eps_update_time", "epsUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}