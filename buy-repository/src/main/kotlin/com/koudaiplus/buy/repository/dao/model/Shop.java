package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Shop {
    private String mspKeyId;

    private Long mspId;

    private String mspName;

    private String mspLogo;

    private String mspCover;

    private String mspOwnerId;

    private String mspState;

    private Date mspCreateTime;

    private Date mspUpdateTime;

    public String getMspKeyId() {
        return mspKeyId;
    }

    public void setMspKeyId(String mspKeyId) {
        this.mspKeyId = mspKeyId == null ? null : mspKeyId.trim();
    }

    public Long getMspId() {
        return mspId;
    }

    public void setMspId(Long mspId) {
        this.mspId = mspId;
    }

    public String getMspName() {
        return mspName;
    }

    public void setMspName(String mspName) {
        this.mspName = mspName == null ? null : mspName.trim();
    }

    public String getMspLogo() {
        return mspLogo;
    }

    public void setMspLogo(String mspLogo) {
        this.mspLogo = mspLogo == null ? null : mspLogo.trim();
    }

    public String getMspCover() {
        return mspCover;
    }

    public void setMspCover(String mspCover) {
        this.mspCover = mspCover == null ? null : mspCover.trim();
    }

    public String getMspOwnerId() {
        return mspOwnerId;
    }

    public void setMspOwnerId(String mspOwnerId) {
        this.mspOwnerId = mspOwnerId == null ? null : mspOwnerId.trim();
    }

    public String getMspState() {
        return mspState;
    }

    public void setMspState(String mspState) {
        this.mspState = mspState == null ? null : mspState.trim();
    }

    public Date getMspCreateTime() {
        return mspCreateTime;
    }

    public void setMspCreateTime(Date mspCreateTime) {
        this.mspCreateTime = mspCreateTime;
    }

    public Date getMspUpdateTime() {
        return mspUpdateTime;
    }

    public void setMspUpdateTime(Date mspUpdateTime) {
        this.mspUpdateTime = mspUpdateTime;
    }

    public static Shop.Builder builder() {
        return new Shop.Builder();
    }

    public static class Builder {
        private Shop obj;

        public Builder() {
            this.obj = new Shop();
        }

        public Builder mspKeyId(String mspKeyId) {
            obj.setMspKeyId(mspKeyId);
            return this;
        }

        public Builder mspId(Long mspId) {
            obj.setMspId(mspId);
            return this;
        }

        public Builder mspName(String mspName) {
            obj.setMspName(mspName);
            return this;
        }

        public Builder mspLogo(String mspLogo) {
            obj.setMspLogo(mspLogo);
            return this;
        }

        public Builder mspCover(String mspCover) {
            obj.setMspCover(mspCover);
            return this;
        }

        public Builder mspOwnerId(String mspOwnerId) {
            obj.setMspOwnerId(mspOwnerId);
            return this;
        }

        public Builder mspState(String mspState) {
            obj.setMspState(mspState);
            return this;
        }

        public Builder mspCreateTime(Date mspCreateTime) {
            obj.setMspCreateTime(mspCreateTime);
            return this;
        }

        public Builder mspUpdateTime(Date mspUpdateTime) {
            obj.setMspUpdateTime(mspUpdateTime);
            return this;
        }

        public Shop build() {
            return this.obj;
        }
    }

    public enum Column {
        mspKeyId("msp_key_id", "mspKeyId", "VARCHAR", false),
        mspId("msp_id", "mspId", "BIGINT", false),
        mspName("msp_name", "mspName", "VARCHAR", false),
        mspLogo("msp_logo", "mspLogo", "VARCHAR", false),
        mspCover("msp_cover", "mspCover", "VARCHAR", false),
        mspOwnerId("msp_owner_id", "mspOwnerId", "VARCHAR", false),
        mspState("msp_state", "mspState", "VARCHAR", false),
        mspCreateTime("msp_create_time", "mspCreateTime", "TIMESTAMP", false),
        mspUpdateTime("msp_update_time", "mspUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}