package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.Cart;
import com.koudaiplus.buy.repository.dao.model.CartExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CartMapper {
    long countByExample(CartExample example);

    int deleteByExample(CartExample example);

    int deleteByPrimaryKey(String mctKeyId);

    int insert(Cart record);

    int insertSelective(@Param("record") Cart record, @Param("selective") Cart.Column ... selective);

    Cart selectOneByExample(CartExample example);

    Cart selectOneByExampleSelective(@Param("example") CartExample example, @Param("selective") Cart.Column ... selective);

    List<Cart> selectByExampleSelective(@Param("example") CartExample example, @Param("selective") Cart.Column ... selective);

    List<Cart> selectByExample(CartExample example);

    Cart selectByPrimaryKeySelective(@Param("mctKeyId") String mctKeyId, @Param("selective") Cart.Column ... selective);

    Cart selectByPrimaryKey(String mctKeyId);

    int updateByExampleSelective(@Param("record") Cart record, @Param("example") CartExample example, @Param("selective") Cart.Column ... selective);

    int updateByExample(@Param("record") Cart record, @Param("example") CartExample example);

    int updateByPrimaryKeySelective(@Param("record") Cart record, @Param("selective") Cart.Column ... selective);

    int updateByPrimaryKey(Cart record);

    int batchInsert(@Param("list") List<Cart> list);

    int batchInsertSelective(@Param("list") List<Cart> list, @Param("selective") Cart.Column ... selective);

    int upsert(Cart record);

    int upsertByExample(@Param("record") Cart record, @Param("example") CartExample example);

    int upsertByExampleSelective(@Param("record") Cart record, @Param("example") CartExample example, @Param("selective") Cart.Column ... selective);

    int upsertSelective(@Param("record") Cart record, @Param("selective") Cart.Column ... selective);
}