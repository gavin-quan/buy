package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.Shop

interface ShopDao {
    fun getInfoByKeyId(keyId: String): Shop?
}