package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Contact {
    private String mcnKeyId;

    private Long mcnId;

    private String mcnUserKeyId;

    private String mcnName;

    private String mcnTelephone;

    private String mcnAddress;

    private Integer mcnSort;

    private String mcnProvince;

    private String mcnCity;

    private String mcnCounty;

    private String mcnAreacode;

    private String mcnPostalcode;

    private String mcnState;

    private Date mcnCreateTime;

    private Date mcnUpdateTime;

    public String getMcnKeyId() {
        return mcnKeyId;
    }

    public void setMcnKeyId(String mcnKeyId) {
        this.mcnKeyId = mcnKeyId == null ? null : mcnKeyId.trim();
    }

    public Long getMcnId() {
        return mcnId;
    }

    public void setMcnId(Long mcnId) {
        this.mcnId = mcnId;
    }

    public String getMcnUserKeyId() {
        return mcnUserKeyId;
    }

    public void setMcnUserKeyId(String mcnUserKeyId) {
        this.mcnUserKeyId = mcnUserKeyId == null ? null : mcnUserKeyId.trim();
    }

    public String getMcnName() {
        return mcnName;
    }

    public void setMcnName(String mcnName) {
        this.mcnName = mcnName == null ? null : mcnName.trim();
    }

    public String getMcnTelephone() {
        return mcnTelephone;
    }

    public void setMcnTelephone(String mcnTelephone) {
        this.mcnTelephone = mcnTelephone == null ? null : mcnTelephone.trim();
    }

    public String getMcnAddress() {
        return mcnAddress;
    }

    public void setMcnAddress(String mcnAddress) {
        this.mcnAddress = mcnAddress == null ? null : mcnAddress.trim();
    }

    public Integer getMcnSort() {
        return mcnSort;
    }

    public void setMcnSort(Integer mcnSort) {
        this.mcnSort = mcnSort;
    }

    public String getMcnProvince() {
        return mcnProvince;
    }

    public void setMcnProvince(String mcnProvince) {
        this.mcnProvince = mcnProvince == null ? null : mcnProvince.trim();
    }

    public String getMcnCity() {
        return mcnCity;
    }

    public void setMcnCity(String mcnCity) {
        this.mcnCity = mcnCity == null ? null : mcnCity.trim();
    }

    public String getMcnCounty() {
        return mcnCounty;
    }

    public void setMcnCounty(String mcnCounty) {
        this.mcnCounty = mcnCounty == null ? null : mcnCounty.trim();
    }

    public String getMcnAreacode() {
        return mcnAreacode;
    }

    public void setMcnAreacode(String mcnAreacode) {
        this.mcnAreacode = mcnAreacode == null ? null : mcnAreacode.trim();
    }

    public String getMcnPostalcode() {
        return mcnPostalcode;
    }

    public void setMcnPostalcode(String mcnPostalcode) {
        this.mcnPostalcode = mcnPostalcode == null ? null : mcnPostalcode.trim();
    }

    public String getMcnState() {
        return mcnState;
    }

    public void setMcnState(String mcnState) {
        this.mcnState = mcnState == null ? null : mcnState.trim();
    }

    public Date getMcnCreateTime() {
        return mcnCreateTime;
    }

    public void setMcnCreateTime(Date mcnCreateTime) {
        this.mcnCreateTime = mcnCreateTime;
    }

    public Date getMcnUpdateTime() {
        return mcnUpdateTime;
    }

    public void setMcnUpdateTime(Date mcnUpdateTime) {
        this.mcnUpdateTime = mcnUpdateTime;
    }

    public static Contact.Builder builder() {
        return new Contact.Builder();
    }

    public static class Builder {
        private Contact obj;

        public Builder() {
            this.obj = new Contact();
        }

        public Builder mcnKeyId(String mcnKeyId) {
            obj.setMcnKeyId(mcnKeyId);
            return this;
        }

        public Builder mcnId(Long mcnId) {
            obj.setMcnId(mcnId);
            return this;
        }

        public Builder mcnUserKeyId(String mcnUserKeyId) {
            obj.setMcnUserKeyId(mcnUserKeyId);
            return this;
        }

        public Builder mcnName(String mcnName) {
            obj.setMcnName(mcnName);
            return this;
        }

        public Builder mcnTelephone(String mcnTelephone) {
            obj.setMcnTelephone(mcnTelephone);
            return this;
        }

        public Builder mcnAddress(String mcnAddress) {
            obj.setMcnAddress(mcnAddress);
            return this;
        }

        public Builder mcnSort(Integer mcnSort) {
            obj.setMcnSort(mcnSort);
            return this;
        }

        public Builder mcnProvince(String mcnProvince) {
            obj.setMcnProvince(mcnProvince);
            return this;
        }

        public Builder mcnCity(String mcnCity) {
            obj.setMcnCity(mcnCity);
            return this;
        }

        public Builder mcnCounty(String mcnCounty) {
            obj.setMcnCounty(mcnCounty);
            return this;
        }

        public Builder mcnAreacode(String mcnAreacode) {
            obj.setMcnAreacode(mcnAreacode);
            return this;
        }

        public Builder mcnPostalcode(String mcnPostalcode) {
            obj.setMcnPostalcode(mcnPostalcode);
            return this;
        }

        public Builder mcnState(String mcnState) {
            obj.setMcnState(mcnState);
            return this;
        }

        public Builder mcnCreateTime(Date mcnCreateTime) {
            obj.setMcnCreateTime(mcnCreateTime);
            return this;
        }

        public Builder mcnUpdateTime(Date mcnUpdateTime) {
            obj.setMcnUpdateTime(mcnUpdateTime);
            return this;
        }

        public Contact build() {
            return this.obj;
        }
    }

    public enum Column {
        mcnKeyId("mcn_key_id", "mcnKeyId", "VARCHAR", false),
        mcnId("mcn_id", "mcnId", "BIGINT", false),
        mcnUserKeyId("mcn_user_key_id", "mcnUserKeyId", "VARCHAR", false),
        mcnName("mcn_name", "mcnName", "VARCHAR", false),
        mcnTelephone("mcn_telephone", "mcnTelephone", "VARCHAR", false),
        mcnAddress("mcn_address", "mcnAddress", "VARCHAR", false),
        mcnSort("mcn_sort", "mcnSort", "INTEGER", false),
        mcnProvince("mcn_province", "mcnProvince", "VARCHAR", false),
        mcnCity("mcn_city", "mcnCity", "VARCHAR", false),
        mcnCounty("mcn_county", "mcnCounty", "VARCHAR", false),
        mcnAreacode("mcn_areacode", "mcnAreacode", "VARCHAR", false),
        mcnPostalcode("mcn_postalcode", "mcnPostalcode", "VARCHAR", false),
        mcnState("mcn_state", "mcnState", "VARCHAR", false),
        mcnCreateTime("mcn_create_time", "mcnCreateTime", "TIMESTAMP", false),
        mcnUpdateTime("mcn_update_time", "mcnUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}