package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.OrderItem;
import com.koudaiplus.buy.repository.dao.model.OrderItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderItemMapper {
    long countByExample(OrderItemExample example);

    int deleteByExample(OrderItemExample example);

    int deleteByPrimaryKey(String moiKeyId);

    int insert(OrderItem record);

    int insertSelective(@Param("record") OrderItem record, @Param("selective") OrderItem.Column ... selective);

    OrderItem selectOneByExample(OrderItemExample example);

    OrderItem selectOneByExampleSelective(@Param("example") OrderItemExample example, @Param("selective") OrderItem.Column ... selective);

    List<OrderItem> selectByExampleSelective(@Param("example") OrderItemExample example, @Param("selective") OrderItem.Column ... selective);

    List<OrderItem> selectByExample(OrderItemExample example);

    OrderItem selectByPrimaryKeySelective(@Param("moiKeyId") String moiKeyId, @Param("selective") OrderItem.Column ... selective);

    OrderItem selectByPrimaryKey(String moiKeyId);

    int updateByExampleSelective(@Param("record") OrderItem record, @Param("example") OrderItemExample example, @Param("selective") OrderItem.Column ... selective);

    int updateByExample(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

    int updateByPrimaryKeySelective(@Param("record") OrderItem record, @Param("selective") OrderItem.Column ... selective);

    int updateByPrimaryKey(OrderItem record);

    int batchInsert(@Param("list") List<OrderItem> list);

    int batchInsertSelective(@Param("list") List<OrderItem> list, @Param("selective") OrderItem.Column ... selective);

    int upsert(OrderItem record);

    int upsertByExample(@Param("record") OrderItem record, @Param("example") OrderItemExample example);

    int upsertByExampleSelective(@Param("record") OrderItem record, @Param("example") OrderItemExample example, @Param("selective") OrderItem.Column ... selective);

    int upsertSelective(@Param("record") OrderItem record, @Param("selective") OrderItem.Column ... selective);
}