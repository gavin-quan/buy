package com.koudaiplus.buy.repository.daox.mapper

import org.apache.ibatis.annotations.Param
import java.util.*

interface OrderMapperx {

    fun findRetryablePayOrderIdList(@Param("orderIdList") orderIdList: List<String>, @Param("count") count: Int, @Param("expireTime")expireTime : Date): List<String>

}