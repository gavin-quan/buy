package com.koudaiplus.buy.repository.daox.mapper

import org.apache.ibatis.annotations.Param

interface GoodsStockMapper {
    fun lockStock(@Param("orderItemKeyId")orderItemKeyId: String, @Param("stockId")stockId: String): Int
    fun deductStock(@Param("orderItemKeyId")orderItemKeyId: String): Int
    fun recoverStock(@Param("orderItemKeyId")orderItemKeyId: String, @Param("stockId")stockId: String): Int
    fun pushStock(@Param("skuKeyId") skuKeyId: String, @Param("quantity")quantity: Int, @Param("stockId")stockId: String): Int
}