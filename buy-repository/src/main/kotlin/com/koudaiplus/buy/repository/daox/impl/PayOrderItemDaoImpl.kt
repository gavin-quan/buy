package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.PayOrderItemMapper
import com.koudaiplus.buy.repository.dao.model.PayOrderItem
import com.koudaiplus.buy.repository.dao.model.PayOrderItemExample
import com.koudaiplus.buy.repository.daox.PayOrderItemDao
import com.koudaiplus.buy.repository.daox.model.PayOrderItemDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class PayOrderItemDaoImpl(
       mapper: PayOrderItemMapper
) : DaoSupport<PayOrderItemMapper, PayOrderItemExample, PayOrderItemExample.Criteria, PayOrderItem.Builder, PayOrderItem>(mapper), PayOrderItemDao {

    override fun findListByPayOrderKeyId(payOrderKeyId: String): List<PayOrderItem> {
        return list { mapper, example, criteria ->
            criteria.andMpoiPayOrderKeyIdEqualTo(payOrderKeyId)
            mapper.selectByExample(example)
        }
    }

    override fun create(param: PayOrderItemDaoModel.Create) {
        insert { mapper, builder ->
            val e = builder
                    .mpoiKeyId(param.keyId)
                    .mpoiOrderKeyId(param.orderKeyId)
                    .mpoiPayOrderKeyId(param.payOrderKeyId)
                    .mpoiCreateTime(Date())
                    .build()
            mapper.insert(e)
        }
    }

    override fun getInfoByOrderKeyId(payOrderKeyId: String, orderKeyId: String): PayOrderItem? {
        return get { mapper, example, criteria ->
            criteria.andMpoiPayOrderKeyIdEqualTo(payOrderKeyId)
                    .andMpoiOrderKeyIdEqualTo(orderKeyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getItemInfoByKeyId(payOrderItemKeyId: String): PayOrderItem? {
        return get { mapper, example, criteria ->
            criteria.andMpoiKeyIdEqualTo(payOrderItemKeyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getExample() = PayOrderItemExample()

    override fun getCriteria(example: PayOrderItemExample) = example.createCriteria()

    override fun getBuilder() = PayOrderItem.builder()

    override fun setPage(limit: Limit, example: PayOrderItemExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
