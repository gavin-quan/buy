package com.koudaiplus.buy.repository.daox.model

import java.math.BigDecimal

object OrderItemDaoModel {

    data class Create(
            val keyId: String,
            val orderKeyId: String,
            val shopKeyId: String,
            val goodsKeyId: String,
            val goodsSkuId: String,
            val goodsName: String,
            val goodsPrice: BigDecimal,
            val goodsQuantity: Int,
            val totalPrice: BigDecimal,
            val state: String,
            val stockState: String
    )

    data class StockRecover(
            val keyId: String,
            val stockStateTo: String,
            val stockStateFrom: List<String>
    )

    data class StockRecoverByOrderKeyId (
            val orderKeyId: String,
            val stockStateTo: String,
            val stockStateFrom: List<String>
    )

}