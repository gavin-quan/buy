package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.OrderItemMapper
import com.koudaiplus.buy.repository.dao.model.OrderItem
import com.koudaiplus.buy.repository.dao.model.OrderItemExample
import com.koudaiplus.buy.repository.daox.OrderItemDao
import com.koudaiplus.buy.repository.daox.model.OrderItemDaoModel
import com.play.core.common.ActivityStateEnum
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository;
import java.util.*

@Repository
class OrderItemDaoImpl(
       mapper: OrderItemMapper
) : DaoSupport<OrderItemMapper, OrderItemExample, OrderItemExample.Criteria, OrderItem.Builder, OrderItem>(mapper), OrderItemDao {

    override fun getInfoByKeyId(keyId: String): OrderItem? {
        return get { mapper, example, criteria ->
            criteria.andMoiKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun findListByOrderKeyId(orderKeyId: String): List<OrderItem> {
        return list { mapper, example, criteria ->
            criteria.andMoiOrderKeyIdEqualTo(orderKeyId)
            mapper.selectByExample(example)
        }
    }

    override fun create(param: OrderItemDaoModel.Create) {
        insert { mapper, builder ->
            val e = builder
                    .moiKeyId(param.keyId)
                    .moiOrderKeyId(param.orderKeyId)
                    .moiShopKeyId(param.shopKeyId)
                    .moiGoodsKeyId(param.goodsKeyId)
                    .moiGoodsSkuId(param.goodsSkuId)
                    .moiGoodsPrice(param.goodsPrice)
                    .moiGoodsQuantity(param.goodsQuantity)
                    .moiTotalPrice(param.totalPrice)
                    .moiStockState(param.stockState)
                    .moiState(param.state)
                    .moiCreateTime(Date())
                    .moiUpdateTime(Date())
                    .build()
            mapper.insert(e)
        }
    }

    override fun tryStockRecover(param: OrderItemDaoModel.StockRecover) {
        execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            val e = builder
                    .moiStockState(param.stockStateTo)
                    .build()
            criteria.andMoiKeyIdEqualTo(param.keyId).andMoiStockStateIn(param.stockStateFrom)
            mapper.updateByExampleSelective(e, example, OrderItem.Column.moiStockState)
        }
    }

    override fun tryStockRecover(param: OrderItemDaoModel.StockRecoverByOrderKeyId) {
        execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            val e = builder
                    .moiStockState(param.stockStateTo)
                    .build()
            criteria.andMoiOrderKeyIdEqualTo(param.orderKeyId).andMoiStockStateIn(param.stockStateFrom)
            mapper.updateByExampleSelective(e, example, OrderItem.Column.moiStockState)
        }
    }

    override fun findCancelStockOrderItemList(): List<OrderItem> {
        return list { mapper, example, criteria ->
            criteria.andMoiStockStateEqualTo(ActivityStateEnum.CANCEL.name)
            mapper.selectByExample(example)
        }
    }

    override fun getExample() = OrderItemExample()

    override fun getCriteria(example: OrderItemExample) = example.createCriteria()

    override fun getBuilder() = OrderItem.builder()

    override fun setPage(limit: Limit, example: OrderItemExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
