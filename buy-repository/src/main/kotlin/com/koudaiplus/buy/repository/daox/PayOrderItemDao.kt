package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.PayOrderItem
import com.koudaiplus.buy.repository.daox.model.PayOrderItemDaoModel

interface PayOrderItemDao {

    fun findListByPayOrderKeyId(payOrderKeyId: String): List<PayOrderItem>

    fun create(param: PayOrderItemDaoModel.Create)

    fun getInfoByOrderKeyId(payOrderKeyId: String, orderKeyId: String): PayOrderItem?

    fun getItemInfoByKeyId(payOrderItemKeyId: String): PayOrderItem?

}