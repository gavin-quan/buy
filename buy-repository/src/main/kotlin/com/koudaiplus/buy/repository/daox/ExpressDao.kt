package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.Express
import com.koudaiplus.buy.repository.daox.model.ExpressDaoModel

interface ExpressDao {

    fun create(param: ExpressDaoModel.Create)

    fun update(param: ExpressDaoModel.Start)

    fun getInfoByKeyId(keyId: String) : Express?

    fun update(param: ExpressDaoModel.Confirm)

}