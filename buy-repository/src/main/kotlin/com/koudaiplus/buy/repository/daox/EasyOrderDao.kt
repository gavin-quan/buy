package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.EasyOrder
import com.koudaiplus.buy.repository.daox.model.EasyOrderDaoModel

interface EasyOrderDao {

    fun getInfoByKeyId(keyId: String): EasyOrder?

    fun create(param: EasyOrderDaoModel.Create)

    fun finishPay(param: EasyOrderDaoModel.FinishPay)

    fun tryCloseByKeyId(param: EasyOrderDaoModel.UpdateState)

}