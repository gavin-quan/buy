package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.mapper.GoodsStockKeepingUnitMapper
import com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnit
import com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnitExample
import com.play.core.common.Dao

interface GoodsStockKeepingUnitDao: Dao<GoodsStockKeepingUnitMapper, GoodsStockKeepingUnitExample, GoodsStockKeepingUnitExample.Criteria, GoodsStockKeepingUnit.Builder, GoodsStockKeepingUnit> {
    fun getInfoByKeyId(keyId: String): GoodsStockKeepingUnit?
    fun findSkuListByKeyId(goodsKeyId: String): List<GoodsStockKeepingUnit>
}