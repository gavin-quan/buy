package com.koudaiplus.buy.repository.daox.model

object ExpressDaoModel {

    data class Create (
        val keyId: String,
        val shopKeyId: String,
        val receiverUserId: String,
        val receiverName: String,
        val receiverTelephone: String,
        val receiverAddress: String,
        val title: String,
        val state: String
    )

    data class Start (
        val keyId: String,
        val companyCode: String,
        val serialNo: String,
        val consignorName: String,
        val consignorTelephone: String,
        val consignorAddress: String,
        val toState: String,
        val fromState: List<String>
    )

    data class Confirm (
        val keyId: String,
        val toState: String,
        val fromState: List<String>
    )

}