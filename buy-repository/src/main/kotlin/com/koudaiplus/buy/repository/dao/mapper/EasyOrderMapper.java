package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.EasyOrder;
import com.koudaiplus.buy.repository.dao.model.EasyOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EasyOrderMapper {
    long countByExample(EasyOrderExample example);

    int deleteByExample(EasyOrderExample example);

    int deleteByPrimaryKey(String eorKeyId);

    int insert(EasyOrder record);

    int insertSelective(@Param("record") EasyOrder record, @Param("selective") EasyOrder.Column ... selective);

    EasyOrder selectOneByExample(EasyOrderExample example);

    EasyOrder selectOneByExampleSelective(@Param("example") EasyOrderExample example, @Param("selective") EasyOrder.Column ... selective);

    List<EasyOrder> selectByExampleSelective(@Param("example") EasyOrderExample example, @Param("selective") EasyOrder.Column ... selective);

    List<EasyOrder> selectByExample(EasyOrderExample example);

    EasyOrder selectByPrimaryKeySelective(@Param("eorKeyId") String eorKeyId, @Param("selective") EasyOrder.Column ... selective);

    EasyOrder selectByPrimaryKey(String eorKeyId);

    int updateByExampleSelective(@Param("record") EasyOrder record, @Param("example") EasyOrderExample example, @Param("selective") EasyOrder.Column ... selective);

    int updateByExample(@Param("record") EasyOrder record, @Param("example") EasyOrderExample example);

    int updateByPrimaryKeySelective(@Param("record") EasyOrder record, @Param("selective") EasyOrder.Column ... selective);

    int updateByPrimaryKey(EasyOrder record);

    int batchInsert(@Param("list") List<EasyOrder> list);

    int batchInsertSelective(@Param("list") List<EasyOrder> list, @Param("selective") EasyOrder.Column ... selective);

    int upsert(EasyOrder record);

    int upsertByExample(@Param("record") EasyOrder record, @Param("example") EasyOrderExample example);

    int upsertByExampleSelective(@Param("record") EasyOrder record, @Param("example") EasyOrderExample example, @Param("selective") EasyOrder.Column ... selective);

    int upsertSelective(@Param("record") EasyOrder record, @Param("selective") EasyOrder.Column ... selective);
}