package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.mapper.OrderItemMapper
import com.koudaiplus.buy.repository.dao.model.OrderItem
import com.koudaiplus.buy.repository.dao.model.OrderItemExample
import com.koudaiplus.buy.repository.daox.model.OrderItemDaoModel
import com.play.core.common.Dao

interface OrderItemDao : Dao<OrderItemMapper, OrderItemExample, OrderItemExample.Criteria, OrderItem.Builder, OrderItem> {

    fun getInfoByKeyId(keyId: String): OrderItem?

    fun findListByOrderKeyId(orderKeyId: String): List<OrderItem>

    fun create(param: OrderItemDaoModel.Create)

    fun tryStockRecover(param: OrderItemDaoModel.StockRecover)

    fun tryStockRecover(param: OrderItemDaoModel.StockRecoverByOrderKeyId)

    fun findCancelStockOrderItemList(): List<OrderItem>

}