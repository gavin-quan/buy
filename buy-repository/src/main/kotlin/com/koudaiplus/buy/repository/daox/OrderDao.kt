package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.mapper.OrderMapper
import com.koudaiplus.buy.repository.dao.model.Order
import com.koudaiplus.buy.repository.dao.model.OrderExample
import com.koudaiplus.buy.repository.daox.model.OrderDaoModel
import com.play.core.common.Dao

interface OrderDao: Dao<OrderMapper, OrderExample, OrderExample.Criteria, Order.Builder, Order> {

    fun getInfoByKeyId(keyId: String): Order?

    fun create(param: OrderDaoModel.Create)

    fun finishPay(param: OrderDaoModel.FinishPay)

    fun tryCloseByKeyId(param: OrderDaoModel.UpdateState)

    fun findListByBatchId(batchId: String): List<Order>

    fun findListByPayOrderId(payOrderId: String): List<Order>

}