package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.Contact;
import com.koudaiplus.buy.repository.dao.model.ContactExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ContactMapper {
    long countByExample(ContactExample example);

    int deleteByExample(ContactExample example);

    int deleteByPrimaryKey(String mcnKeyId);

    int insert(Contact record);

    int insertSelective(@Param("record") Contact record, @Param("selective") Contact.Column ... selective);

    Contact selectOneByExample(ContactExample example);

    Contact selectOneByExampleSelective(@Param("example") ContactExample example, @Param("selective") Contact.Column ... selective);

    List<Contact> selectByExampleSelective(@Param("example") ContactExample example, @Param("selective") Contact.Column ... selective);

    List<Contact> selectByExample(ContactExample example);

    Contact selectByPrimaryKeySelective(@Param("mcnKeyId") String mcnKeyId, @Param("selective") Contact.Column ... selective);

    Contact selectByPrimaryKey(String mcnKeyId);

    int updateByExampleSelective(@Param("record") Contact record, @Param("example") ContactExample example, @Param("selective") Contact.Column ... selective);

    int updateByExample(@Param("record") Contact record, @Param("example") ContactExample example);

    int updateByPrimaryKeySelective(@Param("record") Contact record, @Param("selective") Contact.Column ... selective);

    int updateByPrimaryKey(Contact record);

    int batchInsert(@Param("list") List<Contact> list);

    int batchInsertSelective(@Param("list") List<Contact> list, @Param("selective") Contact.Column ... selective);

    int upsert(Contact record);

    int upsertByExample(@Param("record") Contact record, @Param("example") ContactExample example);

    int upsertByExampleSelective(@Param("record") Contact record, @Param("example") ContactExample example, @Param("selective") Contact.Column ... selective);

    int upsertSelective(@Param("record") Contact record, @Param("selective") Contact.Column ... selective);
}