package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class RepeatPayOrder {
    private String rpoKeyId;

    private Long rpoId;

    private String rpoPayOrderKeyId;

    private String rpoPayOrderItemId;

    private String rpoOrderKeyId;

    private BigDecimal rpoAmount;

    private Date rpoRefundFinishTime;

    private String rpoState;

    private Date rpoCreateTime;

    private Date rpoUpdateTime;

    public String getRpoKeyId() {
        return rpoKeyId;
    }

    public void setRpoKeyId(String rpoKeyId) {
        this.rpoKeyId = rpoKeyId == null ? null : rpoKeyId.trim();
    }

    public Long getRpoId() {
        return rpoId;
    }

    public void setRpoId(Long rpoId) {
        this.rpoId = rpoId;
    }

    public String getRpoPayOrderKeyId() {
        return rpoPayOrderKeyId;
    }

    public void setRpoPayOrderKeyId(String rpoPayOrderKeyId) {
        this.rpoPayOrderKeyId = rpoPayOrderKeyId == null ? null : rpoPayOrderKeyId.trim();
    }

    public String getRpoPayOrderItemId() {
        return rpoPayOrderItemId;
    }

    public void setRpoPayOrderItemId(String rpoPayOrderItemId) {
        this.rpoPayOrderItemId = rpoPayOrderItemId == null ? null : rpoPayOrderItemId.trim();
    }

    public String getRpoOrderKeyId() {
        return rpoOrderKeyId;
    }

    public void setRpoOrderKeyId(String rpoOrderKeyId) {
        this.rpoOrderKeyId = rpoOrderKeyId == null ? null : rpoOrderKeyId.trim();
    }

    public BigDecimal getRpoAmount() {
        return rpoAmount;
    }

    public void setRpoAmount(BigDecimal rpoAmount) {
        this.rpoAmount = rpoAmount;
    }

    public Date getRpoRefundFinishTime() {
        return rpoRefundFinishTime;
    }

    public void setRpoRefundFinishTime(Date rpoRefundFinishTime) {
        this.rpoRefundFinishTime = rpoRefundFinishTime;
    }

    public String getRpoState() {
        return rpoState;
    }

    public void setRpoState(String rpoState) {
        this.rpoState = rpoState == null ? null : rpoState.trim();
    }

    public Date getRpoCreateTime() {
        return rpoCreateTime;
    }

    public void setRpoCreateTime(Date rpoCreateTime) {
        this.rpoCreateTime = rpoCreateTime;
    }

    public Date getRpoUpdateTime() {
        return rpoUpdateTime;
    }

    public void setRpoUpdateTime(Date rpoUpdateTime) {
        this.rpoUpdateTime = rpoUpdateTime;
    }

    public static RepeatPayOrder.Builder builder() {
        return new RepeatPayOrder.Builder();
    }

    public static class Builder {
        private RepeatPayOrder obj;

        public Builder() {
            this.obj = new RepeatPayOrder();
        }

        public Builder rpoKeyId(String rpoKeyId) {
            obj.setRpoKeyId(rpoKeyId);
            return this;
        }

        public Builder rpoId(Long rpoId) {
            obj.setRpoId(rpoId);
            return this;
        }

        public Builder rpoPayOrderKeyId(String rpoPayOrderKeyId) {
            obj.setRpoPayOrderKeyId(rpoPayOrderKeyId);
            return this;
        }

        public Builder rpoPayOrderItemId(String rpoPayOrderItemId) {
            obj.setRpoPayOrderItemId(rpoPayOrderItemId);
            return this;
        }

        public Builder rpoOrderKeyId(String rpoOrderKeyId) {
            obj.setRpoOrderKeyId(rpoOrderKeyId);
            return this;
        }

        public Builder rpoAmount(BigDecimal rpoAmount) {
            obj.setRpoAmount(rpoAmount);
            return this;
        }

        public Builder rpoRefundFinishTime(Date rpoRefundFinishTime) {
            obj.setRpoRefundFinishTime(rpoRefundFinishTime);
            return this;
        }

        public Builder rpoState(String rpoState) {
            obj.setRpoState(rpoState);
            return this;
        }

        public Builder rpoCreateTime(Date rpoCreateTime) {
            obj.setRpoCreateTime(rpoCreateTime);
            return this;
        }

        public Builder rpoUpdateTime(Date rpoUpdateTime) {
            obj.setRpoUpdateTime(rpoUpdateTime);
            return this;
        }

        public RepeatPayOrder build() {
            return this.obj;
        }
    }

    public enum Column {
        rpoKeyId("rpo_key_id", "rpoKeyId", "VARCHAR", false),
        rpoId("rpo_id", "rpoId", "BIGINT", false),
        rpoPayOrderKeyId("rpo_pay_order_key_id", "rpoPayOrderKeyId", "VARCHAR", false),
        rpoPayOrderItemId("rpo_pay_order_item_id", "rpoPayOrderItemId", "VARCHAR", false),
        rpoOrderKeyId("rpo_order_key_id", "rpoOrderKeyId", "VARCHAR", false),
        rpoAmount("rpo_amount", "rpoAmount", "DECIMAL", false),
        rpoRefundFinishTime("rpo_refund_finish_time", "rpoRefundFinishTime", "TIMESTAMP", false),
        rpoState("rpo_state", "rpoState", "VARCHAR", false),
        rpoCreateTime("rpo_create_time", "rpoCreateTime", "TIMESTAMP", false),
        rpoUpdateTime("rpo_update_time", "rpoUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}