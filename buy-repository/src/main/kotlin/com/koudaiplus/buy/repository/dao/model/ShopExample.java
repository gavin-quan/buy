package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShopExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public ShopExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public ShopExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public ShopExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public ShopExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public ShopExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public ShopExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        ShopExample example = new ShopExample();
        return example.createCriteria();
    }

    public ShopExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public ShopExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMspKeyIdIsNull() {
            addCriterion("msp_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdIsNotNull() {
            addCriterion("msp_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdEqualTo(String value) {
            addCriterion("msp_key_id =", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspKeyIdNotEqualTo(String value) {
            addCriterion("msp_key_id <>", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspKeyIdGreaterThan(String value) {
            addCriterion("msp_key_id >", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("msp_key_id >=", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspKeyIdLessThan(String value) {
            addCriterion("msp_key_id <", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspKeyIdLessThanOrEqualTo(String value) {
            addCriterion("msp_key_id <=", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspKeyIdLike(String value) {
            addCriterion("msp_key_id like", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdNotLike(String value) {
            addCriterion("msp_key_id not like", value, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdIn(List<String> values) {
            addCriterion("msp_key_id in", values, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdNotIn(List<String> values) {
            addCriterion("msp_key_id not in", values, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdBetween(String value1, String value2) {
            addCriterion("msp_key_id between", value1, value2, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspKeyIdNotBetween(String value1, String value2) {
            addCriterion("msp_key_id not between", value1, value2, "mspKeyId");
            return (Criteria) this;
        }

        public Criteria andMspIdIsNull() {
            addCriterion("msp_id is null");
            return (Criteria) this;
        }

        public Criteria andMspIdIsNotNull() {
            addCriterion("msp_id is not null");
            return (Criteria) this;
        }

        public Criteria andMspIdEqualTo(Long value) {
            addCriterion("msp_id =", value, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspIdNotEqualTo(Long value) {
            addCriterion("msp_id <>", value, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspIdGreaterThan(Long value) {
            addCriterion("msp_id >", value, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspIdGreaterThanOrEqualTo(Long value) {
            addCriterion("msp_id >=", value, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspIdLessThan(Long value) {
            addCriterion("msp_id <", value, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspIdLessThanOrEqualTo(Long value) {
            addCriterion("msp_id <=", value, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspIdIn(List<Long> values) {
            addCriterion("msp_id in", values, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdNotIn(List<Long> values) {
            addCriterion("msp_id not in", values, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdBetween(Long value1, Long value2) {
            addCriterion("msp_id between", value1, value2, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspIdNotBetween(Long value1, Long value2) {
            addCriterion("msp_id not between", value1, value2, "mspId");
            return (Criteria) this;
        }

        public Criteria andMspNameIsNull() {
            addCriterion("msp_name is null");
            return (Criteria) this;
        }

        public Criteria andMspNameIsNotNull() {
            addCriterion("msp_name is not null");
            return (Criteria) this;
        }

        public Criteria andMspNameEqualTo(String value) {
            addCriterion("msp_name =", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_name = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspNameNotEqualTo(String value) {
            addCriterion("msp_name <>", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_name <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspNameGreaterThan(String value) {
            addCriterion("msp_name >", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_name > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspNameGreaterThanOrEqualTo(String value) {
            addCriterion("msp_name >=", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_name >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspNameLessThan(String value) {
            addCriterion("msp_name <", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_name < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspNameLessThanOrEqualTo(String value) {
            addCriterion("msp_name <=", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_name <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspNameLike(String value) {
            addCriterion("msp_name like", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameNotLike(String value) {
            addCriterion("msp_name not like", value, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameIn(List<String> values) {
            addCriterion("msp_name in", values, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameNotIn(List<String> values) {
            addCriterion("msp_name not in", values, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameBetween(String value1, String value2) {
            addCriterion("msp_name between", value1, value2, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspNameNotBetween(String value1, String value2) {
            addCriterion("msp_name not between", value1, value2, "mspName");
            return (Criteria) this;
        }

        public Criteria andMspLogoIsNull() {
            addCriterion("msp_logo is null");
            return (Criteria) this;
        }

        public Criteria andMspLogoIsNotNull() {
            addCriterion("msp_logo is not null");
            return (Criteria) this;
        }

        public Criteria andMspLogoEqualTo(String value) {
            addCriterion("msp_logo =", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_logo = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspLogoNotEqualTo(String value) {
            addCriterion("msp_logo <>", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_logo <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspLogoGreaterThan(String value) {
            addCriterion("msp_logo >", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_logo > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspLogoGreaterThanOrEqualTo(String value) {
            addCriterion("msp_logo >=", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_logo >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspLogoLessThan(String value) {
            addCriterion("msp_logo <", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_logo < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspLogoLessThanOrEqualTo(String value) {
            addCriterion("msp_logo <=", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_logo <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspLogoLike(String value) {
            addCriterion("msp_logo like", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoNotLike(String value) {
            addCriterion("msp_logo not like", value, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoIn(List<String> values) {
            addCriterion("msp_logo in", values, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoNotIn(List<String> values) {
            addCriterion("msp_logo not in", values, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoBetween(String value1, String value2) {
            addCriterion("msp_logo between", value1, value2, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspLogoNotBetween(String value1, String value2) {
            addCriterion("msp_logo not between", value1, value2, "mspLogo");
            return (Criteria) this;
        }

        public Criteria andMspCoverIsNull() {
            addCriterion("msp_cover is null");
            return (Criteria) this;
        }

        public Criteria andMspCoverIsNotNull() {
            addCriterion("msp_cover is not null");
            return (Criteria) this;
        }

        public Criteria andMspCoverEqualTo(String value) {
            addCriterion("msp_cover =", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_cover = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCoverNotEqualTo(String value) {
            addCriterion("msp_cover <>", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_cover <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCoverGreaterThan(String value) {
            addCriterion("msp_cover >", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_cover > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCoverGreaterThanOrEqualTo(String value) {
            addCriterion("msp_cover >=", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_cover >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCoverLessThan(String value) {
            addCriterion("msp_cover <", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_cover < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCoverLessThanOrEqualTo(String value) {
            addCriterion("msp_cover <=", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_cover <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCoverLike(String value) {
            addCriterion("msp_cover like", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverNotLike(String value) {
            addCriterion("msp_cover not like", value, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverIn(List<String> values) {
            addCriterion("msp_cover in", values, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverNotIn(List<String> values) {
            addCriterion("msp_cover not in", values, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverBetween(String value1, String value2) {
            addCriterion("msp_cover between", value1, value2, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspCoverNotBetween(String value1, String value2) {
            addCriterion("msp_cover not between", value1, value2, "mspCover");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdIsNull() {
            addCriterion("msp_owner_id is null");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdIsNotNull() {
            addCriterion("msp_owner_id is not null");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdEqualTo(String value) {
            addCriterion("msp_owner_id =", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_owner_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdNotEqualTo(String value) {
            addCriterion("msp_owner_id <>", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_owner_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdGreaterThan(String value) {
            addCriterion("msp_owner_id >", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_owner_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdGreaterThanOrEqualTo(String value) {
            addCriterion("msp_owner_id >=", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_owner_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdLessThan(String value) {
            addCriterion("msp_owner_id <", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_owner_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdLessThanOrEqualTo(String value) {
            addCriterion("msp_owner_id <=", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_owner_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdLike(String value) {
            addCriterion("msp_owner_id like", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdNotLike(String value) {
            addCriterion("msp_owner_id not like", value, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdIn(List<String> values) {
            addCriterion("msp_owner_id in", values, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdNotIn(List<String> values) {
            addCriterion("msp_owner_id not in", values, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdBetween(String value1, String value2) {
            addCriterion("msp_owner_id between", value1, value2, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspOwnerIdNotBetween(String value1, String value2) {
            addCriterion("msp_owner_id not between", value1, value2, "mspOwnerId");
            return (Criteria) this;
        }

        public Criteria andMspStateIsNull() {
            addCriterion("msp_state is null");
            return (Criteria) this;
        }

        public Criteria andMspStateIsNotNull() {
            addCriterion("msp_state is not null");
            return (Criteria) this;
        }

        public Criteria andMspStateEqualTo(String value) {
            addCriterion("msp_state =", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspStateNotEqualTo(String value) {
            addCriterion("msp_state <>", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspStateGreaterThan(String value) {
            addCriterion("msp_state >", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspStateGreaterThanOrEqualTo(String value) {
            addCriterion("msp_state >=", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspStateLessThan(String value) {
            addCriterion("msp_state <", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspStateLessThanOrEqualTo(String value) {
            addCriterion("msp_state <=", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspStateLike(String value) {
            addCriterion("msp_state like", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateNotLike(String value) {
            addCriterion("msp_state not like", value, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateIn(List<String> values) {
            addCriterion("msp_state in", values, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateNotIn(List<String> values) {
            addCriterion("msp_state not in", values, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateBetween(String value1, String value2) {
            addCriterion("msp_state between", value1, value2, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspStateNotBetween(String value1, String value2) {
            addCriterion("msp_state not between", value1, value2, "mspState");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeIsNull() {
            addCriterion("msp_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeIsNotNull() {
            addCriterion("msp_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeEqualTo(Date value) {
            addCriterion("msp_create_time =", value, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeNotEqualTo(Date value) {
            addCriterion("msp_create_time <>", value, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeGreaterThan(Date value) {
            addCriterion("msp_create_time >", value, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("msp_create_time >=", value, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeLessThan(Date value) {
            addCriterion("msp_create_time <", value, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("msp_create_time <=", value, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeIn(List<Date> values) {
            addCriterion("msp_create_time in", values, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeNotIn(List<Date> values) {
            addCriterion("msp_create_time not in", values, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeBetween(Date value1, Date value2) {
            addCriterion("msp_create_time between", value1, value2, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("msp_create_time not between", value1, value2, "mspCreateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeIsNull() {
            addCriterion("msp_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeIsNotNull() {
            addCriterion("msp_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeEqualTo(Date value) {
            addCriterion("msp_update_time =", value, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeNotEqualTo(Date value) {
            addCriterion("msp_update_time <>", value, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeNotEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeGreaterThan(Date value) {
            addCriterion("msp_update_time >", value, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeGreaterThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("msp_update_time >=", value, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeGreaterThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeLessThan(Date value) {
            addCriterion("msp_update_time <", value, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeLessThanColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("msp_update_time <=", value, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeLessThanOrEqualToColumn(Shop.Column column) {
            addCriterion(new StringBuilder("msp_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeIn(List<Date> values) {
            addCriterion("msp_update_time in", values, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeNotIn(List<Date> values) {
            addCriterion("msp_update_time not in", values, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("msp_update_time between", value1, value2, "mspUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMspUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("msp_update_time not between", value1, value2, "mspUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private ShopExample example;

        protected Criteria(ShopExample example) {
            super();
            this.example = example;
        }

        public ShopExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.ShopExample example);
    }
}