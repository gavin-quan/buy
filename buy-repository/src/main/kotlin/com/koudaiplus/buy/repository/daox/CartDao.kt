package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.mapper.CartMapper
import com.koudaiplus.buy.repository.dao.model.Cart
import com.koudaiplus.buy.repository.dao.model.CartExample
import com.play.core.common.Dao

interface CartDao: Dao<CartMapper, CartExample, CartExample.Criteria, Cart.Builder, Cart> {

}