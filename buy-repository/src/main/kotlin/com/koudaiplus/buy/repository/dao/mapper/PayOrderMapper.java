package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.PayOrder;
import com.koudaiplus.buy.repository.dao.model.PayOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PayOrderMapper {
    long countByExample(PayOrderExample example);

    int deleteByExample(PayOrderExample example);

    int deleteByPrimaryKey(String mpoKeyId);

    int insert(PayOrder record);

    int insertSelective(@Param("record") PayOrder record, @Param("selective") PayOrder.Column ... selective);

    PayOrder selectOneByExample(PayOrderExample example);

    PayOrder selectOneByExampleSelective(@Param("example") PayOrderExample example, @Param("selective") PayOrder.Column ... selective);

    List<PayOrder> selectByExampleSelective(@Param("example") PayOrderExample example, @Param("selective") PayOrder.Column ... selective);

    List<PayOrder> selectByExample(PayOrderExample example);

    PayOrder selectByPrimaryKeySelective(@Param("mpoKeyId") String mpoKeyId, @Param("selective") PayOrder.Column ... selective);

    PayOrder selectByPrimaryKey(String mpoKeyId);

    int updateByExampleSelective(@Param("record") PayOrder record, @Param("example") PayOrderExample example, @Param("selective") PayOrder.Column ... selective);

    int updateByExample(@Param("record") PayOrder record, @Param("example") PayOrderExample example);

    int updateByPrimaryKeySelective(@Param("record") PayOrder record, @Param("selective") PayOrder.Column ... selective);

    int updateByPrimaryKey(PayOrder record);

    int batchInsert(@Param("list") List<PayOrder> list);

    int batchInsertSelective(@Param("list") List<PayOrder> list, @Param("selective") PayOrder.Column ... selective);

    int upsert(PayOrder record);

    int upsertByExample(@Param("record") PayOrder record, @Param("example") PayOrderExample example);

    int upsertByExampleSelective(@Param("record") PayOrder record, @Param("example") PayOrderExample example, @Param("selective") PayOrder.Column ... selective);

    int upsertSelective(@Param("record") PayOrder record, @Param("selective") PayOrder.Column ... selective);
}