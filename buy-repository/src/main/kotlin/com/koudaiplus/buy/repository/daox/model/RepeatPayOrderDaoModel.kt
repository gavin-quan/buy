package com.koudaiplus.buy.repository.daox.model

import java.math.BigDecimal

object RepeatPayOrderDaoModel {

    data class Create(
        val keyId: String,
        val orderKeyId: String,
        val payOrderItemKeyId: String,
        val amount: BigDecimal,
        val payOrderKeyId: String,
        val state: String
    )

    data class QueryByPayOrderItemKeyId(
        val payOrderItemKeyId: String,
        val state: List<String>
    )

    data class SetStatus(
        val keyId: String,
        val toState: String,
        val fromState: List<String>
    )

    data class QueryByOrderId(
        val payOrderKeyId: String,
        val orderKeyId: String
    )

}