package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class RefundOrder {
    private String mroKeyId;

    private Long mroId;

    private String mroShopKeyId;

    private String mroOrderKeyId;

    private String mroOrderItemKeyId;

    private String mroPayOrderKeyId;

    private String mroExpressId;

    private String mroCreatedBy;

    private BigDecimal mroAmount;

    private Date mroFinishTime;

    private String mroState;

    private Date mroCreateTime;

    private Date mroUpdateTime;

    public String getMroKeyId() {
        return mroKeyId;
    }

    public void setMroKeyId(String mroKeyId) {
        this.mroKeyId = mroKeyId == null ? null : mroKeyId.trim();
    }

    public Long getMroId() {
        return mroId;
    }

    public void setMroId(Long mroId) {
        this.mroId = mroId;
    }

    public String getMroShopKeyId() {
        return mroShopKeyId;
    }

    public void setMroShopKeyId(String mroShopKeyId) {
        this.mroShopKeyId = mroShopKeyId == null ? null : mroShopKeyId.trim();
    }

    public String getMroOrderKeyId() {
        return mroOrderKeyId;
    }

    public void setMroOrderKeyId(String mroOrderKeyId) {
        this.mroOrderKeyId = mroOrderKeyId == null ? null : mroOrderKeyId.trim();
    }

    public String getMroOrderItemKeyId() {
        return mroOrderItemKeyId;
    }

    public void setMroOrderItemKeyId(String mroOrderItemKeyId) {
        this.mroOrderItemKeyId = mroOrderItemKeyId == null ? null : mroOrderItemKeyId.trim();
    }

    public String getMroPayOrderKeyId() {
        return mroPayOrderKeyId;
    }

    public void setMroPayOrderKeyId(String mroPayOrderKeyId) {
        this.mroPayOrderKeyId = mroPayOrderKeyId == null ? null : mroPayOrderKeyId.trim();
    }

    public String getMroExpressId() {
        return mroExpressId;
    }

    public void setMroExpressId(String mroExpressId) {
        this.mroExpressId = mroExpressId == null ? null : mroExpressId.trim();
    }

    public String getMroCreatedBy() {
        return mroCreatedBy;
    }

    public void setMroCreatedBy(String mroCreatedBy) {
        this.mroCreatedBy = mroCreatedBy == null ? null : mroCreatedBy.trim();
    }

    public BigDecimal getMroAmount() {
        return mroAmount;
    }

    public void setMroAmount(BigDecimal mroAmount) {
        this.mroAmount = mroAmount;
    }

    public Date getMroFinishTime() {
        return mroFinishTime;
    }

    public void setMroFinishTime(Date mroFinishTime) {
        this.mroFinishTime = mroFinishTime;
    }

    public String getMroState() {
        return mroState;
    }

    public void setMroState(String mroState) {
        this.mroState = mroState == null ? null : mroState.trim();
    }

    public Date getMroCreateTime() {
        return mroCreateTime;
    }

    public void setMroCreateTime(Date mroCreateTime) {
        this.mroCreateTime = mroCreateTime;
    }

    public Date getMroUpdateTime() {
        return mroUpdateTime;
    }

    public void setMroUpdateTime(Date mroUpdateTime) {
        this.mroUpdateTime = mroUpdateTime;
    }

    public static RefundOrder.Builder builder() {
        return new RefundOrder.Builder();
    }

    public static class Builder {
        private RefundOrder obj;

        public Builder() {
            this.obj = new RefundOrder();
        }

        public Builder mroKeyId(String mroKeyId) {
            obj.setMroKeyId(mroKeyId);
            return this;
        }

        public Builder mroId(Long mroId) {
            obj.setMroId(mroId);
            return this;
        }

        public Builder mroShopKeyId(String mroShopKeyId) {
            obj.setMroShopKeyId(mroShopKeyId);
            return this;
        }

        public Builder mroOrderKeyId(String mroOrderKeyId) {
            obj.setMroOrderKeyId(mroOrderKeyId);
            return this;
        }

        public Builder mroOrderItemKeyId(String mroOrderItemKeyId) {
            obj.setMroOrderItemKeyId(mroOrderItemKeyId);
            return this;
        }

        public Builder mroPayOrderKeyId(String mroPayOrderKeyId) {
            obj.setMroPayOrderKeyId(mroPayOrderKeyId);
            return this;
        }

        public Builder mroExpressId(String mroExpressId) {
            obj.setMroExpressId(mroExpressId);
            return this;
        }

        public Builder mroCreatedBy(String mroCreatedBy) {
            obj.setMroCreatedBy(mroCreatedBy);
            return this;
        }

        public Builder mroAmount(BigDecimal mroAmount) {
            obj.setMroAmount(mroAmount);
            return this;
        }

        public Builder mroFinishTime(Date mroFinishTime) {
            obj.setMroFinishTime(mroFinishTime);
            return this;
        }

        public Builder mroState(String mroState) {
            obj.setMroState(mroState);
            return this;
        }

        public Builder mroCreateTime(Date mroCreateTime) {
            obj.setMroCreateTime(mroCreateTime);
            return this;
        }

        public Builder mroUpdateTime(Date mroUpdateTime) {
            obj.setMroUpdateTime(mroUpdateTime);
            return this;
        }

        public RefundOrder build() {
            return this.obj;
        }
    }

    public enum Column {
        mroKeyId("mro_key_id", "mroKeyId", "VARCHAR", false),
        mroId("mro_id", "mroId", "BIGINT", false),
        mroShopKeyId("mro_shop_key_id", "mroShopKeyId", "VARCHAR", false),
        mroOrderKeyId("mro_order_key_id", "mroOrderKeyId", "VARCHAR", false),
        mroOrderItemKeyId("mro_order_item_key_id", "mroOrderItemKeyId", "VARCHAR", false),
        mroPayOrderKeyId("mro_pay_order_key_id", "mroPayOrderKeyId", "VARCHAR", false),
        mroExpressId("mro_express_id", "mroExpressId", "VARCHAR", false),
        mroCreatedBy("mro_created_by", "mroCreatedBy", "VARCHAR", false),
        mroAmount("mro_amount", "mroAmount", "DECIMAL", false),
        mroFinishTime("mro_finish_time", "mroFinishTime", "TIMESTAMP", false),
        mroState("mro_state", "mroState", "VARCHAR", false),
        mroCreateTime("mro_create_time", "mroCreateTime", "TIMESTAMP", false),
        mroUpdateTime("mro_update_time", "mroUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}