package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.PayOrderItem;
import com.koudaiplus.buy.repository.dao.model.PayOrderItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PayOrderItemMapper {
    long countByExample(PayOrderItemExample example);

    int deleteByExample(PayOrderItemExample example);

    int deleteByPrimaryKey(String mpoiKeyId);

    int insert(PayOrderItem record);

    int insertSelective(@Param("record") PayOrderItem record, @Param("selective") PayOrderItem.Column ... selective);

    PayOrderItem selectOneByExample(PayOrderItemExample example);

    PayOrderItem selectOneByExampleSelective(@Param("example") PayOrderItemExample example, @Param("selective") PayOrderItem.Column ... selective);

    List<PayOrderItem> selectByExampleSelective(@Param("example") PayOrderItemExample example, @Param("selective") PayOrderItem.Column ... selective);

    List<PayOrderItem> selectByExample(PayOrderItemExample example);

    PayOrderItem selectByPrimaryKeySelective(@Param("mpoiKeyId") String mpoiKeyId, @Param("selective") PayOrderItem.Column ... selective);

    PayOrderItem selectByPrimaryKey(String mpoiKeyId);

    int updateByExampleSelective(@Param("record") PayOrderItem record, @Param("example") PayOrderItemExample example, @Param("selective") PayOrderItem.Column ... selective);

    int updateByExample(@Param("record") PayOrderItem record, @Param("example") PayOrderItemExample example);

    int updateByPrimaryKeySelective(@Param("record") PayOrderItem record, @Param("selective") PayOrderItem.Column ... selective);

    int updateByPrimaryKey(PayOrderItem record);

    int batchInsert(@Param("list") List<PayOrderItem> list);

    int batchInsertSelective(@Param("list") List<PayOrderItem> list, @Param("selective") PayOrderItem.Column ... selective);

    int upsert(PayOrderItem record);

    int upsertByExample(@Param("record") PayOrderItem record, @Param("example") PayOrderItemExample example);

    int upsertByExampleSelective(@Param("record") PayOrderItem record, @Param("example") PayOrderItemExample example, @Param("selective") PayOrderItem.Column ... selective);

    int upsertSelective(@Param("record") PayOrderItem record, @Param("selective") PayOrderItem.Column ... selective);
}