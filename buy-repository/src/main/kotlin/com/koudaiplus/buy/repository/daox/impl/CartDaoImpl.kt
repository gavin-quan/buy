package com.koudaiplus.buy.repository.daox.impl

import com.koudaiplus.buy.repository.dao.mapper.CartMapper
import com.koudaiplus.buy.repository.dao.model.Cart
import com.koudaiplus.buy.repository.dao.model.CartExample
import com.koudaiplus.buy.repository.daox.CartDao
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository

@Repository
class CartDaoImpl(
    mapper: CartMapper
) : DaoSupport<CartMapper, CartExample, CartExample.Criteria, Cart.Builder, Cart>(mapper), CartDao {

    override fun getExample() = CartExample()

    override fun getCriteria(example: CartExample) = example.createCriteria()

    override fun getBuilder() = Cart.builder()

    override fun setPage(limit: Limit, example: CartExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}