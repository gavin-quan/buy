package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.OrderMapper
import com.koudaiplus.buy.repository.dao.model.Order
import com.koudaiplus.buy.repository.dao.model.OrderExample
import com.koudaiplus.buy.repository.daox.OrderDao
import com.koudaiplus.buy.repository.daox.model.OrderDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository;
import java.util.*

@Repository
class OrderDaoImpl(
       mapper: OrderMapper
) : DaoSupport<OrderMapper, OrderExample, OrderExample.Criteria, Order.Builder, Order>(mapper), OrderDao {

    override fun getInfoByKeyId(keyId: String): Order? {
        return get { mapper, example, criteria ->
            criteria.andMorKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun create(param: OrderDaoModel.Create) {
        insert { mapper, builder ->
            val e = builder
                    .morKeyId(param.keyId)
                    .morBatchId(param.batchId)
                    .morShopKeyId(param.shopKeyId)
                    .morSubject(param.subject)
                    .morAmount(param.amount)
                    .morExpressId(param.expressId)
                    .morBuyerId(param.buyerId)
                    .morState(param.state)
                    .morCreateTime(Date())
                    .morUpdateTime(Date())
                    .build()
            mapper.insert(e)
        }
    }

    override fun finishPay(param: OrderDaoModel.FinishPay) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .morPayOrderId(param.payOrderKeyId)
                    .morTransOrderNo(param.transOrderNo)
                    .morUpdateTime(Date())
                    .build()
            criteria.andMorKeyIdEqualTo(param.keyId).andMorStateIn(param.fromState)
            mapper.updateByExampleSelective(e, example, Order.Column.morPayOrderId, Order.Column.morTransOrderNo, Order.Column.morUpdateTime)
        }
    }

    override fun tryCloseByKeyId(param: OrderDaoModel.UpdateState) {
        execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            val e = builder
                    .morState(param.toState)
                    .morUpdateTime(Date())
                    .build()
            criteria.andMorKeyIdEqualTo(param.keyId).andMorStateIn(param.fromState).andMorPayOrderIdIsNull()
            mapper.updateByExampleSelective(e, example, Order.Column.morState, Order.Column.morUpdateTime)
        }
    }

    override fun findListByBatchId(batchId: String): List<Order> {
        return list { mapper, example, criteria ->
            criteria.andMorBatchIdEqualTo(batchId)
            mapper.selectByExample(example)
        }
    }

    override fun findListByPayOrderId(payOrderId: String): List<Order> {
        return list { mapper, example, criteria ->
            criteria.andMorPayOrderIdEqualTo(payOrderId)
            mapper.selectByExample(example)
        }
    }

    override fun getExample() = OrderExample()

    override fun getCriteria(example: OrderExample) = example.createCriteria()

    override fun getBuilder() = Order.builder()

    override fun setPage(limit: Limit, example: OrderExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
