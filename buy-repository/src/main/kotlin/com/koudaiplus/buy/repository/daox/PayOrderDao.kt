package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.PayOrder
import com.koudaiplus.buy.repository.daox.model.PayOrderDaoModel

interface PayOrderDao {

    fun getInfoByKeyId(keyId: String): PayOrder?

    fun finishPay(param: PayOrderDaoModel.FinishPay)

    fun create(param: PayOrderDaoModel.Create)

}