package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.mapper.GoodsMapper
import com.koudaiplus.buy.repository.dao.model.Goods
import com.koudaiplus.buy.repository.dao.model.GoodsExample
import com.play.core.common.Dao

interface GoodsDao: Dao<GoodsMapper, GoodsExample, GoodsExample.Criteria, Goods.Builder, Goods> {
    fun getInfoByKeyId(keyId: String): Goods?
}