package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class PayOrder {
    private String mpoKeyId;

    private Long mpoId;

    private String mpoTransOrderNo;

    private String mpoSubject;

    private BigDecimal mpoAmount;

    private Date mpoFinishTime;

    private String mpoState;

    private Date mpoCreateTime;

    private Date mpoUpdateTime;

    public String getMpoKeyId() {
        return mpoKeyId;
    }

    public void setMpoKeyId(String mpoKeyId) {
        this.mpoKeyId = mpoKeyId == null ? null : mpoKeyId.trim();
    }

    public Long getMpoId() {
        return mpoId;
    }

    public void setMpoId(Long mpoId) {
        this.mpoId = mpoId;
    }

    public String getMpoTransOrderNo() {
        return mpoTransOrderNo;
    }

    public void setMpoTransOrderNo(String mpoTransOrderNo) {
        this.mpoTransOrderNo = mpoTransOrderNo == null ? null : mpoTransOrderNo.trim();
    }

    public String getMpoSubject() {
        return mpoSubject;
    }

    public void setMpoSubject(String mpoSubject) {
        this.mpoSubject = mpoSubject == null ? null : mpoSubject.trim();
    }

    public BigDecimal getMpoAmount() {
        return mpoAmount;
    }

    public void setMpoAmount(BigDecimal mpoAmount) {
        this.mpoAmount = mpoAmount;
    }

    public Date getMpoFinishTime() {
        return mpoFinishTime;
    }

    public void setMpoFinishTime(Date mpoFinishTime) {
        this.mpoFinishTime = mpoFinishTime;
    }

    public String getMpoState() {
        return mpoState;
    }

    public void setMpoState(String mpoState) {
        this.mpoState = mpoState == null ? null : mpoState.trim();
    }

    public Date getMpoCreateTime() {
        return mpoCreateTime;
    }

    public void setMpoCreateTime(Date mpoCreateTime) {
        this.mpoCreateTime = mpoCreateTime;
    }

    public Date getMpoUpdateTime() {
        return mpoUpdateTime;
    }

    public void setMpoUpdateTime(Date mpoUpdateTime) {
        this.mpoUpdateTime = mpoUpdateTime;
    }

    public static PayOrder.Builder builder() {
        return new PayOrder.Builder();
    }

    public static class Builder {
        private PayOrder obj;

        public Builder() {
            this.obj = new PayOrder();
        }

        public Builder mpoKeyId(String mpoKeyId) {
            obj.setMpoKeyId(mpoKeyId);
            return this;
        }

        public Builder mpoId(Long mpoId) {
            obj.setMpoId(mpoId);
            return this;
        }

        public Builder mpoTransOrderNo(String mpoTransOrderNo) {
            obj.setMpoTransOrderNo(mpoTransOrderNo);
            return this;
        }

        public Builder mpoSubject(String mpoSubject) {
            obj.setMpoSubject(mpoSubject);
            return this;
        }

        public Builder mpoAmount(BigDecimal mpoAmount) {
            obj.setMpoAmount(mpoAmount);
            return this;
        }

        public Builder mpoFinishTime(Date mpoFinishTime) {
            obj.setMpoFinishTime(mpoFinishTime);
            return this;
        }

        public Builder mpoState(String mpoState) {
            obj.setMpoState(mpoState);
            return this;
        }

        public Builder mpoCreateTime(Date mpoCreateTime) {
            obj.setMpoCreateTime(mpoCreateTime);
            return this;
        }

        public Builder mpoUpdateTime(Date mpoUpdateTime) {
            obj.setMpoUpdateTime(mpoUpdateTime);
            return this;
        }

        public PayOrder build() {
            return this.obj;
        }
    }

    public enum Column {
        mpoKeyId("mpo_key_id", "mpoKeyId", "VARCHAR", false),
        mpoId("mpo_id", "mpoId", "BIGINT", false),
        mpoTransOrderNo("mpo_trans_order_no", "mpoTransOrderNo", "VARCHAR", false),
        mpoSubject("mpo_subject", "mpoSubject", "VARCHAR", false),
        mpoAmount("mpo_amount", "mpoAmount", "DECIMAL", false),
        mpoFinishTime("mpo_finish_time", "mpoFinishTime", "TIMESTAMP", false),
        mpoState("mpo_state", "mpoState", "VARCHAR", false),
        mpoCreateTime("mpo_create_time", "mpoCreateTime", "TIMESTAMP", false),
        mpoUpdateTime("mpo_update_time", "mpoUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}