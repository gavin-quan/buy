package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExpressExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public ExpressExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public ExpressExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public ExpressExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public ExpressExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public ExpressExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public ExpressExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        ExpressExample example = new ExpressExample();
        return example.createCriteria();
    }

    public ExpressExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public ExpressExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEpsKeyIdIsNull() {
            addCriterion("eps_key_id is null");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdIsNotNull() {
            addCriterion("eps_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdEqualTo(String value) {
            addCriterion("eps_key_id =", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdNotEqualTo(String value) {
            addCriterion("eps_key_id <>", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdGreaterThan(String value) {
            addCriterion("eps_key_id >", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("eps_key_id >=", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdLessThan(String value) {
            addCriterion("eps_key_id <", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdLessThanOrEqualTo(String value) {
            addCriterion("eps_key_id <=", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdLike(String value) {
            addCriterion("eps_key_id like", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdNotLike(String value) {
            addCriterion("eps_key_id not like", value, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdIn(List<String> values) {
            addCriterion("eps_key_id in", values, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdNotIn(List<String> values) {
            addCriterion("eps_key_id not in", values, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdBetween(String value1, String value2) {
            addCriterion("eps_key_id between", value1, value2, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsKeyIdNotBetween(String value1, String value2) {
            addCriterion("eps_key_id not between", value1, value2, "epsKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsIdIsNull() {
            addCriterion("eps_id is null");
            return (Criteria) this;
        }

        public Criteria andEpsIdIsNotNull() {
            addCriterion("eps_id is not null");
            return (Criteria) this;
        }

        public Criteria andEpsIdEqualTo(Long value) {
            addCriterion("eps_id =", value, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsIdNotEqualTo(Long value) {
            addCriterion("eps_id <>", value, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsIdGreaterThan(Long value) {
            addCriterion("eps_id >", value, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsIdGreaterThanOrEqualTo(Long value) {
            addCriterion("eps_id >=", value, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsIdLessThan(Long value) {
            addCriterion("eps_id <", value, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsIdLessThanOrEqualTo(Long value) {
            addCriterion("eps_id <=", value, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsIdIn(List<Long> values) {
            addCriterion("eps_id in", values, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdNotIn(List<Long> values) {
            addCriterion("eps_id not in", values, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdBetween(Long value1, Long value2) {
            addCriterion("eps_id between", value1, value2, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsIdNotBetween(Long value1, Long value2) {
            addCriterion("eps_id not between", value1, value2, "epsId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdIsNull() {
            addCriterion("eps_shop_key_id is null");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdIsNotNull() {
            addCriterion("eps_shop_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdEqualTo(String value) {
            addCriterion("eps_shop_key_id =", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_shop_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdNotEqualTo(String value) {
            addCriterion("eps_shop_key_id <>", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_shop_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdGreaterThan(String value) {
            addCriterion("eps_shop_key_id >", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_shop_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("eps_shop_key_id >=", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_shop_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdLessThan(String value) {
            addCriterion("eps_shop_key_id <", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_shop_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdLessThanOrEqualTo(String value) {
            addCriterion("eps_shop_key_id <=", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_shop_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdLike(String value) {
            addCriterion("eps_shop_key_id like", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdNotLike(String value) {
            addCriterion("eps_shop_key_id not like", value, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdIn(List<String> values) {
            addCriterion("eps_shop_key_id in", values, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdNotIn(List<String> values) {
            addCriterion("eps_shop_key_id not in", values, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdBetween(String value1, String value2) {
            addCriterion("eps_shop_key_id between", value1, value2, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsShopKeyIdNotBetween(String value1, String value2) {
            addCriterion("eps_shop_key_id not between", value1, value2, "epsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdIsNull() {
            addCriterion("eps_receiver_user_id is null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdIsNotNull() {
            addCriterion("eps_receiver_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdEqualTo(String value) {
            addCriterion("eps_receiver_user_id =", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_user_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdNotEqualTo(String value) {
            addCriterion("eps_receiver_user_id <>", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_user_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdGreaterThan(String value) {
            addCriterion("eps_receiver_user_id >", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_user_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("eps_receiver_user_id >=", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_user_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdLessThan(String value) {
            addCriterion("eps_receiver_user_id <", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_user_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdLessThanOrEqualTo(String value) {
            addCriterion("eps_receiver_user_id <=", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_user_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdLike(String value) {
            addCriterion("eps_receiver_user_id like", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdNotLike(String value) {
            addCriterion("eps_receiver_user_id not like", value, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdIn(List<String> values) {
            addCriterion("eps_receiver_user_id in", values, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdNotIn(List<String> values) {
            addCriterion("eps_receiver_user_id not in", values, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdBetween(String value1, String value2) {
            addCriterion("eps_receiver_user_id between", value1, value2, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverUserIdNotBetween(String value1, String value2) {
            addCriterion("eps_receiver_user_id not between", value1, value2, "epsReceiverUserId");
            return (Criteria) this;
        }

        public Criteria andEpsTitleIsNull() {
            addCriterion("eps_title is null");
            return (Criteria) this;
        }

        public Criteria andEpsTitleIsNotNull() {
            addCriterion("eps_title is not null");
            return (Criteria) this;
        }

        public Criteria andEpsTitleEqualTo(String value) {
            addCriterion("eps_title =", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_title = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsTitleNotEqualTo(String value) {
            addCriterion("eps_title <>", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_title <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsTitleGreaterThan(String value) {
            addCriterion("eps_title >", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_title > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsTitleGreaterThanOrEqualTo(String value) {
            addCriterion("eps_title >=", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_title >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsTitleLessThan(String value) {
            addCriterion("eps_title <", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_title < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsTitleLessThanOrEqualTo(String value) {
            addCriterion("eps_title <=", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_title <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsTitleLike(String value) {
            addCriterion("eps_title like", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleNotLike(String value) {
            addCriterion("eps_title not like", value, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleIn(List<String> values) {
            addCriterion("eps_title in", values, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleNotIn(List<String> values) {
            addCriterion("eps_title not in", values, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleBetween(String value1, String value2) {
            addCriterion("eps_title between", value1, value2, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsTitleNotBetween(String value1, String value2) {
            addCriterion("eps_title not between", value1, value2, "epsTitle");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameIsNull() {
            addCriterion("eps_receiver_name is null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameIsNotNull() {
            addCriterion("eps_receiver_name is not null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameEqualTo(String value) {
            addCriterion("eps_receiver_name =", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_name = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameNotEqualTo(String value) {
            addCriterion("eps_receiver_name <>", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_name <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameGreaterThan(String value) {
            addCriterion("eps_receiver_name >", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_name > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameGreaterThanOrEqualTo(String value) {
            addCriterion("eps_receiver_name >=", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_name >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameLessThan(String value) {
            addCriterion("eps_receiver_name <", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_name < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameLessThanOrEqualTo(String value) {
            addCriterion("eps_receiver_name <=", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_name <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameLike(String value) {
            addCriterion("eps_receiver_name like", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameNotLike(String value) {
            addCriterion("eps_receiver_name not like", value, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameIn(List<String> values) {
            addCriterion("eps_receiver_name in", values, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameNotIn(List<String> values) {
            addCriterion("eps_receiver_name not in", values, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameBetween(String value1, String value2) {
            addCriterion("eps_receiver_name between", value1, value2, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverNameNotBetween(String value1, String value2) {
            addCriterion("eps_receiver_name not between", value1, value2, "epsReceiverName");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneIsNull() {
            addCriterion("eps_receiver_telephone is null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneIsNotNull() {
            addCriterion("eps_receiver_telephone is not null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneEqualTo(String value) {
            addCriterion("eps_receiver_telephone =", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_telephone = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneNotEqualTo(String value) {
            addCriterion("eps_receiver_telephone <>", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_telephone <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneGreaterThan(String value) {
            addCriterion("eps_receiver_telephone >", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_telephone > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("eps_receiver_telephone >=", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_telephone >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneLessThan(String value) {
            addCriterion("eps_receiver_telephone <", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_telephone < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneLessThanOrEqualTo(String value) {
            addCriterion("eps_receiver_telephone <=", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_telephone <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneLike(String value) {
            addCriterion("eps_receiver_telephone like", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneNotLike(String value) {
            addCriterion("eps_receiver_telephone not like", value, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneIn(List<String> values) {
            addCriterion("eps_receiver_telephone in", values, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneNotIn(List<String> values) {
            addCriterion("eps_receiver_telephone not in", values, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneBetween(String value1, String value2) {
            addCriterion("eps_receiver_telephone between", value1, value2, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverTelephoneNotBetween(String value1, String value2) {
            addCriterion("eps_receiver_telephone not between", value1, value2, "epsReceiverTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressIsNull() {
            addCriterion("eps_receiver_address is null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressIsNotNull() {
            addCriterion("eps_receiver_address is not null");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressEqualTo(String value) {
            addCriterion("eps_receiver_address =", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_address = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressNotEqualTo(String value) {
            addCriterion("eps_receiver_address <>", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_address <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressGreaterThan(String value) {
            addCriterion("eps_receiver_address >", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_address > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressGreaterThanOrEqualTo(String value) {
            addCriterion("eps_receiver_address >=", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_address >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressLessThan(String value) {
            addCriterion("eps_receiver_address <", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_address < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressLessThanOrEqualTo(String value) {
            addCriterion("eps_receiver_address <=", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_receiver_address <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressLike(String value) {
            addCriterion("eps_receiver_address like", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressNotLike(String value) {
            addCriterion("eps_receiver_address not like", value, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressIn(List<String> values) {
            addCriterion("eps_receiver_address in", values, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressNotIn(List<String> values) {
            addCriterion("eps_receiver_address not in", values, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressBetween(String value1, String value2) {
            addCriterion("eps_receiver_address between", value1, value2, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsReceiverAddressNotBetween(String value1, String value2) {
            addCriterion("eps_receiver_address not between", value1, value2, "epsReceiverAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameIsNull() {
            addCriterion("eps_consignor_name is null");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameIsNotNull() {
            addCriterion("eps_consignor_name is not null");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameEqualTo(String value) {
            addCriterion("eps_consignor_name =", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_name = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameNotEqualTo(String value) {
            addCriterion("eps_consignor_name <>", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_name <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameGreaterThan(String value) {
            addCriterion("eps_consignor_name >", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_name > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameGreaterThanOrEqualTo(String value) {
            addCriterion("eps_consignor_name >=", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_name >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameLessThan(String value) {
            addCriterion("eps_consignor_name <", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_name < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameLessThanOrEqualTo(String value) {
            addCriterion("eps_consignor_name <=", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_name <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameLike(String value) {
            addCriterion("eps_consignor_name like", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameNotLike(String value) {
            addCriterion("eps_consignor_name not like", value, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameIn(List<String> values) {
            addCriterion("eps_consignor_name in", values, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameNotIn(List<String> values) {
            addCriterion("eps_consignor_name not in", values, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameBetween(String value1, String value2) {
            addCriterion("eps_consignor_name between", value1, value2, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorNameNotBetween(String value1, String value2) {
            addCriterion("eps_consignor_name not between", value1, value2, "epsConsignorName");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneIsNull() {
            addCriterion("eps_consignor_telephone is null");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneIsNotNull() {
            addCriterion("eps_consignor_telephone is not null");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneEqualTo(String value) {
            addCriterion("eps_consignor_telephone =", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_telephone = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneNotEqualTo(String value) {
            addCriterion("eps_consignor_telephone <>", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_telephone <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneGreaterThan(String value) {
            addCriterion("eps_consignor_telephone >", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_telephone > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("eps_consignor_telephone >=", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_telephone >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneLessThan(String value) {
            addCriterion("eps_consignor_telephone <", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_telephone < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneLessThanOrEqualTo(String value) {
            addCriterion("eps_consignor_telephone <=", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_telephone <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneLike(String value) {
            addCriterion("eps_consignor_telephone like", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneNotLike(String value) {
            addCriterion("eps_consignor_telephone not like", value, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneIn(List<String> values) {
            addCriterion("eps_consignor_telephone in", values, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneNotIn(List<String> values) {
            addCriterion("eps_consignor_telephone not in", values, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneBetween(String value1, String value2) {
            addCriterion("eps_consignor_telephone between", value1, value2, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorTelephoneNotBetween(String value1, String value2) {
            addCriterion("eps_consignor_telephone not between", value1, value2, "epsConsignorTelephone");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressIsNull() {
            addCriterion("eps_consignor_address is null");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressIsNotNull() {
            addCriterion("eps_consignor_address is not null");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressEqualTo(String value) {
            addCriterion("eps_consignor_address =", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_address = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressNotEqualTo(String value) {
            addCriterion("eps_consignor_address <>", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_address <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressGreaterThan(String value) {
            addCriterion("eps_consignor_address >", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_address > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressGreaterThanOrEqualTo(String value) {
            addCriterion("eps_consignor_address >=", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_address >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressLessThan(String value) {
            addCriterion("eps_consignor_address <", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_address < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressLessThanOrEqualTo(String value) {
            addCriterion("eps_consignor_address <=", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_consignor_address <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressLike(String value) {
            addCriterion("eps_consignor_address like", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressNotLike(String value) {
            addCriterion("eps_consignor_address not like", value, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressIn(List<String> values) {
            addCriterion("eps_consignor_address in", values, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressNotIn(List<String> values) {
            addCriterion("eps_consignor_address not in", values, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressBetween(String value1, String value2) {
            addCriterion("eps_consignor_address between", value1, value2, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsConsignorAddressNotBetween(String value1, String value2) {
            addCriterion("eps_consignor_address not between", value1, value2, "epsConsignorAddress");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeIsNull() {
            addCriterion("eps_company_code is null");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeIsNotNull() {
            addCriterion("eps_company_code is not null");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeEqualTo(String value) {
            addCriterion("eps_company_code =", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_company_code = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeNotEqualTo(String value) {
            addCriterion("eps_company_code <>", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_company_code <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeGreaterThan(String value) {
            addCriterion("eps_company_code >", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_company_code > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeGreaterThanOrEqualTo(String value) {
            addCriterion("eps_company_code >=", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_company_code >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeLessThan(String value) {
            addCriterion("eps_company_code <", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_company_code < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeLessThanOrEqualTo(String value) {
            addCriterion("eps_company_code <=", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_company_code <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeLike(String value) {
            addCriterion("eps_company_code like", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeNotLike(String value) {
            addCriterion("eps_company_code not like", value, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeIn(List<String> values) {
            addCriterion("eps_company_code in", values, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeNotIn(List<String> values) {
            addCriterion("eps_company_code not in", values, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeBetween(String value1, String value2) {
            addCriterion("eps_company_code between", value1, value2, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsCompanyCodeNotBetween(String value1, String value2) {
            addCriterion("eps_company_code not between", value1, value2, "epsCompanyCode");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoIsNull() {
            addCriterion("eps_serial_no is null");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoIsNotNull() {
            addCriterion("eps_serial_no is not null");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoEqualTo(String value) {
            addCriterion("eps_serial_no =", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_serial_no = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoNotEqualTo(String value) {
            addCriterion("eps_serial_no <>", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_serial_no <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoGreaterThan(String value) {
            addCriterion("eps_serial_no >", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_serial_no > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoGreaterThanOrEqualTo(String value) {
            addCriterion("eps_serial_no >=", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_serial_no >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoLessThan(String value) {
            addCriterion("eps_serial_no <", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_serial_no < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoLessThanOrEqualTo(String value) {
            addCriterion("eps_serial_no <=", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_serial_no <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoLike(String value) {
            addCriterion("eps_serial_no like", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoNotLike(String value) {
            addCriterion("eps_serial_no not like", value, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoIn(List<String> values) {
            addCriterion("eps_serial_no in", values, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoNotIn(List<String> values) {
            addCriterion("eps_serial_no not in", values, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoBetween(String value1, String value2) {
            addCriterion("eps_serial_no between", value1, value2, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsSerialNoNotBetween(String value1, String value2) {
            addCriterion("eps_serial_no not between", value1, value2, "epsSerialNo");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeIsNull() {
            addCriterion("eps_start_time is null");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeIsNotNull() {
            addCriterion("eps_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeEqualTo(Date value) {
            addCriterion("eps_start_time =", value, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_start_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeNotEqualTo(Date value) {
            addCriterion("eps_start_time <>", value, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_start_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeGreaterThan(Date value) {
            addCriterion("eps_start_time >", value, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_start_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("eps_start_time >=", value, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_start_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeLessThan(Date value) {
            addCriterion("eps_start_time <", value, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_start_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("eps_start_time <=", value, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_start_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeIn(List<Date> values) {
            addCriterion("eps_start_time in", values, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeNotIn(List<Date> values) {
            addCriterion("eps_start_time not in", values, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeBetween(Date value1, Date value2) {
            addCriterion("eps_start_time between", value1, value2, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("eps_start_time not between", value1, value2, "epsStartTime");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksIsNull() {
            addCriterion("eps_remarks is null");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksIsNotNull() {
            addCriterion("eps_remarks is not null");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksEqualTo(String value) {
            addCriterion("eps_remarks =", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_remarks = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsRemarksNotEqualTo(String value) {
            addCriterion("eps_remarks <>", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_remarks <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsRemarksGreaterThan(String value) {
            addCriterion("eps_remarks >", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_remarks > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("eps_remarks >=", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_remarks >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsRemarksLessThan(String value) {
            addCriterion("eps_remarks <", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_remarks < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsRemarksLessThanOrEqualTo(String value) {
            addCriterion("eps_remarks <=", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_remarks <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsRemarksLike(String value) {
            addCriterion("eps_remarks like", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksNotLike(String value) {
            addCriterion("eps_remarks not like", value, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksIn(List<String> values) {
            addCriterion("eps_remarks in", values, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksNotIn(List<String> values) {
            addCriterion("eps_remarks not in", values, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksBetween(String value1, String value2) {
            addCriterion("eps_remarks between", value1, value2, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsRemarksNotBetween(String value1, String value2) {
            addCriterion("eps_remarks not between", value1, value2, "epsRemarks");
            return (Criteria) this;
        }

        public Criteria andEpsStateIsNull() {
            addCriterion("eps_state is null");
            return (Criteria) this;
        }

        public Criteria andEpsStateIsNotNull() {
            addCriterion("eps_state is not null");
            return (Criteria) this;
        }

        public Criteria andEpsStateEqualTo(String value) {
            addCriterion("eps_state =", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStateNotEqualTo(String value) {
            addCriterion("eps_state <>", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStateGreaterThan(String value) {
            addCriterion("eps_state >", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStateGreaterThanOrEqualTo(String value) {
            addCriterion("eps_state >=", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStateLessThan(String value) {
            addCriterion("eps_state <", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStateLessThanOrEqualTo(String value) {
            addCriterion("eps_state <=", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsStateLike(String value) {
            addCriterion("eps_state like", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateNotLike(String value) {
            addCriterion("eps_state not like", value, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateIn(List<String> values) {
            addCriterion("eps_state in", values, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateNotIn(List<String> values) {
            addCriterion("eps_state not in", values, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateBetween(String value1, String value2) {
            addCriterion("eps_state between", value1, value2, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsStateNotBetween(String value1, String value2) {
            addCriterion("eps_state not between", value1, value2, "epsState");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeIsNull() {
            addCriterion("eps_create_time is null");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeIsNotNull() {
            addCriterion("eps_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeEqualTo(Date value) {
            addCriterion("eps_create_time =", value, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeNotEqualTo(Date value) {
            addCriterion("eps_create_time <>", value, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeGreaterThan(Date value) {
            addCriterion("eps_create_time >", value, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("eps_create_time >=", value, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeLessThan(Date value) {
            addCriterion("eps_create_time <", value, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("eps_create_time <=", value, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeIn(List<Date> values) {
            addCriterion("eps_create_time in", values, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeNotIn(List<Date> values) {
            addCriterion("eps_create_time not in", values, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeBetween(Date value1, Date value2) {
            addCriterion("eps_create_time between", value1, value2, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("eps_create_time not between", value1, value2, "epsCreateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeIsNull() {
            addCriterion("eps_update_time is null");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeIsNotNull() {
            addCriterion("eps_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeEqualTo(Date value) {
            addCriterion("eps_update_time =", value, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeNotEqualTo(Date value) {
            addCriterion("eps_update_time <>", value, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeNotEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeGreaterThan(Date value) {
            addCriterion("eps_update_time >", value, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeGreaterThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("eps_update_time >=", value, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeGreaterThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeLessThan(Date value) {
            addCriterion("eps_update_time <", value, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeLessThanColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("eps_update_time <=", value, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeLessThanOrEqualToColumn(Express.Column column) {
            addCriterion(new StringBuilder("eps_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeIn(List<Date> values) {
            addCriterion("eps_update_time in", values, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeNotIn(List<Date> values) {
            addCriterion("eps_update_time not in", values, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("eps_update_time between", value1, value2, "epsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEpsUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("eps_update_time not between", value1, value2, "epsUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private ExpressExample example;

        protected Criteria(ExpressExample example) {
            super();
            this.example = example;
        }

        public ExpressExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.ExpressExample example);
    }
}