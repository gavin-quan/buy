package com.koudaiplus.buy.repository.daox.model

object PayOrderItemDaoModel {

    data class Create(
            val keyId: String,
            val orderKeyId: String,
            val payOrderKeyId: String
    )

}