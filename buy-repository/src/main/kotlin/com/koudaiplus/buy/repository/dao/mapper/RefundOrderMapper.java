package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.RefundOrder;
import com.koudaiplus.buy.repository.dao.model.RefundOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RefundOrderMapper {
    long countByExample(RefundOrderExample example);

    int deleteByExample(RefundOrderExample example);

    int deleteByPrimaryKey(String mroKeyId);

    int insert(RefundOrder record);

    int insertSelective(@Param("record") RefundOrder record, @Param("selective") RefundOrder.Column ... selective);

    RefundOrder selectOneByExample(RefundOrderExample example);

    RefundOrder selectOneByExampleSelective(@Param("example") RefundOrderExample example, @Param("selective") RefundOrder.Column ... selective);

    List<RefundOrder> selectByExampleSelective(@Param("example") RefundOrderExample example, @Param("selective") RefundOrder.Column ... selective);

    List<RefundOrder> selectByExample(RefundOrderExample example);

    RefundOrder selectByPrimaryKeySelective(@Param("mroKeyId") String mroKeyId, @Param("selective") RefundOrder.Column ... selective);

    RefundOrder selectByPrimaryKey(String mroKeyId);

    int updateByExampleSelective(@Param("record") RefundOrder record, @Param("example") RefundOrderExample example, @Param("selective") RefundOrder.Column ... selective);

    int updateByExample(@Param("record") RefundOrder record, @Param("example") RefundOrderExample example);

    int updateByPrimaryKeySelective(@Param("record") RefundOrder record, @Param("selective") RefundOrder.Column ... selective);

    int updateByPrimaryKey(RefundOrder record);

    int batchInsert(@Param("list") List<RefundOrder> list);

    int batchInsertSelective(@Param("list") List<RefundOrder> list, @Param("selective") RefundOrder.Column ... selective);

    int upsert(RefundOrder record);

    int upsertByExample(@Param("record") RefundOrder record, @Param("example") RefundOrderExample example);

    int upsertByExampleSelective(@Param("record") RefundOrder record, @Param("example") RefundOrderExample example, @Param("selective") RefundOrder.Column ... selective);

    int upsertSelective(@Param("record") RefundOrder record, @Param("selective") RefundOrder.Column ... selective);
}