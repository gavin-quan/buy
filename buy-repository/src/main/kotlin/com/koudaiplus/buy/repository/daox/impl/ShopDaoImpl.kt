package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.ShopMapper
import com.koudaiplus.buy.repository.dao.model.Shop
import com.koudaiplus.buy.repository.dao.model.ShopExample
import com.koudaiplus.buy.repository.daox.ShopDao
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository;

@Repository
class ShopDaoImpl(
       mapper: ShopMapper
) : DaoSupport<ShopMapper, ShopExample, ShopExample.Criteria, Shop.Builder, Shop>(mapper), ShopDao {

    override fun getInfoByKeyId(keyId: String): Shop? {
        return get { mapper, example, criteria ->
            criteria.andMspKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getExample() = ShopExample()

    override fun getCriteria(example: ShopExample) = example.createCriteria()

    override fun getBuilder() = Shop.builder()

    override fun setPage(limit: Limit, example: ShopExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
