package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PayOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public PayOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public PayOrderExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public PayOrderExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public PayOrderExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public PayOrderExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public PayOrderExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        PayOrderExample example = new PayOrderExample();
        return example.createCriteria();
    }

    public PayOrderExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public PayOrderExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMpoKeyIdIsNull() {
            addCriterion("mpo_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdIsNotNull() {
            addCriterion("mpo_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdEqualTo(String value) {
            addCriterion("mpo_key_id =", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdNotEqualTo(String value) {
            addCriterion("mpo_key_id <>", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdGreaterThan(String value) {
            addCriterion("mpo_key_id >", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mpo_key_id >=", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdLessThan(String value) {
            addCriterion("mpo_key_id <", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mpo_key_id <=", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdLike(String value) {
            addCriterion("mpo_key_id like", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdNotLike(String value) {
            addCriterion("mpo_key_id not like", value, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdIn(List<String> values) {
            addCriterion("mpo_key_id in", values, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdNotIn(List<String> values) {
            addCriterion("mpo_key_id not in", values, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdBetween(String value1, String value2) {
            addCriterion("mpo_key_id between", value1, value2, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoKeyIdNotBetween(String value1, String value2) {
            addCriterion("mpo_key_id not between", value1, value2, "mpoKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoIdIsNull() {
            addCriterion("mpo_id is null");
            return (Criteria) this;
        }

        public Criteria andMpoIdIsNotNull() {
            addCriterion("mpo_id is not null");
            return (Criteria) this;
        }

        public Criteria andMpoIdEqualTo(Long value) {
            addCriterion("mpo_id =", value, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoIdNotEqualTo(Long value) {
            addCriterion("mpo_id <>", value, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoIdGreaterThan(Long value) {
            addCriterion("mpo_id >", value, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoIdGreaterThanOrEqualTo(Long value) {
            addCriterion("mpo_id >=", value, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoIdLessThan(Long value) {
            addCriterion("mpo_id <", value, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoIdLessThanOrEqualTo(Long value) {
            addCriterion("mpo_id <=", value, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoIdIn(List<Long> values) {
            addCriterion("mpo_id in", values, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdNotIn(List<Long> values) {
            addCriterion("mpo_id not in", values, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdBetween(Long value1, Long value2) {
            addCriterion("mpo_id between", value1, value2, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoIdNotBetween(Long value1, Long value2) {
            addCriterion("mpo_id not between", value1, value2, "mpoId");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoIsNull() {
            addCriterion("mpo_trans_order_no is null");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoIsNotNull() {
            addCriterion("mpo_trans_order_no is not null");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoEqualTo(String value) {
            addCriterion("mpo_trans_order_no =", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_trans_order_no = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoNotEqualTo(String value) {
            addCriterion("mpo_trans_order_no <>", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_trans_order_no <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoGreaterThan(String value) {
            addCriterion("mpo_trans_order_no >", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_trans_order_no > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("mpo_trans_order_no >=", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_trans_order_no >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoLessThan(String value) {
            addCriterion("mpo_trans_order_no <", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_trans_order_no < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoLessThanOrEqualTo(String value) {
            addCriterion("mpo_trans_order_no <=", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_trans_order_no <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoLike(String value) {
            addCriterion("mpo_trans_order_no like", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoNotLike(String value) {
            addCriterion("mpo_trans_order_no not like", value, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoIn(List<String> values) {
            addCriterion("mpo_trans_order_no in", values, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoNotIn(List<String> values) {
            addCriterion("mpo_trans_order_no not in", values, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoBetween(String value1, String value2) {
            addCriterion("mpo_trans_order_no between", value1, value2, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoTransOrderNoNotBetween(String value1, String value2) {
            addCriterion("mpo_trans_order_no not between", value1, value2, "mpoTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectIsNull() {
            addCriterion("mpo_subject is null");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectIsNotNull() {
            addCriterion("mpo_subject is not null");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectEqualTo(String value) {
            addCriterion("mpo_subject =", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_subject = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoSubjectNotEqualTo(String value) {
            addCriterion("mpo_subject <>", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_subject <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoSubjectGreaterThan(String value) {
            addCriterion("mpo_subject >", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_subject > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoSubjectGreaterThanOrEqualTo(String value) {
            addCriterion("mpo_subject >=", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_subject >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoSubjectLessThan(String value) {
            addCriterion("mpo_subject <", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_subject < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoSubjectLessThanOrEqualTo(String value) {
            addCriterion("mpo_subject <=", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_subject <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoSubjectLike(String value) {
            addCriterion("mpo_subject like", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectNotLike(String value) {
            addCriterion("mpo_subject not like", value, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectIn(List<String> values) {
            addCriterion("mpo_subject in", values, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectNotIn(List<String> values) {
            addCriterion("mpo_subject not in", values, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectBetween(String value1, String value2) {
            addCriterion("mpo_subject between", value1, value2, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoSubjectNotBetween(String value1, String value2) {
            addCriterion("mpo_subject not between", value1, value2, "mpoSubject");
            return (Criteria) this;
        }

        public Criteria andMpoAmountIsNull() {
            addCriterion("mpo_amount is null");
            return (Criteria) this;
        }

        public Criteria andMpoAmountIsNotNull() {
            addCriterion("mpo_amount is not null");
            return (Criteria) this;
        }

        public Criteria andMpoAmountEqualTo(BigDecimal value) {
            addCriterion("mpo_amount =", value, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_amount = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoAmountNotEqualTo(BigDecimal value) {
            addCriterion("mpo_amount <>", value, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_amount <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoAmountGreaterThan(BigDecimal value) {
            addCriterion("mpo_amount >", value, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_amount > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mpo_amount >=", value, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_amount >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoAmountLessThan(BigDecimal value) {
            addCriterion("mpo_amount <", value, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_amount < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mpo_amount <=", value, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_amount <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoAmountIn(List<BigDecimal> values) {
            addCriterion("mpo_amount in", values, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountNotIn(List<BigDecimal> values) {
            addCriterion("mpo_amount not in", values, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mpo_amount between", value1, value2, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mpo_amount not between", value1, value2, "mpoAmount");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeIsNull() {
            addCriterion("mpo_finish_time is null");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeIsNotNull() {
            addCriterion("mpo_finish_time is not null");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeEqualTo(Date value) {
            addCriterion("mpo_finish_time =", value, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_finish_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeNotEqualTo(Date value) {
            addCriterion("mpo_finish_time <>", value, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_finish_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeGreaterThan(Date value) {
            addCriterion("mpo_finish_time >", value, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_finish_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mpo_finish_time >=", value, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_finish_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeLessThan(Date value) {
            addCriterion("mpo_finish_time <", value, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_finish_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeLessThanOrEqualTo(Date value) {
            addCriterion("mpo_finish_time <=", value, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_finish_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeIn(List<Date> values) {
            addCriterion("mpo_finish_time in", values, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeNotIn(List<Date> values) {
            addCriterion("mpo_finish_time not in", values, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeBetween(Date value1, Date value2) {
            addCriterion("mpo_finish_time between", value1, value2, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoFinishTimeNotBetween(Date value1, Date value2) {
            addCriterion("mpo_finish_time not between", value1, value2, "mpoFinishTime");
            return (Criteria) this;
        }

        public Criteria andMpoStateIsNull() {
            addCriterion("mpo_state is null");
            return (Criteria) this;
        }

        public Criteria andMpoStateIsNotNull() {
            addCriterion("mpo_state is not null");
            return (Criteria) this;
        }

        public Criteria andMpoStateEqualTo(String value) {
            addCriterion("mpo_state =", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoStateNotEqualTo(String value) {
            addCriterion("mpo_state <>", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoStateGreaterThan(String value) {
            addCriterion("mpo_state >", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoStateGreaterThanOrEqualTo(String value) {
            addCriterion("mpo_state >=", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoStateLessThan(String value) {
            addCriterion("mpo_state <", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoStateLessThanOrEqualTo(String value) {
            addCriterion("mpo_state <=", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoStateLike(String value) {
            addCriterion("mpo_state like", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateNotLike(String value) {
            addCriterion("mpo_state not like", value, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateIn(List<String> values) {
            addCriterion("mpo_state in", values, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateNotIn(List<String> values) {
            addCriterion("mpo_state not in", values, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateBetween(String value1, String value2) {
            addCriterion("mpo_state between", value1, value2, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoStateNotBetween(String value1, String value2) {
            addCriterion("mpo_state not between", value1, value2, "mpoState");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeIsNull() {
            addCriterion("mpo_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeIsNotNull() {
            addCriterion("mpo_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeEqualTo(Date value) {
            addCriterion("mpo_create_time =", value, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeNotEqualTo(Date value) {
            addCriterion("mpo_create_time <>", value, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeGreaterThan(Date value) {
            addCriterion("mpo_create_time >", value, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mpo_create_time >=", value, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeLessThan(Date value) {
            addCriterion("mpo_create_time <", value, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mpo_create_time <=", value, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeIn(List<Date> values) {
            addCriterion("mpo_create_time in", values, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeNotIn(List<Date> values) {
            addCriterion("mpo_create_time not in", values, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mpo_create_time between", value1, value2, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mpo_create_time not between", value1, value2, "mpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeIsNull() {
            addCriterion("mpo_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeIsNotNull() {
            addCriterion("mpo_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeEqualTo(Date value) {
            addCriterion("mpo_update_time =", value, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeNotEqualTo(Date value) {
            addCriterion("mpo_update_time <>", value, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeNotEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeGreaterThan(Date value) {
            addCriterion("mpo_update_time >", value, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeGreaterThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mpo_update_time >=", value, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeGreaterThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeLessThan(Date value) {
            addCriterion("mpo_update_time <", value, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeLessThanColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mpo_update_time <=", value, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeLessThanOrEqualToColumn(PayOrder.Column column) {
            addCriterion(new StringBuilder("mpo_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeIn(List<Date> values) {
            addCriterion("mpo_update_time in", values, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeNotIn(List<Date> values) {
            addCriterion("mpo_update_time not in", values, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("mpo_update_time between", value1, value2, "mpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMpoUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mpo_update_time not between", value1, value2, "mpoUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private PayOrderExample example;

        protected Criteria(PayOrderExample example) {
            super();
            this.example = example;
        }

        public PayOrderExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.PayOrderExample example);
    }
}