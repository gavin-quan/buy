package com.koudaiplus.buy.repository.daox.model

import java.math.BigDecimal
import java.util.*

object RefundOrderDaoModel {
    data class Create (
            val keyId: String,
            val shopKeyId: String,
            val orderKeyId: String,
            val orderItemKeyId: String?,
            val payOrderKeyId: String,
            val amount: BigDecimal,
            val createdBy: String,
            val state: String
    )
    data class SetStatus(
            val keyId: String,
            val finishTime: Date? = null,
            val toState: String,
            val fromState: List<String>
    )
    data class QueryByPayOrderKeyId(
            val payOrderKeyId: String,
            val state: List<String>
    )
    data class QueryByOrderItemKeyId(
            val orderItemKeyId: String,
            val state: List<String>
    )
    data class QueryByOrderKeyId(
            val orderKeyId: String,
            val state: List<String>
    )
}