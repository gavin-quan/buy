package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.Express;
import com.koudaiplus.buy.repository.dao.model.ExpressExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExpressMapper {
    long countByExample(ExpressExample example);

    int deleteByExample(ExpressExample example);

    int deleteByPrimaryKey(String epsKeyId);

    int insert(Express record);

    int insertSelective(@Param("record") Express record, @Param("selective") Express.Column ... selective);

    Express selectOneByExample(ExpressExample example);

    Express selectOneByExampleSelective(@Param("example") ExpressExample example, @Param("selective") Express.Column ... selective);

    List<Express> selectByExampleSelective(@Param("example") ExpressExample example, @Param("selective") Express.Column ... selective);

    List<Express> selectByExample(ExpressExample example);

    Express selectByPrimaryKeySelective(@Param("epsKeyId") String epsKeyId, @Param("selective") Express.Column ... selective);

    Express selectByPrimaryKey(String epsKeyId);

    int updateByExampleSelective(@Param("record") Express record, @Param("example") ExpressExample example, @Param("selective") Express.Column ... selective);

    int updateByExample(@Param("record") Express record, @Param("example") ExpressExample example);

    int updateByPrimaryKeySelective(@Param("record") Express record, @Param("selective") Express.Column ... selective);

    int updateByPrimaryKey(Express record);

    int batchInsert(@Param("list") List<Express> list);

    int batchInsertSelective(@Param("list") List<Express> list, @Param("selective") Express.Column ... selective);

    int upsert(Express record);

    int upsertByExample(@Param("record") Express record, @Param("example") ExpressExample example);

    int upsertByExampleSelective(@Param("record") Express record, @Param("example") ExpressExample example, @Param("selective") Express.Column ... selective);

    int upsertSelective(@Param("record") Express record, @Param("selective") Express.Column ... selective);
}