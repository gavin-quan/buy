package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.EasyOrderMapper
import com.koudaiplus.buy.repository.dao.model.EasyOrder
import com.koudaiplus.buy.repository.dao.model.EasyOrderExample
import com.koudaiplus.buy.repository.daox.EasyOrderDao
import com.koudaiplus.buy.repository.daox.model.EasyOrderDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class EasyOrderDaoImpl(
       mapper: EasyOrderMapper
) : DaoSupport<EasyOrderMapper, EasyOrderExample, EasyOrderExample.Criteria, EasyOrder.Builder, EasyOrder>(mapper), EasyOrderDao {

    override fun getInfoByKeyId(keyId: String): EasyOrder? {
        return get { mapper, example, criteria ->
            criteria.andEorKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun create(param: EasyOrderDaoModel.Create) {
        insert { mapper, builder ->
            val e = builder
                    .eorKeyId(param.keyId)
                    .eorAmount(param.amount)
                    .eorSourceId(param.sourceId)
                    .eorSourceType(param.sourceType)
                    .eorBuyerId(param.buyerId)
                    .eorSubject(param.subject)
                    .eorState(param.state)
                    .eorCreateTime(Date())
                    .eorUpdateTime(Date())
                    .build()
            mapper.insert(e)
        }
    }

    override fun finishPay(param: EasyOrderDaoModel.FinishPay) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .eorPayOrderId(param.payOrderKeyId)
                    .eorTransOrderNo(param.transOrderNo)
                    .eorUpdateTime(Date())
                    .build()
            criteria.andEorKeyIdEqualTo(param.keyId).andEorStateIn(param.fromState).andEorPayOrderIdIsNull().andEorTransOrderNoIsNull()
            mapper.updateByExampleSelective(e, example, EasyOrder.Column.eorPayOrderId, EasyOrder.Column.eorTransOrderNo, EasyOrder.Column.eorUpdateTime)
        }
    }

    override fun tryCloseByKeyId(param: EasyOrderDaoModel.UpdateState) {
        execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            val e = builder
                    .eorState(param.toState)
                    .eorUpdateTime(Date())
                    .build()
            criteria.andEorKeyIdEqualTo(param.keyId).andEorStateIn(param.fromState).andEorPayOrderIdIsNull()
            mapper.updateByExampleSelective(e, example, EasyOrder.Column.eorState, EasyOrder.Column.eorUpdateTime)
        }
    }

    override fun getExample() = EasyOrderExample()

    override fun getCriteria(example: EasyOrderExample) = example.createCriteria()

    override fun getBuilder() = EasyOrder.builder()

    override fun setPage(limit: Limit, example: EasyOrderExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
