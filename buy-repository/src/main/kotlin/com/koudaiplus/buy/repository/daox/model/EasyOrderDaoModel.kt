package com.koudaiplus.buy.repository.daox.model

import java.math.BigDecimal

object EasyOrderDaoModel {

    data class FinishPay(
        val keyId: String,
        val payOrderKeyId: String,
        val transOrderNo: String,
        val fromState: List<String>
    )

    data class Create (
            val keyId: String,
            val sourceId: String,
            val sourceType: String,
            val amount: BigDecimal,
            val buyerId: String,
            val subject: String,
            val state: String
    )

    data class UpdateState(
        val keyId: String,
        val toState: String,
        val fromState: List<String>
    )

}