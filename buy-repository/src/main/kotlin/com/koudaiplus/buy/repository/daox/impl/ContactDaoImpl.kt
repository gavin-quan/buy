package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.ContactMapper
import com.koudaiplus.buy.repository.dao.model.Contact
import com.koudaiplus.buy.repository.dao.model.ContactExample
import com.koudaiplus.buy.repository.daox.ContactDao
import com.koudaiplus.buy.repository.daox.model.ContactDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository;
import java.util.*

@Repository
class ContactDaoImpl(
       mapper: ContactMapper
) : DaoSupport<ContactMapper, ContactExample, ContactExample.Criteria, Contact.Builder, Contact>(mapper), ContactDao {

    override fun findListByUserKeyId(userKeyId: String): List<Contact> {
        return list { mapper, example, criteria ->
            criteria.andMcnUserKeyIdEqualTo(userKeyId)
            example.orderBy(Contact.Column.mcnSort.asc(), Contact.Column.mcnId.asc())
            mapper.selectByExample(example)
        }
    }

    override fun create(param: ContactDaoModel.Create) {
        if(param.sort == 0) {
            execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
                val e = builder
                        .mcnSort(1)
                        .build()
                criteria.andMcnSortEqualTo(0)
                        .andMcnUserKeyIdEqualTo(param.userKeyId)
                mapper.updateByExampleSelective(e, example, Contact.Column.mcnSort)
            }
        }
        insert { mapper, builder ->
            val e = builder
                    .mcnKeyId(param.keyId)
                    .mcnName(param.name)
                    .mcnSort(param.sort)
                    .mcnTelephone(param.telephone)
                    .mcnAddress(param.address)
                    .mcnUserKeyId(param.userKeyId)
                    .mcnProvince(param.province)
                    .mcnCity(param.city)
                    .mcnCounty(param.county)
                    .mcnAreacode(param.areaCode)
                    .mcnPostalcode(param.postalCode)
                    .mcnState(param.state)
                    .mcnCreateTime(Date())
                    .mcnUpdateTime(Date())
                    .build()
            mapper.insert(e)
        }
    }

    override fun update(param: ContactDaoModel.Create) {
        if(param.sort == 0) {
            execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
                val e = builder
                        .mcnSort(1)
                        .build()
                criteria.andMcnSortEqualTo(0)
                        .andMcnUserKeyIdEqualTo(param.userKeyId)
                mapper.updateByExampleSelective(e, example, Contact.Column.mcnSort)
            }
        }
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .mcnProvince(param.province)
                    .mcnCity(param.city)
                    .mcnCounty(param.county)
                    .mcnAddress(param.address)
                    .mcnAreacode(param.areaCode)
                    .mcnPostalcode(param.postalCode)
                    .mcnName(param.name)
                    .mcnTelephone(param.telephone)
                    .mcnSort(param.sort)
                    .build()
            criteria.andMcnKeyIdEqualTo(param.keyId).andMcnUserKeyIdEqualTo(param.userKeyId)
            mapper.updateByExampleSelective(e, example, Contact.Column.mcnProvince, Contact.Column.mcnCity, Contact.Column.mcnCounty, Contact.Column.mcnAreacode, Contact.Column.mcnPostalcode,
                    Contact.Column.mcnAddress, Contact.Column.mcnName, Contact.Column.mcnTelephone, Contact.Column.mcnSort)
        }
    }

    override fun deleteByKeyId(keyId: String, userKeyId: String) {
        execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            criteria.andMcnKeyIdEqualTo(keyId)
                    .andMcnUserKeyIdEqualTo(userKeyId)
            mapper.deleteByExample(example)
        }
    }

    override fun setDefaultByKeyId(keyId: String, userKeyId: String) {
        execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            val e = builder
                    .mcnSort(1)
                    .build()
            criteria.andMcnSortEqualTo(0)
                    .andMcnUserKeyIdEqualTo(userKeyId)
            mapper.updateByExampleSelective(e, example, Contact.Column.mcnSort)
        }
        execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            val e = builder
                    .mcnSort(0)
                    .build()
            criteria.andMcnKeyIdEqualTo(keyId)
                    .andMcnUserKeyIdEqualTo(userKeyId)
            mapper.updateByExampleSelective(e, example, Contact.Column.mcnSort)
        }
    }

    override fun getInfoByKeyId(keyId: String): Contact? {
        return get { mapper, example, criteria ->
            criteria.andMcnKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getInfoByKeyId(keyId: String, userKeyId: String): Contact? {
        return get { mapper, example, criteria ->
            criteria.andMcnKeyIdEqualTo(keyId).andMcnUserKeyIdEqualTo(userKeyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getDefaultInfoByUserKeyId(userKeyId: String): Contact? {
        return get { mapper, example, criteria ->
            criteria.andMcnUserKeyIdEqualTo(userKeyId).andMcnSortEqualTo(0)
            mapper.selectOneByExample(example)
        }
    }

    override fun getExample() = ContactExample()

    override fun getCriteria(example: ContactExample) = example.createCriteria()

    override fun getBuilder() = Contact.builder()

    override fun setPage(limit: Limit, example: ContactExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
