package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class PayOrderItem {
    private String mpoiKeyId;

    private Long mpoiId;

    private String mpoiOrderKeyId;

    private String mpoiPayOrderKeyId;

    private Date mpoiCreateTime;

    public String getMpoiKeyId() {
        return mpoiKeyId;
    }

    public void setMpoiKeyId(String mpoiKeyId) {
        this.mpoiKeyId = mpoiKeyId == null ? null : mpoiKeyId.trim();
    }

    public Long getMpoiId() {
        return mpoiId;
    }

    public void setMpoiId(Long mpoiId) {
        this.mpoiId = mpoiId;
    }

    public String getMpoiOrderKeyId() {
        return mpoiOrderKeyId;
    }

    public void setMpoiOrderKeyId(String mpoiOrderKeyId) {
        this.mpoiOrderKeyId = mpoiOrderKeyId == null ? null : mpoiOrderKeyId.trim();
    }

    public String getMpoiPayOrderKeyId() {
        return mpoiPayOrderKeyId;
    }

    public void setMpoiPayOrderKeyId(String mpoiPayOrderKeyId) {
        this.mpoiPayOrderKeyId = mpoiPayOrderKeyId == null ? null : mpoiPayOrderKeyId.trim();
    }

    public Date getMpoiCreateTime() {
        return mpoiCreateTime;
    }

    public void setMpoiCreateTime(Date mpoiCreateTime) {
        this.mpoiCreateTime = mpoiCreateTime;
    }

    public static PayOrderItem.Builder builder() {
        return new PayOrderItem.Builder();
    }

    public static class Builder {
        private PayOrderItem obj;

        public Builder() {
            this.obj = new PayOrderItem();
        }

        public Builder mpoiKeyId(String mpoiKeyId) {
            obj.setMpoiKeyId(mpoiKeyId);
            return this;
        }

        public Builder mpoiId(Long mpoiId) {
            obj.setMpoiId(mpoiId);
            return this;
        }

        public Builder mpoiOrderKeyId(String mpoiOrderKeyId) {
            obj.setMpoiOrderKeyId(mpoiOrderKeyId);
            return this;
        }

        public Builder mpoiPayOrderKeyId(String mpoiPayOrderKeyId) {
            obj.setMpoiPayOrderKeyId(mpoiPayOrderKeyId);
            return this;
        }

        public Builder mpoiCreateTime(Date mpoiCreateTime) {
            obj.setMpoiCreateTime(mpoiCreateTime);
            return this;
        }

        public PayOrderItem build() {
            return this.obj;
        }
    }

    public enum Column {
        mpoiKeyId("mpoi_key_id", "mpoiKeyId", "VARCHAR", false),
        mpoiId("mpoi_id", "mpoiId", "BIGINT", false),
        mpoiOrderKeyId("mpoi_order_key_id", "mpoiOrderKeyId", "VARCHAR", false),
        mpoiPayOrderKeyId("mpoi_pay_order_key_id", "mpoiPayOrderKeyId", "VARCHAR", false),
        mpoiCreateTime("mpoi_create_time", "mpoiCreateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}