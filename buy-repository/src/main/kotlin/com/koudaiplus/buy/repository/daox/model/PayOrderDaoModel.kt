package com.koudaiplus.buy.repository.daox.model

import java.math.BigDecimal
import java.util.*

object PayOrderDaoModel {

    data class Create (
            val keyId: String,
            val subject: String,
            val amount: BigDecimal,
            val state: String
    )

    data class FinishPay(
            val keyId: String,
            val transOrderNo: String,
            val finishTime: Date,
            val toState: String,
            val fromState: List<String>
    )

}