package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoodsStockKeepingUnitExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public GoodsStockKeepingUnitExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public GoodsStockKeepingUnitExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public GoodsStockKeepingUnitExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public GoodsStockKeepingUnitExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public GoodsStockKeepingUnitExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public GoodsStockKeepingUnitExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        GoodsStockKeepingUnitExample example = new GoodsStockKeepingUnitExample();
        return example.createCriteria();
    }

    public GoodsStockKeepingUnitExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public GoodsStockKeepingUnitExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMguKeyIdIsNull() {
            addCriterion("mgu_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdIsNotNull() {
            addCriterion("mgu_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdEqualTo(String value) {
            addCriterion("mgu_key_id =", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguKeyIdNotEqualTo(String value) {
            addCriterion("mgu_key_id <>", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguKeyIdGreaterThan(String value) {
            addCriterion("mgu_key_id >", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mgu_key_id >=", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguKeyIdLessThan(String value) {
            addCriterion("mgu_key_id <", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mgu_key_id <=", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguKeyIdLike(String value) {
            addCriterion("mgu_key_id like", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdNotLike(String value) {
            addCriterion("mgu_key_id not like", value, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdIn(List<String> values) {
            addCriterion("mgu_key_id in", values, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdNotIn(List<String> values) {
            addCriterion("mgu_key_id not in", values, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdBetween(String value1, String value2) {
            addCriterion("mgu_key_id between", value1, value2, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguKeyIdNotBetween(String value1, String value2) {
            addCriterion("mgu_key_id not between", value1, value2, "mguKeyId");
            return (Criteria) this;
        }

        public Criteria andMguIdIsNull() {
            addCriterion("mgu_id is null");
            return (Criteria) this;
        }

        public Criteria andMguIdIsNotNull() {
            addCriterion("mgu_id is not null");
            return (Criteria) this;
        }

        public Criteria andMguIdEqualTo(Long value) {
            addCriterion("mgu_id =", value, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguIdNotEqualTo(Long value) {
            addCriterion("mgu_id <>", value, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguIdGreaterThan(Long value) {
            addCriterion("mgu_id >", value, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguIdGreaterThanOrEqualTo(Long value) {
            addCriterion("mgu_id >=", value, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguIdLessThan(Long value) {
            addCriterion("mgu_id <", value, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguIdLessThanOrEqualTo(Long value) {
            addCriterion("mgu_id <=", value, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguIdIn(List<Long> values) {
            addCriterion("mgu_id in", values, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdNotIn(List<Long> values) {
            addCriterion("mgu_id not in", values, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdBetween(Long value1, Long value2) {
            addCriterion("mgu_id between", value1, value2, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguIdNotBetween(Long value1, Long value2) {
            addCriterion("mgu_id not between", value1, value2, "mguId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdIsNull() {
            addCriterion("mgu_goods_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdIsNotNull() {
            addCriterion("mgu_goods_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdEqualTo(String value) {
            addCriterion("mgu_goods_key_id =", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_goods_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdNotEqualTo(String value) {
            addCriterion("mgu_goods_key_id <>", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_goods_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdGreaterThan(String value) {
            addCriterion("mgu_goods_key_id >", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_goods_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mgu_goods_key_id >=", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_goods_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdLessThan(String value) {
            addCriterion("mgu_goods_key_id <", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_goods_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mgu_goods_key_id <=", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_goods_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdLike(String value) {
            addCriterion("mgu_goods_key_id like", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdNotLike(String value) {
            addCriterion("mgu_goods_key_id not like", value, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdIn(List<String> values) {
            addCriterion("mgu_goods_key_id in", values, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdNotIn(List<String> values) {
            addCriterion("mgu_goods_key_id not in", values, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdBetween(String value1, String value2) {
            addCriterion("mgu_goods_key_id between", value1, value2, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguGoodsKeyIdNotBetween(String value1, String value2) {
            addCriterion("mgu_goods_key_id not between", value1, value2, "mguGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMguNameIsNull() {
            addCriterion("mgu_name is null");
            return (Criteria) this;
        }

        public Criteria andMguNameIsNotNull() {
            addCriterion("mgu_name is not null");
            return (Criteria) this;
        }

        public Criteria andMguNameEqualTo(String value) {
            addCriterion("mgu_name =", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_name = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguNameNotEqualTo(String value) {
            addCriterion("mgu_name <>", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_name <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguNameGreaterThan(String value) {
            addCriterion("mgu_name >", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_name > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguNameGreaterThanOrEqualTo(String value) {
            addCriterion("mgu_name >=", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_name >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguNameLessThan(String value) {
            addCriterion("mgu_name <", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_name < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguNameLessThanOrEqualTo(String value) {
            addCriterion("mgu_name <=", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_name <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguNameLike(String value) {
            addCriterion("mgu_name like", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameNotLike(String value) {
            addCriterion("mgu_name not like", value, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameIn(List<String> values) {
            addCriterion("mgu_name in", values, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameNotIn(List<String> values) {
            addCriterion("mgu_name not in", values, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameBetween(String value1, String value2) {
            addCriterion("mgu_name between", value1, value2, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguNameNotBetween(String value1, String value2) {
            addCriterion("mgu_name not between", value1, value2, "mguName");
            return (Criteria) this;
        }

        public Criteria andMguCoverIsNull() {
            addCriterion("mgu_cover is null");
            return (Criteria) this;
        }

        public Criteria andMguCoverIsNotNull() {
            addCriterion("mgu_cover is not null");
            return (Criteria) this;
        }

        public Criteria andMguCoverEqualTo(String value) {
            addCriterion("mgu_cover =", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_cover = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCoverNotEqualTo(String value) {
            addCriterion("mgu_cover <>", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_cover <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCoverGreaterThan(String value) {
            addCriterion("mgu_cover >", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_cover > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCoverGreaterThanOrEqualTo(String value) {
            addCriterion("mgu_cover >=", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_cover >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCoverLessThan(String value) {
            addCriterion("mgu_cover <", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_cover < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCoverLessThanOrEqualTo(String value) {
            addCriterion("mgu_cover <=", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_cover <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCoverLike(String value) {
            addCriterion("mgu_cover like", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverNotLike(String value) {
            addCriterion("mgu_cover not like", value, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverIn(List<String> values) {
            addCriterion("mgu_cover in", values, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverNotIn(List<String> values) {
            addCriterion("mgu_cover not in", values, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverBetween(String value1, String value2) {
            addCriterion("mgu_cover between", value1, value2, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguCoverNotBetween(String value1, String value2) {
            addCriterion("mgu_cover not between", value1, value2, "mguCover");
            return (Criteria) this;
        }

        public Criteria andMguPreviewIsNull() {
            addCriterion("mgu_preview is null");
            return (Criteria) this;
        }

        public Criteria andMguPreviewIsNotNull() {
            addCriterion("mgu_preview is not null");
            return (Criteria) this;
        }

        public Criteria andMguPreviewEqualTo(String value) {
            addCriterion("mgu_preview =", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_preview = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPreviewNotEqualTo(String value) {
            addCriterion("mgu_preview <>", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_preview <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPreviewGreaterThan(String value) {
            addCriterion("mgu_preview >", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_preview > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPreviewGreaterThanOrEqualTo(String value) {
            addCriterion("mgu_preview >=", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_preview >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPreviewLessThan(String value) {
            addCriterion("mgu_preview <", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_preview < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPreviewLessThanOrEqualTo(String value) {
            addCriterion("mgu_preview <=", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_preview <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPreviewLike(String value) {
            addCriterion("mgu_preview like", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewNotLike(String value) {
            addCriterion("mgu_preview not like", value, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewIn(List<String> values) {
            addCriterion("mgu_preview in", values, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewNotIn(List<String> values) {
            addCriterion("mgu_preview not in", values, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewBetween(String value1, String value2) {
            addCriterion("mgu_preview between", value1, value2, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPreviewNotBetween(String value1, String value2) {
            addCriterion("mgu_preview not between", value1, value2, "mguPreview");
            return (Criteria) this;
        }

        public Criteria andMguPriceIsNull() {
            addCriterion("mgu_price is null");
            return (Criteria) this;
        }

        public Criteria andMguPriceIsNotNull() {
            addCriterion("mgu_price is not null");
            return (Criteria) this;
        }

        public Criteria andMguPriceEqualTo(BigDecimal value) {
            addCriterion("mgu_price =", value, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_price = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPriceNotEqualTo(BigDecimal value) {
            addCriterion("mgu_price <>", value, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_price <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPriceGreaterThan(BigDecimal value) {
            addCriterion("mgu_price >", value, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_price > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mgu_price >=", value, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_price >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPriceLessThan(BigDecimal value) {
            addCriterion("mgu_price <", value, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_price < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mgu_price <=", value, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_price <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguPriceIn(List<BigDecimal> values) {
            addCriterion("mgu_price in", values, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceNotIn(List<BigDecimal> values) {
            addCriterion("mgu_price not in", values, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgu_price between", value1, value2, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgu_price not between", value1, value2, "mguPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceIsNull() {
            addCriterion("mgu_reduced_price is null");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceIsNotNull() {
            addCriterion("mgu_reduced_price is not null");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceEqualTo(BigDecimal value) {
            addCriterion("mgu_reduced_price =", value, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_reduced_price = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceNotEqualTo(BigDecimal value) {
            addCriterion("mgu_reduced_price <>", value, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_reduced_price <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceGreaterThan(BigDecimal value) {
            addCriterion("mgu_reduced_price >", value, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_reduced_price > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mgu_reduced_price >=", value, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_reduced_price >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceLessThan(BigDecimal value) {
            addCriterion("mgu_reduced_price <", value, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_reduced_price < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mgu_reduced_price <=", value, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_reduced_price <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceIn(List<BigDecimal> values) {
            addCriterion("mgu_reduced_price in", values, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceNotIn(List<BigDecimal> values) {
            addCriterion("mgu_reduced_price not in", values, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgu_reduced_price between", value1, value2, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguReducedPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgu_reduced_price not between", value1, value2, "mguReducedPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceIsNull() {
            addCriterion("mgu_discount_price is null");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceIsNotNull() {
            addCriterion("mgu_discount_price is not null");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceEqualTo(BigDecimal value) {
            addCriterion("mgu_discount_price =", value, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_discount_price = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceNotEqualTo(BigDecimal value) {
            addCriterion("mgu_discount_price <>", value, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_discount_price <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceGreaterThan(BigDecimal value) {
            addCriterion("mgu_discount_price >", value, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_discount_price > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mgu_discount_price >=", value, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_discount_price >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceLessThan(BigDecimal value) {
            addCriterion("mgu_discount_price <", value, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_discount_price < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mgu_discount_price <=", value, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_discount_price <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceIn(List<BigDecimal> values) {
            addCriterion("mgu_discount_price in", values, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceNotIn(List<BigDecimal> values) {
            addCriterion("mgu_discount_price not in", values, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgu_discount_price between", value1, value2, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguDiscountPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgu_discount_price not between", value1, value2, "mguDiscountPrice");
            return (Criteria) this;
        }

        public Criteria andMguQuantityIsNull() {
            addCriterion("mgu_quantity is null");
            return (Criteria) this;
        }

        public Criteria andMguQuantityIsNotNull() {
            addCriterion("mgu_quantity is not null");
            return (Criteria) this;
        }

        public Criteria andMguQuantityEqualTo(Integer value) {
            addCriterion("mgu_quantity =", value, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_quantity = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguQuantityNotEqualTo(Integer value) {
            addCriterion("mgu_quantity <>", value, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_quantity <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguQuantityGreaterThan(Integer value) {
            addCriterion("mgu_quantity >", value, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_quantity > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("mgu_quantity >=", value, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_quantity >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguQuantityLessThan(Integer value) {
            addCriterion("mgu_quantity <", value, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_quantity < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("mgu_quantity <=", value, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_quantity <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguQuantityIn(List<Integer> values) {
            addCriterion("mgu_quantity in", values, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityNotIn(List<Integer> values) {
            addCriterion("mgu_quantity not in", values, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityBetween(Integer value1, Integer value2) {
            addCriterion("mgu_quantity between", value1, value2, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("mgu_quantity not between", value1, value2, "mguQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityIsNull() {
            addCriterion("mgu_lock_quantity is null");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityIsNotNull() {
            addCriterion("mgu_lock_quantity is not null");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityEqualTo(Integer value) {
            addCriterion("mgu_lock_quantity =", value, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_lock_quantity = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityNotEqualTo(Integer value) {
            addCriterion("mgu_lock_quantity <>", value, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_lock_quantity <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityGreaterThan(Integer value) {
            addCriterion("mgu_lock_quantity >", value, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_lock_quantity > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("mgu_lock_quantity >=", value, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_lock_quantity >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityLessThan(Integer value) {
            addCriterion("mgu_lock_quantity <", value, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_lock_quantity < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("mgu_lock_quantity <=", value, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_lock_quantity <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityIn(List<Integer> values) {
            addCriterion("mgu_lock_quantity in", values, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityNotIn(List<Integer> values) {
            addCriterion("mgu_lock_quantity not in", values, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityBetween(Integer value1, Integer value2) {
            addCriterion("mgu_lock_quantity between", value1, value2, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguLockQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("mgu_lock_quantity not between", value1, value2, "mguLockQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityIsNull() {
            addCriterion("mgu_available_quantity is null");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityIsNotNull() {
            addCriterion("mgu_available_quantity is not null");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityEqualTo(Integer value) {
            addCriterion("mgu_available_quantity =", value, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_available_quantity = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityNotEqualTo(Integer value) {
            addCriterion("mgu_available_quantity <>", value, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_available_quantity <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityGreaterThan(Integer value) {
            addCriterion("mgu_available_quantity >", value, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_available_quantity > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("mgu_available_quantity >=", value, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_available_quantity >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityLessThan(Integer value) {
            addCriterion("mgu_available_quantity <", value, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_available_quantity < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("mgu_available_quantity <=", value, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_available_quantity <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityIn(List<Integer> values) {
            addCriterion("mgu_available_quantity in", values, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityNotIn(List<Integer> values) {
            addCriterion("mgu_available_quantity not in", values, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityBetween(Integer value1, Integer value2) {
            addCriterion("mgu_available_quantity between", value1, value2, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguAvailableQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("mgu_available_quantity not between", value1, value2, "mguAvailableQuantity");
            return (Criteria) this;
        }

        public Criteria andMguStockIdIsNull() {
            addCriterion("mgu_stock_id is null");
            return (Criteria) this;
        }

        public Criteria andMguStockIdIsNotNull() {
            addCriterion("mgu_stock_id is not null");
            return (Criteria) this;
        }

        public Criteria andMguStockIdEqualTo(String value) {
            addCriterion("mgu_stock_id =", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_stock_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStockIdNotEqualTo(String value) {
            addCriterion("mgu_stock_id <>", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_stock_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStockIdGreaterThan(String value) {
            addCriterion("mgu_stock_id >", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_stock_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStockIdGreaterThanOrEqualTo(String value) {
            addCriterion("mgu_stock_id >=", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_stock_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStockIdLessThan(String value) {
            addCriterion("mgu_stock_id <", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_stock_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStockIdLessThanOrEqualTo(String value) {
            addCriterion("mgu_stock_id <=", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_stock_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStockIdLike(String value) {
            addCriterion("mgu_stock_id like", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdNotLike(String value) {
            addCriterion("mgu_stock_id not like", value, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdIn(List<String> values) {
            addCriterion("mgu_stock_id in", values, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdNotIn(List<String> values) {
            addCriterion("mgu_stock_id not in", values, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdBetween(String value1, String value2) {
            addCriterion("mgu_stock_id between", value1, value2, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStockIdNotBetween(String value1, String value2) {
            addCriterion("mgu_stock_id not between", value1, value2, "mguStockId");
            return (Criteria) this;
        }

        public Criteria andMguStateIsNull() {
            addCriterion("mgu_state is null");
            return (Criteria) this;
        }

        public Criteria andMguStateIsNotNull() {
            addCriterion("mgu_state is not null");
            return (Criteria) this;
        }

        public Criteria andMguStateEqualTo(String value) {
            addCriterion("mgu_state =", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStateNotEqualTo(String value) {
            addCriterion("mgu_state <>", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStateGreaterThan(String value) {
            addCriterion("mgu_state >", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStateGreaterThanOrEqualTo(String value) {
            addCriterion("mgu_state >=", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStateLessThan(String value) {
            addCriterion("mgu_state <", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStateLessThanOrEqualTo(String value) {
            addCriterion("mgu_state <=", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguStateLike(String value) {
            addCriterion("mgu_state like", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateNotLike(String value) {
            addCriterion("mgu_state not like", value, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateIn(List<String> values) {
            addCriterion("mgu_state in", values, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateNotIn(List<String> values) {
            addCriterion("mgu_state not in", values, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateBetween(String value1, String value2) {
            addCriterion("mgu_state between", value1, value2, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguStateNotBetween(String value1, String value2) {
            addCriterion("mgu_state not between", value1, value2, "mguState");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeIsNull() {
            addCriterion("mgu_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeIsNotNull() {
            addCriterion("mgu_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeEqualTo(Date value) {
            addCriterion("mgu_create_time =", value, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeNotEqualTo(Date value) {
            addCriterion("mgu_create_time <>", value, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeGreaterThan(Date value) {
            addCriterion("mgu_create_time >", value, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mgu_create_time >=", value, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeLessThan(Date value) {
            addCriterion("mgu_create_time <", value, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mgu_create_time <=", value, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeIn(List<Date> values) {
            addCriterion("mgu_create_time in", values, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeNotIn(List<Date> values) {
            addCriterion("mgu_create_time not in", values, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mgu_create_time between", value1, value2, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mgu_create_time not between", value1, value2, "mguCreateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeIsNull() {
            addCriterion("mgu_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeIsNotNull() {
            addCriterion("mgu_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeEqualTo(Date value) {
            addCriterion("mgu_update_time =", value, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeNotEqualTo(Date value) {
            addCriterion("mgu_update_time <>", value, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeNotEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeGreaterThan(Date value) {
            addCriterion("mgu_update_time >", value, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeGreaterThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mgu_update_time >=", value, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeGreaterThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeLessThan(Date value) {
            addCriterion("mgu_update_time <", value, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeLessThanColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mgu_update_time <=", value, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeLessThanOrEqualToColumn(GoodsStockKeepingUnit.Column column) {
            addCriterion(new StringBuilder("mgu_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeIn(List<Date> values) {
            addCriterion("mgu_update_time in", values, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeNotIn(List<Date> values) {
            addCriterion("mgu_update_time not in", values, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("mgu_update_time between", value1, value2, "mguUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMguUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mgu_update_time not between", value1, value2, "mguUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private GoodsStockKeepingUnitExample example;

        protected Criteria(GoodsStockKeepingUnitExample example) {
            super();
            this.example = example;
        }

        public GoodsStockKeepingUnitExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnitExample example);
    }
}