package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.RefundOrder
import com.koudaiplus.buy.repository.daox.model.RefundOrderDaoModel

interface RefundOrderDao {

    fun getInfoByKeyId(keyId: String): RefundOrder?

    fun create(param: RefundOrderDaoModel.Create)

    fun setRequestStatus(param: RefundOrderDaoModel.SetStatus)

    fun setResponseStatus(param: RefundOrderDaoModel.SetStatus)

    fun findList(param: RefundOrderDaoModel.QueryByPayOrderKeyId): List<RefundOrder>

    fun findList(param: RefundOrderDaoModel.QueryByOrderItemKeyId): List<RefundOrder>

    fun findList(param: RefundOrderDaoModel.QueryByOrderKeyId): List<RefundOrder>

    fun setExpressIdByKeyId(keyId: String, expressId: String)

}