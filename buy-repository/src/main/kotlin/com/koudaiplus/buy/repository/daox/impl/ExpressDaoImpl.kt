package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.ExpressMapper
import com.koudaiplus.buy.repository.dao.model.Express
import com.koudaiplus.buy.repository.dao.model.ExpressExample
import com.koudaiplus.buy.repository.daox.ExpressDao
import com.koudaiplus.buy.repository.daox.model.ExpressDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository;
import java.util.*

@Repository
class ExpressDaoImpl(
       mapper: ExpressMapper
) : DaoSupport<ExpressMapper, ExpressExample, ExpressExample.Criteria, Express.Builder, Express>(mapper), ExpressDao {

    override fun create(param: ExpressDaoModel.Create) {
        insert { mapper, builder ->
            val e = builder
                    .epsKeyId(param.keyId)
                    .epsShopKeyId(param.shopKeyId)
                    .epsReceiverUserId(param.receiverUserId)
                    .epsReceiverName(param.receiverName)
                    .epsReceiverAddress(param.receiverAddress)
                    .epsReceiverTelephone(param.receiverTelephone)
                    .epsTitle(param.title)
                    .epsState(param.state)
                    .epsCreateTime(Date())
                    .epsUpdateTime(Date())
                    .build()
            mapper.insert(e)
        }
    }

    override fun update(param: ExpressDaoModel.Start) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .epsCompanyCode(param.companyCode)
                    .epsConsignorName(param.consignorName)
                    .epsConsignorAddress(param.consignorAddress)
                    .epsConsignorTelephone(param.consignorTelephone)
                    .epsSerialNo(param.serialNo)
                    .epsUpdateTime(Date())
                    .epsState(param.toState)
                    .build()
            criteria.andEpsKeyIdEqualTo(param.keyId).andEpsStateIn(param.fromState)
            mapper.updateByExampleSelective(e, example, Express.Column.epsCompanyCode, Express.Column.epsConsignorName,
                    Express.Column.epsConsignorAddress, Express.Column.epsConsignorTelephone, Express.Column.epsSerialNo,
                    Express.Column.epsState, Express.Column.epsUpdateTime)
        }
    }

    override fun update(param: ExpressDaoModel.Confirm) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .epsUpdateTime(Date())
                    .epsState(param.toState)
                    .build()
            criteria.andEpsKeyIdEqualTo(param.keyId).andEpsStateIn(param.fromState)
            mapper.updateByExampleSelective(e, example, Express.Column.epsState, Express.Column.epsUpdateTime)
        }
    }

    override fun getInfoByKeyId(keyId: String): Express? {
        return get { mapper, example, criteria ->
            criteria.andEpsKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getExample() = ExpressExample()

    override fun getCriteria(example: ExpressExample) = example.createCriteria()

    override fun getBuilder() = Express.builder()

    override fun setPage(limit: Limit, example: ExpressExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
