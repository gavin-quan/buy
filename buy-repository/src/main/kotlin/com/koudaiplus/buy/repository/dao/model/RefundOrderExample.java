package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RefundOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public RefundOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public RefundOrderExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public RefundOrderExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public RefundOrderExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public RefundOrderExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public RefundOrderExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        RefundOrderExample example = new RefundOrderExample();
        return example.createCriteria();
    }

    public RefundOrderExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public RefundOrderExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMroKeyIdIsNull() {
            addCriterion("mro_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdIsNotNull() {
            addCriterion("mro_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdEqualTo(String value) {
            addCriterion("mro_key_id =", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroKeyIdNotEqualTo(String value) {
            addCriterion("mro_key_id <>", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroKeyIdGreaterThan(String value) {
            addCriterion("mro_key_id >", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mro_key_id >=", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroKeyIdLessThan(String value) {
            addCriterion("mro_key_id <", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mro_key_id <=", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroKeyIdLike(String value) {
            addCriterion("mro_key_id like", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdNotLike(String value) {
            addCriterion("mro_key_id not like", value, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdIn(List<String> values) {
            addCriterion("mro_key_id in", values, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdNotIn(List<String> values) {
            addCriterion("mro_key_id not in", values, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdBetween(String value1, String value2) {
            addCriterion("mro_key_id between", value1, value2, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroKeyIdNotBetween(String value1, String value2) {
            addCriterion("mro_key_id not between", value1, value2, "mroKeyId");
            return (Criteria) this;
        }

        public Criteria andMroIdIsNull() {
            addCriterion("mro_id is null");
            return (Criteria) this;
        }

        public Criteria andMroIdIsNotNull() {
            addCriterion("mro_id is not null");
            return (Criteria) this;
        }

        public Criteria andMroIdEqualTo(Long value) {
            addCriterion("mro_id =", value, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroIdNotEqualTo(Long value) {
            addCriterion("mro_id <>", value, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroIdGreaterThan(Long value) {
            addCriterion("mro_id >", value, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroIdGreaterThanOrEqualTo(Long value) {
            addCriterion("mro_id >=", value, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroIdLessThan(Long value) {
            addCriterion("mro_id <", value, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroIdLessThanOrEqualTo(Long value) {
            addCriterion("mro_id <=", value, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroIdIn(List<Long> values) {
            addCriterion("mro_id in", values, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdNotIn(List<Long> values) {
            addCriterion("mro_id not in", values, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdBetween(Long value1, Long value2) {
            addCriterion("mro_id between", value1, value2, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroIdNotBetween(Long value1, Long value2) {
            addCriterion("mro_id not between", value1, value2, "mroId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdIsNull() {
            addCriterion("mro_shop_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdIsNotNull() {
            addCriterion("mro_shop_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdEqualTo(String value) {
            addCriterion("mro_shop_key_id =", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_shop_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdNotEqualTo(String value) {
            addCriterion("mro_shop_key_id <>", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_shop_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdGreaterThan(String value) {
            addCriterion("mro_shop_key_id >", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_shop_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mro_shop_key_id >=", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_shop_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdLessThan(String value) {
            addCriterion("mro_shop_key_id <", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_shop_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mro_shop_key_id <=", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_shop_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdLike(String value) {
            addCriterion("mro_shop_key_id like", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdNotLike(String value) {
            addCriterion("mro_shop_key_id not like", value, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdIn(List<String> values) {
            addCriterion("mro_shop_key_id in", values, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdNotIn(List<String> values) {
            addCriterion("mro_shop_key_id not in", values, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdBetween(String value1, String value2) {
            addCriterion("mro_shop_key_id between", value1, value2, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroShopKeyIdNotBetween(String value1, String value2) {
            addCriterion("mro_shop_key_id not between", value1, value2, "mroShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdIsNull() {
            addCriterion("mro_order_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdIsNotNull() {
            addCriterion("mro_order_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdEqualTo(String value) {
            addCriterion("mro_order_key_id =", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdNotEqualTo(String value) {
            addCriterion("mro_order_key_id <>", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdGreaterThan(String value) {
            addCriterion("mro_order_key_id >", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mro_order_key_id >=", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdLessThan(String value) {
            addCriterion("mro_order_key_id <", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mro_order_key_id <=", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdLike(String value) {
            addCriterion("mro_order_key_id like", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdNotLike(String value) {
            addCriterion("mro_order_key_id not like", value, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdIn(List<String> values) {
            addCriterion("mro_order_key_id in", values, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdNotIn(List<String> values) {
            addCriterion("mro_order_key_id not in", values, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdBetween(String value1, String value2) {
            addCriterion("mro_order_key_id between", value1, value2, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderKeyIdNotBetween(String value1, String value2) {
            addCriterion("mro_order_key_id not between", value1, value2, "mroOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdIsNull() {
            addCriterion("mro_order_item_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdIsNotNull() {
            addCriterion("mro_order_item_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdEqualTo(String value) {
            addCriterion("mro_order_item_key_id =", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_item_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdNotEqualTo(String value) {
            addCriterion("mro_order_item_key_id <>", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_item_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdGreaterThan(String value) {
            addCriterion("mro_order_item_key_id >", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_item_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mro_order_item_key_id >=", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_item_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdLessThan(String value) {
            addCriterion("mro_order_item_key_id <", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_item_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mro_order_item_key_id <=", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_order_item_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdLike(String value) {
            addCriterion("mro_order_item_key_id like", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdNotLike(String value) {
            addCriterion("mro_order_item_key_id not like", value, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdIn(List<String> values) {
            addCriterion("mro_order_item_key_id in", values, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdNotIn(List<String> values) {
            addCriterion("mro_order_item_key_id not in", values, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdBetween(String value1, String value2) {
            addCriterion("mro_order_item_key_id between", value1, value2, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroOrderItemKeyIdNotBetween(String value1, String value2) {
            addCriterion("mro_order_item_key_id not between", value1, value2, "mroOrderItemKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdIsNull() {
            addCriterion("mro_pay_order_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdIsNotNull() {
            addCriterion("mro_pay_order_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdEqualTo(String value) {
            addCriterion("mro_pay_order_key_id =", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_pay_order_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdNotEqualTo(String value) {
            addCriterion("mro_pay_order_key_id <>", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_pay_order_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdGreaterThan(String value) {
            addCriterion("mro_pay_order_key_id >", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_pay_order_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mro_pay_order_key_id >=", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_pay_order_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdLessThan(String value) {
            addCriterion("mro_pay_order_key_id <", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_pay_order_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mro_pay_order_key_id <=", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_pay_order_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdLike(String value) {
            addCriterion("mro_pay_order_key_id like", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdNotLike(String value) {
            addCriterion("mro_pay_order_key_id not like", value, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdIn(List<String> values) {
            addCriterion("mro_pay_order_key_id in", values, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdNotIn(List<String> values) {
            addCriterion("mro_pay_order_key_id not in", values, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdBetween(String value1, String value2) {
            addCriterion("mro_pay_order_key_id between", value1, value2, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroPayOrderKeyIdNotBetween(String value1, String value2) {
            addCriterion("mro_pay_order_key_id not between", value1, value2, "mroPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdIsNull() {
            addCriterion("mro_express_id is null");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdIsNotNull() {
            addCriterion("mro_express_id is not null");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdEqualTo(String value) {
            addCriterion("mro_express_id =", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_express_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroExpressIdNotEqualTo(String value) {
            addCriterion("mro_express_id <>", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_express_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroExpressIdGreaterThan(String value) {
            addCriterion("mro_express_id >", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_express_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroExpressIdGreaterThanOrEqualTo(String value) {
            addCriterion("mro_express_id >=", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_express_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroExpressIdLessThan(String value) {
            addCriterion("mro_express_id <", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_express_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroExpressIdLessThanOrEqualTo(String value) {
            addCriterion("mro_express_id <=", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_express_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroExpressIdLike(String value) {
            addCriterion("mro_express_id like", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdNotLike(String value) {
            addCriterion("mro_express_id not like", value, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdIn(List<String> values) {
            addCriterion("mro_express_id in", values, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdNotIn(List<String> values) {
            addCriterion("mro_express_id not in", values, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdBetween(String value1, String value2) {
            addCriterion("mro_express_id between", value1, value2, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroExpressIdNotBetween(String value1, String value2) {
            addCriterion("mro_express_id not between", value1, value2, "mroExpressId");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByIsNull() {
            addCriterion("mro_created_by is null");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByIsNotNull() {
            addCriterion("mro_created_by is not null");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByEqualTo(String value) {
            addCriterion("mro_created_by =", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_created_by = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreatedByNotEqualTo(String value) {
            addCriterion("mro_created_by <>", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_created_by <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreatedByGreaterThan(String value) {
            addCriterion("mro_created_by >", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_created_by > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreatedByGreaterThanOrEqualTo(String value) {
            addCriterion("mro_created_by >=", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_created_by >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreatedByLessThan(String value) {
            addCriterion("mro_created_by <", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_created_by < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreatedByLessThanOrEqualTo(String value) {
            addCriterion("mro_created_by <=", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_created_by <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreatedByLike(String value) {
            addCriterion("mro_created_by like", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByNotLike(String value) {
            addCriterion("mro_created_by not like", value, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByIn(List<String> values) {
            addCriterion("mro_created_by in", values, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByNotIn(List<String> values) {
            addCriterion("mro_created_by not in", values, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByBetween(String value1, String value2) {
            addCriterion("mro_created_by between", value1, value2, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroCreatedByNotBetween(String value1, String value2) {
            addCriterion("mro_created_by not between", value1, value2, "mroCreatedBy");
            return (Criteria) this;
        }

        public Criteria andMroAmountIsNull() {
            addCriterion("mro_amount is null");
            return (Criteria) this;
        }

        public Criteria andMroAmountIsNotNull() {
            addCriterion("mro_amount is not null");
            return (Criteria) this;
        }

        public Criteria andMroAmountEqualTo(BigDecimal value) {
            addCriterion("mro_amount =", value, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_amount = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroAmountNotEqualTo(BigDecimal value) {
            addCriterion("mro_amount <>", value, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_amount <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroAmountGreaterThan(BigDecimal value) {
            addCriterion("mro_amount >", value, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_amount > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mro_amount >=", value, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_amount >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroAmountLessThan(BigDecimal value) {
            addCriterion("mro_amount <", value, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_amount < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mro_amount <=", value, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_amount <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroAmountIn(List<BigDecimal> values) {
            addCriterion("mro_amount in", values, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountNotIn(List<BigDecimal> values) {
            addCriterion("mro_amount not in", values, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mro_amount between", value1, value2, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mro_amount not between", value1, value2, "mroAmount");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeIsNull() {
            addCriterion("mro_finish_time is null");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeIsNotNull() {
            addCriterion("mro_finish_time is not null");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeEqualTo(Date value) {
            addCriterion("mro_finish_time =", value, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_finish_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeNotEqualTo(Date value) {
            addCriterion("mro_finish_time <>", value, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_finish_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeGreaterThan(Date value) {
            addCriterion("mro_finish_time >", value, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_finish_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mro_finish_time >=", value, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_finish_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeLessThan(Date value) {
            addCriterion("mro_finish_time <", value, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_finish_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeLessThanOrEqualTo(Date value) {
            addCriterion("mro_finish_time <=", value, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_finish_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeIn(List<Date> values) {
            addCriterion("mro_finish_time in", values, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeNotIn(List<Date> values) {
            addCriterion("mro_finish_time not in", values, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeBetween(Date value1, Date value2) {
            addCriterion("mro_finish_time between", value1, value2, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroFinishTimeNotBetween(Date value1, Date value2) {
            addCriterion("mro_finish_time not between", value1, value2, "mroFinishTime");
            return (Criteria) this;
        }

        public Criteria andMroStateIsNull() {
            addCriterion("mro_state is null");
            return (Criteria) this;
        }

        public Criteria andMroStateIsNotNull() {
            addCriterion("mro_state is not null");
            return (Criteria) this;
        }

        public Criteria andMroStateEqualTo(String value) {
            addCriterion("mro_state =", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroStateNotEqualTo(String value) {
            addCriterion("mro_state <>", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroStateGreaterThan(String value) {
            addCriterion("mro_state >", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroStateGreaterThanOrEqualTo(String value) {
            addCriterion("mro_state >=", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroStateLessThan(String value) {
            addCriterion("mro_state <", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroStateLessThanOrEqualTo(String value) {
            addCriterion("mro_state <=", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroStateLike(String value) {
            addCriterion("mro_state like", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateNotLike(String value) {
            addCriterion("mro_state not like", value, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateIn(List<String> values) {
            addCriterion("mro_state in", values, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateNotIn(List<String> values) {
            addCriterion("mro_state not in", values, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateBetween(String value1, String value2) {
            addCriterion("mro_state between", value1, value2, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroStateNotBetween(String value1, String value2) {
            addCriterion("mro_state not between", value1, value2, "mroState");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeIsNull() {
            addCriterion("mro_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeIsNotNull() {
            addCriterion("mro_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeEqualTo(Date value) {
            addCriterion("mro_create_time =", value, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeNotEqualTo(Date value) {
            addCriterion("mro_create_time <>", value, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeGreaterThan(Date value) {
            addCriterion("mro_create_time >", value, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mro_create_time >=", value, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeLessThan(Date value) {
            addCriterion("mro_create_time <", value, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mro_create_time <=", value, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeIn(List<Date> values) {
            addCriterion("mro_create_time in", values, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeNotIn(List<Date> values) {
            addCriterion("mro_create_time not in", values, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mro_create_time between", value1, value2, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mro_create_time not between", value1, value2, "mroCreateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeIsNull() {
            addCriterion("mro_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeIsNotNull() {
            addCriterion("mro_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeEqualTo(Date value) {
            addCriterion("mro_update_time =", value, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeNotEqualTo(Date value) {
            addCriterion("mro_update_time <>", value, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeNotEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeGreaterThan(Date value) {
            addCriterion("mro_update_time >", value, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeGreaterThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mro_update_time >=", value, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeGreaterThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeLessThan(Date value) {
            addCriterion("mro_update_time <", value, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeLessThanColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mro_update_time <=", value, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeLessThanOrEqualToColumn(RefundOrder.Column column) {
            addCriterion(new StringBuilder("mro_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeIn(List<Date> values) {
            addCriterion("mro_update_time in", values, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeNotIn(List<Date> values) {
            addCriterion("mro_update_time not in", values, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("mro_update_time between", value1, value2, "mroUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMroUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mro_update_time not between", value1, value2, "mroUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private RefundOrderExample example;

        protected Criteria(RefundOrderExample example) {
            super();
            this.example = example;
        }

        public RefundOrderExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.RefundOrderExample example);
    }
}