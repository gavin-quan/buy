package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RepeatPayOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public RepeatPayOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public RepeatPayOrderExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public RepeatPayOrderExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public RepeatPayOrderExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public RepeatPayOrderExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public RepeatPayOrderExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        RepeatPayOrderExample example = new RepeatPayOrderExample();
        return example.createCriteria();
    }

    public RepeatPayOrderExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public RepeatPayOrderExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRpoKeyIdIsNull() {
            addCriterion("rpo_key_id is null");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdIsNotNull() {
            addCriterion("rpo_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdEqualTo(String value) {
            addCriterion("rpo_key_id =", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdNotEqualTo(String value) {
            addCriterion("rpo_key_id <>", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdGreaterThan(String value) {
            addCriterion("rpo_key_id >", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("rpo_key_id >=", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdLessThan(String value) {
            addCriterion("rpo_key_id <", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdLessThanOrEqualTo(String value) {
            addCriterion("rpo_key_id <=", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdLike(String value) {
            addCriterion("rpo_key_id like", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdNotLike(String value) {
            addCriterion("rpo_key_id not like", value, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdIn(List<String> values) {
            addCriterion("rpo_key_id in", values, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdNotIn(List<String> values) {
            addCriterion("rpo_key_id not in", values, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdBetween(String value1, String value2) {
            addCriterion("rpo_key_id between", value1, value2, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoKeyIdNotBetween(String value1, String value2) {
            addCriterion("rpo_key_id not between", value1, value2, "rpoKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoIdIsNull() {
            addCriterion("rpo_id is null");
            return (Criteria) this;
        }

        public Criteria andRpoIdIsNotNull() {
            addCriterion("rpo_id is not null");
            return (Criteria) this;
        }

        public Criteria andRpoIdEqualTo(Long value) {
            addCriterion("rpo_id =", value, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoIdNotEqualTo(Long value) {
            addCriterion("rpo_id <>", value, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoIdGreaterThan(Long value) {
            addCriterion("rpo_id >", value, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoIdGreaterThanOrEqualTo(Long value) {
            addCriterion("rpo_id >=", value, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoIdLessThan(Long value) {
            addCriterion("rpo_id <", value, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoIdLessThanOrEqualTo(Long value) {
            addCriterion("rpo_id <=", value, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoIdIn(List<Long> values) {
            addCriterion("rpo_id in", values, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdNotIn(List<Long> values) {
            addCriterion("rpo_id not in", values, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdBetween(Long value1, Long value2) {
            addCriterion("rpo_id between", value1, value2, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoIdNotBetween(Long value1, Long value2) {
            addCriterion("rpo_id not between", value1, value2, "rpoId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdIsNull() {
            addCriterion("rpo_pay_order_key_id is null");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdIsNotNull() {
            addCriterion("rpo_pay_order_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdEqualTo(String value) {
            addCriterion("rpo_pay_order_key_id =", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdNotEqualTo(String value) {
            addCriterion("rpo_pay_order_key_id <>", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdGreaterThan(String value) {
            addCriterion("rpo_pay_order_key_id >", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("rpo_pay_order_key_id >=", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdLessThan(String value) {
            addCriterion("rpo_pay_order_key_id <", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdLessThanOrEqualTo(String value) {
            addCriterion("rpo_pay_order_key_id <=", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdLike(String value) {
            addCriterion("rpo_pay_order_key_id like", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdNotLike(String value) {
            addCriterion("rpo_pay_order_key_id not like", value, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdIn(List<String> values) {
            addCriterion("rpo_pay_order_key_id in", values, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdNotIn(List<String> values) {
            addCriterion("rpo_pay_order_key_id not in", values, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdBetween(String value1, String value2) {
            addCriterion("rpo_pay_order_key_id between", value1, value2, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderKeyIdNotBetween(String value1, String value2) {
            addCriterion("rpo_pay_order_key_id not between", value1, value2, "rpoPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdIsNull() {
            addCriterion("rpo_pay_order_item_id is null");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdIsNotNull() {
            addCriterion("rpo_pay_order_item_id is not null");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdEqualTo(String value) {
            addCriterion("rpo_pay_order_item_id =", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_item_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdNotEqualTo(String value) {
            addCriterion("rpo_pay_order_item_id <>", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_item_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdGreaterThan(String value) {
            addCriterion("rpo_pay_order_item_id >", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_item_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdGreaterThanOrEqualTo(String value) {
            addCriterion("rpo_pay_order_item_id >=", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_item_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdLessThan(String value) {
            addCriterion("rpo_pay_order_item_id <", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_item_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdLessThanOrEqualTo(String value) {
            addCriterion("rpo_pay_order_item_id <=", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_pay_order_item_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdLike(String value) {
            addCriterion("rpo_pay_order_item_id like", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdNotLike(String value) {
            addCriterion("rpo_pay_order_item_id not like", value, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdIn(List<String> values) {
            addCriterion("rpo_pay_order_item_id in", values, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdNotIn(List<String> values) {
            addCriterion("rpo_pay_order_item_id not in", values, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdBetween(String value1, String value2) {
            addCriterion("rpo_pay_order_item_id between", value1, value2, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoPayOrderItemIdNotBetween(String value1, String value2) {
            addCriterion("rpo_pay_order_item_id not between", value1, value2, "rpoPayOrderItemId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdIsNull() {
            addCriterion("rpo_order_key_id is null");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdIsNotNull() {
            addCriterion("rpo_order_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdEqualTo(String value) {
            addCriterion("rpo_order_key_id =", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_order_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdNotEqualTo(String value) {
            addCriterion("rpo_order_key_id <>", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_order_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdGreaterThan(String value) {
            addCriterion("rpo_order_key_id >", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_order_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("rpo_order_key_id >=", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_order_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdLessThan(String value) {
            addCriterion("rpo_order_key_id <", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_order_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdLessThanOrEqualTo(String value) {
            addCriterion("rpo_order_key_id <=", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_order_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdLike(String value) {
            addCriterion("rpo_order_key_id like", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdNotLike(String value) {
            addCriterion("rpo_order_key_id not like", value, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdIn(List<String> values) {
            addCriterion("rpo_order_key_id in", values, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdNotIn(List<String> values) {
            addCriterion("rpo_order_key_id not in", values, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdBetween(String value1, String value2) {
            addCriterion("rpo_order_key_id between", value1, value2, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoOrderKeyIdNotBetween(String value1, String value2) {
            addCriterion("rpo_order_key_id not between", value1, value2, "rpoOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andRpoAmountIsNull() {
            addCriterion("rpo_amount is null");
            return (Criteria) this;
        }

        public Criteria andRpoAmountIsNotNull() {
            addCriterion("rpo_amount is not null");
            return (Criteria) this;
        }

        public Criteria andRpoAmountEqualTo(BigDecimal value) {
            addCriterion("rpo_amount =", value, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_amount = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoAmountNotEqualTo(BigDecimal value) {
            addCriterion("rpo_amount <>", value, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_amount <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoAmountGreaterThan(BigDecimal value) {
            addCriterion("rpo_amount >", value, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_amount > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("rpo_amount >=", value, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_amount >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoAmountLessThan(BigDecimal value) {
            addCriterion("rpo_amount <", value, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_amount < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("rpo_amount <=", value, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_amount <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoAmountIn(List<BigDecimal> values) {
            addCriterion("rpo_amount in", values, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountNotIn(List<BigDecimal> values) {
            addCriterion("rpo_amount not in", values, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rpo_amount between", value1, value2, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("rpo_amount not between", value1, value2, "rpoAmount");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeIsNull() {
            addCriterion("rpo_refund_finish_time is null");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeIsNotNull() {
            addCriterion("rpo_refund_finish_time is not null");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeEqualTo(Date value) {
            addCriterion("rpo_refund_finish_time =", value, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_refund_finish_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeNotEqualTo(Date value) {
            addCriterion("rpo_refund_finish_time <>", value, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_refund_finish_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeGreaterThan(Date value) {
            addCriterion("rpo_refund_finish_time >", value, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_refund_finish_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("rpo_refund_finish_time >=", value, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_refund_finish_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeLessThan(Date value) {
            addCriterion("rpo_refund_finish_time <", value, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_refund_finish_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeLessThanOrEqualTo(Date value) {
            addCriterion("rpo_refund_finish_time <=", value, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_refund_finish_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeIn(List<Date> values) {
            addCriterion("rpo_refund_finish_time in", values, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeNotIn(List<Date> values) {
            addCriterion("rpo_refund_finish_time not in", values, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeBetween(Date value1, Date value2) {
            addCriterion("rpo_refund_finish_time between", value1, value2, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoRefundFinishTimeNotBetween(Date value1, Date value2) {
            addCriterion("rpo_refund_finish_time not between", value1, value2, "rpoRefundFinishTime");
            return (Criteria) this;
        }

        public Criteria andRpoStateIsNull() {
            addCriterion("rpo_state is null");
            return (Criteria) this;
        }

        public Criteria andRpoStateIsNotNull() {
            addCriterion("rpo_state is not null");
            return (Criteria) this;
        }

        public Criteria andRpoStateEqualTo(String value) {
            addCriterion("rpo_state =", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoStateNotEqualTo(String value) {
            addCriterion("rpo_state <>", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoStateGreaterThan(String value) {
            addCriterion("rpo_state >", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoStateGreaterThanOrEqualTo(String value) {
            addCriterion("rpo_state >=", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoStateLessThan(String value) {
            addCriterion("rpo_state <", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoStateLessThanOrEqualTo(String value) {
            addCriterion("rpo_state <=", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoStateLike(String value) {
            addCriterion("rpo_state like", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateNotLike(String value) {
            addCriterion("rpo_state not like", value, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateIn(List<String> values) {
            addCriterion("rpo_state in", values, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateNotIn(List<String> values) {
            addCriterion("rpo_state not in", values, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateBetween(String value1, String value2) {
            addCriterion("rpo_state between", value1, value2, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoStateNotBetween(String value1, String value2) {
            addCriterion("rpo_state not between", value1, value2, "rpoState");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeIsNull() {
            addCriterion("rpo_create_time is null");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeIsNotNull() {
            addCriterion("rpo_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeEqualTo(Date value) {
            addCriterion("rpo_create_time =", value, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeNotEqualTo(Date value) {
            addCriterion("rpo_create_time <>", value, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeGreaterThan(Date value) {
            addCriterion("rpo_create_time >", value, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("rpo_create_time >=", value, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeLessThan(Date value) {
            addCriterion("rpo_create_time <", value, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("rpo_create_time <=", value, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeIn(List<Date> values) {
            addCriterion("rpo_create_time in", values, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeNotIn(List<Date> values) {
            addCriterion("rpo_create_time not in", values, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeBetween(Date value1, Date value2) {
            addCriterion("rpo_create_time between", value1, value2, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("rpo_create_time not between", value1, value2, "rpoCreateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeIsNull() {
            addCriterion("rpo_update_time is null");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeIsNotNull() {
            addCriterion("rpo_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeEqualTo(Date value) {
            addCriterion("rpo_update_time =", value, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeNotEqualTo(Date value) {
            addCriterion("rpo_update_time <>", value, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeNotEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeGreaterThan(Date value) {
            addCriterion("rpo_update_time >", value, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeGreaterThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("rpo_update_time >=", value, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeGreaterThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeLessThan(Date value) {
            addCriterion("rpo_update_time <", value, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeLessThanColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("rpo_update_time <=", value, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeLessThanOrEqualToColumn(RepeatPayOrder.Column column) {
            addCriterion(new StringBuilder("rpo_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeIn(List<Date> values) {
            addCriterion("rpo_update_time in", values, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeNotIn(List<Date> values) {
            addCriterion("rpo_update_time not in", values, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("rpo_update_time between", value1, value2, "rpoUpdateTime");
            return (Criteria) this;
        }

        public Criteria andRpoUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("rpo_update_time not between", value1, value2, "rpoUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private RepeatPayOrderExample example;

        protected Criteria(RepeatPayOrderExample example) {
            super();
            this.example = example;
        }

        public RepeatPayOrderExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.RepeatPayOrderExample example);
    }
}