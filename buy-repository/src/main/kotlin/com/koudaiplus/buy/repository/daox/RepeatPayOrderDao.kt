package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.RepeatPayOrder
import com.koudaiplus.buy.repository.daox.model.RepeatPayOrderDaoModel

interface RepeatPayOrderDao {
    fun create(param: RepeatPayOrderDaoModel.Create)
    fun getInfoByPayOrderItemKeyId(param: RepeatPayOrderDaoModel.QueryByPayOrderItemKeyId): RepeatPayOrder?
    fun setRespStatus(param: RepeatPayOrderDaoModel.SetStatus)
    fun getInfoByKeyId(keyId: String): RepeatPayOrder?
    fun getInfo(param: RepeatPayOrderDaoModel.QueryByOrderId): RepeatPayOrder?
}