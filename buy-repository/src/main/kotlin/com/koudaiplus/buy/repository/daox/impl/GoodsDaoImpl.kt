package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.GoodsMapper
import com.koudaiplus.buy.repository.dao.model.Goods
import com.koudaiplus.buy.repository.dao.model.GoodsExample
import com.koudaiplus.buy.repository.daox.GoodsDao
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository

@Repository
class GoodsDaoImpl(
       mapper: GoodsMapper
) : DaoSupport<GoodsMapper, GoodsExample, GoodsExample.Criteria, Goods.Builder, Goods>(mapper), GoodsDao {

    override fun getInfoByKeyId(keyId: String): Goods? {
        return get { mapper, example, criteria ->
            criteria.andMgsKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getExample() = GoodsExample()

    override fun getCriteria(example: GoodsExample) = example.createCriteria()

    override fun getBuilder() = Goods.builder()

    override fun setPage(limit: Limit, example: GoodsExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
