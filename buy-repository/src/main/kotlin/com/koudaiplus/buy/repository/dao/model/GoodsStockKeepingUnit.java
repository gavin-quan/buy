package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class GoodsStockKeepingUnit {
    private String mguKeyId;

    private Long mguId;

    private String mguGoodsKeyId;

    private String mguName;

    private String mguCover;

    private String mguPreview;

    private BigDecimal mguPrice;

    private BigDecimal mguReducedPrice;

    private BigDecimal mguDiscountPrice;

    private Integer mguQuantity;

    private Integer mguLockQuantity;

    private Integer mguAvailableQuantity;

    private String mguStockId;

    private String mguState;

    private Date mguCreateTime;

    private Date mguUpdateTime;

    public String getMguKeyId() {
        return mguKeyId;
    }

    public void setMguKeyId(String mguKeyId) {
        this.mguKeyId = mguKeyId == null ? null : mguKeyId.trim();
    }

    public Long getMguId() {
        return mguId;
    }

    public void setMguId(Long mguId) {
        this.mguId = mguId;
    }

    public String getMguGoodsKeyId() {
        return mguGoodsKeyId;
    }

    public void setMguGoodsKeyId(String mguGoodsKeyId) {
        this.mguGoodsKeyId = mguGoodsKeyId == null ? null : mguGoodsKeyId.trim();
    }

    public String getMguName() {
        return mguName;
    }

    public void setMguName(String mguName) {
        this.mguName = mguName == null ? null : mguName.trim();
    }

    public String getMguCover() {
        return mguCover;
    }

    public void setMguCover(String mguCover) {
        this.mguCover = mguCover == null ? null : mguCover.trim();
    }

    public String getMguPreview() {
        return mguPreview;
    }

    public void setMguPreview(String mguPreview) {
        this.mguPreview = mguPreview == null ? null : mguPreview.trim();
    }

    public BigDecimal getMguPrice() {
        return mguPrice;
    }

    public void setMguPrice(BigDecimal mguPrice) {
        this.mguPrice = mguPrice;
    }

    public BigDecimal getMguReducedPrice() {
        return mguReducedPrice;
    }

    public void setMguReducedPrice(BigDecimal mguReducedPrice) {
        this.mguReducedPrice = mguReducedPrice;
    }

    public BigDecimal getMguDiscountPrice() {
        return mguDiscountPrice;
    }

    public void setMguDiscountPrice(BigDecimal mguDiscountPrice) {
        this.mguDiscountPrice = mguDiscountPrice;
    }

    public Integer getMguQuantity() {
        return mguQuantity;
    }

    public void setMguQuantity(Integer mguQuantity) {
        this.mguQuantity = mguQuantity;
    }

    public Integer getMguLockQuantity() {
        return mguLockQuantity;
    }

    public void setMguLockQuantity(Integer mguLockQuantity) {
        this.mguLockQuantity = mguLockQuantity;
    }

    public Integer getMguAvailableQuantity() {
        return mguAvailableQuantity;
    }

    public void setMguAvailableQuantity(Integer mguAvailableQuantity) {
        this.mguAvailableQuantity = mguAvailableQuantity;
    }

    public String getMguStockId() {
        return mguStockId;
    }

    public void setMguStockId(String mguStockId) {
        this.mguStockId = mguStockId == null ? null : mguStockId.trim();
    }

    public String getMguState() {
        return mguState;
    }

    public void setMguState(String mguState) {
        this.mguState = mguState == null ? null : mguState.trim();
    }

    public Date getMguCreateTime() {
        return mguCreateTime;
    }

    public void setMguCreateTime(Date mguCreateTime) {
        this.mguCreateTime = mguCreateTime;
    }

    public Date getMguUpdateTime() {
        return mguUpdateTime;
    }

    public void setMguUpdateTime(Date mguUpdateTime) {
        this.mguUpdateTime = mguUpdateTime;
    }

    public static GoodsStockKeepingUnit.Builder builder() {
        return new GoodsStockKeepingUnit.Builder();
    }

    public static class Builder {
        private GoodsStockKeepingUnit obj;

        public Builder() {
            this.obj = new GoodsStockKeepingUnit();
        }

        public Builder mguKeyId(String mguKeyId) {
            obj.setMguKeyId(mguKeyId);
            return this;
        }

        public Builder mguId(Long mguId) {
            obj.setMguId(mguId);
            return this;
        }

        public Builder mguGoodsKeyId(String mguGoodsKeyId) {
            obj.setMguGoodsKeyId(mguGoodsKeyId);
            return this;
        }

        public Builder mguName(String mguName) {
            obj.setMguName(mguName);
            return this;
        }

        public Builder mguCover(String mguCover) {
            obj.setMguCover(mguCover);
            return this;
        }

        public Builder mguPreview(String mguPreview) {
            obj.setMguPreview(mguPreview);
            return this;
        }

        public Builder mguPrice(BigDecimal mguPrice) {
            obj.setMguPrice(mguPrice);
            return this;
        }

        public Builder mguReducedPrice(BigDecimal mguReducedPrice) {
            obj.setMguReducedPrice(mguReducedPrice);
            return this;
        }

        public Builder mguDiscountPrice(BigDecimal mguDiscountPrice) {
            obj.setMguDiscountPrice(mguDiscountPrice);
            return this;
        }

        public Builder mguQuantity(Integer mguQuantity) {
            obj.setMguQuantity(mguQuantity);
            return this;
        }

        public Builder mguLockQuantity(Integer mguLockQuantity) {
            obj.setMguLockQuantity(mguLockQuantity);
            return this;
        }

        public Builder mguAvailableQuantity(Integer mguAvailableQuantity) {
            obj.setMguAvailableQuantity(mguAvailableQuantity);
            return this;
        }

        public Builder mguStockId(String mguStockId) {
            obj.setMguStockId(mguStockId);
            return this;
        }

        public Builder mguState(String mguState) {
            obj.setMguState(mguState);
            return this;
        }

        public Builder mguCreateTime(Date mguCreateTime) {
            obj.setMguCreateTime(mguCreateTime);
            return this;
        }

        public Builder mguUpdateTime(Date mguUpdateTime) {
            obj.setMguUpdateTime(mguUpdateTime);
            return this;
        }

        public GoodsStockKeepingUnit build() {
            return this.obj;
        }
    }

    public enum Column {
        mguKeyId("mgu_key_id", "mguKeyId", "VARCHAR", false),
        mguId("mgu_id", "mguId", "BIGINT", false),
        mguGoodsKeyId("mgu_goods_key_id", "mguGoodsKeyId", "VARCHAR", false),
        mguName("mgu_name", "mguName", "VARCHAR", false),
        mguCover("mgu_cover", "mguCover", "VARCHAR", false),
        mguPreview("mgu_preview", "mguPreview", "VARCHAR", false),
        mguPrice("mgu_price", "mguPrice", "DECIMAL", false),
        mguReducedPrice("mgu_reduced_price", "mguReducedPrice", "DECIMAL", false),
        mguDiscountPrice("mgu_discount_price", "mguDiscountPrice", "DECIMAL", false),
        mguQuantity("mgu_quantity", "mguQuantity", "INTEGER", false),
        mguLockQuantity("mgu_lock_quantity", "mguLockQuantity", "INTEGER", false),
        mguAvailableQuantity("mgu_available_quantity", "mguAvailableQuantity", "INTEGER", false),
        mguStockId("mgu_stock_id", "mguStockId", "VARCHAR", false),
        mguState("mgu_state", "mguState", "VARCHAR", false),
        mguCreateTime("mgu_create_time", "mguCreateTime", "TIMESTAMP", false),
        mguUpdateTime("mgu_update_time", "mguUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}