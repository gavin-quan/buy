package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.GoodsStockKeepingUnitMapper
import com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnit
import com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnitExample
import com.koudaiplus.buy.repository.daox.GoodsStockKeepingUnitDao
import com.play.core.common.ClassStateEnum
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository

@Repository
class GoodsStockKeepingUnitDaoImpl(
       mapper: GoodsStockKeepingUnitMapper
) : DaoSupport<GoodsStockKeepingUnitMapper, GoodsStockKeepingUnitExample, GoodsStockKeepingUnitExample.Criteria, GoodsStockKeepingUnit.Builder, GoodsStockKeepingUnit>(mapper), GoodsStockKeepingUnitDao {

    override fun getInfoByKeyId(keyId: String): GoodsStockKeepingUnit? {
        return get { mapper, example, criteria ->
            criteria.andMguKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun findSkuListByKeyId(goodsKeyId: String): List<GoodsStockKeepingUnit> {
        return list { mapper, example, criteria ->
            criteria.andMguGoodsKeyIdEqualTo(goodsKeyId)
                    .andMguStateEqualTo(ClassStateEnum.ENABLE.name)
            example.orderBy(GoodsStockKeepingUnit.Column.mguId.asc())
            mapper.selectByExample(example)
        }
    }

    override fun getExample() = GoodsStockKeepingUnitExample()

    override fun getCriteria(example: GoodsStockKeepingUnitExample) = example.createCriteria()

    override fun getBuilder() = GoodsStockKeepingUnit.builder()

    override fun setPage(limit: Limit, example: GoodsStockKeepingUnitExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}
