package com.koudaiplus.buy.repository.daox

import com.koudaiplus.buy.repository.dao.model.Contact
import com.koudaiplus.buy.repository.daox.model.ContactDaoModel


interface ContactDao {

    fun getInfoByKeyId(keyId: String): Contact?

    fun getInfoByKeyId(keyId: String, userKeyId: String): Contact?

    fun getDefaultInfoByUserKeyId(userKeyId: String): Contact?

    fun findListByUserKeyId(userKeyId: String): List<Contact>

    fun create(param: ContactDaoModel.Create)

    fun update(param: ContactDaoModel.Create)

    fun deleteByKeyId(keyId: String, userKeyId: String)

    fun setDefaultByKeyId(keyId: String, userKeyId: String)

}