package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PayOrderItemExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public PayOrderItemExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public PayOrderItemExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public PayOrderItemExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public PayOrderItemExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public PayOrderItemExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public PayOrderItemExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        PayOrderItemExample example = new PayOrderItemExample();
        return example.createCriteria();
    }

    public PayOrderItemExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public PayOrderItemExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMpoiKeyIdIsNull() {
            addCriterion("mpoi_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdIsNotNull() {
            addCriterion("mpoi_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdEqualTo(String value) {
            addCriterion("mpoi_key_id =", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdNotEqualTo(String value) {
            addCriterion("mpoi_key_id <>", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdNotEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdGreaterThan(String value) {
            addCriterion("mpoi_key_id >", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdGreaterThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mpoi_key_id >=", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdGreaterThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdLessThan(String value) {
            addCriterion("mpoi_key_id <", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdLessThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mpoi_key_id <=", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdLessThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdLike(String value) {
            addCriterion("mpoi_key_id like", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdNotLike(String value) {
            addCriterion("mpoi_key_id not like", value, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdIn(List<String> values) {
            addCriterion("mpoi_key_id in", values, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdNotIn(List<String> values) {
            addCriterion("mpoi_key_id not in", values, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdBetween(String value1, String value2) {
            addCriterion("mpoi_key_id between", value1, value2, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiKeyIdNotBetween(String value1, String value2) {
            addCriterion("mpoi_key_id not between", value1, value2, "mpoiKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdIsNull() {
            addCriterion("mpoi_id is null");
            return (Criteria) this;
        }

        public Criteria andMpoiIdIsNotNull() {
            addCriterion("mpoi_id is not null");
            return (Criteria) this;
        }

        public Criteria andMpoiIdEqualTo(Long value) {
            addCriterion("mpoi_id =", value, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiIdNotEqualTo(Long value) {
            addCriterion("mpoi_id <>", value, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdNotEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiIdGreaterThan(Long value) {
            addCriterion("mpoi_id >", value, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdGreaterThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiIdGreaterThanOrEqualTo(Long value) {
            addCriterion("mpoi_id >=", value, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdGreaterThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiIdLessThan(Long value) {
            addCriterion("mpoi_id <", value, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdLessThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiIdLessThanOrEqualTo(Long value) {
            addCriterion("mpoi_id <=", value, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdLessThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiIdIn(List<Long> values) {
            addCriterion("mpoi_id in", values, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdNotIn(List<Long> values) {
            addCriterion("mpoi_id not in", values, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdBetween(Long value1, Long value2) {
            addCriterion("mpoi_id between", value1, value2, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiIdNotBetween(Long value1, Long value2) {
            addCriterion("mpoi_id not between", value1, value2, "mpoiId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdIsNull() {
            addCriterion("mpoi_order_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdIsNotNull() {
            addCriterion("mpoi_order_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdEqualTo(String value) {
            addCriterion("mpoi_order_key_id =", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_order_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdNotEqualTo(String value) {
            addCriterion("mpoi_order_key_id <>", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdNotEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_order_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdGreaterThan(String value) {
            addCriterion("mpoi_order_key_id >", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdGreaterThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_order_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mpoi_order_key_id >=", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdGreaterThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_order_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdLessThan(String value) {
            addCriterion("mpoi_order_key_id <", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdLessThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_order_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mpoi_order_key_id <=", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdLessThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_order_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdLike(String value) {
            addCriterion("mpoi_order_key_id like", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdNotLike(String value) {
            addCriterion("mpoi_order_key_id not like", value, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdIn(List<String> values) {
            addCriterion("mpoi_order_key_id in", values, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdNotIn(List<String> values) {
            addCriterion("mpoi_order_key_id not in", values, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdBetween(String value1, String value2) {
            addCriterion("mpoi_order_key_id between", value1, value2, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiOrderKeyIdNotBetween(String value1, String value2) {
            addCriterion("mpoi_order_key_id not between", value1, value2, "mpoiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdIsNull() {
            addCriterion("mpoi_pay_order_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdIsNotNull() {
            addCriterion("mpoi_pay_order_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdEqualTo(String value) {
            addCriterion("mpoi_pay_order_key_id =", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_pay_order_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdNotEqualTo(String value) {
            addCriterion("mpoi_pay_order_key_id <>", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdNotEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_pay_order_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdGreaterThan(String value) {
            addCriterion("mpoi_pay_order_key_id >", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdGreaterThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_pay_order_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mpoi_pay_order_key_id >=", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdGreaterThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_pay_order_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdLessThan(String value) {
            addCriterion("mpoi_pay_order_key_id <", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdLessThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_pay_order_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mpoi_pay_order_key_id <=", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdLessThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_pay_order_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdLike(String value) {
            addCriterion("mpoi_pay_order_key_id like", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdNotLike(String value) {
            addCriterion("mpoi_pay_order_key_id not like", value, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdIn(List<String> values) {
            addCriterion("mpoi_pay_order_key_id in", values, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdNotIn(List<String> values) {
            addCriterion("mpoi_pay_order_key_id not in", values, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdBetween(String value1, String value2) {
            addCriterion("mpoi_pay_order_key_id between", value1, value2, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiPayOrderKeyIdNotBetween(String value1, String value2) {
            addCriterion("mpoi_pay_order_key_id not between", value1, value2, "mpoiPayOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeIsNull() {
            addCriterion("mpoi_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeIsNotNull() {
            addCriterion("mpoi_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeEqualTo(Date value) {
            addCriterion("mpoi_create_time =", value, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeNotEqualTo(Date value) {
            addCriterion("mpoi_create_time <>", value, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeNotEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeGreaterThan(Date value) {
            addCriterion("mpoi_create_time >", value, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeGreaterThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mpoi_create_time >=", value, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeGreaterThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeLessThan(Date value) {
            addCriterion("mpoi_create_time <", value, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeLessThanColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mpoi_create_time <=", value, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeLessThanOrEqualToColumn(PayOrderItem.Column column) {
            addCriterion(new StringBuilder("mpoi_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeIn(List<Date> values) {
            addCriterion("mpoi_create_time in", values, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeNotIn(List<Date> values) {
            addCriterion("mpoi_create_time not in", values, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mpoi_create_time between", value1, value2, "mpoiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMpoiCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mpoi_create_time not between", value1, value2, "mpoiCreateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private PayOrderItemExample example;

        protected Criteria(PayOrderItemExample example) {
            super();
            this.example = example;
        }

        public PayOrderItemExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.PayOrderItemExample example);
    }
}