package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EasyOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public EasyOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public EasyOrderExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public EasyOrderExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public EasyOrderExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public EasyOrderExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public EasyOrderExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        EasyOrderExample example = new EasyOrderExample();
        return example.createCriteria();
    }

    public EasyOrderExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public EasyOrderExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEorKeyIdIsNull() {
            addCriterion("eor_key_id is null");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdIsNotNull() {
            addCriterion("eor_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdEqualTo(String value) {
            addCriterion("eor_key_id =", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorKeyIdNotEqualTo(String value) {
            addCriterion("eor_key_id <>", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorKeyIdGreaterThan(String value) {
            addCriterion("eor_key_id >", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("eor_key_id >=", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorKeyIdLessThan(String value) {
            addCriterion("eor_key_id <", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorKeyIdLessThanOrEqualTo(String value) {
            addCriterion("eor_key_id <=", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorKeyIdLike(String value) {
            addCriterion("eor_key_id like", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdNotLike(String value) {
            addCriterion("eor_key_id not like", value, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdIn(List<String> values) {
            addCriterion("eor_key_id in", values, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdNotIn(List<String> values) {
            addCriterion("eor_key_id not in", values, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdBetween(String value1, String value2) {
            addCriterion("eor_key_id between", value1, value2, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorKeyIdNotBetween(String value1, String value2) {
            addCriterion("eor_key_id not between", value1, value2, "eorKeyId");
            return (Criteria) this;
        }

        public Criteria andEorIdIsNull() {
            addCriterion("eor_id is null");
            return (Criteria) this;
        }

        public Criteria andEorIdIsNotNull() {
            addCriterion("eor_id is not null");
            return (Criteria) this;
        }

        public Criteria andEorIdEqualTo(Long value) {
            addCriterion("eor_id =", value, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorIdNotEqualTo(Long value) {
            addCriterion("eor_id <>", value, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorIdGreaterThan(Long value) {
            addCriterion("eor_id >", value, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorIdGreaterThanOrEqualTo(Long value) {
            addCriterion("eor_id >=", value, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorIdLessThan(Long value) {
            addCriterion("eor_id <", value, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorIdLessThanOrEqualTo(Long value) {
            addCriterion("eor_id <=", value, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorIdIn(List<Long> values) {
            addCriterion("eor_id in", values, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdNotIn(List<Long> values) {
            addCriterion("eor_id not in", values, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdBetween(Long value1, Long value2) {
            addCriterion("eor_id between", value1, value2, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorIdNotBetween(Long value1, Long value2) {
            addCriterion("eor_id not between", value1, value2, "eorId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdIsNull() {
            addCriterion("eor_source_id is null");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdIsNotNull() {
            addCriterion("eor_source_id is not null");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdEqualTo(String value) {
            addCriterion("eor_source_id =", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceIdNotEqualTo(String value) {
            addCriterion("eor_source_id <>", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceIdGreaterThan(String value) {
            addCriterion("eor_source_id >", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceIdGreaterThanOrEqualTo(String value) {
            addCriterion("eor_source_id >=", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceIdLessThan(String value) {
            addCriterion("eor_source_id <", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceIdLessThanOrEqualTo(String value) {
            addCriterion("eor_source_id <=", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceIdLike(String value) {
            addCriterion("eor_source_id like", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdNotLike(String value) {
            addCriterion("eor_source_id not like", value, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdIn(List<String> values) {
            addCriterion("eor_source_id in", values, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdNotIn(List<String> values) {
            addCriterion("eor_source_id not in", values, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdBetween(String value1, String value2) {
            addCriterion("eor_source_id between", value1, value2, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceIdNotBetween(String value1, String value2) {
            addCriterion("eor_source_id not between", value1, value2, "eorSourceId");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeIsNull() {
            addCriterion("eor_source_type is null");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeIsNotNull() {
            addCriterion("eor_source_type is not null");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeEqualTo(String value) {
            addCriterion("eor_source_type =", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_type = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeNotEqualTo(String value) {
            addCriterion("eor_source_type <>", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_type <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeGreaterThan(String value) {
            addCriterion("eor_source_type >", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_type > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeGreaterThanOrEqualTo(String value) {
            addCriterion("eor_source_type >=", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_type >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeLessThan(String value) {
            addCriterion("eor_source_type <", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_type < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeLessThanOrEqualTo(String value) {
            addCriterion("eor_source_type <=", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_source_type <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeLike(String value) {
            addCriterion("eor_source_type like", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeNotLike(String value) {
            addCriterion("eor_source_type not like", value, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeIn(List<String> values) {
            addCriterion("eor_source_type in", values, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeNotIn(List<String> values) {
            addCriterion("eor_source_type not in", values, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeBetween(String value1, String value2) {
            addCriterion("eor_source_type between", value1, value2, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorSourceTypeNotBetween(String value1, String value2) {
            addCriterion("eor_source_type not between", value1, value2, "eorSourceType");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdIsNull() {
            addCriterion("eor_pay_order_id is null");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdIsNotNull() {
            addCriterion("eor_pay_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdEqualTo(String value) {
            addCriterion("eor_pay_order_id =", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_pay_order_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdNotEqualTo(String value) {
            addCriterion("eor_pay_order_id <>", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_pay_order_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdGreaterThan(String value) {
            addCriterion("eor_pay_order_id >", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_pay_order_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("eor_pay_order_id >=", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_pay_order_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdLessThan(String value) {
            addCriterion("eor_pay_order_id <", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_pay_order_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdLessThanOrEqualTo(String value) {
            addCriterion("eor_pay_order_id <=", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_pay_order_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdLike(String value) {
            addCriterion("eor_pay_order_id like", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdNotLike(String value) {
            addCriterion("eor_pay_order_id not like", value, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdIn(List<String> values) {
            addCriterion("eor_pay_order_id in", values, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdNotIn(List<String> values) {
            addCriterion("eor_pay_order_id not in", values, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdBetween(String value1, String value2) {
            addCriterion("eor_pay_order_id between", value1, value2, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorPayOrderIdNotBetween(String value1, String value2) {
            addCriterion("eor_pay_order_id not between", value1, value2, "eorPayOrderId");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoIsNull() {
            addCriterion("eor_trans_order_no is null");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoIsNotNull() {
            addCriterion("eor_trans_order_no is not null");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoEqualTo(String value) {
            addCriterion("eor_trans_order_no =", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_trans_order_no = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoNotEqualTo(String value) {
            addCriterion("eor_trans_order_no <>", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_trans_order_no <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoGreaterThan(String value) {
            addCriterion("eor_trans_order_no >", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_trans_order_no > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("eor_trans_order_no >=", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_trans_order_no >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoLessThan(String value) {
            addCriterion("eor_trans_order_no <", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_trans_order_no < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoLessThanOrEqualTo(String value) {
            addCriterion("eor_trans_order_no <=", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_trans_order_no <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoLike(String value) {
            addCriterion("eor_trans_order_no like", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoNotLike(String value) {
            addCriterion("eor_trans_order_no not like", value, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoIn(List<String> values) {
            addCriterion("eor_trans_order_no in", values, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoNotIn(List<String> values) {
            addCriterion("eor_trans_order_no not in", values, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoBetween(String value1, String value2) {
            addCriterion("eor_trans_order_no between", value1, value2, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorTransOrderNoNotBetween(String value1, String value2) {
            addCriterion("eor_trans_order_no not between", value1, value2, "eorTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andEorSubjectIsNull() {
            addCriterion("eor_subject is null");
            return (Criteria) this;
        }

        public Criteria andEorSubjectIsNotNull() {
            addCriterion("eor_subject is not null");
            return (Criteria) this;
        }

        public Criteria andEorSubjectEqualTo(String value) {
            addCriterion("eor_subject =", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_subject = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSubjectNotEqualTo(String value) {
            addCriterion("eor_subject <>", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_subject <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSubjectGreaterThan(String value) {
            addCriterion("eor_subject >", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_subject > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSubjectGreaterThanOrEqualTo(String value) {
            addCriterion("eor_subject >=", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_subject >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSubjectLessThan(String value) {
            addCriterion("eor_subject <", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_subject < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSubjectLessThanOrEqualTo(String value) {
            addCriterion("eor_subject <=", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_subject <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorSubjectLike(String value) {
            addCriterion("eor_subject like", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectNotLike(String value) {
            addCriterion("eor_subject not like", value, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectIn(List<String> values) {
            addCriterion("eor_subject in", values, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectNotIn(List<String> values) {
            addCriterion("eor_subject not in", values, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectBetween(String value1, String value2) {
            addCriterion("eor_subject between", value1, value2, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorSubjectNotBetween(String value1, String value2) {
            addCriterion("eor_subject not between", value1, value2, "eorSubject");
            return (Criteria) this;
        }

        public Criteria andEorAmountIsNull() {
            addCriterion("eor_amount is null");
            return (Criteria) this;
        }

        public Criteria andEorAmountIsNotNull() {
            addCriterion("eor_amount is not null");
            return (Criteria) this;
        }

        public Criteria andEorAmountEqualTo(BigDecimal value) {
            addCriterion("eor_amount =", value, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_amount = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorAmountNotEqualTo(BigDecimal value) {
            addCriterion("eor_amount <>", value, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_amount <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorAmountGreaterThan(BigDecimal value) {
            addCriterion("eor_amount >", value, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_amount > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("eor_amount >=", value, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_amount >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorAmountLessThan(BigDecimal value) {
            addCriterion("eor_amount <", value, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_amount < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("eor_amount <=", value, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_amount <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorAmountIn(List<BigDecimal> values) {
            addCriterion("eor_amount in", values, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountNotIn(List<BigDecimal> values) {
            addCriterion("eor_amount not in", values, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("eor_amount between", value1, value2, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("eor_amount not between", value1, value2, "eorAmount");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdIsNull() {
            addCriterion("eor_buyer_id is null");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdIsNotNull() {
            addCriterion("eor_buyer_id is not null");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdEqualTo(String value) {
            addCriterion("eor_buyer_id =", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_buyer_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdNotEqualTo(String value) {
            addCriterion("eor_buyer_id <>", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_buyer_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdGreaterThan(String value) {
            addCriterion("eor_buyer_id >", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_buyer_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdGreaterThanOrEqualTo(String value) {
            addCriterion("eor_buyer_id >=", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_buyer_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdLessThan(String value) {
            addCriterion("eor_buyer_id <", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_buyer_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdLessThanOrEqualTo(String value) {
            addCriterion("eor_buyer_id <=", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_buyer_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdLike(String value) {
            addCriterion("eor_buyer_id like", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdNotLike(String value) {
            addCriterion("eor_buyer_id not like", value, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdIn(List<String> values) {
            addCriterion("eor_buyer_id in", values, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdNotIn(List<String> values) {
            addCriterion("eor_buyer_id not in", values, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdBetween(String value1, String value2) {
            addCriterion("eor_buyer_id between", value1, value2, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorBuyerIdNotBetween(String value1, String value2) {
            addCriterion("eor_buyer_id not between", value1, value2, "eorBuyerId");
            return (Criteria) this;
        }

        public Criteria andEorStateIsNull() {
            addCriterion("eor_state is null");
            return (Criteria) this;
        }

        public Criteria andEorStateIsNotNull() {
            addCriterion("eor_state is not null");
            return (Criteria) this;
        }

        public Criteria andEorStateEqualTo(String value) {
            addCriterion("eor_state =", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorStateNotEqualTo(String value) {
            addCriterion("eor_state <>", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorStateGreaterThan(String value) {
            addCriterion("eor_state >", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorStateGreaterThanOrEqualTo(String value) {
            addCriterion("eor_state >=", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorStateLessThan(String value) {
            addCriterion("eor_state <", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorStateLessThanOrEqualTo(String value) {
            addCriterion("eor_state <=", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorStateLike(String value) {
            addCriterion("eor_state like", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateNotLike(String value) {
            addCriterion("eor_state not like", value, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateIn(List<String> values) {
            addCriterion("eor_state in", values, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateNotIn(List<String> values) {
            addCriterion("eor_state not in", values, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateBetween(String value1, String value2) {
            addCriterion("eor_state between", value1, value2, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorStateNotBetween(String value1, String value2) {
            addCriterion("eor_state not between", value1, value2, "eorState");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeIsNull() {
            addCriterion("eor_create_time is null");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeIsNotNull() {
            addCriterion("eor_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeEqualTo(Date value) {
            addCriterion("eor_create_time =", value, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeNotEqualTo(Date value) {
            addCriterion("eor_create_time <>", value, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeGreaterThan(Date value) {
            addCriterion("eor_create_time >", value, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("eor_create_time >=", value, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeLessThan(Date value) {
            addCriterion("eor_create_time <", value, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("eor_create_time <=", value, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeIn(List<Date> values) {
            addCriterion("eor_create_time in", values, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeNotIn(List<Date> values) {
            addCriterion("eor_create_time not in", values, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeBetween(Date value1, Date value2) {
            addCriterion("eor_create_time between", value1, value2, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("eor_create_time not between", value1, value2, "eorCreateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeIsNull() {
            addCriterion("eor_update_time is null");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeIsNotNull() {
            addCriterion("eor_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeEqualTo(Date value) {
            addCriterion("eor_update_time =", value, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeNotEqualTo(Date value) {
            addCriterion("eor_update_time <>", value, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeNotEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeGreaterThan(Date value) {
            addCriterion("eor_update_time >", value, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeGreaterThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("eor_update_time >=", value, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeGreaterThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeLessThan(Date value) {
            addCriterion("eor_update_time <", value, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeLessThanColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("eor_update_time <=", value, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeLessThanOrEqualToColumn(EasyOrder.Column column) {
            addCriterion(new StringBuilder("eor_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeIn(List<Date> values) {
            addCriterion("eor_update_time in", values, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeNotIn(List<Date> values) {
            addCriterion("eor_update_time not in", values, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("eor_update_time between", value1, value2, "eorUpdateTime");
            return (Criteria) this;
        }

        public Criteria andEorUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("eor_update_time not between", value1, value2, "eorUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private EasyOrderExample example;

        protected Criteria(EasyOrderExample example) {
            super();
            this.example = example;
        }

        public EasyOrderExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.EasyOrderExample example);
    }
}