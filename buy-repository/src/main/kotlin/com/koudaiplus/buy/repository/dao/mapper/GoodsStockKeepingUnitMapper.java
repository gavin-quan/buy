package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnit;
import com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnitExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GoodsStockKeepingUnitMapper {
    long countByExample(GoodsStockKeepingUnitExample example);

    int deleteByExample(GoodsStockKeepingUnitExample example);

    int deleteByPrimaryKey(String mguKeyId);

    int insert(GoodsStockKeepingUnit record);

    int insertSelective(@Param("record") GoodsStockKeepingUnit record, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    GoodsStockKeepingUnit selectOneByExample(GoodsStockKeepingUnitExample example);

    GoodsStockKeepingUnit selectOneByExampleSelective(@Param("example") GoodsStockKeepingUnitExample example, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    List<GoodsStockKeepingUnit> selectByExampleSelective(@Param("example") GoodsStockKeepingUnitExample example, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    List<GoodsStockKeepingUnit> selectByExample(GoodsStockKeepingUnitExample example);

    GoodsStockKeepingUnit selectByPrimaryKeySelective(@Param("mguKeyId") String mguKeyId, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    GoodsStockKeepingUnit selectByPrimaryKey(String mguKeyId);

    int updateByExampleSelective(@Param("record") GoodsStockKeepingUnit record, @Param("example") GoodsStockKeepingUnitExample example, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    int updateByExample(@Param("record") GoodsStockKeepingUnit record, @Param("example") GoodsStockKeepingUnitExample example);

    int updateByPrimaryKeySelective(@Param("record") GoodsStockKeepingUnit record, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    int updateByPrimaryKey(GoodsStockKeepingUnit record);

    int batchInsert(@Param("list") List<GoodsStockKeepingUnit> list);

    int batchInsertSelective(@Param("list") List<GoodsStockKeepingUnit> list, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    int upsert(GoodsStockKeepingUnit record);

    int upsertByExample(@Param("record") GoodsStockKeepingUnit record, @Param("example") GoodsStockKeepingUnitExample example);

    int upsertByExampleSelective(@Param("record") GoodsStockKeepingUnit record, @Param("example") GoodsStockKeepingUnitExample example, @Param("selective") GoodsStockKeepingUnit.Column ... selective);

    int upsertSelective(@Param("record") GoodsStockKeepingUnit record, @Param("selective") GoodsStockKeepingUnit.Column ... selective);
}