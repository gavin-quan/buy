package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoodsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public GoodsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public GoodsExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public GoodsExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public GoodsExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public GoodsExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public GoodsExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        GoodsExample example = new GoodsExample();
        return example.createCriteria();
    }

    public GoodsExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public GoodsExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMgsKeyIdIsNull() {
            addCriterion("mgs_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdIsNotNull() {
            addCriterion("mgs_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdEqualTo(String value) {
            addCriterion("mgs_key_id =", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdNotEqualTo(String value) {
            addCriterion("mgs_key_id <>", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdGreaterThan(String value) {
            addCriterion("mgs_key_id >", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mgs_key_id >=", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdLessThan(String value) {
            addCriterion("mgs_key_id <", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mgs_key_id <=", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdLike(String value) {
            addCriterion("mgs_key_id like", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdNotLike(String value) {
            addCriterion("mgs_key_id not like", value, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdIn(List<String> values) {
            addCriterion("mgs_key_id in", values, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdNotIn(List<String> values) {
            addCriterion("mgs_key_id not in", values, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdBetween(String value1, String value2) {
            addCriterion("mgs_key_id between", value1, value2, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsKeyIdNotBetween(String value1, String value2) {
            addCriterion("mgs_key_id not between", value1, value2, "mgsKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsIdIsNull() {
            addCriterion("mgs_id is null");
            return (Criteria) this;
        }

        public Criteria andMgsIdIsNotNull() {
            addCriterion("mgs_id is not null");
            return (Criteria) this;
        }

        public Criteria andMgsIdEqualTo(Long value) {
            addCriterion("mgs_id =", value, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsIdNotEqualTo(Long value) {
            addCriterion("mgs_id <>", value, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsIdGreaterThan(Long value) {
            addCriterion("mgs_id >", value, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsIdGreaterThanOrEqualTo(Long value) {
            addCriterion("mgs_id >=", value, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsIdLessThan(Long value) {
            addCriterion("mgs_id <", value, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsIdLessThanOrEqualTo(Long value) {
            addCriterion("mgs_id <=", value, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsIdIn(List<Long> values) {
            addCriterion("mgs_id in", values, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdNotIn(List<Long> values) {
            addCriterion("mgs_id not in", values, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdBetween(Long value1, Long value2) {
            addCriterion("mgs_id between", value1, value2, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsIdNotBetween(Long value1, Long value2) {
            addCriterion("mgs_id not between", value1, value2, "mgsId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdIsNull() {
            addCriterion("mgs_shop_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdIsNotNull() {
            addCriterion("mgs_shop_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdEqualTo(String value) {
            addCriterion("mgs_shop_key_id =", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_shop_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdNotEqualTo(String value) {
            addCriterion("mgs_shop_key_id <>", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_shop_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdGreaterThan(String value) {
            addCriterion("mgs_shop_key_id >", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_shop_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mgs_shop_key_id >=", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_shop_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdLessThan(String value) {
            addCriterion("mgs_shop_key_id <", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_shop_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mgs_shop_key_id <=", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_shop_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdLike(String value) {
            addCriterion("mgs_shop_key_id like", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdNotLike(String value) {
            addCriterion("mgs_shop_key_id not like", value, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdIn(List<String> values) {
            addCriterion("mgs_shop_key_id in", values, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdNotIn(List<String> values) {
            addCriterion("mgs_shop_key_id not in", values, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdBetween(String value1, String value2) {
            addCriterion("mgs_shop_key_id between", value1, value2, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsShopKeyIdNotBetween(String value1, String value2) {
            addCriterion("mgs_shop_key_id not between", value1, value2, "mgsShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMgsNameIsNull() {
            addCriterion("mgs_name is null");
            return (Criteria) this;
        }

        public Criteria andMgsNameIsNotNull() {
            addCriterion("mgs_name is not null");
            return (Criteria) this;
        }

        public Criteria andMgsNameEqualTo(String value) {
            addCriterion("mgs_name =", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_name = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsNameNotEqualTo(String value) {
            addCriterion("mgs_name <>", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_name <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsNameGreaterThan(String value) {
            addCriterion("mgs_name >", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_name > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsNameGreaterThanOrEqualTo(String value) {
            addCriterion("mgs_name >=", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_name >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsNameLessThan(String value) {
            addCriterion("mgs_name <", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_name < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsNameLessThanOrEqualTo(String value) {
            addCriterion("mgs_name <=", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_name <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsNameLike(String value) {
            addCriterion("mgs_name like", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameNotLike(String value) {
            addCriterion("mgs_name not like", value, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameIn(List<String> values) {
            addCriterion("mgs_name in", values, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameNotIn(List<String> values) {
            addCriterion("mgs_name not in", values, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameBetween(String value1, String value2) {
            addCriterion("mgs_name between", value1, value2, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsNameNotBetween(String value1, String value2) {
            addCriterion("mgs_name not between", value1, value2, "mgsName");
            return (Criteria) this;
        }

        public Criteria andMgsCoverIsNull() {
            addCriterion("mgs_cover is null");
            return (Criteria) this;
        }

        public Criteria andMgsCoverIsNotNull() {
            addCriterion("mgs_cover is not null");
            return (Criteria) this;
        }

        public Criteria andMgsCoverEqualTo(String value) {
            addCriterion("mgs_cover =", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_cover = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCoverNotEqualTo(String value) {
            addCriterion("mgs_cover <>", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_cover <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCoverGreaterThan(String value) {
            addCriterion("mgs_cover >", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_cover > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCoverGreaterThanOrEqualTo(String value) {
            addCriterion("mgs_cover >=", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_cover >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCoverLessThan(String value) {
            addCriterion("mgs_cover <", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_cover < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCoverLessThanOrEqualTo(String value) {
            addCriterion("mgs_cover <=", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_cover <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCoverLike(String value) {
            addCriterion("mgs_cover like", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverNotLike(String value) {
            addCriterion("mgs_cover not like", value, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverIn(List<String> values) {
            addCriterion("mgs_cover in", values, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverNotIn(List<String> values) {
            addCriterion("mgs_cover not in", values, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverBetween(String value1, String value2) {
            addCriterion("mgs_cover between", value1, value2, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsCoverNotBetween(String value1, String value2) {
            addCriterion("mgs_cover not between", value1, value2, "mgsCover");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewIsNull() {
            addCriterion("mgs_preview is null");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewIsNotNull() {
            addCriterion("mgs_preview is not null");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewEqualTo(String value) {
            addCriterion("mgs_preview =", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_preview = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsPreviewNotEqualTo(String value) {
            addCriterion("mgs_preview <>", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_preview <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsPreviewGreaterThan(String value) {
            addCriterion("mgs_preview >", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_preview > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsPreviewGreaterThanOrEqualTo(String value) {
            addCriterion("mgs_preview >=", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_preview >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsPreviewLessThan(String value) {
            addCriterion("mgs_preview <", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_preview < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsPreviewLessThanOrEqualTo(String value) {
            addCriterion("mgs_preview <=", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_preview <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsPreviewLike(String value) {
            addCriterion("mgs_preview like", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewNotLike(String value) {
            addCriterion("mgs_preview not like", value, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewIn(List<String> values) {
            addCriterion("mgs_preview in", values, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewNotIn(List<String> values) {
            addCriterion("mgs_preview not in", values, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewBetween(String value1, String value2) {
            addCriterion("mgs_preview between", value1, value2, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsPreviewNotBetween(String value1, String value2) {
            addCriterion("mgs_preview not between", value1, value2, "mgsPreview");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceIsNull() {
            addCriterion("mgs_min_price is null");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceIsNotNull() {
            addCriterion("mgs_min_price is not null");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceEqualTo(BigDecimal value) {
            addCriterion("mgs_min_price =", value, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_min_price = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceNotEqualTo(BigDecimal value) {
            addCriterion("mgs_min_price <>", value, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_min_price <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceGreaterThan(BigDecimal value) {
            addCriterion("mgs_min_price >", value, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_min_price > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mgs_min_price >=", value, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_min_price >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceLessThan(BigDecimal value) {
            addCriterion("mgs_min_price <", value, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_min_price < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mgs_min_price <=", value, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_min_price <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceIn(List<BigDecimal> values) {
            addCriterion("mgs_min_price in", values, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceNotIn(List<BigDecimal> values) {
            addCriterion("mgs_min_price not in", values, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgs_min_price between", value1, value2, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMinPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgs_min_price not between", value1, value2, "mgsMinPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceIsNull() {
            addCriterion("mgs_max_price is null");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceIsNotNull() {
            addCriterion("mgs_max_price is not null");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceEqualTo(BigDecimal value) {
            addCriterion("mgs_max_price =", value, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_max_price = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceNotEqualTo(BigDecimal value) {
            addCriterion("mgs_max_price <>", value, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_max_price <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceGreaterThan(BigDecimal value) {
            addCriterion("mgs_max_price >", value, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_max_price > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mgs_max_price >=", value, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_max_price >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceLessThan(BigDecimal value) {
            addCriterion("mgs_max_price <", value, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_max_price < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mgs_max_price <=", value, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_max_price <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceIn(List<BigDecimal> values) {
            addCriterion("mgs_max_price in", values, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceNotIn(List<BigDecimal> values) {
            addCriterion("mgs_max_price not in", values, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgs_max_price between", value1, value2, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsMaxPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mgs_max_price not between", value1, value2, "mgsMaxPrice");
            return (Criteria) this;
        }

        public Criteria andMgsContentIsNull() {
            addCriterion("mgs_content is null");
            return (Criteria) this;
        }

        public Criteria andMgsContentIsNotNull() {
            addCriterion("mgs_content is not null");
            return (Criteria) this;
        }

        public Criteria andMgsContentEqualTo(String value) {
            addCriterion("mgs_content =", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_content = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsContentNotEqualTo(String value) {
            addCriterion("mgs_content <>", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_content <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsContentGreaterThan(String value) {
            addCriterion("mgs_content >", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_content > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsContentGreaterThanOrEqualTo(String value) {
            addCriterion("mgs_content >=", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_content >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsContentLessThan(String value) {
            addCriterion("mgs_content <", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_content < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsContentLessThanOrEqualTo(String value) {
            addCriterion("mgs_content <=", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_content <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsContentLike(String value) {
            addCriterion("mgs_content like", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentNotLike(String value) {
            addCriterion("mgs_content not like", value, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentIn(List<String> values) {
            addCriterion("mgs_content in", values, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentNotIn(List<String> values) {
            addCriterion("mgs_content not in", values, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentBetween(String value1, String value2) {
            addCriterion("mgs_content between", value1, value2, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsContentNotBetween(String value1, String value2) {
            addCriterion("mgs_content not between", value1, value2, "mgsContent");
            return (Criteria) this;
        }

        public Criteria andMgsStateIsNull() {
            addCriterion("mgs_state is null");
            return (Criteria) this;
        }

        public Criteria andMgsStateIsNotNull() {
            addCriterion("mgs_state is not null");
            return (Criteria) this;
        }

        public Criteria andMgsStateEqualTo(String value) {
            addCriterion("mgs_state =", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsStateNotEqualTo(String value) {
            addCriterion("mgs_state <>", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsStateGreaterThan(String value) {
            addCriterion("mgs_state >", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsStateGreaterThanOrEqualTo(String value) {
            addCriterion("mgs_state >=", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsStateLessThan(String value) {
            addCriterion("mgs_state <", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsStateLessThanOrEqualTo(String value) {
            addCriterion("mgs_state <=", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsStateLike(String value) {
            addCriterion("mgs_state like", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateNotLike(String value) {
            addCriterion("mgs_state not like", value, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateIn(List<String> values) {
            addCriterion("mgs_state in", values, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateNotIn(List<String> values) {
            addCriterion("mgs_state not in", values, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateBetween(String value1, String value2) {
            addCriterion("mgs_state between", value1, value2, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsStateNotBetween(String value1, String value2) {
            addCriterion("mgs_state not between", value1, value2, "mgsState");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeIsNull() {
            addCriterion("mgs_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeIsNotNull() {
            addCriterion("mgs_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeEqualTo(Date value) {
            addCriterion("mgs_create_time =", value, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeNotEqualTo(Date value) {
            addCriterion("mgs_create_time <>", value, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeGreaterThan(Date value) {
            addCriterion("mgs_create_time >", value, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mgs_create_time >=", value, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeLessThan(Date value) {
            addCriterion("mgs_create_time <", value, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mgs_create_time <=", value, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeIn(List<Date> values) {
            addCriterion("mgs_create_time in", values, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeNotIn(List<Date> values) {
            addCriterion("mgs_create_time not in", values, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mgs_create_time between", value1, value2, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mgs_create_time not between", value1, value2, "mgsCreateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeIsNull() {
            addCriterion("mgs_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeIsNotNull() {
            addCriterion("mgs_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeEqualTo(Date value) {
            addCriterion("mgs_update_time =", value, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeNotEqualTo(Date value) {
            addCriterion("mgs_update_time <>", value, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeNotEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeGreaterThan(Date value) {
            addCriterion("mgs_update_time >", value, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeGreaterThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mgs_update_time >=", value, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeGreaterThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeLessThan(Date value) {
            addCriterion("mgs_update_time <", value, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeLessThanColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mgs_update_time <=", value, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeLessThanOrEqualToColumn(Goods.Column column) {
            addCriterion(new StringBuilder("mgs_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeIn(List<Date> values) {
            addCriterion("mgs_update_time in", values, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeNotIn(List<Date> values) {
            addCriterion("mgs_update_time not in", values, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("mgs_update_time between", value1, value2, "mgsUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMgsUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mgs_update_time not between", value1, value2, "mgsUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private GoodsExample example;

        protected Criteria(GoodsExample example) {
            super();
            this.example = example;
        }

        public GoodsExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.GoodsExample example);
    }
}