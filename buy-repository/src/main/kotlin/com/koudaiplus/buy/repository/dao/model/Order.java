package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Order {
    private String morKeyId;

    private Long morId;

    private String morBatchId;

    private String morShopKeyId;

    private String morPayOrderId;

    private String morTransOrderNo;

    private String morExpressId;

    private String morSubject;

    private BigDecimal morAmount;

    private String morBuyerId;

    private String morState;

    private Date morCreateTime;

    private Date morUpdateTime;

    public String getMorKeyId() {
        return morKeyId;
    }

    public void setMorKeyId(String morKeyId) {
        this.morKeyId = morKeyId == null ? null : morKeyId.trim();
    }

    public Long getMorId() {
        return morId;
    }

    public void setMorId(Long morId) {
        this.morId = morId;
    }

    public String getMorBatchId() {
        return morBatchId;
    }

    public void setMorBatchId(String morBatchId) {
        this.morBatchId = morBatchId == null ? null : morBatchId.trim();
    }

    public String getMorShopKeyId() {
        return morShopKeyId;
    }

    public void setMorShopKeyId(String morShopKeyId) {
        this.morShopKeyId = morShopKeyId == null ? null : morShopKeyId.trim();
    }

    public String getMorPayOrderId() {
        return morPayOrderId;
    }

    public void setMorPayOrderId(String morPayOrderId) {
        this.morPayOrderId = morPayOrderId == null ? null : morPayOrderId.trim();
    }

    public String getMorTransOrderNo() {
        return morTransOrderNo;
    }

    public void setMorTransOrderNo(String morTransOrderNo) {
        this.morTransOrderNo = morTransOrderNo == null ? null : morTransOrderNo.trim();
    }

    public String getMorExpressId() {
        return morExpressId;
    }

    public void setMorExpressId(String morExpressId) {
        this.morExpressId = morExpressId == null ? null : morExpressId.trim();
    }

    public String getMorSubject() {
        return morSubject;
    }

    public void setMorSubject(String morSubject) {
        this.morSubject = morSubject == null ? null : morSubject.trim();
    }

    public BigDecimal getMorAmount() {
        return morAmount;
    }

    public void setMorAmount(BigDecimal morAmount) {
        this.morAmount = morAmount;
    }

    public String getMorBuyerId() {
        return morBuyerId;
    }

    public void setMorBuyerId(String morBuyerId) {
        this.morBuyerId = morBuyerId == null ? null : morBuyerId.trim();
    }

    public String getMorState() {
        return morState;
    }

    public void setMorState(String morState) {
        this.morState = morState == null ? null : morState.trim();
    }

    public Date getMorCreateTime() {
        return morCreateTime;
    }

    public void setMorCreateTime(Date morCreateTime) {
        this.morCreateTime = morCreateTime;
    }

    public Date getMorUpdateTime() {
        return morUpdateTime;
    }

    public void setMorUpdateTime(Date morUpdateTime) {
        this.morUpdateTime = morUpdateTime;
    }

    public static Order.Builder builder() {
        return new Order.Builder();
    }

    public static class Builder {
        private Order obj;

        public Builder() {
            this.obj = new Order();
        }

        public Builder morKeyId(String morKeyId) {
            obj.setMorKeyId(morKeyId);
            return this;
        }

        public Builder morId(Long morId) {
            obj.setMorId(morId);
            return this;
        }

        public Builder morBatchId(String morBatchId) {
            obj.setMorBatchId(morBatchId);
            return this;
        }

        public Builder morShopKeyId(String morShopKeyId) {
            obj.setMorShopKeyId(morShopKeyId);
            return this;
        }

        public Builder morPayOrderId(String morPayOrderId) {
            obj.setMorPayOrderId(morPayOrderId);
            return this;
        }

        public Builder morTransOrderNo(String morTransOrderNo) {
            obj.setMorTransOrderNo(morTransOrderNo);
            return this;
        }

        public Builder morExpressId(String morExpressId) {
            obj.setMorExpressId(morExpressId);
            return this;
        }

        public Builder morSubject(String morSubject) {
            obj.setMorSubject(morSubject);
            return this;
        }

        public Builder morAmount(BigDecimal morAmount) {
            obj.setMorAmount(morAmount);
            return this;
        }

        public Builder morBuyerId(String morBuyerId) {
            obj.setMorBuyerId(morBuyerId);
            return this;
        }

        public Builder morState(String morState) {
            obj.setMorState(morState);
            return this;
        }

        public Builder morCreateTime(Date morCreateTime) {
            obj.setMorCreateTime(morCreateTime);
            return this;
        }

        public Builder morUpdateTime(Date morUpdateTime) {
            obj.setMorUpdateTime(morUpdateTime);
            return this;
        }

        public Order build() {
            return this.obj;
        }
    }

    public enum Column {
        morKeyId("mor_key_id", "morKeyId", "VARCHAR", false),
        morId("mor_id", "morId", "BIGINT", false),
        morBatchId("mor_batch_id", "morBatchId", "VARCHAR", false),
        morShopKeyId("mor_shop_key_id", "morShopKeyId", "VARCHAR", false),
        morPayOrderId("mor_pay_order_id", "morPayOrderId", "VARCHAR", false),
        morTransOrderNo("mor_trans_order_no", "morTransOrderNo", "VARCHAR", false),
        morExpressId("mor_express_id", "morExpressId", "VARCHAR", false),
        morSubject("mor_subject", "morSubject", "VARCHAR", false),
        morAmount("mor_amount", "morAmount", "DECIMAL", false),
        morBuyerId("mor_buyer_id", "morBuyerId", "VARCHAR", false),
        morState("mor_state", "morState", "VARCHAR", false),
        morCreateTime("mor_create_time", "morCreateTime", "TIMESTAMP", false),
        morUpdateTime("mor_update_time", "morUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}