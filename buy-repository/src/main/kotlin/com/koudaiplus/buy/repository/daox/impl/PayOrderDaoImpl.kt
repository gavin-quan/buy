package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.PayOrderMapper
import com.koudaiplus.buy.repository.dao.model.PayOrder
import com.koudaiplus.buy.repository.dao.model.PayOrderExample
import com.koudaiplus.buy.repository.daox.PayOrderDao
import com.koudaiplus.buy.repository.daox.model.PayOrderDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository;
import java.util.*

@Repository
class PayOrderDaoImpl(
       mapper: PayOrderMapper
) : DaoSupport<PayOrderMapper, PayOrderExample, PayOrderExample.Criteria, PayOrder.Builder, PayOrder>(mapper), PayOrderDao {

    override fun getInfoByKeyId(keyId: String): PayOrder? {
        return get { mapper, example, criteria ->
            criteria.andMpoKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun finishPay(param: PayOrderDaoModel.FinishPay) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .mpoTransOrderNo(param.transOrderNo)
                    .mpoFinishTime(param.finishTime)
                    .mpoState(param.toState)
                    .mpoUpdateTime(Date())
                    .build()
            criteria.andMpoKeyIdEqualTo(param.keyId).andMpoStateIn(param.fromState)
            mapper.updateByExampleSelective(e, example, PayOrder.Column.mpoTransOrderNo, PayOrder.Column.mpoFinishTime,
                    PayOrder.Column.mpoState, PayOrder.Column.mpoUpdateTime)
        }
    }

    override fun create(param: PayOrderDaoModel.Create) {
        insert { mapper, builder ->
            val e = builder
                    .mpoKeyId(param.keyId)
                    .mpoSubject(param.subject)
                    .mpoState(param.state)
                    .mpoAmount(param.amount)
                    .mpoCreateTime(Date())
                    .mpoUpdateTime(Date())
                    .build()
            mapper.insert(e)
        }
    }

    override fun getExample() = PayOrderExample()

    override fun getCriteria(example: PayOrderExample) = example.createCriteria()

    override fun getBuilder() = PayOrder.builder()

    override fun setPage(limit: Limit, example: PayOrderExample) {
        example.limit(limit.offset(), limit.pageSize())
    }
}
