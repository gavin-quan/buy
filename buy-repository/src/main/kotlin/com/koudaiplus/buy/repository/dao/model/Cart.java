package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Cart {
    private String mctKeyId;

    private String mctShopKeyId;

    private String mctGoodsKeyId;

    private String mctGoodsSkuId;

    private Integer mctQuantity;

    private String mctBuyerId;

    private Date mctCreateTime;

    private Date mctUpdateTime;

    protected final Map<String, Builder.Inc> incrementsColumnsInfoMap = new HashMap<String, Builder.Inc>();

    public String getMctKeyId() {
        return mctKeyId;
    }

    public void setMctKeyId(String mctKeyId) {
        this.mctKeyId = mctKeyId == null ? null : mctKeyId.trim();
    }

    public String getMctShopKeyId() {
        return mctShopKeyId;
    }

    public void setMctShopKeyId(String mctShopKeyId) {
        this.mctShopKeyId = mctShopKeyId == null ? null : mctShopKeyId.trim();
    }

    public String getMctGoodsKeyId() {
        return mctGoodsKeyId;
    }

    public void setMctGoodsKeyId(String mctGoodsKeyId) {
        this.mctGoodsKeyId = mctGoodsKeyId == null ? null : mctGoodsKeyId.trim();
    }

    public String getMctGoodsSkuId() {
        return mctGoodsSkuId;
    }

    public void setMctGoodsSkuId(String mctGoodsSkuId) {
        this.mctGoodsSkuId = mctGoodsSkuId == null ? null : mctGoodsSkuId.trim();
    }

    public Integer getMctQuantity() {
        return mctQuantity;
    }

    public void setMctQuantity(Integer mctQuantity) {
        this.mctQuantity = mctQuantity;
    }

    public String getMctBuyerId() {
        return mctBuyerId;
    }

    public void setMctBuyerId(String mctBuyerId) {
        this.mctBuyerId = mctBuyerId == null ? null : mctBuyerId.trim();
    }

    public Date getMctCreateTime() {
        return mctCreateTime;
    }

    public void setMctCreateTime(Date mctCreateTime) {
        this.mctCreateTime = mctCreateTime;
    }

    public Date getMctUpdateTime() {
        return mctUpdateTime;
    }

    public void setMctUpdateTime(Date mctUpdateTime) {
        this.mctUpdateTime = mctUpdateTime;
    }

    public static Cart.Builder builder() {
        return new Cart.Builder();
    }

    public Map<String, Builder.Inc> incrementsColumnsInfoMap() {
        return this.incrementsColumnsInfoMap;
    }

    public boolean hasIncsForColumn(String column) {
        return incrementsColumnsInfoMap.get(column) != null;
    }

    public static class Builder {
        private Cart obj;

        public Builder() {
            this.obj = new Cart();
        }

        public Builder mctKeyId(String mctKeyId) {
            obj.setMctKeyId(mctKeyId);
            return this;
        }

        public Builder mctShopKeyId(String mctShopKeyId) {
            obj.setMctShopKeyId(mctShopKeyId);
            return this;
        }

        public Builder mctGoodsKeyId(String mctGoodsKeyId) {
            obj.setMctGoodsKeyId(mctGoodsKeyId);
            return this;
        }

        public Builder mctGoodsSkuId(String mctGoodsSkuId) {
            obj.setMctGoodsSkuId(mctGoodsSkuId);
            return this;
        }

        public Builder mctQuantity(Integer mctQuantity) {
            obj.setMctQuantity(mctQuantity);
            return this;
        }

        public Builder mctQuantity(Integer mctQuantity, Builder.Inc inc) {
            obj.incrementsColumnsInfoMap.put("mct_quantity", inc);
            obj.setMctQuantity(mctQuantity);
            return this;
        }

        public Builder mctBuyerId(String mctBuyerId) {
            obj.setMctBuyerId(mctBuyerId);
            return this;
        }

        public Builder mctCreateTime(Date mctCreateTime) {
            obj.setMctCreateTime(mctCreateTime);
            return this;
        }

        public Builder mctUpdateTime(Date mctUpdateTime) {
            obj.setMctUpdateTime(mctUpdateTime);
            return this;
        }

        public Cart build() {
            return this.obj;
        }

        public enum Inc {
            INC("+"),
            DEC("-");

            private final String value;

            Inc(String value) {
                this.value = value;
            }

            public String getValue() {
                return this.value;
            }
        }
    }

    public enum Column {
        mctKeyId("mct_key_id", "mctKeyId", "VARCHAR", false),
        mctShopKeyId("mct_shop_key_id", "mctShopKeyId", "VARCHAR", false),
        mctGoodsKeyId("mct_goods_key_id", "mctGoodsKeyId", "VARCHAR", false),
        mctGoodsSkuId("mct_goods_sku_id", "mctGoodsSkuId", "VARCHAR", false),
        mctQuantity("mct_quantity", "mctQuantity", "INTEGER", false),
        mctBuyerId("mct_buyer_id", "mctBuyerId", "VARCHAR", false),
        mctCreateTime("mct_create_time", "mctCreateTime", "TIMESTAMP", false),
        mctUpdateTime("mct_update_time", "mctUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}