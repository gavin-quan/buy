package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderItemExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public OrderItemExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public OrderItemExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public OrderItemExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public OrderItemExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public OrderItemExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public OrderItemExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        OrderItemExample example = new OrderItemExample();
        return example.createCriteria();
    }

    public OrderItemExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public OrderItemExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMoiKeyIdIsNull() {
            addCriterion("moi_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdIsNotNull() {
            addCriterion("moi_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdEqualTo(String value) {
            addCriterion("moi_key_id =", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdNotEqualTo(String value) {
            addCriterion("moi_key_id <>", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdGreaterThan(String value) {
            addCriterion("moi_key_id >", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("moi_key_id >=", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdLessThan(String value) {
            addCriterion("moi_key_id <", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdLessThanOrEqualTo(String value) {
            addCriterion("moi_key_id <=", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdLike(String value) {
            addCriterion("moi_key_id like", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdNotLike(String value) {
            addCriterion("moi_key_id not like", value, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdIn(List<String> values) {
            addCriterion("moi_key_id in", values, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdNotIn(List<String> values) {
            addCriterion("moi_key_id not in", values, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdBetween(String value1, String value2) {
            addCriterion("moi_key_id between", value1, value2, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiKeyIdNotBetween(String value1, String value2) {
            addCriterion("moi_key_id not between", value1, value2, "moiKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiIdIsNull() {
            addCriterion("moi_id is null");
            return (Criteria) this;
        }

        public Criteria andMoiIdIsNotNull() {
            addCriterion("moi_id is not null");
            return (Criteria) this;
        }

        public Criteria andMoiIdEqualTo(Long value) {
            addCriterion("moi_id =", value, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiIdNotEqualTo(Long value) {
            addCriterion("moi_id <>", value, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiIdGreaterThan(Long value) {
            addCriterion("moi_id >", value, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiIdGreaterThanOrEqualTo(Long value) {
            addCriterion("moi_id >=", value, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiIdLessThan(Long value) {
            addCriterion("moi_id <", value, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiIdLessThanOrEqualTo(Long value) {
            addCriterion("moi_id <=", value, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiIdIn(List<Long> values) {
            addCriterion("moi_id in", values, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdNotIn(List<Long> values) {
            addCriterion("moi_id not in", values, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdBetween(Long value1, Long value2) {
            addCriterion("moi_id between", value1, value2, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiIdNotBetween(Long value1, Long value2) {
            addCriterion("moi_id not between", value1, value2, "moiId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdIsNull() {
            addCriterion("moi_shop_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdIsNotNull() {
            addCriterion("moi_shop_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdEqualTo(String value) {
            addCriterion("moi_shop_key_id =", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_shop_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdNotEqualTo(String value) {
            addCriterion("moi_shop_key_id <>", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_shop_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdGreaterThan(String value) {
            addCriterion("moi_shop_key_id >", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_shop_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("moi_shop_key_id >=", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_shop_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdLessThan(String value) {
            addCriterion("moi_shop_key_id <", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_shop_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdLessThanOrEqualTo(String value) {
            addCriterion("moi_shop_key_id <=", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_shop_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdLike(String value) {
            addCriterion("moi_shop_key_id like", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdNotLike(String value) {
            addCriterion("moi_shop_key_id not like", value, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdIn(List<String> values) {
            addCriterion("moi_shop_key_id in", values, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdNotIn(List<String> values) {
            addCriterion("moi_shop_key_id not in", values, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdBetween(String value1, String value2) {
            addCriterion("moi_shop_key_id between", value1, value2, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiShopKeyIdNotBetween(String value1, String value2) {
            addCriterion("moi_shop_key_id not between", value1, value2, "moiShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdIsNull() {
            addCriterion("moi_goods_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdIsNotNull() {
            addCriterion("moi_goods_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdEqualTo(String value) {
            addCriterion("moi_goods_key_id =", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdNotEqualTo(String value) {
            addCriterion("moi_goods_key_id <>", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdGreaterThan(String value) {
            addCriterion("moi_goods_key_id >", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("moi_goods_key_id >=", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdLessThan(String value) {
            addCriterion("moi_goods_key_id <", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdLessThanOrEqualTo(String value) {
            addCriterion("moi_goods_key_id <=", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdLike(String value) {
            addCriterion("moi_goods_key_id like", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdNotLike(String value) {
            addCriterion("moi_goods_key_id not like", value, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdIn(List<String> values) {
            addCriterion("moi_goods_key_id in", values, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdNotIn(List<String> values) {
            addCriterion("moi_goods_key_id not in", values, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdBetween(String value1, String value2) {
            addCriterion("moi_goods_key_id between", value1, value2, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsKeyIdNotBetween(String value1, String value2) {
            addCriterion("moi_goods_key_id not between", value1, value2, "moiGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdIsNull() {
            addCriterion("moi_goods_sku_id is null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdIsNotNull() {
            addCriterion("moi_goods_sku_id is not null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdEqualTo(String value) {
            addCriterion("moi_goods_sku_id =", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_sku_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdNotEqualTo(String value) {
            addCriterion("moi_goods_sku_id <>", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_sku_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdGreaterThan(String value) {
            addCriterion("moi_goods_sku_id >", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_sku_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdGreaterThanOrEqualTo(String value) {
            addCriterion("moi_goods_sku_id >=", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_sku_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdLessThan(String value) {
            addCriterion("moi_goods_sku_id <", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_sku_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdLessThanOrEqualTo(String value) {
            addCriterion("moi_goods_sku_id <=", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_sku_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdLike(String value) {
            addCriterion("moi_goods_sku_id like", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdNotLike(String value) {
            addCriterion("moi_goods_sku_id not like", value, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdIn(List<String> values) {
            addCriterion("moi_goods_sku_id in", values, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdNotIn(List<String> values) {
            addCriterion("moi_goods_sku_id not in", values, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdBetween(String value1, String value2) {
            addCriterion("moi_goods_sku_id between", value1, value2, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsSkuIdNotBetween(String value1, String value2) {
            addCriterion("moi_goods_sku_id not between", value1, value2, "moiGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdIsNull() {
            addCriterion("moi_order_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdIsNotNull() {
            addCriterion("moi_order_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdEqualTo(String value) {
            addCriterion("moi_order_key_id =", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_order_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdNotEqualTo(String value) {
            addCriterion("moi_order_key_id <>", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_order_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdGreaterThan(String value) {
            addCriterion("moi_order_key_id >", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_order_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("moi_order_key_id >=", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_order_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdLessThan(String value) {
            addCriterion("moi_order_key_id <", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_order_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdLessThanOrEqualTo(String value) {
            addCriterion("moi_order_key_id <=", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_order_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdLike(String value) {
            addCriterion("moi_order_key_id like", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdNotLike(String value) {
            addCriterion("moi_order_key_id not like", value, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdIn(List<String> values) {
            addCriterion("moi_order_key_id in", values, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdNotIn(List<String> values) {
            addCriterion("moi_order_key_id not in", values, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdBetween(String value1, String value2) {
            addCriterion("moi_order_key_id between", value1, value2, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiOrderKeyIdNotBetween(String value1, String value2) {
            addCriterion("moi_order_key_id not between", value1, value2, "moiOrderKeyId");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceIsNull() {
            addCriterion("moi_goods_price is null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceIsNotNull() {
            addCriterion("moi_goods_price is not null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceEqualTo(BigDecimal value) {
            addCriterion("moi_goods_price =", value, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_price = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceNotEqualTo(BigDecimal value) {
            addCriterion("moi_goods_price <>", value, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_price <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceGreaterThan(BigDecimal value) {
            addCriterion("moi_goods_price >", value, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_price > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("moi_goods_price >=", value, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_price >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceLessThan(BigDecimal value) {
            addCriterion("moi_goods_price <", value, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_price < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("moi_goods_price <=", value, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_price <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceIn(List<BigDecimal> values) {
            addCriterion("moi_goods_price in", values, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceNotIn(List<BigDecimal> values) {
            addCriterion("moi_goods_price not in", values, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("moi_goods_price between", value1, value2, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("moi_goods_price not between", value1, value2, "moiGoodsPrice");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityIsNull() {
            addCriterion("moi_goods_quantity is null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityIsNotNull() {
            addCriterion("moi_goods_quantity is not null");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityEqualTo(Integer value) {
            addCriterion("moi_goods_quantity =", value, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_quantity = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityNotEqualTo(Integer value) {
            addCriterion("moi_goods_quantity <>", value, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_quantity <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityGreaterThan(Integer value) {
            addCriterion("moi_goods_quantity >", value, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_quantity > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("moi_goods_quantity >=", value, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_quantity >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityLessThan(Integer value) {
            addCriterion("moi_goods_quantity <", value, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_quantity < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("moi_goods_quantity <=", value, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_goods_quantity <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityIn(List<Integer> values) {
            addCriterion("moi_goods_quantity in", values, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityNotIn(List<Integer> values) {
            addCriterion("moi_goods_quantity not in", values, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityBetween(Integer value1, Integer value2) {
            addCriterion("moi_goods_quantity between", value1, value2, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiGoodsQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("moi_goods_quantity not between", value1, value2, "moiGoodsQuantity");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceIsNull() {
            addCriterion("moi_total_price is null");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceIsNotNull() {
            addCriterion("moi_total_price is not null");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceEqualTo(BigDecimal value) {
            addCriterion("moi_total_price =", value, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_total_price = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceNotEqualTo(BigDecimal value) {
            addCriterion("moi_total_price <>", value, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_total_price <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceGreaterThan(BigDecimal value) {
            addCriterion("moi_total_price >", value, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_total_price > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("moi_total_price >=", value, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_total_price >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceLessThan(BigDecimal value) {
            addCriterion("moi_total_price <", value, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_total_price < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("moi_total_price <=", value, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_total_price <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceIn(List<BigDecimal> values) {
            addCriterion("moi_total_price in", values, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceNotIn(List<BigDecimal> values) {
            addCriterion("moi_total_price not in", values, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("moi_total_price between", value1, value2, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiTotalPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("moi_total_price not between", value1, value2, "moiTotalPrice");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateIsNull() {
            addCriterion("moi_stock_state is null");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateIsNotNull() {
            addCriterion("moi_stock_state is not null");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateEqualTo(String value) {
            addCriterion("moi_stock_state =", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_stock_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStockStateNotEqualTo(String value) {
            addCriterion("moi_stock_state <>", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_stock_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStockStateGreaterThan(String value) {
            addCriterion("moi_stock_state >", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_stock_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStockStateGreaterThanOrEqualTo(String value) {
            addCriterion("moi_stock_state >=", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_stock_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStockStateLessThan(String value) {
            addCriterion("moi_stock_state <", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_stock_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStockStateLessThanOrEqualTo(String value) {
            addCriterion("moi_stock_state <=", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_stock_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStockStateLike(String value) {
            addCriterion("moi_stock_state like", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateNotLike(String value) {
            addCriterion("moi_stock_state not like", value, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateIn(List<String> values) {
            addCriterion("moi_stock_state in", values, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateNotIn(List<String> values) {
            addCriterion("moi_stock_state not in", values, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateBetween(String value1, String value2) {
            addCriterion("moi_stock_state between", value1, value2, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStockStateNotBetween(String value1, String value2) {
            addCriterion("moi_stock_state not between", value1, value2, "moiStockState");
            return (Criteria) this;
        }

        public Criteria andMoiStateIsNull() {
            addCriterion("moi_state is null");
            return (Criteria) this;
        }

        public Criteria andMoiStateIsNotNull() {
            addCriterion("moi_state is not null");
            return (Criteria) this;
        }

        public Criteria andMoiStateEqualTo(String value) {
            addCriterion("moi_state =", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStateNotEqualTo(String value) {
            addCriterion("moi_state <>", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStateGreaterThan(String value) {
            addCriterion("moi_state >", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStateGreaterThanOrEqualTo(String value) {
            addCriterion("moi_state >=", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStateLessThan(String value) {
            addCriterion("moi_state <", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStateLessThanOrEqualTo(String value) {
            addCriterion("moi_state <=", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiStateLike(String value) {
            addCriterion("moi_state like", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateNotLike(String value) {
            addCriterion("moi_state not like", value, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateIn(List<String> values) {
            addCriterion("moi_state in", values, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateNotIn(List<String> values) {
            addCriterion("moi_state not in", values, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateBetween(String value1, String value2) {
            addCriterion("moi_state between", value1, value2, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiStateNotBetween(String value1, String value2) {
            addCriterion("moi_state not between", value1, value2, "moiState");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeIsNull() {
            addCriterion("moi_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeIsNotNull() {
            addCriterion("moi_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeEqualTo(Date value) {
            addCriterion("moi_create_time =", value, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeNotEqualTo(Date value) {
            addCriterion("moi_create_time <>", value, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeGreaterThan(Date value) {
            addCriterion("moi_create_time >", value, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("moi_create_time >=", value, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeLessThan(Date value) {
            addCriterion("moi_create_time <", value, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("moi_create_time <=", value, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeIn(List<Date> values) {
            addCriterion("moi_create_time in", values, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeNotIn(List<Date> values) {
            addCriterion("moi_create_time not in", values, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeBetween(Date value1, Date value2) {
            addCriterion("moi_create_time between", value1, value2, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("moi_create_time not between", value1, value2, "moiCreateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeIsNull() {
            addCriterion("moi_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeIsNotNull() {
            addCriterion("moi_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeEqualTo(Date value) {
            addCriterion("moi_update_time =", value, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeNotEqualTo(Date value) {
            addCriterion("moi_update_time <>", value, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeNotEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeGreaterThan(Date value) {
            addCriterion("moi_update_time >", value, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeGreaterThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("moi_update_time >=", value, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeGreaterThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeLessThan(Date value) {
            addCriterion("moi_update_time <", value, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeLessThanColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("moi_update_time <=", value, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeLessThanOrEqualToColumn(OrderItem.Column column) {
            addCriterion(new StringBuilder("moi_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeIn(List<Date> values) {
            addCriterion("moi_update_time in", values, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeNotIn(List<Date> values) {
            addCriterion("moi_update_time not in", values, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("moi_update_time between", value1, value2, "moiUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMoiUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("moi_update_time not between", value1, value2, "moiUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private OrderItemExample example;

        protected Criteria(OrderItemExample example) {
            super();
            this.example = example;
        }

        public OrderItemExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.OrderItemExample example);
    }
}