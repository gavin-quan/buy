package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CartExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public CartExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public CartExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public CartExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public CartExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public CartExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public CartExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        CartExample example = new CartExample();
        return example.createCriteria();
    }

    public CartExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public CartExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMctKeyIdIsNull() {
            addCriterion("mct_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdIsNotNull() {
            addCriterion("mct_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdEqualTo(String value) {
            addCriterion("mct_key_id =", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctKeyIdNotEqualTo(String value) {
            addCriterion("mct_key_id <>", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctKeyIdGreaterThan(String value) {
            addCriterion("mct_key_id >", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mct_key_id >=", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctKeyIdLessThan(String value) {
            addCriterion("mct_key_id <", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mct_key_id <=", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctKeyIdLike(String value) {
            addCriterion("mct_key_id like", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdNotLike(String value) {
            addCriterion("mct_key_id not like", value, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdIn(List<String> values) {
            addCriterion("mct_key_id in", values, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdNotIn(List<String> values) {
            addCriterion("mct_key_id not in", values, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdBetween(String value1, String value2) {
            addCriterion("mct_key_id between", value1, value2, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctKeyIdNotBetween(String value1, String value2) {
            addCriterion("mct_key_id not between", value1, value2, "mctKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdIsNull() {
            addCriterion("mct_shop_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdIsNotNull() {
            addCriterion("mct_shop_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdEqualTo(String value) {
            addCriterion("mct_shop_key_id =", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_shop_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdNotEqualTo(String value) {
            addCriterion("mct_shop_key_id <>", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_shop_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdGreaterThan(String value) {
            addCriterion("mct_shop_key_id >", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_shop_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mct_shop_key_id >=", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_shop_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdLessThan(String value) {
            addCriterion("mct_shop_key_id <", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_shop_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mct_shop_key_id <=", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_shop_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdLike(String value) {
            addCriterion("mct_shop_key_id like", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdNotLike(String value) {
            addCriterion("mct_shop_key_id not like", value, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdIn(List<String> values) {
            addCriterion("mct_shop_key_id in", values, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdNotIn(List<String> values) {
            addCriterion("mct_shop_key_id not in", values, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdBetween(String value1, String value2) {
            addCriterion("mct_shop_key_id between", value1, value2, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctShopKeyIdNotBetween(String value1, String value2) {
            addCriterion("mct_shop_key_id not between", value1, value2, "mctShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdIsNull() {
            addCriterion("mct_goods_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdIsNotNull() {
            addCriterion("mct_goods_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdEqualTo(String value) {
            addCriterion("mct_goods_key_id =", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdNotEqualTo(String value) {
            addCriterion("mct_goods_key_id <>", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdGreaterThan(String value) {
            addCriterion("mct_goods_key_id >", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mct_goods_key_id >=", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdLessThan(String value) {
            addCriterion("mct_goods_key_id <", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mct_goods_key_id <=", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdLike(String value) {
            addCriterion("mct_goods_key_id like", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdNotLike(String value) {
            addCriterion("mct_goods_key_id not like", value, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdIn(List<String> values) {
            addCriterion("mct_goods_key_id in", values, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdNotIn(List<String> values) {
            addCriterion("mct_goods_key_id not in", values, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdBetween(String value1, String value2) {
            addCriterion("mct_goods_key_id between", value1, value2, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsKeyIdNotBetween(String value1, String value2) {
            addCriterion("mct_goods_key_id not between", value1, value2, "mctGoodsKeyId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdIsNull() {
            addCriterion("mct_goods_sku_id is null");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdIsNotNull() {
            addCriterion("mct_goods_sku_id is not null");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdEqualTo(String value) {
            addCriterion("mct_goods_sku_id =", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_sku_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdNotEqualTo(String value) {
            addCriterion("mct_goods_sku_id <>", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_sku_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdGreaterThan(String value) {
            addCriterion("mct_goods_sku_id >", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_sku_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdGreaterThanOrEqualTo(String value) {
            addCriterion("mct_goods_sku_id >=", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_sku_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdLessThan(String value) {
            addCriterion("mct_goods_sku_id <", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_sku_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdLessThanOrEqualTo(String value) {
            addCriterion("mct_goods_sku_id <=", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_goods_sku_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdLike(String value) {
            addCriterion("mct_goods_sku_id like", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdNotLike(String value) {
            addCriterion("mct_goods_sku_id not like", value, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdIn(List<String> values) {
            addCriterion("mct_goods_sku_id in", values, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdNotIn(List<String> values) {
            addCriterion("mct_goods_sku_id not in", values, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdBetween(String value1, String value2) {
            addCriterion("mct_goods_sku_id between", value1, value2, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctGoodsSkuIdNotBetween(String value1, String value2) {
            addCriterion("mct_goods_sku_id not between", value1, value2, "mctGoodsSkuId");
            return (Criteria) this;
        }

        public Criteria andMctQuantityIsNull() {
            addCriterion("mct_quantity is null");
            return (Criteria) this;
        }

        public Criteria andMctQuantityIsNotNull() {
            addCriterion("mct_quantity is not null");
            return (Criteria) this;
        }

        public Criteria andMctQuantityEqualTo(Integer value) {
            addCriterion("mct_quantity =", value, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_quantity = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctQuantityNotEqualTo(Integer value) {
            addCriterion("mct_quantity <>", value, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_quantity <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctQuantityGreaterThan(Integer value) {
            addCriterion("mct_quantity >", value, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_quantity > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("mct_quantity >=", value, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_quantity >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctQuantityLessThan(Integer value) {
            addCriterion("mct_quantity <", value, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_quantity < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("mct_quantity <=", value, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_quantity <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctQuantityIn(List<Integer> values) {
            addCriterion("mct_quantity in", values, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityNotIn(List<Integer> values) {
            addCriterion("mct_quantity not in", values, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityBetween(Integer value1, Integer value2) {
            addCriterion("mct_quantity between", value1, value2, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("mct_quantity not between", value1, value2, "mctQuantity");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdIsNull() {
            addCriterion("mct_buyer_id is null");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdIsNotNull() {
            addCriterion("mct_buyer_id is not null");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdEqualTo(String value) {
            addCriterion("mct_buyer_id =", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_buyer_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdNotEqualTo(String value) {
            addCriterion("mct_buyer_id <>", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_buyer_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdGreaterThan(String value) {
            addCriterion("mct_buyer_id >", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_buyer_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdGreaterThanOrEqualTo(String value) {
            addCriterion("mct_buyer_id >=", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_buyer_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdLessThan(String value) {
            addCriterion("mct_buyer_id <", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_buyer_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdLessThanOrEqualTo(String value) {
            addCriterion("mct_buyer_id <=", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_buyer_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdLike(String value) {
            addCriterion("mct_buyer_id like", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdNotLike(String value) {
            addCriterion("mct_buyer_id not like", value, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdIn(List<String> values) {
            addCriterion("mct_buyer_id in", values, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdNotIn(List<String> values) {
            addCriterion("mct_buyer_id not in", values, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdBetween(String value1, String value2) {
            addCriterion("mct_buyer_id between", value1, value2, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctBuyerIdNotBetween(String value1, String value2) {
            addCriterion("mct_buyer_id not between", value1, value2, "mctBuyerId");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeIsNull() {
            addCriterion("mct_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeIsNotNull() {
            addCriterion("mct_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeEqualTo(Date value) {
            addCriterion("mct_create_time =", value, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeNotEqualTo(Date value) {
            addCriterion("mct_create_time <>", value, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeGreaterThan(Date value) {
            addCriterion("mct_create_time >", value, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mct_create_time >=", value, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeLessThan(Date value) {
            addCriterion("mct_create_time <", value, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mct_create_time <=", value, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeIn(List<Date> values) {
            addCriterion("mct_create_time in", values, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeNotIn(List<Date> values) {
            addCriterion("mct_create_time not in", values, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mct_create_time between", value1, value2, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mct_create_time not between", value1, value2, "mctCreateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeIsNull() {
            addCriterion("mct_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeIsNotNull() {
            addCriterion("mct_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeEqualTo(Date value) {
            addCriterion("mct_update_time =", value, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeNotEqualTo(Date value) {
            addCriterion("mct_update_time <>", value, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeNotEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeGreaterThan(Date value) {
            addCriterion("mct_update_time >", value, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeGreaterThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mct_update_time >=", value, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeGreaterThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeLessThan(Date value) {
            addCriterion("mct_update_time <", value, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeLessThanColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mct_update_time <=", value, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeLessThanOrEqualToColumn(Cart.Column column) {
            addCriterion(new StringBuilder("mct_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeIn(List<Date> values) {
            addCriterion("mct_update_time in", values, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeNotIn(List<Date> values) {
            addCriterion("mct_update_time not in", values, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("mct_update_time between", value1, value2, "mctUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMctUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mct_update_time not between", value1, value2, "mctUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private CartExample example;

        protected Criteria(CartExample example) {
            super();
            this.example = example;
        }

        public CartExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.CartExample example);
    }
}