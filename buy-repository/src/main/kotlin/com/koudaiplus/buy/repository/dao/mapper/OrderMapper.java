package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.Order;
import com.koudaiplus.buy.repository.dao.model.OrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
    long countByExample(OrderExample example);

    int deleteByExample(OrderExample example);

    int deleteByPrimaryKey(String morKeyId);

    int insert(Order record);

    int insertSelective(@Param("record") Order record, @Param("selective") Order.Column ... selective);

    Order selectOneByExample(OrderExample example);

    Order selectOneByExampleSelective(@Param("example") OrderExample example, @Param("selective") Order.Column ... selective);

    List<Order> selectByExampleSelective(@Param("example") OrderExample example, @Param("selective") Order.Column ... selective);

    List<Order> selectByExample(OrderExample example);

    Order selectByPrimaryKeySelective(@Param("morKeyId") String morKeyId, @Param("selective") Order.Column ... selective);

    Order selectByPrimaryKey(String morKeyId);

    int updateByExampleSelective(@Param("record") Order record, @Param("example") OrderExample example, @Param("selective") Order.Column ... selective);

    int updateByExample(@Param("record") Order record, @Param("example") OrderExample example);

    int updateByPrimaryKeySelective(@Param("record") Order record, @Param("selective") Order.Column ... selective);

    int updateByPrimaryKey(Order record);

    int batchInsert(@Param("list") List<Order> list);

    int batchInsertSelective(@Param("list") List<Order> list, @Param("selective") Order.Column ... selective);

    int upsert(Order record);

    int upsertByExample(@Param("record") Order record, @Param("example") OrderExample example);

    int upsertByExampleSelective(@Param("record") Order record, @Param("example") OrderExample example, @Param("selective") Order.Column ... selective);

    int upsertSelective(@Param("record") Order record, @Param("selective") Order.Column ... selective);
}