package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.Shop;
import com.koudaiplus.buy.repository.dao.model.ShopExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ShopMapper {
    long countByExample(ShopExample example);

    int deleteByExample(ShopExample example);

    int deleteByPrimaryKey(String mspKeyId);

    int insert(Shop record);

    int insertSelective(@Param("record") Shop record, @Param("selective") Shop.Column ... selective);

    Shop selectOneByExample(ShopExample example);

    Shop selectOneByExampleSelective(@Param("example") ShopExample example, @Param("selective") Shop.Column ... selective);

    List<Shop> selectByExampleSelective(@Param("example") ShopExample example, @Param("selective") Shop.Column ... selective);

    List<Shop> selectByExample(ShopExample example);

    Shop selectByPrimaryKeySelective(@Param("mspKeyId") String mspKeyId, @Param("selective") Shop.Column ... selective);

    Shop selectByPrimaryKey(String mspKeyId);

    int updateByExampleSelective(@Param("record") Shop record, @Param("example") ShopExample example, @Param("selective") Shop.Column ... selective);

    int updateByExample(@Param("record") Shop record, @Param("example") ShopExample example);

    int updateByPrimaryKeySelective(@Param("record") Shop record, @Param("selective") Shop.Column ... selective);

    int updateByPrimaryKey(Shop record);

    int batchInsert(@Param("list") List<Shop> list);

    int batchInsertSelective(@Param("list") List<Shop> list, @Param("selective") Shop.Column ... selective);

    int upsert(Shop record);

    int upsertByExample(@Param("record") Shop record, @Param("example") ShopExample example);

    int upsertByExampleSelective(@Param("record") Shop record, @Param("example") ShopExample example, @Param("selective") Shop.Column ... selective);

    int upsertSelective(@Param("record") Shop record, @Param("selective") Shop.Column ... selective);
}