package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class EasyOrder {
    private String eorKeyId;

    private Long eorId;

    private String eorSourceId;

    private String eorSourceType;

    private String eorPayOrderId;

    private String eorTransOrderNo;

    private String eorSubject;

    private BigDecimal eorAmount;

    private String eorBuyerId;

    private String eorState;

    private Date eorCreateTime;

    private Date eorUpdateTime;

    public String getEorKeyId() {
        return eorKeyId;
    }

    public void setEorKeyId(String eorKeyId) {
        this.eorKeyId = eorKeyId == null ? null : eorKeyId.trim();
    }

    public Long getEorId() {
        return eorId;
    }

    public void setEorId(Long eorId) {
        this.eorId = eorId;
    }

    public String getEorSourceId() {
        return eorSourceId;
    }

    public void setEorSourceId(String eorSourceId) {
        this.eorSourceId = eorSourceId == null ? null : eorSourceId.trim();
    }

    public String getEorSourceType() {
        return eorSourceType;
    }

    public void setEorSourceType(String eorSourceType) {
        this.eorSourceType = eorSourceType == null ? null : eorSourceType.trim();
    }

    public String getEorPayOrderId() {
        return eorPayOrderId;
    }

    public void setEorPayOrderId(String eorPayOrderId) {
        this.eorPayOrderId = eorPayOrderId == null ? null : eorPayOrderId.trim();
    }

    public String getEorTransOrderNo() {
        return eorTransOrderNo;
    }

    public void setEorTransOrderNo(String eorTransOrderNo) {
        this.eorTransOrderNo = eorTransOrderNo == null ? null : eorTransOrderNo.trim();
    }

    public String getEorSubject() {
        return eorSubject;
    }

    public void setEorSubject(String eorSubject) {
        this.eorSubject = eorSubject == null ? null : eorSubject.trim();
    }

    public BigDecimal getEorAmount() {
        return eorAmount;
    }

    public void setEorAmount(BigDecimal eorAmount) {
        this.eorAmount = eorAmount;
    }

    public String getEorBuyerId() {
        return eorBuyerId;
    }

    public void setEorBuyerId(String eorBuyerId) {
        this.eorBuyerId = eorBuyerId == null ? null : eorBuyerId.trim();
    }

    public String getEorState() {
        return eorState;
    }

    public void setEorState(String eorState) {
        this.eorState = eorState == null ? null : eorState.trim();
    }

    public Date getEorCreateTime() {
        return eorCreateTime;
    }

    public void setEorCreateTime(Date eorCreateTime) {
        this.eorCreateTime = eorCreateTime;
    }

    public Date getEorUpdateTime() {
        return eorUpdateTime;
    }

    public void setEorUpdateTime(Date eorUpdateTime) {
        this.eorUpdateTime = eorUpdateTime;
    }

    public static EasyOrder.Builder builder() {
        return new EasyOrder.Builder();
    }

    public static class Builder {
        private EasyOrder obj;

        public Builder() {
            this.obj = new EasyOrder();
        }

        public Builder eorKeyId(String eorKeyId) {
            obj.setEorKeyId(eorKeyId);
            return this;
        }

        public Builder eorId(Long eorId) {
            obj.setEorId(eorId);
            return this;
        }

        public Builder eorSourceId(String eorSourceId) {
            obj.setEorSourceId(eorSourceId);
            return this;
        }

        public Builder eorSourceType(String eorSourceType) {
            obj.setEorSourceType(eorSourceType);
            return this;
        }

        public Builder eorPayOrderId(String eorPayOrderId) {
            obj.setEorPayOrderId(eorPayOrderId);
            return this;
        }

        public Builder eorTransOrderNo(String eorTransOrderNo) {
            obj.setEorTransOrderNo(eorTransOrderNo);
            return this;
        }

        public Builder eorSubject(String eorSubject) {
            obj.setEorSubject(eorSubject);
            return this;
        }

        public Builder eorAmount(BigDecimal eorAmount) {
            obj.setEorAmount(eorAmount);
            return this;
        }

        public Builder eorBuyerId(String eorBuyerId) {
            obj.setEorBuyerId(eorBuyerId);
            return this;
        }

        public Builder eorState(String eorState) {
            obj.setEorState(eorState);
            return this;
        }

        public Builder eorCreateTime(Date eorCreateTime) {
            obj.setEorCreateTime(eorCreateTime);
            return this;
        }

        public Builder eorUpdateTime(Date eorUpdateTime) {
            obj.setEorUpdateTime(eorUpdateTime);
            return this;
        }

        public EasyOrder build() {
            return this.obj;
        }
    }

    public enum Column {
        eorKeyId("eor_key_id", "eorKeyId", "VARCHAR", false),
        eorId("eor_id", "eorId", "BIGINT", false),
        eorSourceId("eor_source_id", "eorSourceId", "VARCHAR", false),
        eorSourceType("eor_source_type", "eorSourceType", "VARCHAR", false),
        eorPayOrderId("eor_pay_order_id", "eorPayOrderId", "VARCHAR", false),
        eorTransOrderNo("eor_trans_order_no", "eorTransOrderNo", "VARCHAR", false),
        eorSubject("eor_subject", "eorSubject", "VARCHAR", false),
        eorAmount("eor_amount", "eorAmount", "DECIMAL", false),
        eorBuyerId("eor_buyer_id", "eorBuyerId", "VARCHAR", false),
        eorState("eor_state", "eorState", "VARCHAR", false),
        eorCreateTime("eor_create_time", "eorCreateTime", "TIMESTAMP", false),
        eorUpdateTime("eor_update_time", "eorUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}