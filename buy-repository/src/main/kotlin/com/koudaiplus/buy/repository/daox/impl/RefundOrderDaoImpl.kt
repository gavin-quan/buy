package com.koudaiplus.buy.repository.daox.impl;

import com.koudaiplus.buy.repository.dao.mapper.RefundOrderMapper
import com.koudaiplus.buy.repository.dao.model.RefundOrder
import com.koudaiplus.buy.repository.dao.model.RefundOrderExample
import com.koudaiplus.buy.repository.daox.RefundOrderDao
import com.koudaiplus.buy.repository.daox.model.RefundOrderDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class RefundOrderDaoImpl(
       mapper: RefundOrderMapper
) : DaoSupport<RefundOrderMapper, RefundOrderExample, RefundOrderExample.Criteria, RefundOrder.Builder, RefundOrder>(mapper), RefundOrderDao {

    override fun findList(param: RefundOrderDaoModel.QueryByOrderKeyId): List<RefundOrder> {
        return list { mapper, example, criteria ->
            criteria.andMroOrderKeyIdEqualTo(param.orderKeyId).andMroStateIn(param.state)
            mapper.selectByExample(example)
        }
    }

    override fun getInfoByKeyId(keyId: String): RefundOrder? {
        return get { mapper, example, criteria ->
            criteria.andMroKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun findList(param: RefundOrderDaoModel.QueryByOrderItemKeyId): List<RefundOrder> {
        return list { mapper, example, criteria ->
            criteria.andMroOrderItemKeyIdEqualTo(param.orderItemKeyId).andMroStateIn(param.state)
            mapper.selectByExample(example)
        }
    }

    override fun create(param: RefundOrderDaoModel.Create) {
        insert { mapper, builder ->
            val e = builder
                .mroKeyId(param.keyId)
                .mroShopKeyId(param.shopKeyId)
                .mroOrderKeyId(param.orderKeyId)
                .mroOrderItemKeyId(param.orderItemKeyId)
                .mroPayOrderKeyId(param.payOrderKeyId)
                .mroAmount(param.amount)
                .mroState(param.state)
                .mroCreatedBy(param.createdBy)
                .mroCreateTime(Date())
                .mroUpdateTime(Date())
                .build()
            mapper.insert(e)
        }
    }

    override fun setRequestStatus(param: RefundOrderDaoModel.SetStatus) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .mroState(param.toState)
                    .mroUpdateTime(Date())
                    .build()
            criteria.andMroKeyIdEqualTo(param.keyId).andMroStateIn(param.fromState)
            mapper.updateByExampleSelective(e, example, RefundOrder.Column.mroState, RefundOrder.Column.mroUpdateTime)
        }
    }

    override fun setResponseStatus(param: RefundOrderDaoModel.SetStatus) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .mroState(param.toState)
                    .mroFinishTime(param.finishTime)
                    .mroUpdateTime(Date())
                    .build()
            criteria.andMroKeyIdEqualTo(param.keyId).andMroStateIn(param.fromState)
            mapper.updateByExampleSelective(e, example, RefundOrder.Column.mroFinishTime, RefundOrder.Column.mroState, RefundOrder.Column.mroUpdateTime)
        }
    }

    override fun findList(param: RefundOrderDaoModel.QueryByPayOrderKeyId): List<RefundOrder> {
        return list { mapper, example, criteria ->
            criteria.andMroStateIn(param.state).andMroPayOrderKeyIdEqualTo(param.payOrderKeyId)
            mapper.selectByExample(example)
        }
    }

    override fun setExpressIdByKeyId(keyId: String, expressId: String) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .mroExpressId(expressId)
                    .mroUpdateTime(Date())
                    .build()
            criteria.andMroKeyIdEqualTo(keyId)
            mapper.updateByExampleSelective(e, example, RefundOrder.Column.mroExpressId, RefundOrder.Column.mroUpdateTime)
        }
    }

    override fun getExample() = RefundOrderExample()

    override fun getCriteria(example: RefundOrderExample) = example.createCriteria()

    override fun getBuilder() = RefundOrder.builder()

    override fun setPage(limit: Limit, example: RefundOrderExample) {
        example.limit(limit.offset(), limit.pageSize())
    }
}
