package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.Goods;
import com.koudaiplus.buy.repository.dao.model.GoodsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GoodsMapper {
    long countByExample(GoodsExample example);

    int deleteByExample(GoodsExample example);

    int deleteByPrimaryKey(String mgsKeyId);

    int insert(Goods record);

    int insertSelective(@Param("record") Goods record, @Param("selective") Goods.Column ... selective);

    Goods selectOneByExample(GoodsExample example);

    Goods selectOneByExampleSelective(@Param("example") GoodsExample example, @Param("selective") Goods.Column ... selective);

    List<Goods> selectByExampleSelective(@Param("example") GoodsExample example, @Param("selective") Goods.Column ... selective);

    List<Goods> selectByExample(GoodsExample example);

    Goods selectByPrimaryKeySelective(@Param("mgsKeyId") String mgsKeyId, @Param("selective") Goods.Column ... selective);

    Goods selectByPrimaryKey(String mgsKeyId);

    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example, @Param("selective") Goods.Column ... selective);

    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByPrimaryKeySelective(@Param("record") Goods record, @Param("selective") Goods.Column ... selective);

    int updateByPrimaryKey(Goods record);

    int batchInsert(@Param("list") List<Goods> list);

    int batchInsertSelective(@Param("list") List<Goods> list, @Param("selective") Goods.Column ... selective);

    int upsert(Goods record);

    int upsertByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    int upsertByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example, @Param("selective") Goods.Column ... selective);

    int upsertSelective(@Param("record") Goods record, @Param("selective") Goods.Column ... selective);
}