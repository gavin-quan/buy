package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Goods {
    private String mgsKeyId;

    private Long mgsId;

    private String mgsShopKeyId;

    private String mgsName;

    private String mgsCover;

    private String mgsPreview;

    private BigDecimal mgsMinPrice;

    private BigDecimal mgsMaxPrice;

    private String mgsContent;

    private String mgsState;

    private Date mgsCreateTime;

    private Date mgsUpdateTime;

    public String getMgsKeyId() {
        return mgsKeyId;
    }

    public void setMgsKeyId(String mgsKeyId) {
        this.mgsKeyId = mgsKeyId == null ? null : mgsKeyId.trim();
    }

    public Long getMgsId() {
        return mgsId;
    }

    public void setMgsId(Long mgsId) {
        this.mgsId = mgsId;
    }

    public String getMgsShopKeyId() {
        return mgsShopKeyId;
    }

    public void setMgsShopKeyId(String mgsShopKeyId) {
        this.mgsShopKeyId = mgsShopKeyId == null ? null : mgsShopKeyId.trim();
    }

    public String getMgsName() {
        return mgsName;
    }

    public void setMgsName(String mgsName) {
        this.mgsName = mgsName == null ? null : mgsName.trim();
    }

    public String getMgsCover() {
        return mgsCover;
    }

    public void setMgsCover(String mgsCover) {
        this.mgsCover = mgsCover == null ? null : mgsCover.trim();
    }

    public String getMgsPreview() {
        return mgsPreview;
    }

    public void setMgsPreview(String mgsPreview) {
        this.mgsPreview = mgsPreview == null ? null : mgsPreview.trim();
    }

    public BigDecimal getMgsMinPrice() {
        return mgsMinPrice;
    }

    public void setMgsMinPrice(BigDecimal mgsMinPrice) {
        this.mgsMinPrice = mgsMinPrice;
    }

    public BigDecimal getMgsMaxPrice() {
        return mgsMaxPrice;
    }

    public void setMgsMaxPrice(BigDecimal mgsMaxPrice) {
        this.mgsMaxPrice = mgsMaxPrice;
    }

    public String getMgsContent() {
        return mgsContent;
    }

    public void setMgsContent(String mgsContent) {
        this.mgsContent = mgsContent == null ? null : mgsContent.trim();
    }

    public String getMgsState() {
        return mgsState;
    }

    public void setMgsState(String mgsState) {
        this.mgsState = mgsState == null ? null : mgsState.trim();
    }

    public Date getMgsCreateTime() {
        return mgsCreateTime;
    }

    public void setMgsCreateTime(Date mgsCreateTime) {
        this.mgsCreateTime = mgsCreateTime;
    }

    public Date getMgsUpdateTime() {
        return mgsUpdateTime;
    }

    public void setMgsUpdateTime(Date mgsUpdateTime) {
        this.mgsUpdateTime = mgsUpdateTime;
    }

    public static Goods.Builder builder() {
        return new Goods.Builder();
    }

    public static class Builder {
        private Goods obj;

        public Builder() {
            this.obj = new Goods();
        }

        public Builder mgsKeyId(String mgsKeyId) {
            obj.setMgsKeyId(mgsKeyId);
            return this;
        }

        public Builder mgsId(Long mgsId) {
            obj.setMgsId(mgsId);
            return this;
        }

        public Builder mgsShopKeyId(String mgsShopKeyId) {
            obj.setMgsShopKeyId(mgsShopKeyId);
            return this;
        }

        public Builder mgsName(String mgsName) {
            obj.setMgsName(mgsName);
            return this;
        }

        public Builder mgsCover(String mgsCover) {
            obj.setMgsCover(mgsCover);
            return this;
        }

        public Builder mgsPreview(String mgsPreview) {
            obj.setMgsPreview(mgsPreview);
            return this;
        }

        public Builder mgsMinPrice(BigDecimal mgsMinPrice) {
            obj.setMgsMinPrice(mgsMinPrice);
            return this;
        }

        public Builder mgsMaxPrice(BigDecimal mgsMaxPrice) {
            obj.setMgsMaxPrice(mgsMaxPrice);
            return this;
        }

        public Builder mgsContent(String mgsContent) {
            obj.setMgsContent(mgsContent);
            return this;
        }

        public Builder mgsState(String mgsState) {
            obj.setMgsState(mgsState);
            return this;
        }

        public Builder mgsCreateTime(Date mgsCreateTime) {
            obj.setMgsCreateTime(mgsCreateTime);
            return this;
        }

        public Builder mgsUpdateTime(Date mgsUpdateTime) {
            obj.setMgsUpdateTime(mgsUpdateTime);
            return this;
        }

        public Goods build() {
            return this.obj;
        }
    }

    public enum Column {
        mgsKeyId("mgs_key_id", "mgsKeyId", "VARCHAR", false),
        mgsId("mgs_id", "mgsId", "BIGINT", false),
        mgsShopKeyId("mgs_shop_key_id", "mgsShopKeyId", "VARCHAR", false),
        mgsName("mgs_name", "mgsName", "VARCHAR", false),
        mgsCover("mgs_cover", "mgsCover", "VARCHAR", false),
        mgsPreview("mgs_preview", "mgsPreview", "VARCHAR", false),
        mgsMinPrice("mgs_min_price", "mgsMinPrice", "DECIMAL", false),
        mgsMaxPrice("mgs_max_price", "mgsMaxPrice", "DECIMAL", false),
        mgsContent("mgs_content", "mgsContent", "VARCHAR", false),
        mgsState("mgs_state", "mgsState", "VARCHAR", false),
        mgsCreateTime("mgs_create_time", "mgsCreateTime", "TIMESTAMP", false),
        mgsUpdateTime("mgs_update_time", "mgsUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}