package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public OrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public OrderExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public OrderExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public OrderExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public OrderExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public OrderExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        OrderExample example = new OrderExample();
        return example.createCriteria();
    }

    public OrderExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public OrderExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMorKeyIdIsNull() {
            addCriterion("mor_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdIsNotNull() {
            addCriterion("mor_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdEqualTo(String value) {
            addCriterion("mor_key_id =", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorKeyIdNotEqualTo(String value) {
            addCriterion("mor_key_id <>", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorKeyIdGreaterThan(String value) {
            addCriterion("mor_key_id >", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mor_key_id >=", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorKeyIdLessThan(String value) {
            addCriterion("mor_key_id <", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mor_key_id <=", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorKeyIdLike(String value) {
            addCriterion("mor_key_id like", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdNotLike(String value) {
            addCriterion("mor_key_id not like", value, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdIn(List<String> values) {
            addCriterion("mor_key_id in", values, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdNotIn(List<String> values) {
            addCriterion("mor_key_id not in", values, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdBetween(String value1, String value2) {
            addCriterion("mor_key_id between", value1, value2, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorKeyIdNotBetween(String value1, String value2) {
            addCriterion("mor_key_id not between", value1, value2, "morKeyId");
            return (Criteria) this;
        }

        public Criteria andMorIdIsNull() {
            addCriterion("mor_id is null");
            return (Criteria) this;
        }

        public Criteria andMorIdIsNotNull() {
            addCriterion("mor_id is not null");
            return (Criteria) this;
        }

        public Criteria andMorIdEqualTo(Long value) {
            addCriterion("mor_id =", value, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorIdNotEqualTo(Long value) {
            addCriterion("mor_id <>", value, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorIdGreaterThan(Long value) {
            addCriterion("mor_id >", value, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorIdGreaterThanOrEqualTo(Long value) {
            addCriterion("mor_id >=", value, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorIdLessThan(Long value) {
            addCriterion("mor_id <", value, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorIdLessThanOrEqualTo(Long value) {
            addCriterion("mor_id <=", value, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorIdIn(List<Long> values) {
            addCriterion("mor_id in", values, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdNotIn(List<Long> values) {
            addCriterion("mor_id not in", values, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdBetween(Long value1, Long value2) {
            addCriterion("mor_id between", value1, value2, "morId");
            return (Criteria) this;
        }

        public Criteria andMorIdNotBetween(Long value1, Long value2) {
            addCriterion("mor_id not between", value1, value2, "morId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdIsNull() {
            addCriterion("mor_batch_id is null");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdIsNotNull() {
            addCriterion("mor_batch_id is not null");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdEqualTo(String value) {
            addCriterion("mor_batch_id =", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_batch_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBatchIdNotEqualTo(String value) {
            addCriterion("mor_batch_id <>", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_batch_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBatchIdGreaterThan(String value) {
            addCriterion("mor_batch_id >", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_batch_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBatchIdGreaterThanOrEqualTo(String value) {
            addCriterion("mor_batch_id >=", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_batch_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBatchIdLessThan(String value) {
            addCriterion("mor_batch_id <", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_batch_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBatchIdLessThanOrEqualTo(String value) {
            addCriterion("mor_batch_id <=", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_batch_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBatchIdLike(String value) {
            addCriterion("mor_batch_id like", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdNotLike(String value) {
            addCriterion("mor_batch_id not like", value, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdIn(List<String> values) {
            addCriterion("mor_batch_id in", values, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdNotIn(List<String> values) {
            addCriterion("mor_batch_id not in", values, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdBetween(String value1, String value2) {
            addCriterion("mor_batch_id between", value1, value2, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorBatchIdNotBetween(String value1, String value2) {
            addCriterion("mor_batch_id not between", value1, value2, "morBatchId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdIsNull() {
            addCriterion("mor_shop_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdIsNotNull() {
            addCriterion("mor_shop_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdEqualTo(String value) {
            addCriterion("mor_shop_key_id =", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_shop_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdNotEqualTo(String value) {
            addCriterion("mor_shop_key_id <>", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_shop_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdGreaterThan(String value) {
            addCriterion("mor_shop_key_id >", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_shop_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mor_shop_key_id >=", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_shop_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdLessThan(String value) {
            addCriterion("mor_shop_key_id <", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_shop_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mor_shop_key_id <=", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_shop_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdLike(String value) {
            addCriterion("mor_shop_key_id like", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdNotLike(String value) {
            addCriterion("mor_shop_key_id not like", value, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdIn(List<String> values) {
            addCriterion("mor_shop_key_id in", values, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdNotIn(List<String> values) {
            addCriterion("mor_shop_key_id not in", values, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdBetween(String value1, String value2) {
            addCriterion("mor_shop_key_id between", value1, value2, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorShopKeyIdNotBetween(String value1, String value2) {
            addCriterion("mor_shop_key_id not between", value1, value2, "morShopKeyId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdIsNull() {
            addCriterion("mor_pay_order_id is null");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdIsNotNull() {
            addCriterion("mor_pay_order_id is not null");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdEqualTo(String value) {
            addCriterion("mor_pay_order_id =", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_pay_order_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdNotEqualTo(String value) {
            addCriterion("mor_pay_order_id <>", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_pay_order_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdGreaterThan(String value) {
            addCriterion("mor_pay_order_id >", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_pay_order_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("mor_pay_order_id >=", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_pay_order_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdLessThan(String value) {
            addCriterion("mor_pay_order_id <", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_pay_order_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdLessThanOrEqualTo(String value) {
            addCriterion("mor_pay_order_id <=", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_pay_order_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdLike(String value) {
            addCriterion("mor_pay_order_id like", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdNotLike(String value) {
            addCriterion("mor_pay_order_id not like", value, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdIn(List<String> values) {
            addCriterion("mor_pay_order_id in", values, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdNotIn(List<String> values) {
            addCriterion("mor_pay_order_id not in", values, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdBetween(String value1, String value2) {
            addCriterion("mor_pay_order_id between", value1, value2, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorPayOrderIdNotBetween(String value1, String value2) {
            addCriterion("mor_pay_order_id not between", value1, value2, "morPayOrderId");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoIsNull() {
            addCriterion("mor_trans_order_no is null");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoIsNotNull() {
            addCriterion("mor_trans_order_no is not null");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoEqualTo(String value) {
            addCriterion("mor_trans_order_no =", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_trans_order_no = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoNotEqualTo(String value) {
            addCriterion("mor_trans_order_no <>", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_trans_order_no <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoGreaterThan(String value) {
            addCriterion("mor_trans_order_no >", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_trans_order_no > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("mor_trans_order_no >=", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_trans_order_no >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoLessThan(String value) {
            addCriterion("mor_trans_order_no <", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_trans_order_no < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoLessThanOrEqualTo(String value) {
            addCriterion("mor_trans_order_no <=", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_trans_order_no <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoLike(String value) {
            addCriterion("mor_trans_order_no like", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoNotLike(String value) {
            addCriterion("mor_trans_order_no not like", value, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoIn(List<String> values) {
            addCriterion("mor_trans_order_no in", values, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoNotIn(List<String> values) {
            addCriterion("mor_trans_order_no not in", values, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoBetween(String value1, String value2) {
            addCriterion("mor_trans_order_no between", value1, value2, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorTransOrderNoNotBetween(String value1, String value2) {
            addCriterion("mor_trans_order_no not between", value1, value2, "morTransOrderNo");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdIsNull() {
            addCriterion("mor_express_id is null");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdIsNotNull() {
            addCriterion("mor_express_id is not null");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdEqualTo(String value) {
            addCriterion("mor_express_id =", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_express_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorExpressIdNotEqualTo(String value) {
            addCriterion("mor_express_id <>", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_express_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorExpressIdGreaterThan(String value) {
            addCriterion("mor_express_id >", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_express_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorExpressIdGreaterThanOrEqualTo(String value) {
            addCriterion("mor_express_id >=", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_express_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorExpressIdLessThan(String value) {
            addCriterion("mor_express_id <", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_express_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorExpressIdLessThanOrEqualTo(String value) {
            addCriterion("mor_express_id <=", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_express_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorExpressIdLike(String value) {
            addCriterion("mor_express_id like", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdNotLike(String value) {
            addCriterion("mor_express_id not like", value, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdIn(List<String> values) {
            addCriterion("mor_express_id in", values, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdNotIn(List<String> values) {
            addCriterion("mor_express_id not in", values, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdBetween(String value1, String value2) {
            addCriterion("mor_express_id between", value1, value2, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorExpressIdNotBetween(String value1, String value2) {
            addCriterion("mor_express_id not between", value1, value2, "morExpressId");
            return (Criteria) this;
        }

        public Criteria andMorSubjectIsNull() {
            addCriterion("mor_subject is null");
            return (Criteria) this;
        }

        public Criteria andMorSubjectIsNotNull() {
            addCriterion("mor_subject is not null");
            return (Criteria) this;
        }

        public Criteria andMorSubjectEqualTo(String value) {
            addCriterion("mor_subject =", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_subject = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorSubjectNotEqualTo(String value) {
            addCriterion("mor_subject <>", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_subject <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorSubjectGreaterThan(String value) {
            addCriterion("mor_subject >", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_subject > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorSubjectGreaterThanOrEqualTo(String value) {
            addCriterion("mor_subject >=", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_subject >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorSubjectLessThan(String value) {
            addCriterion("mor_subject <", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_subject < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorSubjectLessThanOrEqualTo(String value) {
            addCriterion("mor_subject <=", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_subject <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorSubjectLike(String value) {
            addCriterion("mor_subject like", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectNotLike(String value) {
            addCriterion("mor_subject not like", value, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectIn(List<String> values) {
            addCriterion("mor_subject in", values, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectNotIn(List<String> values) {
            addCriterion("mor_subject not in", values, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectBetween(String value1, String value2) {
            addCriterion("mor_subject between", value1, value2, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorSubjectNotBetween(String value1, String value2) {
            addCriterion("mor_subject not between", value1, value2, "morSubject");
            return (Criteria) this;
        }

        public Criteria andMorAmountIsNull() {
            addCriterion("mor_amount is null");
            return (Criteria) this;
        }

        public Criteria andMorAmountIsNotNull() {
            addCriterion("mor_amount is not null");
            return (Criteria) this;
        }

        public Criteria andMorAmountEqualTo(BigDecimal value) {
            addCriterion("mor_amount =", value, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_amount = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorAmountNotEqualTo(BigDecimal value) {
            addCriterion("mor_amount <>", value, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_amount <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorAmountGreaterThan(BigDecimal value) {
            addCriterion("mor_amount >", value, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_amount > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("mor_amount >=", value, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_amount >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorAmountLessThan(BigDecimal value) {
            addCriterion("mor_amount <", value, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_amount < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("mor_amount <=", value, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_amount <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorAmountIn(List<BigDecimal> values) {
            addCriterion("mor_amount in", values, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountNotIn(List<BigDecimal> values) {
            addCriterion("mor_amount not in", values, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mor_amount between", value1, value2, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("mor_amount not between", value1, value2, "morAmount");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdIsNull() {
            addCriterion("mor_buyer_id is null");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdIsNotNull() {
            addCriterion("mor_buyer_id is not null");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdEqualTo(String value) {
            addCriterion("mor_buyer_id =", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_buyer_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdNotEqualTo(String value) {
            addCriterion("mor_buyer_id <>", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_buyer_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdGreaterThan(String value) {
            addCriterion("mor_buyer_id >", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_buyer_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdGreaterThanOrEqualTo(String value) {
            addCriterion("mor_buyer_id >=", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_buyer_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdLessThan(String value) {
            addCriterion("mor_buyer_id <", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_buyer_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdLessThanOrEqualTo(String value) {
            addCriterion("mor_buyer_id <=", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_buyer_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdLike(String value) {
            addCriterion("mor_buyer_id like", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdNotLike(String value) {
            addCriterion("mor_buyer_id not like", value, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdIn(List<String> values) {
            addCriterion("mor_buyer_id in", values, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdNotIn(List<String> values) {
            addCriterion("mor_buyer_id not in", values, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdBetween(String value1, String value2) {
            addCriterion("mor_buyer_id between", value1, value2, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorBuyerIdNotBetween(String value1, String value2) {
            addCriterion("mor_buyer_id not between", value1, value2, "morBuyerId");
            return (Criteria) this;
        }

        public Criteria andMorStateIsNull() {
            addCriterion("mor_state is null");
            return (Criteria) this;
        }

        public Criteria andMorStateIsNotNull() {
            addCriterion("mor_state is not null");
            return (Criteria) this;
        }

        public Criteria andMorStateEqualTo(String value) {
            addCriterion("mor_state =", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorStateNotEqualTo(String value) {
            addCriterion("mor_state <>", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorStateGreaterThan(String value) {
            addCriterion("mor_state >", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorStateGreaterThanOrEqualTo(String value) {
            addCriterion("mor_state >=", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorStateLessThan(String value) {
            addCriterion("mor_state <", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorStateLessThanOrEqualTo(String value) {
            addCriterion("mor_state <=", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorStateLike(String value) {
            addCriterion("mor_state like", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateNotLike(String value) {
            addCriterion("mor_state not like", value, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateIn(List<String> values) {
            addCriterion("mor_state in", values, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateNotIn(List<String> values) {
            addCriterion("mor_state not in", values, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateBetween(String value1, String value2) {
            addCriterion("mor_state between", value1, value2, "morState");
            return (Criteria) this;
        }

        public Criteria andMorStateNotBetween(String value1, String value2) {
            addCriterion("mor_state not between", value1, value2, "morState");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeIsNull() {
            addCriterion("mor_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeIsNotNull() {
            addCriterion("mor_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeEqualTo(Date value) {
            addCriterion("mor_create_time =", value, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeNotEqualTo(Date value) {
            addCriterion("mor_create_time <>", value, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeGreaterThan(Date value) {
            addCriterion("mor_create_time >", value, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mor_create_time >=", value, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeLessThan(Date value) {
            addCriterion("mor_create_time <", value, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mor_create_time <=", value, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeIn(List<Date> values) {
            addCriterion("mor_create_time in", values, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeNotIn(List<Date> values) {
            addCriterion("mor_create_time not in", values, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mor_create_time between", value1, value2, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mor_create_time not between", value1, value2, "morCreateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeIsNull() {
            addCriterion("mor_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeIsNotNull() {
            addCriterion("mor_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeEqualTo(Date value) {
            addCriterion("mor_update_time =", value, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeNotEqualTo(Date value) {
            addCriterion("mor_update_time <>", value, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeNotEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeGreaterThan(Date value) {
            addCriterion("mor_update_time >", value, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeGreaterThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mor_update_time >=", value, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeGreaterThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeLessThan(Date value) {
            addCriterion("mor_update_time <", value, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeLessThanColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mor_update_time <=", value, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeLessThanOrEqualToColumn(Order.Column column) {
            addCriterion(new StringBuilder("mor_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeIn(List<Date> values) {
            addCriterion("mor_update_time in", values, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeNotIn(List<Date> values) {
            addCriterion("mor_update_time not in", values, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("mor_update_time between", value1, value2, "morUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMorUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mor_update_time not between", value1, value2, "morUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private OrderExample example;

        protected Criteria(OrderExample example) {
            super();
            this.example = example;
        }

        public OrderExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.OrderExample example);
    }
}