package com.koudaiplus.buy.repository.daox.impl

import com.koudaiplus.buy.repository.dao.mapper.RepeatPayOrderMapper
import com.koudaiplus.buy.repository.dao.model.RepeatPayOrder
import com.koudaiplus.buy.repository.dao.model.RepeatPayOrderExample
import com.koudaiplus.buy.repository.daox.RepeatPayOrderDao
import com.koudaiplus.buy.repository.daox.model.RepeatPayOrderDaoModel
import com.play.core.common.DaoSupport
import com.play.core.common.Limit
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class RepeatPayOrderDaoImpl(
    mapper: RepeatPayOrderMapper
) : DaoSupport<RepeatPayOrderMapper, RepeatPayOrderExample, RepeatPayOrderExample.Criteria, RepeatPayOrder.Builder, RepeatPayOrder>(mapper), RepeatPayOrderDao {

    override fun create(param: RepeatPayOrderDaoModel.Create) {
        insert { mapper, builder ->
            val r = builder
                    .rpoKeyId(param.keyId)
                    .rpoOrderKeyId(param.orderKeyId)
                    .rpoPayOrderKeyId(param.payOrderKeyId)
                    .rpoPayOrderItemId(param.payOrderItemKeyId)
                    .rpoAmount(param.amount)
                    .rpoState(param.state)
                    .rpoCreateTime(Date())
                    .rpoUpdateTime(Date())
                    .build()
            mapper.insert(r)
        }
    }

    override fun getInfoByPayOrderItemKeyId(param: RepeatPayOrderDaoModel.QueryByPayOrderItemKeyId): RepeatPayOrder? {
        return get { mapper, example, criteria ->
            criteria.andRpoPayOrderItemIdEqualTo(param.payOrderItemKeyId).andRpoStateIn(param.state)
            mapper.selectOneByExample(example)
        }
    }

    override fun setRespStatus(param: RepeatPayOrderDaoModel.SetStatus) {
        execute { mapper, example, criteria, builder ->
            val e = builder
                    .rpoState(param.toState)
                    .rpoUpdateTime(Date())
                    .build()
            criteria.andRpoKeyIdEqualTo(param.keyId).andRpoStateIn(param.fromState)
            mapper.updateByExampleSelective(e, example, RepeatPayOrder.Column.rpoState, RepeatPayOrder.Column.rpoUpdateTime)
        }
    }

    override fun getInfoByKeyId(keyId: String): RepeatPayOrder? {
        return get { mapper, example, criteria ->
            criteria.andRpoKeyIdEqualTo(keyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getInfo(param: RepeatPayOrderDaoModel.QueryByOrderId): RepeatPayOrder? {
        return get { mapper, example, criteria ->
            criteria.andRpoPayOrderKeyIdEqualTo(param.payOrderKeyId).andRpoOrderKeyIdEqualTo(param.orderKeyId)
            mapper.selectOneByExample(example)
        }
    }

    override fun getExample() = RepeatPayOrderExample()

    override fun getCriteria(example: RepeatPayOrderExample) = example.createCriteria()

    override fun getBuilder() = RepeatPayOrder.builder()

    override fun setPage(limit: Limit, example: RepeatPayOrderExample) {
        example.limit(limit.offset(), limit.pageSize())
    }

}