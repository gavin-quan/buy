package com.koudaiplus.buy.repository.dao.mapper;

import com.koudaiplus.buy.repository.dao.model.RepeatPayOrder;
import com.koudaiplus.buy.repository.dao.model.RepeatPayOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RepeatPayOrderMapper {
    long countByExample(RepeatPayOrderExample example);

    int deleteByExample(RepeatPayOrderExample example);

    int deleteByPrimaryKey(String rpoKeyId);

    int insert(RepeatPayOrder record);

    int insertSelective(@Param("record") RepeatPayOrder record, @Param("selective") RepeatPayOrder.Column ... selective);

    RepeatPayOrder selectOneByExample(RepeatPayOrderExample example);

    RepeatPayOrder selectOneByExampleSelective(@Param("example") RepeatPayOrderExample example, @Param("selective") RepeatPayOrder.Column ... selective);

    List<RepeatPayOrder> selectByExampleSelective(@Param("example") RepeatPayOrderExample example, @Param("selective") RepeatPayOrder.Column ... selective);

    List<RepeatPayOrder> selectByExample(RepeatPayOrderExample example);

    RepeatPayOrder selectByPrimaryKeySelective(@Param("rpoKeyId") String rpoKeyId, @Param("selective") RepeatPayOrder.Column ... selective);

    RepeatPayOrder selectByPrimaryKey(String rpoKeyId);

    int updateByExampleSelective(@Param("record") RepeatPayOrder record, @Param("example") RepeatPayOrderExample example, @Param("selective") RepeatPayOrder.Column ... selective);

    int updateByExample(@Param("record") RepeatPayOrder record, @Param("example") RepeatPayOrderExample example);

    int updateByPrimaryKeySelective(@Param("record") RepeatPayOrder record, @Param("selective") RepeatPayOrder.Column ... selective);

    int updateByPrimaryKey(RepeatPayOrder record);

    int batchInsert(@Param("list") List<RepeatPayOrder> list);

    int batchInsertSelective(@Param("list") List<RepeatPayOrder> list, @Param("selective") RepeatPayOrder.Column ... selective);

    int upsert(RepeatPayOrder record);

    int upsertByExample(@Param("record") RepeatPayOrder record, @Param("example") RepeatPayOrderExample example);

    int upsertByExampleSelective(@Param("record") RepeatPayOrder record, @Param("example") RepeatPayOrderExample example, @Param("selective") RepeatPayOrder.Column ... selective);

    int upsertSelective(@Param("record") RepeatPayOrder record, @Param("selective") RepeatPayOrder.Column ... selective);
}