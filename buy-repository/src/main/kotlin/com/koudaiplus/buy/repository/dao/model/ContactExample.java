package com.koudaiplus.buy.repository.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContactExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer offset;

    protected Integer rows;

    public ContactExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public ContactExample orderBy(String orderByClause) {
        this.setOrderByClause(orderByClause);
        return this;
    }

    public ContactExample orderBy(String ... orderByClauses) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < orderByClauses.length; i++) {
            sb.append(orderByClauses[i]);
            if (i < orderByClauses.length - 1) {
                sb.append(" , ");
            }
        }
        this.setOrderByClause(sb.toString());
        return this;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria(this);
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
        rows = null;
        offset = null;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return this.rows;
    }

    public ContactExample limit(Integer rows) {
        this.rows = rows;
        return this;
    }

    public ContactExample limit(Integer offset, Integer rows) {
        this.offset = offset;
        this.rows = rows;
        return this;
    }

    public ContactExample page(Integer page, Integer pageSize) {
        this.offset = page * pageSize;
        this.rows = pageSize;
        return this;
    }

    public static Criteria newAndCreateCriteria() {
        ContactExample example = new ContactExample();
        return example.createCriteria();
    }

    public ContactExample when(boolean condition, IExampleWhen then) {
        if (condition) {
            then.example(this);
        }
        return this;
    }

    public ContactExample when(boolean condition, IExampleWhen then, IExampleWhen otherwise) {
        if (condition) {
            then.example(this);
        } else {
            otherwise.example(this);
        }
        return this;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMcnKeyIdIsNull() {
            addCriterion("mcn_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdIsNotNull() {
            addCriterion("mcn_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdEqualTo(String value) {
            addCriterion("mcn_key_id =", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdNotEqualTo(String value) {
            addCriterion("mcn_key_id <>", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdGreaterThan(String value) {
            addCriterion("mcn_key_id >", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_key_id >=", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdLessThan(String value) {
            addCriterion("mcn_key_id <", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mcn_key_id <=", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdLike(String value) {
            addCriterion("mcn_key_id like", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdNotLike(String value) {
            addCriterion("mcn_key_id not like", value, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdIn(List<String> values) {
            addCriterion("mcn_key_id in", values, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdNotIn(List<String> values) {
            addCriterion("mcn_key_id not in", values, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdBetween(String value1, String value2) {
            addCriterion("mcn_key_id between", value1, value2, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnKeyIdNotBetween(String value1, String value2) {
            addCriterion("mcn_key_id not between", value1, value2, "mcnKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnIdIsNull() {
            addCriterion("mcn_id is null");
            return (Criteria) this;
        }

        public Criteria andMcnIdIsNotNull() {
            addCriterion("mcn_id is not null");
            return (Criteria) this;
        }

        public Criteria andMcnIdEqualTo(Long value) {
            addCriterion("mcn_id =", value, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnIdNotEqualTo(Long value) {
            addCriterion("mcn_id <>", value, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnIdGreaterThan(Long value) {
            addCriterion("mcn_id >", value, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnIdGreaterThanOrEqualTo(Long value) {
            addCriterion("mcn_id >=", value, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnIdLessThan(Long value) {
            addCriterion("mcn_id <", value, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnIdLessThanOrEqualTo(Long value) {
            addCriterion("mcn_id <=", value, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnIdIn(List<Long> values) {
            addCriterion("mcn_id in", values, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdNotIn(List<Long> values) {
            addCriterion("mcn_id not in", values, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdBetween(Long value1, Long value2) {
            addCriterion("mcn_id between", value1, value2, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnIdNotBetween(Long value1, Long value2) {
            addCriterion("mcn_id not between", value1, value2, "mcnId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdIsNull() {
            addCriterion("mcn_user_key_id is null");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdIsNotNull() {
            addCriterion("mcn_user_key_id is not null");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdEqualTo(String value) {
            addCriterion("mcn_user_key_id =", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_user_key_id = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdNotEqualTo(String value) {
            addCriterion("mcn_user_key_id <>", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_user_key_id <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdGreaterThan(String value) {
            addCriterion("mcn_user_key_id >", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_user_key_id > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_user_key_id >=", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_user_key_id >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdLessThan(String value) {
            addCriterion("mcn_user_key_id <", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_user_key_id < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdLessThanOrEqualTo(String value) {
            addCriterion("mcn_user_key_id <=", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_user_key_id <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdLike(String value) {
            addCriterion("mcn_user_key_id like", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdNotLike(String value) {
            addCriterion("mcn_user_key_id not like", value, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdIn(List<String> values) {
            addCriterion("mcn_user_key_id in", values, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdNotIn(List<String> values) {
            addCriterion("mcn_user_key_id not in", values, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdBetween(String value1, String value2) {
            addCriterion("mcn_user_key_id between", value1, value2, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnUserKeyIdNotBetween(String value1, String value2) {
            addCriterion("mcn_user_key_id not between", value1, value2, "mcnUserKeyId");
            return (Criteria) this;
        }

        public Criteria andMcnNameIsNull() {
            addCriterion("mcn_name is null");
            return (Criteria) this;
        }

        public Criteria andMcnNameIsNotNull() {
            addCriterion("mcn_name is not null");
            return (Criteria) this;
        }

        public Criteria andMcnNameEqualTo(String value) {
            addCriterion("mcn_name =", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_name = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnNameNotEqualTo(String value) {
            addCriterion("mcn_name <>", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_name <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnNameGreaterThan(String value) {
            addCriterion("mcn_name >", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_name > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnNameGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_name >=", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_name >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnNameLessThan(String value) {
            addCriterion("mcn_name <", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_name < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnNameLessThanOrEqualTo(String value) {
            addCriterion("mcn_name <=", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_name <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnNameLike(String value) {
            addCriterion("mcn_name like", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameNotLike(String value) {
            addCriterion("mcn_name not like", value, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameIn(List<String> values) {
            addCriterion("mcn_name in", values, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameNotIn(List<String> values) {
            addCriterion("mcn_name not in", values, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameBetween(String value1, String value2) {
            addCriterion("mcn_name between", value1, value2, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnNameNotBetween(String value1, String value2) {
            addCriterion("mcn_name not between", value1, value2, "mcnName");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneIsNull() {
            addCriterion("mcn_telephone is null");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneIsNotNull() {
            addCriterion("mcn_telephone is not null");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneEqualTo(String value) {
            addCriterion("mcn_telephone =", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_telephone = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneNotEqualTo(String value) {
            addCriterion("mcn_telephone <>", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_telephone <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneGreaterThan(String value) {
            addCriterion("mcn_telephone >", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_telephone > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_telephone >=", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_telephone >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneLessThan(String value) {
            addCriterion("mcn_telephone <", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_telephone < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneLessThanOrEqualTo(String value) {
            addCriterion("mcn_telephone <=", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_telephone <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneLike(String value) {
            addCriterion("mcn_telephone like", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneNotLike(String value) {
            addCriterion("mcn_telephone not like", value, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneIn(List<String> values) {
            addCriterion("mcn_telephone in", values, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneNotIn(List<String> values) {
            addCriterion("mcn_telephone not in", values, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneBetween(String value1, String value2) {
            addCriterion("mcn_telephone between", value1, value2, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnTelephoneNotBetween(String value1, String value2) {
            addCriterion("mcn_telephone not between", value1, value2, "mcnTelephone");
            return (Criteria) this;
        }

        public Criteria andMcnAddressIsNull() {
            addCriterion("mcn_address is null");
            return (Criteria) this;
        }

        public Criteria andMcnAddressIsNotNull() {
            addCriterion("mcn_address is not null");
            return (Criteria) this;
        }

        public Criteria andMcnAddressEqualTo(String value) {
            addCriterion("mcn_address =", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_address = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAddressNotEqualTo(String value) {
            addCriterion("mcn_address <>", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_address <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAddressGreaterThan(String value) {
            addCriterion("mcn_address >", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_address > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAddressGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_address >=", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_address >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAddressLessThan(String value) {
            addCriterion("mcn_address <", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_address < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAddressLessThanOrEqualTo(String value) {
            addCriterion("mcn_address <=", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_address <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAddressLike(String value) {
            addCriterion("mcn_address like", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressNotLike(String value) {
            addCriterion("mcn_address not like", value, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressIn(List<String> values) {
            addCriterion("mcn_address in", values, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressNotIn(List<String> values) {
            addCriterion("mcn_address not in", values, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressBetween(String value1, String value2) {
            addCriterion("mcn_address between", value1, value2, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnAddressNotBetween(String value1, String value2) {
            addCriterion("mcn_address not between", value1, value2, "mcnAddress");
            return (Criteria) this;
        }

        public Criteria andMcnSortIsNull() {
            addCriterion("mcn_sort is null");
            return (Criteria) this;
        }

        public Criteria andMcnSortIsNotNull() {
            addCriterion("mcn_sort is not null");
            return (Criteria) this;
        }

        public Criteria andMcnSortEqualTo(Integer value) {
            addCriterion("mcn_sort =", value, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_sort = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnSortNotEqualTo(Integer value) {
            addCriterion("mcn_sort <>", value, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_sort <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnSortGreaterThan(Integer value) {
            addCriterion("mcn_sort >", value, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_sort > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("mcn_sort >=", value, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_sort >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnSortLessThan(Integer value) {
            addCriterion("mcn_sort <", value, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_sort < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnSortLessThanOrEqualTo(Integer value) {
            addCriterion("mcn_sort <=", value, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_sort <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnSortIn(List<Integer> values) {
            addCriterion("mcn_sort in", values, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortNotIn(List<Integer> values) {
            addCriterion("mcn_sort not in", values, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortBetween(Integer value1, Integer value2) {
            addCriterion("mcn_sort between", value1, value2, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnSortNotBetween(Integer value1, Integer value2) {
            addCriterion("mcn_sort not between", value1, value2, "mcnSort");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceIsNull() {
            addCriterion("mcn_province is null");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceIsNotNull() {
            addCriterion("mcn_province is not null");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceEqualTo(String value) {
            addCriterion("mcn_province =", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_province = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnProvinceNotEqualTo(String value) {
            addCriterion("mcn_province <>", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_province <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnProvinceGreaterThan(String value) {
            addCriterion("mcn_province >", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_province > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_province >=", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_province >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnProvinceLessThan(String value) {
            addCriterion("mcn_province <", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_province < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnProvinceLessThanOrEqualTo(String value) {
            addCriterion("mcn_province <=", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_province <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnProvinceLike(String value) {
            addCriterion("mcn_province like", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceNotLike(String value) {
            addCriterion("mcn_province not like", value, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceIn(List<String> values) {
            addCriterion("mcn_province in", values, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceNotIn(List<String> values) {
            addCriterion("mcn_province not in", values, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceBetween(String value1, String value2) {
            addCriterion("mcn_province between", value1, value2, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnProvinceNotBetween(String value1, String value2) {
            addCriterion("mcn_province not between", value1, value2, "mcnProvince");
            return (Criteria) this;
        }

        public Criteria andMcnCityIsNull() {
            addCriterion("mcn_city is null");
            return (Criteria) this;
        }

        public Criteria andMcnCityIsNotNull() {
            addCriterion("mcn_city is not null");
            return (Criteria) this;
        }

        public Criteria andMcnCityEqualTo(String value) {
            addCriterion("mcn_city =", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_city = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCityNotEqualTo(String value) {
            addCriterion("mcn_city <>", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_city <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCityGreaterThan(String value) {
            addCriterion("mcn_city >", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_city > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCityGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_city >=", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_city >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCityLessThan(String value) {
            addCriterion("mcn_city <", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_city < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCityLessThanOrEqualTo(String value) {
            addCriterion("mcn_city <=", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_city <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCityLike(String value) {
            addCriterion("mcn_city like", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityNotLike(String value) {
            addCriterion("mcn_city not like", value, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityIn(List<String> values) {
            addCriterion("mcn_city in", values, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityNotIn(List<String> values) {
            addCriterion("mcn_city not in", values, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityBetween(String value1, String value2) {
            addCriterion("mcn_city between", value1, value2, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCityNotBetween(String value1, String value2) {
            addCriterion("mcn_city not between", value1, value2, "mcnCity");
            return (Criteria) this;
        }

        public Criteria andMcnCountyIsNull() {
            addCriterion("mcn_county is null");
            return (Criteria) this;
        }

        public Criteria andMcnCountyIsNotNull() {
            addCriterion("mcn_county is not null");
            return (Criteria) this;
        }

        public Criteria andMcnCountyEqualTo(String value) {
            addCriterion("mcn_county =", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_county = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCountyNotEqualTo(String value) {
            addCriterion("mcn_county <>", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_county <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCountyGreaterThan(String value) {
            addCriterion("mcn_county >", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_county > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCountyGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_county >=", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_county >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCountyLessThan(String value) {
            addCriterion("mcn_county <", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_county < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCountyLessThanOrEqualTo(String value) {
            addCriterion("mcn_county <=", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_county <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCountyLike(String value) {
            addCriterion("mcn_county like", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyNotLike(String value) {
            addCriterion("mcn_county not like", value, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyIn(List<String> values) {
            addCriterion("mcn_county in", values, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyNotIn(List<String> values) {
            addCriterion("mcn_county not in", values, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyBetween(String value1, String value2) {
            addCriterion("mcn_county between", value1, value2, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnCountyNotBetween(String value1, String value2) {
            addCriterion("mcn_county not between", value1, value2, "mcnCounty");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeIsNull() {
            addCriterion("mcn_areacode is null");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeIsNotNull() {
            addCriterion("mcn_areacode is not null");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeEqualTo(String value) {
            addCriterion("mcn_areacode =", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_areacode = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeNotEqualTo(String value) {
            addCriterion("mcn_areacode <>", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_areacode <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeGreaterThan(String value) {
            addCriterion("mcn_areacode >", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_areacode > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_areacode >=", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_areacode >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeLessThan(String value) {
            addCriterion("mcn_areacode <", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_areacode < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeLessThanOrEqualTo(String value) {
            addCriterion("mcn_areacode <=", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_areacode <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeLike(String value) {
            addCriterion("mcn_areacode like", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeNotLike(String value) {
            addCriterion("mcn_areacode not like", value, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeIn(List<String> values) {
            addCriterion("mcn_areacode in", values, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeNotIn(List<String> values) {
            addCriterion("mcn_areacode not in", values, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeBetween(String value1, String value2) {
            addCriterion("mcn_areacode between", value1, value2, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnAreacodeNotBetween(String value1, String value2) {
            addCriterion("mcn_areacode not between", value1, value2, "mcnAreacode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeIsNull() {
            addCriterion("mcn_postalcode is null");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeIsNotNull() {
            addCriterion("mcn_postalcode is not null");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeEqualTo(String value) {
            addCriterion("mcn_postalcode =", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_postalcode = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeNotEqualTo(String value) {
            addCriterion("mcn_postalcode <>", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_postalcode <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeGreaterThan(String value) {
            addCriterion("mcn_postalcode >", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_postalcode > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_postalcode >=", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_postalcode >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeLessThan(String value) {
            addCriterion("mcn_postalcode <", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_postalcode < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeLessThanOrEqualTo(String value) {
            addCriterion("mcn_postalcode <=", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_postalcode <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeLike(String value) {
            addCriterion("mcn_postalcode like", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeNotLike(String value) {
            addCriterion("mcn_postalcode not like", value, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeIn(List<String> values) {
            addCriterion("mcn_postalcode in", values, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeNotIn(List<String> values) {
            addCriterion("mcn_postalcode not in", values, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeBetween(String value1, String value2) {
            addCriterion("mcn_postalcode between", value1, value2, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnPostalcodeNotBetween(String value1, String value2) {
            addCriterion("mcn_postalcode not between", value1, value2, "mcnPostalcode");
            return (Criteria) this;
        }

        public Criteria andMcnStateIsNull() {
            addCriterion("mcn_state is null");
            return (Criteria) this;
        }

        public Criteria andMcnStateIsNotNull() {
            addCriterion("mcn_state is not null");
            return (Criteria) this;
        }

        public Criteria andMcnStateEqualTo(String value) {
            addCriterion("mcn_state =", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_state = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnStateNotEqualTo(String value) {
            addCriterion("mcn_state <>", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_state <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnStateGreaterThan(String value) {
            addCriterion("mcn_state >", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_state > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnStateGreaterThanOrEqualTo(String value) {
            addCriterion("mcn_state >=", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_state >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnStateLessThan(String value) {
            addCriterion("mcn_state <", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_state < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnStateLessThanOrEqualTo(String value) {
            addCriterion("mcn_state <=", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_state <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnStateLike(String value) {
            addCriterion("mcn_state like", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateNotLike(String value) {
            addCriterion("mcn_state not like", value, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateIn(List<String> values) {
            addCriterion("mcn_state in", values, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateNotIn(List<String> values) {
            addCriterion("mcn_state not in", values, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateBetween(String value1, String value2) {
            addCriterion("mcn_state between", value1, value2, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnStateNotBetween(String value1, String value2) {
            addCriterion("mcn_state not between", value1, value2, "mcnState");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeIsNull() {
            addCriterion("mcn_create_time is null");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeIsNotNull() {
            addCriterion("mcn_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeEqualTo(Date value) {
            addCriterion("mcn_create_time =", value, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_create_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeNotEqualTo(Date value) {
            addCriterion("mcn_create_time <>", value, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_create_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeGreaterThan(Date value) {
            addCriterion("mcn_create_time >", value, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_create_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mcn_create_time >=", value, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_create_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeLessThan(Date value) {
            addCriterion("mcn_create_time <", value, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_create_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mcn_create_time <=", value, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_create_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeIn(List<Date> values) {
            addCriterion("mcn_create_time in", values, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeNotIn(List<Date> values) {
            addCriterion("mcn_create_time not in", values, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeBetween(Date value1, Date value2) {
            addCriterion("mcn_create_time between", value1, value2, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mcn_create_time not between", value1, value2, "mcnCreateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeIsNull() {
            addCriterion("mcn_update_time is null");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeIsNotNull() {
            addCriterion("mcn_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeEqualTo(Date value) {
            addCriterion("mcn_update_time =", value, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_update_time = ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeNotEqualTo(Date value) {
            addCriterion("mcn_update_time <>", value, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeNotEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_update_time <> ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeGreaterThan(Date value) {
            addCriterion("mcn_update_time >", value, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeGreaterThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_update_time > ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("mcn_update_time >=", value, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeGreaterThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_update_time >= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeLessThan(Date value) {
            addCriterion("mcn_update_time <", value, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeLessThanColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_update_time < ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("mcn_update_time <=", value, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeLessThanOrEqualToColumn(Contact.Column column) {
            addCriterion(new StringBuilder("mcn_update_time <= ").append(column.getEscapedColumnName()).toString());
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeIn(List<Date> values) {
            addCriterion("mcn_update_time in", values, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeNotIn(List<Date> values) {
            addCriterion("mcn_update_time not in", values, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("mcn_update_time between", value1, value2, "mcnUpdateTime");
            return (Criteria) this;
        }

        public Criteria andMcnUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("mcn_update_time not between", value1, value2, "mcnUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        private ContactExample example;

        protected Criteria(ContactExample example) {
            super();
            this.example = example;
        }

        public ContactExample example() {
            return this.example;
        }

        @Deprecated
        public Criteria andIf(boolean ifAdd, ICriteriaAdd add) {
            if (ifAdd) {
                add.add(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then) {
            if (condition) {
                then.criteria(this);
            }
            return this;
        }

        public Criteria when(boolean condition, ICriteriaWhen then, ICriteriaWhen otherwise) {
            if (condition) {
                then.criteria(this);
            } else {
                otherwise.criteria(this);
            }
            return this;
        }

        @Deprecated
        public interface ICriteriaAdd {
            Criteria add(Criteria add);
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }

    public interface ICriteriaWhen {
        void criteria(Criteria criteria);
    }

    public interface IExampleWhen {
        void example(com.koudaiplus.buy.repository.dao.model.ContactExample example);
    }
}