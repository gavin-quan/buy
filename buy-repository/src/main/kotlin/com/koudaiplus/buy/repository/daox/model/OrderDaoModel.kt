package com.koudaiplus.buy.repository.daox.model

import java.math.BigDecimal

object OrderDaoModel {

    data class Create (
            val keyId: String,
            val batchId: String,
            val shopKeyId: String,
            val subject: String,
            val amount: BigDecimal,
            val expressId: String,
            val buyerId: String,
            val state: String
    )

    data class FinishPay(
            val keyId: String,
            val payOrderKeyId: String,
            val transOrderNo: String,
            val fromState: List<String>
    )

    data class UpdateState(
            val keyId: String,
            val toState: String,
            val fromState: List<String>
    )

}