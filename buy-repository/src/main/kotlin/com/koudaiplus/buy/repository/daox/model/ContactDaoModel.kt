package com.koudaiplus.buy.repository.daox.model

object ContactDaoModel {

    data class Create (
            val keyId: String,
            val name: String,
            val address: String,
            val telephone: String,
            val sort: Int,
            val userKeyId: String,
            val state: String,
            val province: String,
            val city: String,
            val county: String,
            val areaCode: String?,
            val postalCode: String?
    )

}