package com.koudaiplus.buy.repository.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class OrderItem {
    private String moiKeyId;

    private Long moiId;

    private String moiShopKeyId;

    private String moiGoodsKeyId;

    private String moiGoodsSkuId;

    private String moiOrderKeyId;

    private BigDecimal moiGoodsPrice;

    private Integer moiGoodsQuantity;

    private BigDecimal moiTotalPrice;

    private String moiStockState;

    private String moiState;

    private Date moiCreateTime;

    private Date moiUpdateTime;

    public String getMoiKeyId() {
        return moiKeyId;
    }

    public void setMoiKeyId(String moiKeyId) {
        this.moiKeyId = moiKeyId == null ? null : moiKeyId.trim();
    }

    public Long getMoiId() {
        return moiId;
    }

    public void setMoiId(Long moiId) {
        this.moiId = moiId;
    }

    public String getMoiShopKeyId() {
        return moiShopKeyId;
    }

    public void setMoiShopKeyId(String moiShopKeyId) {
        this.moiShopKeyId = moiShopKeyId == null ? null : moiShopKeyId.trim();
    }

    public String getMoiGoodsKeyId() {
        return moiGoodsKeyId;
    }

    public void setMoiGoodsKeyId(String moiGoodsKeyId) {
        this.moiGoodsKeyId = moiGoodsKeyId == null ? null : moiGoodsKeyId.trim();
    }

    public String getMoiGoodsSkuId() {
        return moiGoodsSkuId;
    }

    public void setMoiGoodsSkuId(String moiGoodsSkuId) {
        this.moiGoodsSkuId = moiGoodsSkuId == null ? null : moiGoodsSkuId.trim();
    }

    public String getMoiOrderKeyId() {
        return moiOrderKeyId;
    }

    public void setMoiOrderKeyId(String moiOrderKeyId) {
        this.moiOrderKeyId = moiOrderKeyId == null ? null : moiOrderKeyId.trim();
    }

    public BigDecimal getMoiGoodsPrice() {
        return moiGoodsPrice;
    }

    public void setMoiGoodsPrice(BigDecimal moiGoodsPrice) {
        this.moiGoodsPrice = moiGoodsPrice;
    }

    public Integer getMoiGoodsQuantity() {
        return moiGoodsQuantity;
    }

    public void setMoiGoodsQuantity(Integer moiGoodsQuantity) {
        this.moiGoodsQuantity = moiGoodsQuantity;
    }

    public BigDecimal getMoiTotalPrice() {
        return moiTotalPrice;
    }

    public void setMoiTotalPrice(BigDecimal moiTotalPrice) {
        this.moiTotalPrice = moiTotalPrice;
    }

    public String getMoiStockState() {
        return moiStockState;
    }

    public void setMoiStockState(String moiStockState) {
        this.moiStockState = moiStockState == null ? null : moiStockState.trim();
    }

    public String getMoiState() {
        return moiState;
    }

    public void setMoiState(String moiState) {
        this.moiState = moiState == null ? null : moiState.trim();
    }

    public Date getMoiCreateTime() {
        return moiCreateTime;
    }

    public void setMoiCreateTime(Date moiCreateTime) {
        this.moiCreateTime = moiCreateTime;
    }

    public Date getMoiUpdateTime() {
        return moiUpdateTime;
    }

    public void setMoiUpdateTime(Date moiUpdateTime) {
        this.moiUpdateTime = moiUpdateTime;
    }

    public static OrderItem.Builder builder() {
        return new OrderItem.Builder();
    }

    public static class Builder {
        private OrderItem obj;

        public Builder() {
            this.obj = new OrderItem();
        }

        public Builder moiKeyId(String moiKeyId) {
            obj.setMoiKeyId(moiKeyId);
            return this;
        }

        public Builder moiId(Long moiId) {
            obj.setMoiId(moiId);
            return this;
        }

        public Builder moiShopKeyId(String moiShopKeyId) {
            obj.setMoiShopKeyId(moiShopKeyId);
            return this;
        }

        public Builder moiGoodsKeyId(String moiGoodsKeyId) {
            obj.setMoiGoodsKeyId(moiGoodsKeyId);
            return this;
        }

        public Builder moiGoodsSkuId(String moiGoodsSkuId) {
            obj.setMoiGoodsSkuId(moiGoodsSkuId);
            return this;
        }

        public Builder moiOrderKeyId(String moiOrderKeyId) {
            obj.setMoiOrderKeyId(moiOrderKeyId);
            return this;
        }

        public Builder moiGoodsPrice(BigDecimal moiGoodsPrice) {
            obj.setMoiGoodsPrice(moiGoodsPrice);
            return this;
        }

        public Builder moiGoodsQuantity(Integer moiGoodsQuantity) {
            obj.setMoiGoodsQuantity(moiGoodsQuantity);
            return this;
        }

        public Builder moiTotalPrice(BigDecimal moiTotalPrice) {
            obj.setMoiTotalPrice(moiTotalPrice);
            return this;
        }

        public Builder moiStockState(String moiStockState) {
            obj.setMoiStockState(moiStockState);
            return this;
        }

        public Builder moiState(String moiState) {
            obj.setMoiState(moiState);
            return this;
        }

        public Builder moiCreateTime(Date moiCreateTime) {
            obj.setMoiCreateTime(moiCreateTime);
            return this;
        }

        public Builder moiUpdateTime(Date moiUpdateTime) {
            obj.setMoiUpdateTime(moiUpdateTime);
            return this;
        }

        public OrderItem build() {
            return this.obj;
        }
    }

    public enum Column {
        moiKeyId("moi_key_id", "moiKeyId", "VARCHAR", false),
        moiId("moi_id", "moiId", "BIGINT", false),
        moiShopKeyId("moi_shop_key_id", "moiShopKeyId", "VARCHAR", false),
        moiGoodsKeyId("moi_goods_key_id", "moiGoodsKeyId", "VARCHAR", false),
        moiGoodsSkuId("moi_goods_sku_id", "moiGoodsSkuId", "VARCHAR", false),
        moiOrderKeyId("moi_order_key_id", "moiOrderKeyId", "VARCHAR", false),
        moiGoodsPrice("moi_goods_price", "moiGoodsPrice", "DECIMAL", false),
        moiGoodsQuantity("moi_goods_quantity", "moiGoodsQuantity", "INTEGER", false),
        moiTotalPrice("moi_total_price", "moiTotalPrice", "DECIMAL", false),
        moiStockState("moi_stock_state", "moiStockState", "VARCHAR", false),
        moiState("moi_state", "moiState", "VARCHAR", false),
        moiCreateTime("moi_create_time", "moiCreateTime", "TIMESTAMP", false),
        moiUpdateTime("moi_update_time", "moiUpdateTime", "TIMESTAMP", false);

        private static final String BEGINNING_DELIMITER = "`";

        private static final String ENDING_DELIMITER = "`";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public static Column[] all() {
            return Column.values();
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}