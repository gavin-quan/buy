package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.PayBizModel

interface PaymentBiz {

    fun directPay(param: PayBizModel.DirectPay): String

    fun orderPay(param: PayBizModel.OrderPay): String

}