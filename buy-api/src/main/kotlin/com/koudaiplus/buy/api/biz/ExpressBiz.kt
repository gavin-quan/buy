package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.ExpressBizModel

interface ExpressBiz {

    fun findList(keyId: String): ExpressBizModel.ExpressVO

}