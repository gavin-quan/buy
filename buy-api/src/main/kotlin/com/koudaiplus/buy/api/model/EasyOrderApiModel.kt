package com.koudaiplus.buy.api.model

import java.math.BigDecimal

object EasyOrderApiModel {

    data class Order (
            val subject: String,
            val amount: BigDecimal
    )

}