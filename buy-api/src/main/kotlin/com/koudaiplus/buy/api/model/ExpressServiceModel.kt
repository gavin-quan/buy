package com.koudaiplus.buy.api.model

import com.play.core.common.ActivityStateEnum

object ExpressServiceModel {

    data class Create(
        val shopKeyId: String,
        val receiverUserId: String,
        val receiverName: String,
        val receiverTelephone: String,
        val receiverAddress: String,
        val title: String
    )

    data class Start(
        val keyId: String,
        val companyCode: String,
        val serialNo: String,
        val consignorName: String,
        val consignorTelephone: String,
        val consignorAddress: String
    )

    data class ExpressDTO(
        val keyId: String,
        val shopKeyId: String,
        val receiverUserId: String,
        val title: String,
        val receiverName: String?,
        val receiverTelephone: String?,
        val receiverAddress: String?,
        val companyCode: String?,
        val serialNo: String?,
        val consignorName: String?,
        val consignorTelephone: String?,
        val consignorAddress: String?,
        val state: ActivityStateEnum
    )

}