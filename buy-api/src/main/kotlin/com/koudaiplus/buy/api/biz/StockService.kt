package com.koudaiplus.buy.api.biz

interface StockService {

    fun tryLock(skuId: String, quantity: Int)

    fun recoverLock(skuId: String): Long

    fun forceRecoverLock(skuId: String): Long

    fun push(skuId: String, quantity: Int)

    fun lock(orderItemKeyId: String)

    fun deduct(orderItemKeyId: String)

    fun recover(orderItemKeyId: String)

}