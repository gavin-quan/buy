package com.koudaiplus.buy.api.model

import com.play.core.common.checkBiz
import java.math.BigDecimal
import java.util.*

object PayOrderServiceModel {

    data class Create (
            val subject: String,
            val amount: BigDecimal
    )

    data class CreateItem(
            val orderKeyId: String,
            val payOrderKeyId: String
    )

    data class FinishPay(
            val keyId: String,
            val transOrderNo: String,
            val finishTime: Date
    )

    data class PayOrderDTO(
            val keyId: String,
            val subject: String,
            val amount: BigDecimal,
            val createTime: Date,
            val finishTime: Date?,
            val transOrderNo: String?,
            val state: BuyConstants.PayStateEnum
    ) {
        fun isRepeatNotify(transOrderNo: String):  Boolean {
            return this.transOrderNo == transOrderNo
        }
        fun checkWhenPay() {
            checkBiz(!state.isWaitPay()) {"订单状态已失效${keyId}"}
        }

        override fun toString(): String {
            return "PayOrderDTO(keyId='$keyId', subject='$subject', amount=$amount, createTime=$createTime, finishTime=$finishTime, transOrderNo=$transOrderNo, state=$state)"
        }

    }

    data class PayOrderItemDTO(
            val keyId: String,
            val payOrderKeyId: String,
            val orderKeyId: String
    )

}