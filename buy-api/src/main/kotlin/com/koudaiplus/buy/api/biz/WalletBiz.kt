package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.WalletBizModel
import com.koudaiplus.wallet.api.model.AccountServiceModel
import com.koudaiplus.wallet.api.model.BillItemServiceModel
import com.play.core.common.Limit
import com.play.core.common.Pages
import com.play.core.common.RespResult

interface WalletBiz {
    fun info(userId: String): RespResult<AccountServiceModel.AccountVO?, Unit, Unit>
    fun resetPassword(userId: String, req: WalletBizModel.ResetPassword): RespResult<Unit, Unit, Unit>
    fun transfer(userId: String, req: WalletBizModel.TransferByPassword): RespResult<Unit, Unit, Unit>
    fun findList(userId: String, limit: Limit): RespResult<Pages<BillItemServiceModel.BillItemVO>, Unit, Unit>
}