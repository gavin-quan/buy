package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.RepeatPayOrderServiceModel

interface RepeatPayOrderService {

    fun getInfoByPayOrderItemKeyId(payOrderItemKeyId: String): RepeatPayOrderServiceModel.RepeatPayOrderDTO?

    fun create(param: RepeatPayOrderServiceModel.Create): String

    fun setRefundRespStatus(param: RepeatPayOrderServiceModel.SetStatus)

    fun getInfoByKeyId(keyId: String): RepeatPayOrderServiceModel.RepeatPayOrderDTO?

    fun isRepeatPay(payOrderKeyId: String, orderKeyId: String): Boolean

}