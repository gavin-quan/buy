package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.RefundOrderBizModel
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.refund.api.model.MerchantRefundOrderFeignModel

interface RefundOrderBiz {

    fun createByMarketOrder(param: RefundOrderBizModel.Create): String

    fun createByEasyOrder(param: RefundOrderBizModel.Create): String

    fun retry(refundOrderKeyId: String)

    fun cancel(param: RefundOrderBizModel.Cancel)

    fun receiveRefundNotify(param: NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>): String

    fun setRefundResponse(param: NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>)

    fun createRefundExpress(refundOrderKeyId: String)

}