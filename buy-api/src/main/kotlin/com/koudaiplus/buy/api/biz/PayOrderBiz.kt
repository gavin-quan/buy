package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.PayOrderBizModel
import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.pay.api.model.PayConstants

interface PayOrderBiz {

    fun createByMarketOrder(param: PayOrderBizModel.Create): String

    fun createByEasyOrder(param: PayOrderBizModel.Create): String

    fun pay(param: PayOrderBizModel.Pay): String

    fun receivePayFinishNotify(param: NotifyConstants.WebHookEvent<PayConstants.TradePaySuccessResponse>): String

    fun receivePayReturnNotify(param: Map<String, String>): String

    fun finishPay(param: NotifyConstants.WebHookEvent<PayConstants.TradePaySuccessResponse>)

}