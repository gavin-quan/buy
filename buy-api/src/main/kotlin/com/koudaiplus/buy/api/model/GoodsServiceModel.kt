package com.koudaiplus.buy.api.model

import com.play.core.common.ClassStateEnum
import java.math.BigDecimal

object GoodsServiceModel {

    data class GoodsDTO(
            val keyId: String,
            val name: String,
            val cover: String,
            val preview: String?,
            val minPrice: BigDecimal,
            val maxPrice: BigDecimal,
            val shopKeyId: String,
            val state: ClassStateEnum
    )

    data class GoodsStockKeepingUnitDTO(
            val keyId: String,
            val name: String,
            val price: BigDecimal,
            val goodsKeyId: String,
            val reducedPrice: BigDecimal,
            val discountPrice: BigDecimal,
            val quantity: Int,
            val lockQuantity: Int,
            val availableQuantity: Int,
            val stockId: String,
            val state: ClassStateEnum
    )

}