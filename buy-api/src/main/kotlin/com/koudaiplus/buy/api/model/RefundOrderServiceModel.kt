package com.koudaiplus.buy.api.model

import com.play.core.common.ActivityStateEnum
import java.math.BigDecimal
import java.util.*

object RefundOrderServiceModel {

    data class RefundOrderDTO(
        val keyId: String,
        val shopKeyId: String,
        val orderKeyId: String,
        val orderItemKeyId: String?,
        val payOrderKeyId: String,
        val expressId: String?,
        val amount: BigDecimal,
        val finishTime: Date?,
        val createdBy: String,
        val state: ActivityStateEnum
    )

    data class Create (
        val shopKeyId: String,
        val orderKeyId: String,
        val orderItemKeyId: String?,
        val payOrderKeyId: String,
        val amount: BigDecimal,
        val createdBy: String
    )

    data class SetStatus(
        val keyId: String,
        val finishTime: Date?,
        val state: ActivityStateEnum
    )

}