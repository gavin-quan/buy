package com.koudaiplus.buy.api.model

object CartBizModel {

    data class Create(
            val skuId: String,
            val quantity: Int,
            val buyerId: String
    )
}