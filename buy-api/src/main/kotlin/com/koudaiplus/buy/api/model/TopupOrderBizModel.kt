package com.koudaiplus.buy.api.model

import com.koudaiplus.pay.api.model.PayConstants
import com.play.core.common.Validator
import com.play.core.common.checkBiz
import java.math.BigDecimal

object TopupOrderBizModel {

    data class Create(
        val buyerId: String,
        val amount: BigDecimal,
        val tradeType: String,
        val tradeSubtype: String
    ): Validator {
        override fun validate() {
            //充值是无法使用钱包余额付款的
            checkBiz(PayConstants.TradeTypeEnum.KOUDAI.name == tradeType) {"参数错误"}
        }
    }

}