package com.koudaiplus.buy.api.model

import com.google.gson.annotations.SerializedName
import org.apache.commons.lang3.RandomStringUtils

object ExpressBizModel {

    data class ExpressItemVO (
        @SerializedName("context")
        val address: String,
        @SerializedName("time")
        val time: String
    ) {
        fun getId() = RandomStringUtils.random(5, false, true)
    }

    data class ExpressVO (
        val serialNo: String,
        val companyName: String,
        val itemList: List<ExpressItemVO>
    )

}