package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.GoodsServiceModel
import com.play.core.common.Limit
import com.play.core.common.Pages

interface GoodsService {

    fun getInfoByKeyId(keyId: String): GoodsServiceModel.GoodsDTO?

    fun getSkuInfoByKeyId(skuKeyId: String): GoodsServiceModel.GoodsStockKeepingUnitDTO?

    fun lockStock(orderItemKeyId: String)

    fun deductStock(orderItemKeyId: String)

    fun recoverStock(orderItemKeyId: String)

    fun pushStock(skuKeyId: String, quantity: Int)

    fun getContentByKeyId(keyId: String): String?

    fun findList(limit: Limit): Pages<GoodsServiceModel.GoodsDTO>

    fun findListByShopId(shopKeyId: String, limit: Limit): Pages<GoodsServiceModel.GoodsDTO>

}