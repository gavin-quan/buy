package com.koudaiplus.buy.api.model

import org.apache.commons.lang3.StringUtils
import java.math.BigDecimal
import java.util.*

object OrderBizModel {

    data class Create(
        val skuList: List<SkuItem>,
        val buyerContactId: String,
        val buyerId: String
    )

    data class SkuItem(
        val skuId: String,
        val remarks: String, //用户留言备注
        val quantity: Int //数量
    )

    class CreateBO (val shopKeyId: String) {
        val skuList: MutableList<SkuBO> = LinkedList()
        fun totalPrice(): BigDecimal = skuList.stream().map {
            item->item.sku.discountPrice.times(item.item.quantity.toBigDecimal())
        }.reduce(BigDecimal.ZERO, BigDecimal::add)
        fun subject(): String = StringUtils.left(skuList.stream().map {
            item->item.goods.name
        }.reduce { t: String, u: String -> t.plus("&").plus(u) }.get(), 50)
    }

    data class SkuBO (
        val goods: GoodsServiceModel.GoodsDTO,
        val sku: GoodsServiceModel.GoodsStockKeepingUnitDTO,
        val item: SkuItem
    )

}