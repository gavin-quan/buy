package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.OrderServiceModel

interface OrderService {

    fun getInfoByKeyId(keyId: String): OrderServiceModel.OrderDTO?

    fun findListByPayOrderId(payOrderId: String): List<OrderServiceModel.OrderDTO>

    fun create(param: OrderServiceModel.Create): String

    fun createItem(param: OrderServiceModel.CreateItem): String

    fun findOrderItemList(orderKeyId: String): List<OrderServiceModel.OrderItemDTO>

    fun getOrderItemInfoByKeyId(keyId: String): OrderServiceModel.OrderItemDTO?

    fun tryCloseByKeyId(keyId: String)

    fun tryForceRecoverStockByOrderItemKeyId(orderItemKeyId: String)

    fun finishPay(param: OrderServiceModel.FinishPay)

    fun findCancelStockOrderItemList(): List<OrderServiceModel.OrderItemDTO>

    fun findOrderKeyIdListByBatchId(batchId: String): List<String>

}
