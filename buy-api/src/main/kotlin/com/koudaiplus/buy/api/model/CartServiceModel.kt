package com.koudaiplus.buy.api.model

object CartServiceModel {

    data class Create(
            val shopKeyId: String,
            val goodsKeyId: String,
            val goodsSkuId: String,
            val quantity: Int,
            val buyerId: String
    )

    data class Delete(
        val keyIdList: List<String>,
        val buyerId: String
    )

}