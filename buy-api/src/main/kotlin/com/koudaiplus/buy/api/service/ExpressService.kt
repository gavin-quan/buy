package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.ExpressServiceModel

interface ExpressService {

    fun create(param: ExpressServiceModel.Create): String

    fun start(param: ExpressServiceModel.Start)

    fun getInfoByKeyId(keyId: String): ExpressServiceModel.ExpressDTO?

    fun confirm(keyId: String)

}