package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.OrderBizModel
import com.koudaiplus.buy.api.model.OrderServiceModel
import com.koudaiplus.buy.api.model.RefundOrderApiModel

interface OrderBiz {

    fun createBySync(param: OrderBizModel.Create): List<String>

    fun createByAsync(param: OrderBizModel.Create): String

    fun create(batchId: String, param: OrderBizModel.Create): List<String>

    fun tryCloseByKeyId(keyId: String)

    fun getRefundBuilder(keyId: String, userId: String): RefundOrderApiModel.RefundBuilder?

    fun finishPay(param: OrderServiceModel.FinishPay)

    fun recoverStock()

    fun delay(keyId: String, buyerId: String)

    fun confirm(keyId: String, buyerId: String)

}