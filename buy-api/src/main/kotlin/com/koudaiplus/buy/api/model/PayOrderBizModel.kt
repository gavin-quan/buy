package com.koudaiplus.buy.api.model

import com.play.core.common.checkBiz
import org.apache.commons.lang3.StringUtils
import java.math.BigDecimal
import java.util.*

object PayOrderBizModel {

    data class Create(
        val orderIdList: List<String>
    )

    class CreateBO {
        val orderList : MutableList<OrderApiModel.Order> = LinkedList()
        fun totalAmount(): BigDecimal {
            val amount = orderList.stream().map { item->item.amount }.reduce(BigDecimal.ZERO, BigDecimal::add)
            checkBiz(amount <= BigDecimal.ZERO) {"付款金额错误"}
            return amount
        }
        fun subject(): String = StringUtils.left(orderList.stream().map { item->item.subject }.reduce { t: String, u: String -> t.plus(u) }.get(), 50)
    }

    data class Pay(
        val payOrderKeyId: String,
        val tradeType: String,
        val tradeSubtype: String,
        val returnUrl: String,
        val merchantAppId: String,
        val merchantRequestKey: String,
        val metadata: String? = ""
    )

}