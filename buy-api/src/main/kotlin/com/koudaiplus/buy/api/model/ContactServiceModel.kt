package com.koudaiplus.buy.api.model

import com.play.core.common.ClassStateEnum

object ContactServiceModel {

    data class Create (
            val userKeyId: String,
            val name: String,
            val province: String,
            val city: String,
            val county: String,
            val address: String,
            val telephone: String,
            val areaCode: String?,
            val postalCode: String?,
            val default: Boolean
    )

    data class Update (
            val keyId: String,
            val userKeyId: String,
            val name: String,
            val province: String,
            val city: String,
            val county: String,
            val address: String,
            val telephone: String,
            val areaCode: String?,
            val postalCode: String?,
            val default: Boolean
    )

    data class ContactVO (
            val keyId: String,
            val name: String,
            val province: String,
            val city: String,
            val county: String,
            val address: String,
            val telephone: String,
            val areaCode: String?,
            val postalCode: String?,
            val default: Boolean,
            val fullAddress: String
    )

    data class ContactDTO(
            val keyId: String,
            val userKeyId: String,
            val name: String,
            val province: String,
            val city: String,
            val county: String,
            val address: String,
            val telephone: String,
            val areaCode: String?,
            val postalCode: String?,
            val sort: Int,
            val state: ClassStateEnum
    ) {
        fun toContactVO() = ContactVO(keyId = keyId, name = name, telephone = telephone, address = address, default = sort == 0,
        areaCode = areaCode, postalCode = postalCode, province = province, city = city, county = county, fullAddress = province.plus(city).plus(county).plus(address))
    }

}