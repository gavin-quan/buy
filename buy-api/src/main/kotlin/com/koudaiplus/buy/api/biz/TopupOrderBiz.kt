package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.TopupFeignModel
import com.koudaiplus.buy.api.model.TopupOrderBizModel

interface TopupOrderBiz {

    fun pay(param: TopupOrderBizModel.Create): String

    fun pay(param: TopupFeignModel.CreateByMerchant): String

}