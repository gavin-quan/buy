package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.PayOrderServiceModel

interface PayOrderService {

    fun create(param: PayOrderServiceModel.Create): String

    fun createItem(param: PayOrderServiceModel.CreateItem): String

    fun finishPay(param: PayOrderServiceModel.FinishPay)

    fun getInfoByKeyId(keyId: String): PayOrderServiceModel.PayOrderDTO?

    fun findItemListByKeyId(payOrderKeyId: String): List<PayOrderServiceModel.PayOrderItemDTO>

    fun getItemInfoByOrderKeyId(payOrderKeyId: String, orderKeyId: String): PayOrderServiceModel.PayOrderItemDTO?

    fun getItemInfoByKeyId(payOrderItemKeyId: String): PayOrderServiceModel.PayOrderItemDTO?

    fun findRetryablePayOrderIdList(orderIdList: List<String>): List<String>

}