package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.PayoutBizModel
import com.play.core.common.RespResult

interface PayoutBiz {
    fun getPayoutViewData(userId: String): Map<String, *>
    fun createTransfer(userId: String, param: PayoutBizModel.CreateTransfer): RespResult<Unit, Unit, Unit>
    fun cancel(userId: String, param: PayoutBizModel.CancelPayout): RespResult<Unit, Unit, Unit>
}