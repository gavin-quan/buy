package com.koudaiplus.buy.api.model

import java.time.Duration

object BuyConstants {

    const val REPEAT_PAY_REFUND_METADATA = "repeat-pay"

    const val REPEAT_PAY_ERROR_CODE = "1000000"
    const val REDO_RECOVER_THRESHOLD = 10

    const val ORDER_PAYMENT_MSG_ARRIVED_QUEUE = "order_payment_msg_arrived"
    const val ORDER_PAYMENT_MSG_ARRIVED_QUEUE_BEAN = "orderPaymentMsgArrivedQueue"

    const val PAY_ORDER_REPEAT_PAY_QUEUE = "pay_order_repeat_pay"
    const val PAY_ORDER_REPEAT_PAY_QUEUE_BEAN = "payOrderRepeatPayQueue"

    const val ORDER_REFUND_MSG_ARRIVED_QUEUE = "order_refund_msg_arrived"
    const val ORDER_REFUND_MSG_ARRIVED_QUEUE_BEAN = "orderRefundMsgArrivedQueue"

    const val ORDER_CREATE_QUEUE = "order_create"
    const val ORDER_CREATE_QUEUE_BEAN = "orderCreateQueue"
    data class OrderCreateEvent(val batchId: String, val msg: String)

    const val ORDER_PAY_FINISH_TOPIC = "order_pay_finish"
    const val ORDER_PAY_FINISH_TOPIC_BEAN = "orderPayFinishTopic"
    data class OrderPayFinishEvent(val payOrderKeyId: String)

    const val REFUND_STATE_CHANGE_TOPIC = "refund_state_change"
    const val REFUND_STATE_CHANGE_TOPIC_BEAN = "refundStateChangeTopic"
    data class RefundStateChangeEvent(val refundOrderKeyId: String)
    data class RefundExpressCreatedEvent(val refundOrderKeyId: String)

    //订单超时(35分钟), 比核心支付系统多出5分钟, 防止库存恢复后又支付成功的情况
    val ORDER_TIMEOUT_DURATION = Duration.ofMinutes(35)
    const val ORDER_CLOSE_QUEUE = "order_close"
    const val ORDER_CLOSE_QUEUE_BEAN = "orderCloseQueue"

    const val ORDER_CREATED_TOPIC = "order_created"
    const val ORDER_CREATED_TOPIC_BEAN = "orderCreatedTopic"
    data class OrderCreatedEvent(val orderKeyId: String)

    fun getOrderLockKey(orderKeyId: String) = "lock:order:$orderKeyId"
    fun getPayOrderLockKey(payOrderKeyId: String) = "lock:payOrder:$payOrderKeyId"
    fun getRefundOrderLockKey(refundOrderKeyId: String) = "lock:refundOrder:$refundOrderKeyId"
    fun getCreateRefundOrderLockKey(orderItemKeyId: String) = "lock:createRefundOrder:$orderItemKeyId"
    fun getSkuKey(skuId: String) = "stock:$skuId"
    fun getRecoverSkuLockKey(skuId: String) = "lock:stock:recover:$skuId"
    fun getTryLockSkuLockKey(skuId: String) = "lock:stock:$skuId"

    enum class PayStateEnum(val cnName: String) {
        INIT("初始化"),
        WAIT_PAY("等待付款"),
        PAID("已完成付款"),
        CANCEL("已取消"),
        CLOSED("已关闭");
        fun isInit() = name == INIT.name
        fun isWaitPay() = name == WAIT_PAY.name
        fun isPaid() = name == PAID.name
        fun isCancel() = name == CANCEL.name
        fun isClosed() = name == CLOSED.name
    }

    enum class SourceType {
        TOPUP, //充值
        EBOOK, //电子书
        PARKING, //停车
        ECOURSE //网课
    }

}