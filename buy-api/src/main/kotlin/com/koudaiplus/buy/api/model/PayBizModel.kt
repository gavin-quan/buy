package com.koudaiplus.buy.api.model

object PayBizModel {

    data class DirectPay (
        val skuList: List<OrderBizModel.SkuItem>,
        val buyerContactId: String,
        val tradeType: String,
        val tradeSubtype: String,
        val buyerId: String
    )

    data class OrderPay (
        val orderId: String,
        val tradeType: String,
        val tradeSubtype: String
    )

}