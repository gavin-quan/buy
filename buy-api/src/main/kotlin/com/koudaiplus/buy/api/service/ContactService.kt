package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.ContactServiceModel

interface ContactService {

    fun getInfoByKeyId(keyId: String): ContactServiceModel.ContactDTO?

    fun getInfoByKeyId(keyId: String, userKeyId: String): ContactServiceModel.ContactDTO?

    fun getDefaultInfoByUserKeyId(userKeyId: String): ContactServiceModel.ContactDTO?

    fun findListByUserKeyId(userKeyId: String): List<ContactServiceModel.ContactVO>

    fun create(param: ContactServiceModel.Create)

    fun update(param: ContactServiceModel.Update)

    fun deleteByKeyId(keyId: String, userKeyId: String)

    fun setDefaultByKeyId(keyId: String, userKeyId: String)

}