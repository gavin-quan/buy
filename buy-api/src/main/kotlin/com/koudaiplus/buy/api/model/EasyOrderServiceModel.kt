package com.koudaiplus.buy.api.model

import com.play.core.common.ClassStateEnum
import java.math.BigDecimal

object EasyOrderServiceModel {

    data class Create (
            val sourceId: String,
            val subject: String,
            val sourceType: BuyConstants.SourceType,
            val amount: BigDecimal,
            val buyerId: String
    )

    data class FinishPay(
        val keyId: String,
        val payOrderKeyId: String,
        val transOrderNo: String
    )

    data class EasyOrderDTO(
            val keyId: String,
            val subject: String,
            val buyerId: String,
            val payOrderKeyId: String?,
            val transOrderNo: String?,
            val sourceId: String,
            val sourceType: BuyConstants.SourceType,
            val amount: BigDecimal,
            val state: ClassStateEnum
    ) {
        fun isRepeatNotify(transOrderNo: String, payOrderKeyId: String): Boolean {
            return this.transOrderNo == transOrderNo && this.payOrderKeyId == payOrderKeyId
        }

        fun isPaid(): Boolean {
            return this.transOrderNo != null && this.payOrderKeyId != null
        }

        fun getOrder() = OrderApiModel.Order(
            keyId = keyId, subject = subject,
            amount = amount, state = state.name
        )

    }

}