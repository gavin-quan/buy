package com.koudaiplus.buy.api.biz

import com.koudaiplus.notify.api.model.NotifyConstants
import com.koudaiplus.refund.api.model.MerchantRefundOrderFeignModel

interface RepeatPayOrderBiz {

    fun createRefund(payOrderItemKeyId: String): String

    fun retryRefund(repeatPayKeyId: String)

    fun setRefundResponse(param: NotifyConstants.WebHookEvent<MerchantRefundOrderFeignModel.RefundResponse>)

}