package com.koudaiplus.buy.api.model

import java.math.BigDecimal

object RefundOrderApiModel {

    data class RefundBuilder (
        val orderKeyId : String,
        val payOrderKeyId : String,
        val orderItemKeyId: String?,
        val price: BigDecimal,
        val shopKeyId: String
    )

}