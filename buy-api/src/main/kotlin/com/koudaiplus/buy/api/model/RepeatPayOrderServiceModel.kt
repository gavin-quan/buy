package com.koudaiplus.buy.api.model

import com.play.core.common.ActivityStateEnum
import java.math.BigDecimal

object RepeatPayOrderServiceModel {

    data class Create(
        val payOrderItemKeyId: String,
        val orderKeyId: String,
        val amount: BigDecimal,
        val payOrderKeyId: String
    )

    data class RepeatPayOrderDTO(
        val keyId: String,
        val payOrderItemKeyId: String,
        val amount: BigDecimal,
        val orderKeyId: String,
        val payOrderKeyId: String,
        val state: ActivityStateEnum
    )

    data class SetStatus(
        val keyId: String,
        val state: ActivityStateEnum
    )

}