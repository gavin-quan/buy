package com.koudaiplus.buy.api.model

import java.math.BigDecimal

object EasyOrderBizModel {

    data class Create(
            val buyerId: String,
            val subject: String,
            val sourceId: String,
            val sourceType: BuyConstants.SourceType,
            val amount: BigDecimal
    )

}