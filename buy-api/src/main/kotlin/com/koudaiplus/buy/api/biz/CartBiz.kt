package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.CartBizModel

interface CartBiz {
    fun create(param: CartBizModel.Create)
}