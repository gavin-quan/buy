package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.CartServiceModel

interface CartService {

    fun create(param: CartServiceModel.Create)

    fun delete(param: CartServiceModel.Delete)

    fun getPendingCount(buyerId: String): Long

    fun delete(buyerId: String, skuId: String)

}