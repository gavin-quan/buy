package com.koudaiplus.buy.api.model

import com.play.core.common.ClassStateEnum

object ShopServiceModel {
    data class ShopDTO(
        val keyId: String,
        val name: String,
        val ownerId: String,
        val logo: String?,
        val cover: String?,
        val state: ClassStateEnum
    )
}