package com.koudaiplus.buy.api.model

import com.play.core.common.Validator

object TopupFeignModel {

    data class CreateByLoginUser(
        val amount: String,
        val tradeType: String,
        val tradeSubtype: String
    ): Validator {
        override fun validate() {
        }
    }

    data class CreateByMerchant(
        val buyerId: String,
        val walletNo: String,
        val amount: String,
        val merchantAppId: String,
        val merchantRequestKey: String,
        val returnUrl: String,
        val tradeType: String,
        val tradeSubtype: String
    ): Validator {
        override fun validate() {
        }
    }

}