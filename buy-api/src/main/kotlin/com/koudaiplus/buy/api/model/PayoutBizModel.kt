package com.koudaiplus.buy.api.model

import java.math.BigDecimal

object PayoutBizModel {

    class WalletDTO {
        var name: String? = null
        var number: String? = null
        var amount: BigDecimal? = null
        var currency: String? = null
        var freezeAmount: BigDecimal? = null
        fun getActiveAmount() = amount!!.subtract(freezeAmount)
    }

    data class CreateTransfer(
        val password: String,
        val amount: String,
        val cardId: String
    )

    data class CancelPayout(
        val requestNo: String
    )

}