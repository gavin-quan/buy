package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.ShopServiceModel

interface ShopService {

    fun getInfoByKeyId(keyId: String): ShopServiceModel.ShopDTO?

}