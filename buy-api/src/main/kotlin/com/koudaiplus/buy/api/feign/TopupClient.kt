package com.koudaiplus.buy.api.feign

import com.koudaiplus.buy.api.model.TopupFeignModel
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@FeignClient(value = "\${app.buy.applicationName}", path = "\${app.buy.contextPath}", contextId = "TopupClientClient")
@RestController
@RequestMapping("/micro-service/topup")
@Api(description = "用户充值", tags = ["Topup"])
interface TopupClient {

    @ApiOperation(value = "下单付款")
    @PostMapping("/pay", produces = [MediaType.TEXT_HTML_VALUE])
    fun pay(@RequestBody req: TopupFeignModel.CreateByLoginUser): String

    @ApiOperation(value = "下单付款")
    @PostMapping("/merchant/pay", produces = [MediaType.TEXT_HTML_VALUE])
    fun pay(@RequestBody req: TopupFeignModel.CreateByMerchant): String

}