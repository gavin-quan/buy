package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.RefundOrderServiceModel

interface RefundOrderService {

    fun getInfoByKeyId(keyId: String): RefundOrderServiceModel.RefundOrderDTO?

    fun getInfoByOrderItemKeyId(orderItemKeyId: String): RefundOrderServiceModel.RefundOrderDTO?

    fun getInfoByOrderKeyId(orderKeyId: String): RefundOrderServiceModel.RefundOrderDTO?

    fun create(param: RefundOrderServiceModel.Create): String

    fun setRequestStatus(param: RefundOrderServiceModel.SetStatus)

    fun setResponseStatus(param: RefundOrderServiceModel.SetStatus)

    fun findActiveListByPayOrderKeyId(payOrderKeyId: String): List<RefundOrderServiceModel.RefundOrderDTO>

    fun findFinishListByPayOrderKeyId(payOrderKeyId: String): List<RefundOrderServiceModel.RefundOrderDTO>

    fun setExpressIdByKeyId(keyId: String, expressId: String)

}