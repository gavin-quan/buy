package com.koudaiplus.buy.api.model

import com.play.core.common.ActivityStateEnum
import com.play.core.common.ClassStateEnum
import java.math.BigDecimal
import java.util.*

object OrderServiceModel {

    data class Create (
        val batchId: String,
        val shopKeyId: String,
        val subject: String,
        val amount: BigDecimal,
        val expressId: String,
        val buyerId: String
    )

    data class CreateItem(
        val orderKeyId: String,
        val shopKeyId: String,
        val goodsKeyId: String,
        val goodsSkuId: String,
        val goodsName: String,
        val goodsPrice: BigDecimal,
        val goodsQuantity: Int
    ) {
        fun totalPrice() = goodsPrice.times(goodsQuantity.toBigDecimal())
    }

    data class FinishPay(
        val keyId: String,
        val payOrderKeyId: String,
        val transOrderNo: String
    ) {
        override fun toString(): String {
            return "FinishPay(keyId='$keyId', payOrderKeyId='$payOrderKeyId', transOrderNo='$transOrderNo')"
        }
    }

    data class OrderItemDTO(
        val keyId: String,
        val shopKeyId: String,
        val goodsKeyId: String,
        val goodsSkuKeyId: String,
        val orderKeyId: String,
        val goodsPrice: BigDecimal,
        val goodsQuantity: Int,
        val totalPrice: BigDecimal,
        val state: ClassStateEnum,
        val stockState: ActivityStateEnum
    )

    data class OrderDTO(
        val keyId: String,
        val buyerId: String,
        val expressId: String?,
        val payOrderKeyId: String?,
        val transOrderNo: String?,
        val shopKeyId: String,
        val subject: String,
        val createTime: Date,
        val amount: BigDecimal,
        val state: ClassStateEnum
    ) {

        fun isRepeatNotify(transOrderNo: String, payOrderKeyId: String): Boolean {
            return this.transOrderNo == transOrderNo && this.payOrderKeyId == payOrderKeyId
        }

        fun isPaid(): Boolean {
            return this.transOrderNo != null && this.payOrderKeyId != null
        }

        fun getOrder() = OrderApiModel.Order(
            keyId = keyId, subject = subject,
            amount = amount, state = state.name
        )

        override fun toString(): String {
            return "OrderDTO(keyId='$keyId', buyerId='$buyerId', expressId=$expressId, payOrderKeyId=$payOrderKeyId, transOrderNo=$transOrderNo, shopKeyId='$shopKeyId', subject='$subject', createTime=$createTime, amount=$amount, state=$state)"
        }

    }

}