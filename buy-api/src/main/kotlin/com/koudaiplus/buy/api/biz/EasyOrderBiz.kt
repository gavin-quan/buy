package com.koudaiplus.buy.api.biz

import com.koudaiplus.buy.api.model.EasyOrderBizModel
import com.koudaiplus.buy.api.model.EasyOrderServiceModel
import com.koudaiplus.buy.api.model.RefundOrderApiModel

interface EasyOrderBiz {

    fun create(param: EasyOrderBizModel.Create): String

    fun tryCloseByKeyId(keyId: String)

    fun getRefundBuilder(keyId: String, userId: String): RefundOrderApiModel.RefundBuilder?

    fun finishTopupOrderPay(param: EasyOrderServiceModel.FinishPay)

}