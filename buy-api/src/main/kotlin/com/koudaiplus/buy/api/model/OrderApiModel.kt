package com.koudaiplus.buy.api.model

import java.math.BigDecimal

object OrderApiModel {

    data class Order (
        val keyId: String,
        val subject: String,
        val amount: BigDecimal,
        val state: String
    )

    data class FinishPay(
        val orderKeyId: String,
        val payOrderKeyId: String,
        val transOrderNo: String
    )

}