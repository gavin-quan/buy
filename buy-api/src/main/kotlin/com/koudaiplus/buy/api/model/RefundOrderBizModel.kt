package com.koudaiplus.buy.api.model

object RefundOrderBizModel {

    data class Create(
        val orderKeyId: String,
        val userId: String
    )

    data class Cancel(
        val refundOrderKeyId: String,
        val userId: String
    )

}