package com.koudaiplus.buy.api.model

import java.math.BigDecimal

object WalletBizModel {

    data class ResetPassword(
        val oldPassword: String,
        val newPassword: String
    )

    data class TransferByPassword(
        val toNumber: String,
        val amount: BigDecimal,
        val password: String
    )

}