package com.koudaiplus.buy.api.service

import com.koudaiplus.buy.api.model.EasyOrderServiceModel

interface EasyOrderService {

    fun getInfoByKeyId(keyId: String): EasyOrderServiceModel.EasyOrderDTO?

    fun create(param: EasyOrderServiceModel.Create): String

    fun tryCloseByKeyId(keyId: String)

    fun finishPay(param: EasyOrderServiceModel.FinishPay)

}
