package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.RefundOrderServiceModel
import com.koudaiplus.buy.api.service.RefundOrderService
import com.koudaiplus.buy.repository.dao.model.RefundOrder
import com.koudaiplus.buy.repository.daox.RefundOrderDao
import com.koudaiplus.buy.repository.daox.model.RefundOrderDaoModel
import com.play.core.common.ActivityStateEnum
import com.play.core.common.SnowFlakeGenerator
import com.play.core.common.checkBiz
import org.apache.commons.collections4.CollectionUtils
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class RefundOrderServiceImpl (
    private val idGenerator: SnowFlakeGenerator,
    private val dao: RefundOrderDao
): RefundOrderService {

    override fun getInfoByOrderKeyId(orderKeyId: String): RefundOrderServiceModel.RefundOrderDTO? {
        val refundOrderList = dao.findList(param = RefundOrderDaoModel.QueryByOrderKeyId(
            orderKeyId = orderKeyId, state = ActivityStateEnum.normals()
        ))
        if(CollectionUtils.isEmpty(refundOrderList)) return null
        checkBiz(refundOrderList.size > 1) {"退款数据异常,重复退款"}
        return toDTO(it = refundOrderList[0])
    }

    override fun getInfoByKeyId(keyId: String): RefundOrderServiceModel.RefundOrderDTO? {
        val refundOrder = dao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(it = refundOrder)
    }

    override fun getInfoByOrderItemKeyId(orderItemKeyId: String): RefundOrderServiceModel.RefundOrderDTO? {
        val refundOrderList = dao.findList(param = RefundOrderDaoModel.QueryByOrderItemKeyId(
            orderItemKeyId = orderItemKeyId, state = ActivityStateEnum.normals()
        ))
        if(CollectionUtils.isEmpty(refundOrderList)) return null
        checkBiz(refundOrderList.size > 1) {"退款数据异常,重复退款"}
        return toDTO(it = refundOrderList[0])
    }

    @Transactional
    override fun create(param: RefundOrderServiceModel.Create): String {
        val keyId = "ror-${idGenerator.nextId()}"
        //自己生成的清分批处理id
        dao.create(param = RefundOrderDaoModel.Create(
            keyId = keyId, amount = param.amount, shopKeyId = param.shopKeyId,
            orderItemKeyId = param.orderItemKeyId,orderKeyId = param.orderKeyId,
            state = ActivityStateEnum.PENDING.name, createdBy = param.createdBy,
            payOrderKeyId = param.payOrderKeyId
        ))
        return keyId
    }

    @Transactional
    override fun setRequestStatus(param: RefundOrderServiceModel.SetStatus) {
        dao.setRequestStatus(param = RefundOrderDaoModel.SetStatus(
            keyId = param.keyId, toState = param.state.name,
            fromState = listOf(ActivityStateEnum.PENDING.name, ActivityStateEnum.PROCESSING.name)
        ))
    }

    @Transactional
    override fun setResponseStatus(param: RefundOrderServiceModel.SetStatus) {
        dao.setResponseStatus(param = RefundOrderDaoModel.SetStatus(
            keyId = param.keyId, toState = param.state.name, finishTime = param.finishTime,
            fromState = listOf(ActivityStateEnum.PENDING.name, ActivityStateEnum.PROCESSING.name)
        ))
    }

    override fun findActiveListByPayOrderKeyId(payOrderKeyId: String): List<RefundOrderServiceModel.RefundOrderDTO> {
        val refundOrders = dao.findList(param = RefundOrderDaoModel.QueryByPayOrderKeyId(
            payOrderKeyId = payOrderKeyId, state = ActivityStateEnum.normals()
        ))
        if(CollectionUtils.isEmpty(refundOrders)) return emptyList()
        val dtoList = LinkedList<RefundOrderServiceModel.RefundOrderDTO>()
        refundOrders.forEach {
            dtoList.add(toDTO(it = it))
        }
        return dtoList
    }

    override fun findFinishListByPayOrderKeyId(payOrderKeyId: String): List<RefundOrderServiceModel.RefundOrderDTO> {
        val refundOrders = dao.findList(param = RefundOrderDaoModel.QueryByPayOrderKeyId(
            payOrderKeyId = payOrderKeyId, state = listOf(ActivityStateEnum.FINISH.name)
        ))
        if(CollectionUtils.isEmpty(refundOrders)) return emptyList()
        val dtoList = LinkedList<RefundOrderServiceModel.RefundOrderDTO>()
        refundOrders.forEach {
            dtoList.add(toDTO(it = it))
        }
        return dtoList
    }

    @Transactional
    override fun setExpressIdByKeyId(keyId: String, expressId: String) {
        dao.setExpressIdByKeyId(keyId = keyId, expressId= expressId)
    }

    private fun toDTO(it: RefundOrder): RefundOrderServiceModel.RefundOrderDTO {
        return RefundOrderServiceModel.RefundOrderDTO(
            keyId = it.mroKeyId, amount = it.mroAmount, shopKeyId = it.mroShopKeyId,
            orderKeyId = it.mroOrderKeyId,finishTime = it.mroFinishTime, expressId = it.mroExpressId,
            orderItemKeyId = it.mroOrderItemKeyId, payOrderKeyId = it.mroPayOrderKeyId,
            state = ActivityStateEnum.valueOf(it.mroState), createdBy = it.mroCreatedBy
        )
    }

}