package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.ShopServiceModel
import com.koudaiplus.buy.api.service.ShopService
import com.koudaiplus.buy.repository.dao.model.Shop
import com.koudaiplus.buy.repository.daox.ShopDao
import com.play.core.common.ClassStateEnum
import org.springframework.stereotype.Service

@Service
class ShopServiceImpl(
    private val dao: ShopDao
): ShopService {

    override fun getInfoByKeyId(keyId: String): ShopServiceModel.ShopDTO? {
        val shop = dao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(it = shop)
    }

    private fun toDTO(it: Shop): ShopServiceModel.ShopDTO {
        return ShopServiceModel.ShopDTO(
            keyId = it.mspKeyId, name = it.mspName, ownerId = it.mspOwnerId,
            state = ClassStateEnum.valueOf(it.mspState), logo = it.mspLogo, cover = it.mspCover
        )
    }

}