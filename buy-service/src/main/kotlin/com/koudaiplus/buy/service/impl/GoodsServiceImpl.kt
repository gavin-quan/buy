package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.GoodsServiceModel
import com.koudaiplus.buy.api.service.GoodsService
import com.koudaiplus.buy.repository.dao.model.Goods
import com.koudaiplus.buy.repository.dao.model.GoodsStockKeepingUnit
import com.koudaiplus.buy.repository.daox.GoodsDao
import com.koudaiplus.buy.repository.daox.GoodsStockKeepingUnitDao
import com.koudaiplus.buy.repository.daox.mapper.GoodsStockMapper
import com.play.core.common.ClassStateEnum
import com.play.core.common.Limit
import com.play.core.common.Pages
import com.play.core.common.checkBiz
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class GoodsServiceImpl(
    private val goodsDao: GoodsDao,
    private val goodsStockKeepingUnitDao: GoodsStockKeepingUnitDao,
    private val goodsStockMapper: GoodsStockMapper
): GoodsService {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun getInfoByKeyId(keyId: String): GoodsServiceModel.GoodsDTO? {
        val info = goodsDao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(info)
    }

    override fun getSkuInfoByKeyId(skuKeyId: String): GoodsServiceModel.GoodsStockKeepingUnitDTO? {
        val sku = goodsStockKeepingUnitDao.getInfoByKeyId(keyId = skuKeyId) ?: return null
        return toDTO(sku)
    }

    @Transactional
    override fun lockStock(orderItemKeyId: String) {
        logger.warn("!!!!!!!!(此日志不保证最终会执行)正在尝试锁定库存!!! $orderItemKeyId!!!!!!!!!!!")
        val stockId = System.currentTimeMillis().toString()
        val rows = goodsStockMapper.lockStock(orderItemKeyId = orderItemKeyId, stockId = stockId)
        checkBiz(rows != 2) {"锁定库存失败, $orderItemKeyId"}
    }

    @Transactional
    override fun deductStock(orderItemKeyId: String) {
        logger.warn("!!!!!!!!(此日志不保证最终会执行)正在尝试扣减库存!!! $orderItemKeyId!!!!!!!!!!!")
        val rows = goodsStockMapper.deductStock(orderItemKeyId = orderItemKeyId)
        checkBiz(rows != 2) {"扣减库存失败, $orderItemKeyId"}
    }

    @Transactional
    override fun recoverStock(orderItemKeyId: String) {
        logger.warn("!!!!!!!!(此日志不保证最终会执行)正在尝试恢复库存!!! $orderItemKeyId!!!!!!!!!!!")
        val stockId = System.currentTimeMillis().toString()
        val rows = goodsStockMapper.recoverStock(orderItemKeyId = orderItemKeyId, stockId = stockId)
        checkBiz(rows != 2) {"恢复库存失败, $orderItemKeyId"}
    }

    @Transactional
    override fun pushStock(skuKeyId: String, quantity: Int) {
        logger.warn("!!!!!!!!(此日志不保证最终会执行)正在尝试新加库存!!! $skuKeyId $quantity!!!!!!!!!!!")
        val stockId = System.currentTimeMillis().toString()
        goodsStockMapper.pushStock(skuKeyId = skuKeyId, quantity = quantity, stockId = stockId)
    }

    override fun getContentByKeyId(keyId: String): String? {
        return goodsDao.get { mapper, example, criteria ->
            criteria.andMgsKeyIdEqualTo(keyId)
            mapper.selectOneByExampleSelective(example, Goods.Column.mgsId, Goods.Column.mgsContent).mgsContent
        }
    }

    override fun findList(limit: Limit): Pages<GoodsServiceModel.GoodsDTO> {
        val data = goodsDao.list (limit = limit) { mapper, example, criteria ->
            example.orderBy(Goods.Column.mgsId.desc())
            criteria.andMgsStateEqualTo(ClassStateEnum.ENABLE.name)
            mapper.selectByExampleSelective(example, *Goods.Column.excludes(Goods.Column.mgsContent))
        }
        return Pages.of(content = data, limit = limit) { toDTO(it) }
    }

    override fun findListByShopId(shopKeyId: String, limit: Limit): Pages<GoodsServiceModel.GoodsDTO> {
        val data = goodsDao.list (limit = limit) { mapper, example, criteria ->
            example.orderBy(Goods.Column.mgsId.desc())
            criteria.andMgsShopKeyIdEqualTo(shopKeyId)
            criteria.andMgsStateEqualTo(ClassStateEnum.ENABLE.name)
            mapper.selectByExampleSelective(example, *Goods.Column.excludes(Goods.Column.mgsContent))
        }
        return Pages.of(content = data, limit = limit) { toDTO(it) }
    }

    private fun toDTO(it: Goods): GoodsServiceModel.GoodsDTO {
        return GoodsServiceModel.GoodsDTO(
            keyId = it.mgsKeyId, name = it.mgsName, minPrice = it.mgsMinPrice,
            maxPrice = it.mgsMaxPrice, shopKeyId = it.mgsShopKeyId, preview = it.mgsPreview,
            state = ClassStateEnum.valueOf(it.mgsState), cover = it.mgsCover
        )
    }

    private fun toDTO(it: GoodsStockKeepingUnit): GoodsServiceModel.GoodsStockKeepingUnitDTO {
        return GoodsServiceModel.GoodsStockKeepingUnitDTO(
            keyId = it.mguKeyId, name = it.mguName, price = it.mguPrice,
            goodsKeyId = it.mguGoodsKeyId, state = ClassStateEnum.valueOf(it.mguState),
            reducedPrice = it.mguReducedPrice, discountPrice = it.mguDiscountPrice,
            quantity = it.mguQuantity, lockQuantity = it.mguLockQuantity,
            availableQuantity = it.mguAvailableQuantity, stockId = it.mguStockId
        )
    }

}