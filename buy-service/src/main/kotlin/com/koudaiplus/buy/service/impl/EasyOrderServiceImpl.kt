package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.EasyOrderServiceModel
import com.koudaiplus.buy.api.service.EasyOrderService
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.repository.dao.model.EasyOrder
import com.koudaiplus.buy.repository.daox.EasyOrderDao
import com.koudaiplus.buy.repository.daox.model.EasyOrderDaoModel
import com.play.core.common.ClassStateEnum
import com.play.core.common.SnowFlakeGenerator
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class EasyOrderServiceImpl(
    private val idGenerator: SnowFlakeGenerator,
    private val dao: EasyOrderDao
): EasyOrderService {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun getInfoByKeyId(keyId: String): EasyOrderServiceModel.EasyOrderDTO? {
        val order = dao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(order)
    }

    @Transactional
    override fun create(param: EasyOrderServiceModel.Create): String {
        val keyId = "eor-${idGenerator.nextId()}"
        dao.create(param = EasyOrderDaoModel.Create(
            keyId = keyId, buyerId = param.buyerId, sourceId = param.sourceId, subject = param.subject,
            amount = param.amount, state = ClassStateEnum.ENABLE.name, sourceType = param.sourceType.name
        ))
        return keyId
    }


    @Transactional
    override fun tryCloseByKeyId(keyId: String) {
        dao.tryCloseByKeyId(param = EasyOrderDaoModel.UpdateState(
            keyId = keyId, toState = ClassStateEnum.DISABLE.name, fromState = listOf(ClassStateEnum.ENABLE.name)
        ))
    }

    @Transactional
    override fun finishPay(param: EasyOrderServiceModel.FinishPay) {
        dao.finishPay(param = EasyOrderDaoModel.FinishPay(
            keyId = param.keyId, payOrderKeyId = param.payOrderKeyId, transOrderNo = param.transOrderNo,
            fromState = listOf(ClassStateEnum.ENABLE.name)
        ))
    }

    private fun toDTO(it: EasyOrder): EasyOrderServiceModel.EasyOrderDTO {
        return EasyOrderServiceModel.EasyOrderDTO(
            keyId = it.eorKeyId, buyerId = it.eorBuyerId, payOrderKeyId = it.eorPayOrderId,
            state = ClassStateEnum.valueOf(it.eorState), transOrderNo = it.eorTransOrderNo,
            sourceId = it.eorSourceId, sourceType = BuyConstants.SourceType.valueOf(it.eorSourceType),
            amount = it.eorAmount, subject = it.eorSubject
        )
    }

}