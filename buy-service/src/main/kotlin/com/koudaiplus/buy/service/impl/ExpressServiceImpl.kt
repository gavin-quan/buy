package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.ExpressServiceModel
import com.koudaiplus.buy.api.service.ExpressService
import com.koudaiplus.buy.repository.dao.model.Express
import com.koudaiplus.buy.repository.daox.ExpressDao
import com.koudaiplus.buy.repository.daox.model.ExpressDaoModel
import com.play.core.common.ActivityStateEnum
import com.play.core.common.SnowFlakeGenerator
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ExpressServiceImpl(
    private val idGenerator: SnowFlakeGenerator,
    private val dao: ExpressDao
): ExpressService {

    @Transactional
    override fun create(param: ExpressServiceModel.Create): String {
        val keyId = "eps-${idGenerator.nextId()}"
        dao.create(param = ExpressDaoModel.Create(
            keyId = keyId, shopKeyId = param.shopKeyId, receiverName = param.receiverName,
            receiverAddress = param.receiverAddress, receiverTelephone = param.receiverTelephone,
            receiverUserId = param.receiverUserId, title = param.title,
            state = ActivityStateEnum.PENDING.name
        ))
        return keyId
    }

    @Transactional
    override fun start(param: ExpressServiceModel.Start) {
        dao.update(ExpressDaoModel.Start(
            keyId = param.keyId, companyCode = param.companyCode, serialNo = param.serialNo,
            consignorTelephone = param.consignorTelephone, consignorAddress = param.consignorAddress,
            consignorName = param.consignorName, toState = ActivityStateEnum.PROCESSING.name,
            fromState = listOf(ActivityStateEnum.PENDING.name)
        ))
    }

    override fun getInfoByKeyId(keyId: String): ExpressServiceModel.ExpressDTO? {
        val data = dao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(it = data)
    }

    override fun confirm(keyId: String) {
        dao.update(ExpressDaoModel.Confirm(
            keyId = keyId, toState = ActivityStateEnum.FINISH.name,
            fromState = listOf(ActivityStateEnum.PROCESSING.name)
        ))
    }

    private fun toDTO(it: Express): ExpressServiceModel.ExpressDTO {
        return ExpressServiceModel.ExpressDTO(
            keyId = it.epsKeyId, shopKeyId = it.epsShopKeyId, receiverUserId = it.epsReceiverUserId,
            title = it.epsTitle, receiverName = it.epsReceiverName, receiverTelephone = it.epsReceiverTelephone,
            receiverAddress = it.epsReceiverAddress, companyCode = it.epsCompanyCode, serialNo = it.epsSerialNo,
            consignorName = it.epsConsignorName, consignorAddress = it.epsConsignorAddress,
            consignorTelephone = it.epsConsignorTelephone, state = ActivityStateEnum.valueOf(it.epsState)
        )
    }

}