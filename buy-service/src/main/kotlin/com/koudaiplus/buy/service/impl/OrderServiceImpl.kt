package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.OrderServiceModel
import com.koudaiplus.buy.api.service.OrderService
import com.koudaiplus.buy.repository.dao.model.Order
import com.koudaiplus.buy.repository.dao.model.OrderItem
import com.koudaiplus.buy.repository.daox.OrderDao
import com.koudaiplus.buy.repository.daox.OrderItemDao
import com.koudaiplus.buy.repository.daox.model.OrderDaoModel
import com.koudaiplus.buy.repository.daox.model.OrderItemDaoModel
import com.play.core.common.ActivityStateEnum
import com.play.core.common.ClassStateEnum
import com.play.core.common.SnowFlakeGenerator
import org.apache.commons.collections4.CollectionUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import java.util.stream.Collectors

@Service
class OrderServiceImpl(
    private val idGenerator: SnowFlakeGenerator,
    private val orderDao: OrderDao,
    private val orderItemDao: OrderItemDao
): OrderService {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun getInfoByKeyId(keyId: String): OrderServiceModel.OrderDTO? {
        val order = orderDao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(order)
    }

    override fun findListByPayOrderId(payOrderId: String): List<OrderServiceModel.OrderDTO> {
        val data = orderDao.findListByPayOrderId(payOrderId = payOrderId)
        val dtoList = LinkedList<OrderServiceModel.OrderDTO>()
        data.forEach { dtoList.add(toDTO(it)) }
        return dtoList
    }

    @Transactional
    override fun create(param: OrderServiceModel.Create): String {
        val keyId = "odr-${idGenerator.nextId()}"
        orderDao.create(param = OrderDaoModel.Create(
            keyId = keyId, shopKeyId = param.shopKeyId,
            buyerId = param.buyerId, subject = param.subject,
            amount = param.amount, state = ClassStateEnum.ENABLE.name,
            expressId = param.expressId, batchId = param.batchId
        ))
        return keyId
    }

    @Transactional
    override fun createItem(param: OrderServiceModel.CreateItem): String {
        val keyId = "odi-${idGenerator.nextId()}"
        orderItemDao.create(param = OrderItemDaoModel.Create(
            keyId = keyId, orderKeyId = param.orderKeyId,
            shopKeyId = param.shopKeyId, goodsKeyId = param.goodsKeyId,
            goodsName = param.goodsName, goodsPrice = param.goodsPrice,
            goodsQuantity = param.goodsQuantity, goodsSkuId = param.goodsSkuId,
            totalPrice = param.totalPrice(), state = ClassStateEnum.ENABLE.name,
            stockState = ActivityStateEnum.PENDING.name
        ))
        return keyId
    }

    override fun findOrderItemList(orderKeyId: String): List<OrderServiceModel.OrderItemDTO> {
        val orderItems = orderItemDao.findListByOrderKeyId(orderKeyId = orderKeyId)
        if(CollectionUtils.isEmpty(orderItems)) return emptyList()
        val dtoList = LinkedList<OrderServiceModel.OrderItemDTO>()
        orderItems.forEach {
            dtoList.add(toDTO(it = it))
        }
        return dtoList
    }

    override fun getOrderItemInfoByKeyId(keyId: String): OrderServiceModel.OrderItemDTO? {
        val orderItem = orderItemDao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(it = orderItem)
    }

    @Transactional
    override fun tryCloseByKeyId(keyId: String) {
        orderDao.tryCloseByKeyId(param = OrderDaoModel.UpdateState(
            keyId = keyId, toState = ClassStateEnum.DISABLE.name,
            fromState = listOf(ClassStateEnum.ENABLE.name)
        ))
        orderItemDao.tryStockRecover(param = OrderItemDaoModel.StockRecoverByOrderKeyId(
            orderKeyId = keyId, stockStateTo = ActivityStateEnum.CANCEL.name,
            stockStateFrom = listOf(ActivityStateEnum.PROCESSING.name)
        ))
    }

    @Transactional
    override fun tryForceRecoverStockByOrderItemKeyId(orderItemKeyId: String) {
        logger.warn("!!!!!!!!(此日志不保证最终会执行)正在尝试强制恢复库存!!! 该方法请在退款时使用!!! $orderItemKeyId!!!!!!!!!!!")
        orderItemDao.tryStockRecover(param = OrderItemDaoModel.StockRecover(
            keyId = orderItemKeyId, stockStateTo = ActivityStateEnum.CANCEL.name,
            stockStateFrom = listOf(ActivityStateEnum.FINISH.name)
        ))
    }

    @Transactional
    override fun finishPay(param: OrderServiceModel.FinishPay) {
        orderDao.finishPay(param = OrderDaoModel.FinishPay(
            keyId = param.keyId, payOrderKeyId = param.payOrderKeyId,
            transOrderNo = param.transOrderNo, fromState = listOf(ClassStateEnum.ENABLE.name)
        ))
    }

    override fun findCancelStockOrderItemList(): List<OrderServiceModel.OrderItemDTO> {
        val orderItems = orderItemDao.findCancelStockOrderItemList()
        if(CollectionUtils.isEmpty(orderItems)) return emptyList()
        val dtoList = LinkedList<OrderServiceModel.OrderItemDTO>()
        orderItems.forEach {
            dtoList.add(toDTO(it = it))
        }
        return dtoList
    }

    override fun findOrderKeyIdListByBatchId(batchId: String): List<String> {
        val orders = orderDao.findListByBatchId(batchId = batchId)
        return orders.stream().map { s->s.morKeyId }.collect(Collectors.toList()).orEmpty()
    }

    private fun toDTO(it: Order): OrderServiceModel.OrderDTO {
        return OrderServiceModel.OrderDTO(
            keyId = it.morKeyId, buyerId = it.morBuyerId, expressId = it.morExpressId,
            payOrderKeyId = it.morPayOrderId, transOrderNo = it.morTransOrderNo,
            shopKeyId = it.morShopKeyId, subject = it.morSubject, amount = it.morAmount,
            state = ClassStateEnum.valueOf(it.morState), createTime = it.morCreateTime
        )
    }

    private fun toDTO(it: OrderItem): OrderServiceModel.OrderItemDTO {
        return OrderServiceModel.OrderItemDTO(
            keyId = it.moiKeyId, goodsKeyId = it.moiGoodsKeyId, goodsPrice = it.moiGoodsPrice,
            goodsQuantity = it.moiGoodsQuantity, orderKeyId = it.moiOrderKeyId,
            shopKeyId = it.moiShopKeyId, totalPrice = it.moiTotalPrice, state = ClassStateEnum.valueOf(it.moiState),
            stockState = ActivityStateEnum.valueOf(it.moiStockState), goodsSkuKeyId = it.moiGoodsSkuId
        )
    }

}