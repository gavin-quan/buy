package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.CartServiceModel
import com.koudaiplus.buy.api.service.CartService
import com.koudaiplus.buy.repository.dao.model.Cart
import com.koudaiplus.buy.repository.daox.CartDao
import com.koudaiplus.buy.repository.daox.model.CartDaoModel
import com.play.core.common.Limit
import com.play.core.common.Pages
import com.play.core.common.SnowFlakeGenerator
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class CartServiceImpl(
    private val idGenerator: SnowFlakeGenerator,
    private val dao: CartDao
): CartService {

    @Transactional
    override fun delete(buyerId: String, skuId: String) {
        dao.execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            criteria.andMctBuyerIdEqualTo(buyerId).andMctGoodsSkuIdEqualTo(skuId)
            mapper.deleteByExample(example)
        }
    }

    @Transactional
    override fun create(param: CartServiceModel.Create) {
        val keyId = idGenerator.nextId().toString()
        dao.execute(requireRowsAffected = null) {  mapper, example, criteria, builder ->
            val e = builder
                    .mctKeyId(keyId)
                    .mctShopKeyId(param.shopKeyId)
                    .mctGoodsKeyId(param.goodsKeyId)
                    .mctGoodsSkuId(param.goodsSkuId)
                    .mctQuantity(param.quantity, Cart.Builder.Inc.INC)
                    .mctBuyerId(param.buyerId)
                    .mctCreateTime(Date())
                    .mctUpdateTime(Date())
                    .build()
            criteria.andMctBuyerIdEqualTo(param.buyerId)
                    .andMctGoodsSkuIdEqualTo(param.goodsSkuId)
            mapper.upsertByExample(e, example)
        }
    }

    @Transactional
    override fun delete(param: CartServiceModel.Delete) {
        dao.execute (requireRowsAffected = null) { mapper, example, criteria, builder ->
            criteria.andMctKeyIdIn(param.keyIdList).andMctBuyerIdEqualTo(param.buyerId)
            mapper.deleteByExample(example)
        }
    }

    override fun getPendingCount(buyerId: String): Long {
        return dao.count { mapper, example, criteria ->
            criteria.andMctBuyerIdEqualTo(buyerId)
            mapper.countByExample(example)
        }
    }

}