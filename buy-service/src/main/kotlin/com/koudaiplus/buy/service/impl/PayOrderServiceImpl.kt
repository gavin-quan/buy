package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.PayOrderServiceModel
import com.koudaiplus.buy.api.service.PayOrderService
import com.koudaiplus.buy.api.model.BuyConstants
import com.koudaiplus.buy.repository.dao.model.PayOrder
import com.koudaiplus.buy.repository.dao.model.PayOrderItem
import com.koudaiplus.buy.repository.daox.PayOrderDao
import com.koudaiplus.buy.repository.daox.PayOrderItemDao
import com.koudaiplus.buy.repository.daox.mapper.OrderMapperx
import com.koudaiplus.buy.repository.daox.model.PayOrderDaoModel
import com.koudaiplus.buy.repository.daox.model.PayOrderItemDaoModel
import com.play.core.common.SnowFlakeGenerator
import org.apache.commons.collections4.CollectionUtils
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.util.*

@Service
class PayOrderServiceImpl(
    private val idGenerator: SnowFlakeGenerator,
    private val payOrderDao: PayOrderDao,
    private val payOrderItemDao: PayOrderItemDao,
    private val orderMapperx: OrderMapperx
): PayOrderService {

    @Transactional
    override fun create(param: PayOrderServiceModel.Create): String {
        val keyId = "por-${idGenerator.nextId()}"
        payOrderDao.create(param = PayOrderDaoModel.Create(
            keyId = keyId, subject = param.subject, amount = param.amount, state = BuyConstants.PayStateEnum.WAIT_PAY.name
        ))
        return keyId
    }

    @Transactional
    override fun createItem(param: PayOrderServiceModel.CreateItem): String {
        val keyId = "pim-${idGenerator.nextId()}"
        payOrderItemDao.create(param = PayOrderItemDaoModel.Create(
            keyId = keyId, payOrderKeyId = param.payOrderKeyId, orderKeyId = param.orderKeyId
        ))
        return keyId
    }

    @Transactional
    override fun finishPay(param: PayOrderServiceModel.FinishPay) {
        payOrderDao.finishPay(param = PayOrderDaoModel.FinishPay(
            keyId = param.keyId, transOrderNo = param.transOrderNo, finishTime = param.finishTime,
            toState = BuyConstants.PayStateEnum.PAID.name, fromState = listOf(BuyConstants.PayStateEnum.WAIT_PAY.name)
        ))
    }

    override fun getInfoByKeyId(keyId: String): PayOrderServiceModel.PayOrderDTO? {
        val payOrder = payOrderDao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(it = payOrder)
    }

    override fun findItemListByKeyId(payOrderKeyId: String): List<PayOrderServiceModel.PayOrderItemDTO> {
        val payOrderItems = payOrderItemDao.findListByPayOrderKeyId(payOrderKeyId = payOrderKeyId)
        if(CollectionUtils.isEmpty(payOrderItems)) return emptyList()
        val dtoList = LinkedList<PayOrderServiceModel.PayOrderItemDTO>()
        payOrderItems.forEach {
            dtoList.add(toDTO(it = it))
        }
        return dtoList
    }

    override fun getItemInfoByOrderKeyId(payOrderKeyId: String, orderKeyId: String): PayOrderServiceModel.PayOrderItemDTO? {
        val data = payOrderItemDao.getInfoByOrderKeyId(payOrderKeyId = payOrderKeyId, orderKeyId = orderKeyId) ?: return null
        return toDTO(it = data)
    }

    override fun getItemInfoByKeyId(payOrderItemKeyId: String): PayOrderServiceModel.PayOrderItemDTO? {
        val data = payOrderItemDao.getItemInfoByKeyId(payOrderItemKeyId = payOrderItemKeyId) ?: return null
        return toDTO(it = data)
    }

    override fun findRetryablePayOrderIdList(orderIdList: List<String>): List<String> {
        if(CollectionUtils.isEmpty(orderIdList)) return emptyList()
        val expireTimeDuration = Duration.ofMillis(System.currentTimeMillis()).minusMinutes(20)
        val expireTime = Date(expireTimeDuration.toMillis())
        return orderMapperx.findRetryablePayOrderIdList(orderIdList = orderIdList, count = orderIdList.size, expireTime = expireTime)
    }

    private fun toDTO(it: PayOrder): PayOrderServiceModel.PayOrderDTO {
        return PayOrderServiceModel.PayOrderDTO(
            keyId = it.mpoKeyId, amount = it.mpoAmount, subject = it.mpoSubject,
            state = BuyConstants.PayStateEnum.valueOf(it.mpoState), transOrderNo = it.mpoTransOrderNo,
            createTime = it.mpoCreateTime, finishTime = it.mpoFinishTime
        )
    }

    private fun toDTO(it: PayOrderItem): PayOrderServiceModel.PayOrderItemDTO {
        return PayOrderServiceModel.PayOrderItemDTO(
            keyId = it.mpoiKeyId, orderKeyId = it.mpoiOrderKeyId,
            payOrderKeyId = it.mpoiPayOrderKeyId
        )
    }

}