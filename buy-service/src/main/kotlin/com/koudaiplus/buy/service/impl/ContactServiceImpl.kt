package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.ContactServiceModel
import com.koudaiplus.buy.api.service.ContactService
import com.koudaiplus.buy.repository.dao.model.Contact
import com.koudaiplus.buy.repository.daox.ContactDao
import com.koudaiplus.buy.repository.daox.model.ContactDaoModel
import com.play.core.common.ClassStateEnum
import com.play.core.common.SnowFlakeGenerator
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.stream.Collectors

@Service
class ContactServiceImpl(
    private val idGenerator: SnowFlakeGenerator,
    private val dao: ContactDao
): ContactService {

    override fun findListByUserKeyId(userKeyId: String): List<ContactServiceModel.ContactVO> {
        val list = dao.findListByUserKeyId(userKeyId = userKeyId)
        return list.stream().map { s->toDTO(s).toContactVO() }.collect(Collectors.toList()).orEmpty()
    }

    @Transactional
    override fun create(param: ContactServiceModel.Create) {
        val keyId = "ctt-${idGenerator.nextId()}"
        val sort = if (param.default) 0 else 1
        dao.create(param = ContactDaoModel.Create(
            keyId = keyId, name = param.name, address = param.address,
            telephone = param.telephone, sort = sort, userKeyId = param.userKeyId,
            state = ClassStateEnum.ENABLE.name, province = param.province,
            city = param.city, county = param.county, areaCode = param.areaCode,
            postalCode = param.postalCode
        ))
    }

    @Transactional
    override fun update(param: ContactServiceModel.Update) {
        val sort = if (param.default) 0 else 1
        dao.update(param = ContactDaoModel.Create(
            keyId = param.keyId, name = param.name, address = param.address,
            telephone = param.telephone, sort = sort, userKeyId = param.userKeyId,
            state = ClassStateEnum.ENABLE.name, province = param.province,
            city = param.city, county = param.county, areaCode = param.areaCode,
            postalCode = param.postalCode
        ))
    }

    @Transactional
    override fun deleteByKeyId(keyId: String, userKeyId: String) {
        dao.deleteByKeyId(keyId = keyId, userKeyId = userKeyId)
    }

    @Transactional
    override fun setDefaultByKeyId(keyId: String, userKeyId: String) {
        dao.setDefaultByKeyId(keyId = keyId, userKeyId = userKeyId)
    }

    override fun getInfoByKeyId(keyId: String): ContactServiceModel.ContactDTO? {
        val info = dao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(info)
    }

    override fun getInfoByKeyId(keyId: String, userKeyId: String): ContactServiceModel.ContactDTO? {
        val info = dao.getInfoByKeyId(keyId = keyId, userKeyId = userKeyId) ?: return null
        return toDTO(info)
    }

    override fun getDefaultInfoByUserKeyId(userKeyId: String): ContactServiceModel.ContactDTO? {
        val info = dao.getDefaultInfoByUserKeyId(userKeyId = userKeyId) ?: return null
        return toDTO(info)
    }

    private fun toDTO(it: Contact): ContactServiceModel.ContactDTO {
        return ContactServiceModel.ContactDTO(
            keyId = it.mcnKeyId, userKeyId = it.mcnUserKeyId, name = it.mcnName,
            telephone = it.mcnTelephone, address = it.mcnAddress,
            state = ClassStateEnum.valueOf(it.mcnState), sort = it.mcnSort,
            province = it.mcnProvince, city = it.mcnCity, county = it.mcnCounty,
            areaCode = it.mcnAreacode, postalCode = it.mcnPostalcode
        )
    }

}