package com.koudaiplus.buy.service.impl

import com.koudaiplus.buy.api.model.RepeatPayOrderServiceModel
import com.koudaiplus.buy.api.service.RepeatPayOrderService
import com.koudaiplus.buy.repository.dao.model.RepeatPayOrder
import com.koudaiplus.buy.repository.daox.RepeatPayOrderDao
import com.koudaiplus.buy.repository.daox.model.RepeatPayOrderDaoModel
import com.play.core.common.ActivityStateEnum
import com.play.core.common.SnowFlakeGenerator
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class RepeatPayOrderServiceImpl (
    private val idGenerator: SnowFlakeGenerator,
    private val dao: RepeatPayOrderDao
): RepeatPayOrderService {

    override fun getInfoByPayOrderItemKeyId(payOrderItemKeyId: String): RepeatPayOrderServiceModel.RepeatPayOrderDTO? {
        val data = dao.getInfoByPayOrderItemKeyId(param = RepeatPayOrderDaoModel.QueryByPayOrderItemKeyId(
            payOrderItemKeyId = payOrderItemKeyId, state = ActivityStateEnum.normals()
        )) ?: return null
        return toDTO(it = data)
    }

    @Transactional
    override fun create(param: RepeatPayOrderServiceModel.Create): String {
        val keyId = "rtpo-${idGenerator.nextId()}"
        dao.create(param = RepeatPayOrderDaoModel.Create(
            keyId = keyId, payOrderKeyId = param.payOrderKeyId, amount = param.amount,
            payOrderItemKeyId = param.payOrderItemKeyId, state = ActivityStateEnum.PENDING.name,
            orderKeyId = param.orderKeyId
        ))
        return keyId
    }

    @Transactional
    override fun setRefundRespStatus(param: RepeatPayOrderServiceModel.SetStatus) {
        dao.setRespStatus(param = RepeatPayOrderDaoModel.SetStatus(
            keyId = param.keyId, toState = param.state.name,
            fromState = listOf(ActivityStateEnum.PENDING.name, ActivityStateEnum.PROCESSING.name)
        ))
    }

    override fun getInfoByKeyId(keyId: String): RepeatPayOrderServiceModel.RepeatPayOrderDTO? {
        val data = dao.getInfoByKeyId(keyId = keyId) ?: return null
        return toDTO(it = data)
    }

    override fun isRepeatPay(payOrderKeyId: String, orderKeyId: String): Boolean {
        val data = dao.getInfo(param = RepeatPayOrderDaoModel.QueryByOrderId(
            payOrderKeyId = payOrderKeyId, orderKeyId = orderKeyId
        ))
        return null != data
    }

    private fun toDTO(it: RepeatPayOrder): RepeatPayOrderServiceModel.RepeatPayOrderDTO {
        return RepeatPayOrderServiceModel.RepeatPayOrderDTO(
            keyId = it.rpoKeyId, amount = it.rpoAmount, orderKeyId = it.rpoOrderKeyId,
            payOrderKeyId = it.rpoPayOrderKeyId, payOrderItemKeyId = it.rpoPayOrderItemId,
            state = ActivityStateEnum.valueOf(it.rpoState)
        )
    }

}