<html>
<head>
    <meta charset="UTF-8">
    <title>Test WebSocket</title>
    <script src="https://cdn.bootcss.com/sockjs-client/1.0.0/sockjs.min.js"></script>
    <script src="https://cdn.bootcss.com/stomp.js/2.3.3/stomp.min.js"></script>
</head>
<body>
Please open console to debug this program.
</body>
</html>
<script type="text/javascript">
    var stompClient = null,
            username = "zhangsan",
            endpoint = "http://localhost:7102/api/v1/xxx/endpoint?token=" + username,
            serverDest = "/app", clientTopicDest = "/topic", clientQueueDest = "/user",
            queueDest = clientQueueDest + "/" + username + "/wss/movies",
            topicDest = clientTopicDest + "/wss/movies";
    function connect() {
        var socket = new SockJS(endpoint);
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function(frame) {
            console.log('Connected');
            stompClient.subscribe(topicDest, function(response){
                console.log('Receive(Topic):' + response.body)
            });
            stompClient.subscribe(queueDest, function(response){
                console.log('Receive(P2P):' + response.body)
            });
        });
    }
    function disconnect() {
        stompClient.disconnect();
        console.log("Disconnected");
    }
    function send(message) {
        message = message || "hello";
        stompClient.send(serverDest + "/toTopic/m1", {}, message);
        stompClient.send(serverDest + "/toTopic/m2", {}, message);
        stompClient.send(serverDest + "/toQueue/m1", {}, message);
        stompClient.send(serverDest + "/toQueue/m2", {}, message);
    }
</script>