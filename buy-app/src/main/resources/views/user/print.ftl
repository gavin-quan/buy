<#import "/commons/variable.ftl" as variable>
<#import "/commons/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="zh">
<head>
  <title>首页</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
  <h1>判断locale:${(.locale='en_US')?string("Aenus", "A")}</h1>
  <h1>国际化1:<@spring.message code="user.title"/></h1>
  <h1>国际化2:<@spring.message code="live.title"/></h1>
  <h1>Request Attribute:${appProps.key}</h1>
  <h1>Request Attribute:${variable.appKey}</h1>
</body>
</html>
<script type="text/javascript" src="${variable.contextPath}/static/lib/jquery.min.js"></script>