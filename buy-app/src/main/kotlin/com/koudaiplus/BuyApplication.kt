package com.koudaiplus

import com.koudaiplus.payout.api.feign.OpenPayoutTransferClient
import com.koudaiplus.refund.api.feign.OpenMerchantRefundOrderClient
import com.koudaiplus.settlement.api.feign.SettlementClient
import com.koudaiplus.wallet.api.feign.AccountClient
import com.koudaiplus.wallet.api.feign.BillItemClient
import com.play.core.common.config.*
import org.apache.commons.lang3.ClassUtils
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.beans.factory.support.BeanNameGenerator
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType
import org.springframework.context.annotation.Import
import org.springframework.retry.annotation.EnableRetry

@ComponentScan(excludeFilters = [
    ComponentScan.Filter(type = FilterType.REGEX, pattern = [
        "com.koudaiplus.wallet.biz.listener.*"
    ])
], nameGenerator = AppBeanNameGenerator::class)
@SpringBootApplication(exclude = [
    DataSourceAutoConfiguration::class,
    ErrorMvcAutoConfiguration::class
    //,TransactionAutoConfiguration::class
])
@EnableFeignClients(clients = [
    OpenMerchantRefundOrderClient::class, BillItemClient::class,
    AccountClient::class, SettlementClient::class, OpenPayoutTransferClient::class
])
@EnableDiscoveryClient
@Import(value = [
    AppConfiguration::class, JmsConfiguration::class,
    ExceptionAdviceConfiguration::class, SwaggerConfiguration::class,
    WebConfiguration::class, RedissonConfiguration::class,
    CuratorConfiguration::class, XXlJobConfiguration::class,
    ArthasConfiguration::class, FeignConfiguration::class
])
@EnableRetry
class BuyApplication

fun main(args : Array<String>) {
    runApplication<BuyApplication>(*args)
}

class AppBeanNameGenerator : BeanNameGenerator {
    override fun generateBeanName(beanDefinition: BeanDefinition, beanDefinitionRegistry: BeanDefinitionRegistry): String {
        if(beanDefinition.beanClassName.startsWith(prefix = "com.koudaiplus.buy", ignoreCase = true)) {
            return "Buy${ClassUtils.getShortClassName(beanDefinition.beanClassName)}"
        }
        return ClassUtils.getShortClassName(beanDefinition.beanClassName)
    }
}