package com.koudaiplus.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.data.redis.RedisProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.redis.connection.RedisStandaloneConfiguration
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.core.StringRedisTemplate

@Configuration
class RedisConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.redis")
    @Primary
    fun redisProperties(): RedisProperties {
        return RedisProperties()
    }

    @Bean
    @Primary
    fun lettuceConnectionFactory(redisProperties: RedisProperties): LettuceConnectionFactory {
        val redisConfig = RedisStandaloneConfiguration()
        redisConfig.hostName = redisProperties.host
        redisConfig.database = redisProperties.database
        redisConfig.port = redisProperties.port
        redisConfig.setPassword(redisProperties.password)
        val clientConfig = LettuceClientConfiguration.builder().build()
        return LettuceConnectionFactory(redisConfig, clientConfig)
    }

    @Bean
    @Primary
    fun redisTemplate(connectionFactory: LettuceConnectionFactory): StringRedisTemplate {
        return StringRedisTemplate(connectionFactory)
    }

    @Bean("userRedisProperties")
    @ConfigurationProperties(prefix = "redis.user")
    fun userRedisProperties(): RedisProperties {
        return RedisProperties()
    }

    @Bean("userConnectionFactory")
    fun userConnectionFactory(@Qualifier("userRedisProperties")redisProperties: RedisProperties): LettuceConnectionFactory {
        val redisConfig = RedisStandaloneConfiguration()
        redisConfig.hostName = redisProperties.host
        redisConfig.database = redisProperties.database
        redisConfig.port = redisProperties.port
        redisConfig.setPassword(redisProperties.password)
        val clientConfig = LettuceClientConfiguration.builder().build()
        return LettuceConnectionFactory(redisConfig, clientConfig)
    }

    @Bean("userRedisTemplate")
    fun userRedisTemplate(@Qualifier("userConnectionFactory") connectionFactory: LettuceConnectionFactory): StringRedisTemplate {
        return StringRedisTemplate(connectionFactory)
    }

}