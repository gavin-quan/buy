package com.koudaiplus.config

import com.play.core.common.Constants
import com.play.core.common.JSON
import com.play.core.common.RespResult
import com.play.core.common.StorageService
import com.play.core.common.authentication.AuthenticationService
import com.play.core.common.authentication.RedisStorageService
import com.play.core.common.security.JWTTokenService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.http.MediaType
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.security.web.firewall.StrictHttpFirewall
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Spring Security中文文档
 * http://springcloud.cc/spring-security-zhcn.html
 */
@Configuration
@EnableWebSecurity
class WebSecurityConfig (
) : WebSecurityConfigurerAdapter() {

    override fun configure(web: WebSecurity) {
        val firewall = StrictHttpFirewall()
        firewall.setAllowUrlEncodedSlash(true) // 防止url带两个// spring security 5会有问题 所以必须放开这个配置
        web.httpFirewall(firewall)
        web.ignoring().antMatchers("/css/**")
    }

    override fun configure(http: HttpSecurity) {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(
                        "/",
                        "/actuator/health",
                        "/webjars/springfox-swagger-ui/**",
                        "/swagger-ui.html",
                        "/configuration/ui",
                        "/swagger-resources",
                        "/v2/api-docs",
                        "/swagger-resources/**",
                        "/notify/**",
                        "/goods/**"
                ).permitAll()
                .anyRequest().access("@authenticationService.checkAuthentication(request)")
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http.exceptionHandling().accessDeniedHandler(object : AccessDeniedHandler {
            override fun handle(request: HttpServletRequest, response: HttpServletResponse, exception: AccessDeniedException) {
                val s : RespResult<Unit, Unit, Unit> = RespResult.fail(Constants.ERR_CODE_FORBIDDEN, "被禁止(Forbidden)")
                response.status = HttpServletResponse.SC_FORBIDDEN
                response.characterEncoding = "utf-8"
                response.contentType = MediaType.APPLICATION_JSON_VALUE
                response.writer.use {
                    it.print(JSON.getJSON().toJson(s))
                    it.flush()
                }
            }
        }).authenticationEntryPoint(object : AuthenticationEntryPoint {
            override fun commence(request: HttpServletRequest, response: HttpServletResponse, exception: AuthenticationException) {
                val s : RespResult<Unit, Unit, Unit> = RespResult.fail(Constants.ERR_CODE_UNAUTHORIZED, "未授权(Unauthorized)")
                response.status = HttpServletResponse.SC_UNAUTHORIZED
                response.characterEncoding = "utf-8"
                response.contentType = MediaType.APPLICATION_JSON_VALUE
                response.writer.use {
                    it.print(JSON.getJSON().toJson(s))
                    it.flush()
                }
            }
        })
    }

    @Bean
    fun ssoStorageService(@Qualifier("userRedisTemplate") redisTemplate: StringRedisTemplate) : StorageService {
        return RedisStorageService(redisTemplate)
    }

    @Bean
    fun authenticationService(jwtTokenService: JWTTokenService, @Autowired(required = false) storageService: StorageService): AuthenticationService {
        return AuthenticationService(jwtTokenService = jwtTokenService, storageService = storageService)
    }

}