package com.koudaiplus.config

import com.alibaba.druid.pool.DruidDataSource
import com.play.core.common.ListItem
import org.apache.commons.lang3.ClassUtils
import org.apache.ibatis.logging.LogFactory
import org.mybatis.spring.SqlSessionFactoryBean
import org.mybatis.spring.annotation.MapperScan
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.beans.factory.support.BeanNameGenerator
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.io.Resource
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import javax.sql.DataSource

@Configuration
@MapperScan(
    nameGenerator = JdbcConfig.DaoBeanNameGenerator::class,
    basePackages = [
        "com.koudaiplus.wallet.repository.dao.mapper",
        "com.koudaiplus.wallet.repository.daox.mapper",
        "com.koudaiplus.buy.repository.dao.mapper",
        "com.koudaiplus.buy.repository.daox.mapper",
        "com.koudaiplus.pay.repository.dao.mapper",
        "com.koudaiplus.pay.repository.daox.mapper",
        "com.koudaiplus.message.repository.dao.mapper",
        "com.koudaiplus.message.repository.daox.mapper",
        "com.koudaiplus.user.repository.dao.mapper",
        "com.koudaiplus.user.repository.daox.mapper",
        "com.koudaiplus.settlement.repository.dao.mapper",
        "com.koudaiplus.settlement.repository.daox.mapper"
    ],
    sqlSessionFactoryRef = "primarySqlSessionFactory"
)
class JdbcConfig {

    @Bean("primaryDataSourceProps")
    @Primary
    @ConfigurationProperties(prefix = "datasource.primary")
    fun dataSourceProps() : DataSourceProperties {
        return DataSourceProperties()
    }

    @Bean("mapperLocations")
    @Primary
    @ConfigurationProperties(prefix = "datasource.primary.mapper-locations")
    fun mapperLocations() : ListItem {
        return ListItem()
    }

    @Bean(value= ["primaryDataSource"], initMethod = "init", destroyMethod = "close")
    @Primary
    fun dataSource(@Qualifier("primaryDataSourceProps")properties: DataSourceProperties): DruidDataSource {
        val datasource = DruidDataSource()
        datasource.url = properties.url
        datasource.username = properties.username
        datasource.password = properties.password
        // 配置初始化大小、最小、最大
        datasource.initialSize = 5
        datasource.minIdle = 10
        datasource.maxActive = 20
        // 配置获取连接等待超时的时间
        datasource.maxWait = 60000
        // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        datasource.timeBetweenEvictionRunsMillis = 60000
        // 配置一个连接在池中最小生存的时间，单位是毫秒
        datasource.minEvictableIdleTimeMillis = 300000
        // mysql可以配置为false。分库分表较多的数据库，建议配置为false。
        datasource.isPoolPreparedStatements = false
        return datasource
    }

    @Bean("primaryJdbcTemplate")
    @Primary
    fun jdbcTemplate(@Qualifier("primaryDataSource") dataSource: DataSource): JdbcTemplate {
        return JdbcTemplate(dataSource)
    }

    @Bean(value = ["primaryTransactionManager"])
    @Primary
    fun transactionManager(@Qualifier("primaryDataSource")dataSource: DataSource): DataSourceTransactionManager {
        return DataSourceTransactionManager(dataSource)
    }

    @Bean("primarySqlSessionFactory")
    @Primary
    fun sqlSessionFactory(@Qualifier("primaryDataSource")dataSource: DataSource, @Qualifier("mapperLocations") listItem: ListItem): SqlSessionFactoryBean {
        LogFactory.useStdOutLogging()
        val factory = SqlSessionFactoryBean()
        factory.setDataSource(dataSource)
        val resources = ArrayList<Resource>()
        val pathMatchingResourcePatternResolver = PathMatchingResourcePatternResolver()
        listItem.items.forEach {mapperLocation->
            resources.addAll(pathMatchingResourcePatternResolver.getResources(mapperLocation))
        }
        factory.setMapperLocations(resources.toTypedArray())
        return factory
    }

    class DaoBeanNameGenerator : BeanNameGenerator {
        override fun generateBeanName(beanDefinition: BeanDefinition, beanDefinitionRegistry: BeanDefinitionRegistry): String {
            if(beanDefinition.beanClassName.startsWith(prefix = "com.koudaiplus.buy", ignoreCase = true)) {
                return "Buy${ClassUtils.getShortClassName(beanDefinition.beanClassName)}"
            }
            return ClassUtils.getShortClassName(beanDefinition.beanClassName)
        }
    }

}