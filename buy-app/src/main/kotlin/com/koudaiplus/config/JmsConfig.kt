package com.koudaiplus.config

import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CLOSE_QUEUE
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CLOSE_QUEUE_BEAN
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CREATE_QUEUE
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_CREATE_QUEUE_BEAN
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_PAYMENT_MSG_ARRIVED_QUEUE
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_PAYMENT_MSG_ARRIVED_QUEUE_BEAN
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_REFUND_MSG_ARRIVED_QUEUE
import com.koudaiplus.buy.api.model.BuyConstants.ORDER_REFUND_MSG_ARRIVED_QUEUE_BEAN
import com.koudaiplus.buy.api.model.BuyConstants.PAY_ORDER_REPEAT_PAY_QUEUE
import com.koudaiplus.buy.api.model.BuyConstants.PAY_ORDER_REPEAT_PAY_QUEUE_BEAN
import com.koudaiplus.settlement.api.model.SettlementConstants.SUBMIT_SETTLE_QUEUE
import com.koudaiplus.settlement.api.model.SettlementConstants.SUBMIT_SETTLE_QUEUE_BEAN
import com.play.core.common.jms.AbstractRocketMQTransactionListener
import org.apache.activemq.command.ActiveMQQueue
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jms.annotation.EnableJms

@Configuration
@EnableJms
class MQConfig {

    @Bean(ORDER_PAYMENT_MSG_ARRIVED_QUEUE_BEAN)
    fun queue1() = ActiveMQQueue(ORDER_PAYMENT_MSG_ARRIVED_QUEUE)

    @Bean(ORDER_REFUND_MSG_ARRIVED_QUEUE_BEAN)
    fun queue2() = ActiveMQQueue(ORDER_REFUND_MSG_ARRIVED_QUEUE)

    @Bean(ORDER_CLOSE_QUEUE_BEAN)
    fun queue3() = ActiveMQQueue(ORDER_CLOSE_QUEUE)

    @Bean(SUBMIT_SETTLE_QUEUE_BEAN)
    fun queue4() = ActiveMQQueue(SUBMIT_SETTLE_QUEUE)

    @Bean(ORDER_CREATE_QUEUE_BEAN)
    fun queue6() = ActiveMQQueue(ORDER_CREATE_QUEUE)

    @Bean(PAY_ORDER_REPEAT_PAY_QUEUE_BEAN)
    fun queue7() = ActiveMQQueue(PAY_ORDER_REPEAT_PAY_QUEUE)

    /*
    @Bean(ORDER_PAY_FINISH_TOPIC_BEAN)
    fun topic1() = ActiveMQTopic(ORDER_PAY_FINISH_TOPIC)

    @Bean(REFUND_STATE_CHANGE_TOPIC_BEAN)
    fun topic2() = ActiveMQTopic(REFUND_STATE_CHANGE_TOPIC)

    @Bean(ORDER_CREATED_TOPIC_BEAN)
    fun topic3() = ActiveMQTopic(ORDER_CREATED_TOPIC)
    */
}

@RocketMQTransactionListener
class AppRocketMQTransactionListener (
        applicationContext: ApplicationContext
) : AbstractRocketMQTransactionListener(applicationContext)
