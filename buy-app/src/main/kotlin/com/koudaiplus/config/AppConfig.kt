package com.koudaiplus.config

import com.google.gson.JsonParser
import com.koudaiplus.buy.integration.service.YiYuan
import com.play.core.common.DefaultHolidayDataSource
import com.play.core.common.HolidayService
import com.play.core.common.RedisHolidayDataSource
import com.play.core.common.TimorTechHolidayDataSource
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.util.StreamUtils
import org.springframework.web.client.RestTemplate
import java.nio.charset.Charset
import java.time.Duration
import java.util.ArrayList
import java.util.HashMap

@Configuration
@ComponentScan(basePackages = [
    "com.play.core.common.config"
])
class AppConfig {

    @Bean
    fun resetTemplate(): RestTemplate {
        return RestTemplateBuilder()
                .setReadTimeout(Duration.ofSeconds(5))
                .setConnectTimeout(Duration.ofSeconds(5))
                .build()
    }

    @Bean
    fun holidayService(@Autowired redisTemplate: StringRedisTemplate, @Autowired restTemplate: RestTemplate) : HolidayService {
        return HolidayService(dataSource = DefaultHolidayDataSource(holidayDataSources = listOf(
            TimorTechHolidayDataSource(restTemplate = restTemplate)
        ), redisHolidayDataSource = RedisHolidayDataSource(redisTemplate = redisTemplate)))
    }

    @Bean
    fun yiyuan(): YiYuan {
        val httpConfig = RequestConfig.custom().setConnectTimeout(2000).setSocketTimeout(2000).setConnectionRequestTimeout(2000).build()
        val httpClient = HttpClientBuilder.create().setDefaultRequestConfig(httpConfig).build()
        val resource = ClassPathResource("/express-config.json")
        val configText = resource.inputStream.use {
            StreamUtils.copyToString(it, Charset.forName("UTF-8"))
        }
        return YiYuan(httpClient = httpClient, config = JsonParser.parseString(configText))
    }

    @Bean("express")
    @ConfigurationProperties(prefix = "express")
    fun express() : List<Map<String, String>> {
        return ArrayList<HashMap<String, String>>()
    }

}