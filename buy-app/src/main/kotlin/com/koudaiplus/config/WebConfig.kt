package com.koudaiplus.config

import org.springframework.boot.web.server.ErrorPage
import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
//@EnableWebMvc
class WebConfig : WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>, WebMvcConfigurer {

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/")
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/")
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/")
        registry.addResourceHandler("/swagger/**").addResourceLocations("classpath:/statics/swagger/")
    }

    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        configurer.favorPathExtension(false) //是否通过路径指定返回数据类型
        configurer.favorParameter(false) //是否使用url上的参数来指定数据返回类型
    }

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("PUT", "DELETE", "POST", "GET", "OPTIONS")
                //.allowedHeaders("header1")
                //.exposedHeaders("header2")
                .allowCredentials(true)
                .maxAge(3600)
    }

    override fun customize(factory: ConfigurableServletWebServerFactory) {
        factory.addErrorPages(ErrorPage("/errors"))
    }

}
