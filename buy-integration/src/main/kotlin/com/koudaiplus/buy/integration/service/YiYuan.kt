package com.koudaiplus.buy.integration.service

import com.google.common.base.Joiner
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.koudaiplus.buy.integration.model.YiYuanServiceModel
import com.play.core.common.BusinessException
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.apache.http.client.HttpClient
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.slf4j.LoggerFactory
import java.io.FileInputStream

/**
 * 阿里云/登录/云市场
 * 易源数据-快递物流查询API接口-支持1000多家快递公司-高实时-高稳定-高并发
 * 支持顺丰！支持顺丰！超过1000家快递公司查询，中国很全面的快递查询，自动识别快递公司。
 * 支持申通、顺丰、圆通、宅急送、韵达、中通、百世、天天、四通一达、京东、EMS等国内/国际快递查询。
 * https://market.aliyun.com/products/57126001/cmapi010996.html?spm=5176.2020520132.101.1.1dae7218FpzTLT#sku=yuncode499600008
 */
class YiYuan (
    val httpClient: HttpClient,
    val config: JsonElement
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun fetchList(param: YiYuanServiceModel.Query): String {
        val parameters = HashMap<String, Any>()
        parameters["com"] = param.company
        parameters["nu"] = param.serialNo
        if(StringUtils.isNotBlank(param.phone)) {
            parameters["phone"] = param.phone!!
        }
        val get = HttpGet("http://ali-deliver.showapi.com/showapi_expInfo?".plus(Joiner.on("&").useForNull("").withKeyValueSeparator("=").join(parameters)))
        get.addHeader("Authorization", "APPCODE ".plus(getAppCode()))
        try {
            val resp = httpClient.execute(get)
            if(200 == resp.statusLine.statusCode) {
                return EntityUtils.toString(resp.entity)
            }
            logger.warn("查询失败,返回非200, parameters={}, resp={}", param, resp)
        } catch (e: Exception) {
            logger.warn("查询失败", e)
            logger.warn("查询失败, parameters={}", param)
        } finally {
            get.releaseConnection()
        }
        throw BusinessException(message = "调用接口失败")
    }

    fun getAppCode() = config.asJsonObject.get("yiyuan").asJsonObject.get("appcode").asString

    fun getCompanyCode(code: String) =  config.asJsonObject.get("yiyuan").asJsonObject.get("config").asJsonObject.get(code).asJsonObject.get("code").asString

    fun getCompanyName(code: String) =  config.asJsonObject.get("yiyuan").asJsonObject.get("config").asJsonObject.get(code).asJsonObject.get("name").asString

}

fun main(args: Array<String>) {
    val httpConfig = RequestConfig.custom().setConnectTimeout(2000).setSocketTimeout(2000).setConnectionRequestTimeout(2000).build()
    val t = HttpClientBuilder.create().setDefaultRequestConfig(httpConfig).build()
    val s = YiYuan(t, JsonParser.parseString(IOUtils.toString(FileInputStream("C:\\gavin\\SourceCode\\koudaiplus\\express\\express-app\\src\\main\\resources\\express.json"), "utf-8")))
    val r = s.fetchList(YiYuanServiceModel.Query(company = "auto", serialNo = "SF1310733909460", phone = ""))
    println(r)
   /* val e = IOUtils.toString(FileInputStream("C:\\gavin\\SourceCode\\koudaiplus\\express\\express-app\\src\\main\\resources\\express.json"), "utf-8")
    val f = JsonParser.parseString(e).asJsonObject.get("sf").asJsonObject.get("yiyuan").asJsonObject.get("code")
    println(f)*/
}