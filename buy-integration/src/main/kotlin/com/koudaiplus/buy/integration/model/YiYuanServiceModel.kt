package com.koudaiplus.buy.integration.model

object YiYuanServiceModel {

    data class Query(
        val company: String,
        val serialNo: String,
        val phone: String? //顺丰快递必须填写本字段
    ) {
        override fun toString(): String {
            return "Query(company='$company', serialNo='$serialNo', phone=$phone)"
        }
    }

}